<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ("config.inc.php");
include ("common.inc.php");

$PGM_SESSION["user"] = array();
unset($PGM_SESSION["user"]);
unset($PGM_SESSION["zone_id"]);
$PGM_SESSION["CART"]->calc_grandtotal();
unset($PGM_SESSION["wantsurl"]);
unset($PGM_SESSION["orderinfo"]);

pgm_session_close($PGM_SESSION, $session_name);
redirect($CFG["firstpage"]);
die;
?>

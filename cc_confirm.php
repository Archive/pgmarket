<html>
<head>
<title>Small Pieces of Time - Credit Card Validation</title>
<style type="text/css">
<!--
.normal		{ font-family: Arial, sans-serif; font-size: 10pt; }
h1		{ font-family: Arial, sans-serif; font-size: 14pt; font-weight: bold; color: #99335d; }
.h1bar		{ font-family: Arial, sans-serif; font-size: 14pt; font-weight: bold; color: #669933; }
-->
</style>
</head>
<body bgcolor="#cec8be" topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
<center>
<table width="90%" border="0"><tr><td class=normal>
<h1 class=h1bar>Credit Card Verification</h1>
<p>
Checking your credit card using eSec's secure credit card processing facility...
<p>
Our reference number <? echo htmlentities($r)?>
<hr>
<?php
function validate_order($orderid, $authcode, $signature)
{
	if (!($conn = pg_connect('dbname=pgmarket user=www-data password=pgmarket'))) {
		return "Problem connecting to database.";
	}
	$sql = "UPDATE orderscc SET state_id=2, codaut='$authcode', otpris='$signature' WHERE id = '$orderid'";
	if (!@pg_exec($conn, $sql)) {
		return "Problem with query: ".pg_errormessage($conn)."<br>SQL was: $sql";
		pg_close($conn);
	}
	return "";
}


// Clear out status
$bank_auth = '';
$order_id = '';

// validate order number
if ($r == '') {
	$gw_error ='the payment gateway did not give us the order number.';
	$order_id='(unknown)';
	$t->parse('gateway_reply_blck', 'gateway_reply', true);
	$t->parse('back_card_blck', 'back_card', true);
} else {
	$order_id=htmlentities($r);
	if ($m == '') {
		$gw_error ='the payment gateway did not give us the response code';
	} else {
		$auth_code = substr($m, 0, 3);
		if ($auth_code == 200) {
		if ($a == '') {
			$gw_error = 'the payment gateway did not give us the bank authorisation code';
		} else {
		// Its ok
		$bank_auth=htmlentities($a);
      	validate_order($r, $a, $s);
		}
	} elseif ($auth_code == 400) {
	 	$result_title = 'Card Refused';	
		$gw_error = '
<P>The bank has refused to authorise the payment of this transaction.  This 
usually means there is something wrong with the credit card or the credit
card account.  You order will sit in our ordered but unpaid queue.  Please
contact Small Pieces of Time direct to continue.
		';
  	} elseif ($auth_code == 401 ) {
		$gw_error=htmlentities($m);
		if (preg_match('/^401 invalid expiry/', $m)) {
			$result_title = 'Expiry Date Problem';
			$gw_error = '
<p>There is something wrong with the expiry date you gave, most likely you
have tried to use a card that is past its expiry date.  The gateway message
was '.htmlentities($m);
		} else {
			$result_title = 'Bad Payment Gateway Request';
			$gw_error = '
<p>The payment gateway has given our website a request that we just do not 
understand.  Please contact Small Pieces of Time for help.
<p>The problem with the request was '.htmlentities($m);
		}
	} else {
		$result_title = 'Gateway Problem';
		$gw_error = '
<p>There is either a problem with the payment gateway or the way that the website
communicates with the gateway.  In any case the credit card payment has
not been processed.</p.
<p>The specific error message that the payment gateway gave was '.
		htmlentities($m);
	}
	}
}
if (empty($gw_error)) {?>
<h1>Authorisation OK</h1>
<p>The bank has approved the credit card transaction.  Your order will be
processed by Small Pieces of Time shortly.

<P>Bank authorisation number: <?echo $bank_auth;?>

<hr><p>Click <a href="http://www.smallpiecesoftime.com.au/shopping/?id=1">here</a>
to return back to the store.

<? } else {
	echo "<h1>$result_title</h1>\n$gw_error";
?>
<form>
<div class=normal><font size=-1>
<input type="button" value="back" onClick="javascript: history.go(-1)">
</font></div>
</form>&nbsp;to the credit card payment page.
<? } ?>
</td></tr></table>
</center>
<br>

</body>
</html>


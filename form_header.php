<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

if (!empty($errormsg)) {
	include ($CFG["localelangdir"] . "global-error.inc.php");
	echo "<blockquote>";
	echo "<div class=\"warning\">" . $Errors . "</div>";
	echo "<div class=\"normal\">";
	echo $errormsg;
	echo "</div>";
	echo "</blockquote>";
}

if (!empty($noticemsg)) {
	$t = new Template();
	include ($CFG["localelangdir"] . "global-common.inc.php");
	echo "<blockquote>";
	echo "<div class=\"notice\">";
	echo $t->get_var($noticemsg);
	echo "</div>";
	echo "</blockquote>";
}

?>

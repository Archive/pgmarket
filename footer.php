<?php
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

$t = new Template();
$t->set_file("page", $CFG["dirroot"] . "templates/footer.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");

if (($PGM_SESSION["layersmenu_used"]) && strncmp(me(), "/info", 5) != 0) {
	if ($CFG["layersmenu_cached"]) {
		$t->set_var("layersmenu_footer", $qid_layersmenu->f("footer"));
	} else {
		if ($categories != "") {
			$t->set_var("layersmenu_footer", $mid->make_footer());
		} else {
			$t->set_var("layersmenu_footer", "");
		}
	}
} else {
	$t->set_var("layersmenu_footer", "");
}

$t->pparse("out", "page");
  
/* BEGIN BENCHMARK CODE
$time_end = getmicrotime();
$time = $time_end - $time_start;
echo "TIME ELAPSED = " . $time . "\n<br>";
/* END BENCHMARK CODE */
?>

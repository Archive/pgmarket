<?php
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

/* BEGIN BENCHMARK CODE
$time_start = getmicrotime();
/* END BENCHMARK CODE */

$t = new Template();
$t->set_file("page", $CFG["dirroot"] . "templates/header.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");

if ($PGM_SESSION["layersmenu_used"]) {
	if ($CFG["layersmenu_cached"]) {
		$qid_layersmenu = new PGM_Sql("
			SELECT header, first_level_menu, footer
			FROM cache
			WHERE lang = '" . $PGM_SESSION["lang"] . "'
		");
		$qid_layersmenu->next_record();
		$t->set_var(array(
			"layersmenu_header"	=> $qid_layersmenu->f("header"),
			"vermenu"		=> $qid_layersmenu->f("first_level_menu")
		));
	} else {
		include ($CFG["libdir"] . $CFG["dbms"] . "_layersmenu_queries.inc.php");
		include ($CFG["libdir"] . "phplayersmenu/layersmenu.inc.php");
		build_catbrowser_js($categories);
		if ($categories != "") {
			$mid = new LayersMenu(100);
			$mid->set_separator("^");
			$mid->set_dirroot($CFG["dirroot"]);
			$mid->set_libdir($CFG["libdir"] . "phplayersmenu/");
			$mid->set_libwww($CFG["libwww"] . "phplayersmenu/");
			$mid->set_tpldir($CFG["dirroot"] . "templates/");
			$mid->set_imgdir($CFG["imagesdir"]);
			$mid->set_imgwww($CFG["imageswww"]);
			$mid->set_forward_arrow_img("forward-gnome.png");
			$mid->set_vertical_menu_tpl("layersmenu-vertical_menu.ihtml");
			$mid->set_sub_menu_tpl("layersmenu-sub_menu.ihtml");
			$mid->set_menu_structure_string($categories);
			$mid->parse_menu_structure("vermenu");
			$mid->new_vertical_menu("vermenu");
			$t->set_var(array(
				"layersmenu_header"	=> $mid->make_header(),
				"vermenu"		=> $mid->get_menu("vermenu")
			));
		} else {
			$t->set_var(array(
				"layersmenu_header"	=> "",
				"vermenu"		=> ""
			));
		}
	}
} else {
	$t->set_var(array(
		"layersmenu_header"	=> "",
		"vermenu"		=> ""
	));
}

$t->set_var(array(
	"DOC_TITLE"		=> $t->get_var($DOC_TITLE),
	"wwwroot"		=> $CFG["wwwroot"],
	"firstpage"		=> $CFG["firstpage"],
	"cart_itemcount"	=> $PGM_SESSION["CART"]->num_items(),
	"cart_fgrandtotal"	=> formatted_price($PGM_SESSION["CART"]->get_grandtotal()),
	"session_stringtsf"	=> nvl($PGM_SESSION["stringtsf"], ""),
	"session_concatenation_checked_or"	=> nvl($PGM_SESSION["concatenation"], "") != "AND" ? "checked" : "",
	"session_concatenation_checked_and"	=> nvl($PGM_SESSION["concatenation"], "") == "AND" ? "checked" : "",
	"session_case_sensitive_checked"	=> nvl($PGM_SESSION["case_sensitive"], 0) ? "checked" : ""
));
$t->set_block("page", "is_logged_in", "is_logged_in_blck");
$t->set_var(array("is_logged_in_blck" => ""));
$t->set_block("page", "is_not_logged_in", "is_not_logged_in_blck");
$t->set_var(array("is_not_logged_in_blck" => ""));
if (is_logged_in()) {
	$t->parse("is_logged_in_blck", "is_logged_in", true);
} else {
	if (!isset($PGM_SESSION["challenge"]) ) {
        srand((double) microtime() * 1000000);
		$PGM_SESSION["challenge"] = md5(uniqid(rand()));
	}
	$t->set_var(array(
		'loginqualifiedme' =>  qualified_me(),
	    'menufrm_username'	=> ov(nvl($_POST["username"], "")),
		"menufrm_challenge"	=> $PGM_SESSION["challenge"],
		"libwww" => $CFG['libwww']
	));
	$t->parse("is_not_logged_in_blck", "is_not_logged_in", true);
}
$t->set_block("page", "is_info", "is_info_blck");
$t->set_var(array("is_info_blck" => ""));
$t->set_block("page", "is_not_info", "is_not_info_blck");
$t->set_var(array("is_not_info_blck" => ""));
$t->set_block("is_not_info", "layersmenu_used", "layersmenu_used_blck");
$t->set_var(array("layersmenu_used_blck" => ""));
if ($PGM_SESSION["layersmenu_used"]) {
	$t->parse("layersmenu_used_blck", "layersmenu_used", true);
}
$t->set_block("is_not_info", "frames_used", "frames_used_blck");
$t->set_var(array("frames_used_blck" => ""));
$t->set_block("is_not_info", "frames_not_used", "frames_not_used_blck");
$t->set_var(array("frames_not_used_blck" => ""));
if ($PGM_SESSION["frames_used"]) {
	$t->parse("frames_used_blck", "frames_used", true);
} else {
	$t->parse("frames_not_used_blck", "frames_not_used", true);
}
$t->set_block("is_not_info", "layersmenu_used_bis", "layersmenu_used_bis_blck");
$t->set_var(array("layersmenu_used_bis_blck" => ""));
$t->set_block("is_not_info", "layersmenu_not_used_bis", "layersmenu_not_used_bis_blck");
$t->set_var(array("layersmenu_not_used_bis_blck" => ""));
if ($PGM_SESSION["layersmenu_used"]) {
	$t->parse("layersmenu_used_bis_blck", "layersmenu_used_bis", true);
} else {
	$t->parse("layersmenu_not_used_bis_blck", "layersmenu_not_used_bis", true);
}
$t->set_block("is_not_info", "is_logged_in_admin", "is_logged_in_admin_blck");
$t->set_var(array("is_logged_in_admin_blck" => ""));
if (is_logged_in() && has_priv("admin")) {
	$t->parse("is_logged_in_admin_blck", "is_logged_in_admin", true);
}
$t->set_var(array("user_discount_used_blck" => ""));
$t->set_block("is_not_info", "user_discount_used", "user_discount_used_blck");
if ($CFG["user_discount_used"] && is_logged_in() && $PGM_SESSION["user"]["user_discount"] > 0) {
	$t->set_var("user_discount", $PGM_SESSION["user"]["user_discount"]);
	$t->parse("user_discount_used_blck", "user_discount_used", true);
}
$t->set_block("is_not_info", "delivery_used", "delivery_used_blck");
$t->set_var(array("delivery_used_blck" => ""));
$t->set_block("is_not_info", "delivery_calc", "delivery_calc_blck");
$t->set_var(array("delivery_calc_blck" => ""));
$t->set_block("is_not_info", "delivery_costs", "delivery_costs_blck");
$t->set_var(array("delivery_costs_blck" => ""));
$thispage = me();
if (strncmp($thispage, "/info", 5) == 0 || strcmp($thispage, "/") == 0 
    || strcmp($thispage, "/index.php") == 0 || preg_match('/\.html$/', $thispage)) {
	$t->parse('is_info_blck', 'is_info', true);
} else {
  if ($CFG["delivery_used"]) {
	$t->set_var(array(
		"cart_ftotal"		=> formatted_price($PGM_SESSION["CART"]->get_discounted_total()),
		"cart_fdelivery"	=> formatted_price($PGM_SESSION["CART"]->get_delivery())
	));
	if (is_logged_in() || $CFG["only_one_delivery_zone"]) {
		$t->parse("delivery_used_blck", "delivery_used", true);
	} else {
		$t->parse("delivery_calc_blck", "delivery_calc", true);
	}
	if (isset($PGM_SESSION["zone_id"])) {
		$t->parse("delivery_costs_blck", "delivery_costs", true);
	}
}
	$t->set_var('loginqualifiedme', qualified_me());
	$t->parse("is_not_info_blck", "is_not_info", true);
}
$t->set_block("page", "is_logged_in_horbartop", "is_logged_in_horbartop_blck");
$t->set_var(array("is_logged_in_horbartop_blck" => ""));
if (is_logged_in()) {
	$t->set_var(array(
		"session_user_firstname"	=> o($PGM_SESSION["user"]["firstname"]),
		"session_user_lastname"		=> o($PGM_SESSION["user"]["lastname"])
	));
	$t->parse("is_logged_in_horbartop_blck", "is_logged_in_horbartop", true);
}

$t->pparse("out", "page");

?>

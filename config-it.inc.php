<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

$CFG = array();

$CFG["dbms"] = "pgsql";	// PostgreSQL
//$CFG["dbms"] = "mysql";	// MySQL

$CFG["available_languages"] = array("it", "en");
$CFG["use_http_accept_language"] = 1;	// set it to 1 to try
// to automatically choose the browser's preferred language
// among the available ones, relying on HTTP_ACCEPT_LANGUAGE
// if the language session variable is still not defined

$session_name = "pgm_example_session";	// not significant if PHP 4.0 is used

// *all* directories stored in $CFG *must* be indicated *with* an ending "/"
$CFG["wwwroot"]		= "/~pratesi/pgmarket/";		// *with* an ending "/"
$CFG["dirroot"]		= "/home/pratesi/public_html/pgmarket/";// *with* an ending "/"
$CFG["firstpage"]	= $CFG["wwwroot"] . "index.php";
$CFG["libdir"]		= $CFG["dirroot"] . "lib/";		// *with* an ending "/"
$CFG["libwww"]		= $CFG["wwwroot"] . "lib/";		// ... and so on...
$CFG["localedir"]	= $CFG["dirroot"] . "locale/";
$CFG["productswww"]	= $CFG["wwwroot"] . "products/";
$CFG["productsdir"]	= $CFG["dirroot"] . "products/";
$CFG["imageswww"]	= $CFG["wwwroot"] . "images/";
$CFG["imagesdir"]	= $CFG["dirroot"] . "images/";

$CFG["myname"] = "PgMarket 2.2.1";

$CFG["support"]		= "pratesi@localhost";	// "Technical support" at PgMarket
$CFG["seller_fullname"]	= "Marco Pratesi";
$CFG["seller_email"]	= "pratesi@localhost";
$CFG["subject_contact"]			= "Contattaci!";
$CFG["subject_cash_on_delivery"]	= "Ordine con Pagamento in Contrassegno";
$CFG["subject_credit_card"]		= "Ordine con Pagamento con Carta di Credito";

$CFG["currency"] = "&euro;";
//$CFG["currency"] = "$";
$CFG["price_must_be_integer"] = 0;
	// 0 --> the price can be float (Euro and US Dollars, for example)
	// 1 --> the price must be integer (Italian Lit, for example)
$CFG["brand_used"] = 1;
$CFG["iva_used"] = 1;		// IVA, i.e. VAT
//$CFG["iva_value"] = 0;	// not significant if IVA is used
$CFG["IVA"] = "IVA";
$CFG["weight_used"] = 1;
$CFG["weight_unit"] = "Kg";	// not significant if weight is not used
$CFG["max_weight"] = 10000.0;	// not significant if weight is not used
$CFG["delivery_used"] = 1;
$CFG["only_one_delivery_zone"] = 0;	// set it to 1 if you will use only the default delivery zone, with id = 0
$CFG["color_used"] = 1;
$CFG["user_discount_used"] = 1;
$CFG["max_file_size_images"] = 500000;		// upload of product images
$CFG["max_file_size_import"] = 10000000;	// upload of import files
$CFG["max_file_size_tables_data"] = 10000000;	// upload of import files

$CFG["frames_used_by_default"] = 0;	// 0 --> no, 1 --> yes
$CFG["layersmenu_used_by_default"] = 1;	// 0 --> no, 1 --> yes
$CFG["layersmenu_cached"] = 1;		// 0 --> no, 1 --> yes

$CFG["create_thumbs"] = 1;	// do you want to enable generation of thumbs on the fly?
$CFG["rmf"] = "rm -f";
$CFG["rmrf"] = "rm -rf";
$CFG["unix"] = 1;
//$CFG["create_thumbs"] = 0;	// on WAMP (Windows, Apache, MySQL, PHP)
//$CFG["rmf"] = "del";		// on WAMP
//$CFG["rmrf"] = "deltree";	// on WAMP
//$CFG["unix"] = 0;		// on WAMP

// Note: if safe_mode is enabled and/or the open_basedir restriction
// is in effect, you are not able to use ImageMagick and netpbm + libjpeg
$CFG["use_imagemagick"] = 1;	// Do you want to use ImageMagick to generate thumbs on the fly?
$CFG["use_libgd"] = 0;		// Do you want to use PHP libgd support to generate thumbs on the fly?
$CFG["use_netpbm"] = 0;		// Do you want to use netpbm + libjpeg to generate thumbs on the fly?

// On Debian Woody:
$CFG["imagemagick_path"] = "/usr/bin/";		// *with* an ending "/"
//
// On Red Hat:
//$CFG["imagemagick_path"] = "/usr/X11R6/bin/";	// *with* an ending "/"
//
$CFG["netpbm_path"] = "/usr/bin/";		// *with* an ending "/"
$CFG["libjpeg_path"] = "/usr/bin/";		// *with* an ending "/"

$CFG["images_on_file_system"] = 1;	// set it to 0 if you want to store product images and thumbs in the DB

$CFG["base_lang"] = "it";
//$CFG["base_lang"] = "en";
$CFG["yes"] = "si";
//$CFG["yes"] = "yes";
$CFG["default_i18n"] = "en";	// The default internazionalization language
//$CFG["default_i18n"] = "it";

function print_formatted_price($price) {
	global $CFG;
	echo $CFG["currency"] . " " . number_format ($price, 2, ".", ",");
}
function formatted_price($price) {
	global $CFG;
	return $CFG["currency"] . " " . number_format ($price, 2, ".", ",");
}
function formatted_price_text($price) {
	global $CFG;
	return "Euro " . number_format ($price, 2, ".", ",");
}

function print_formatted_weight($weight) {
	echo number_format ($weight, 2, ",", ".");
//	echo number_format ($weight, 2, ".", ",");
}
function formatted_weight($weight) {
	return number_format ($weight, 2, ",", ".");
//	return number_format ($weight, 2, ".", ",");
}

if (!isset($_SERVER)) $_SERVER = $HTTP_SERVER_VARS;	// For CoMPaTiBiLiTy with PHP 4.0
if (!isset($_GET)) $_GET = $HTTP_GET_VARS;		// For CoMPaTiBiLiTy with PHP 4.0
if (!isset($_POST)) $_POST = $HTTP_POST_VARS;		// For CoMPaTiBiLiTy with PHP 4.0
if (!isset($_COOKIE)) $_COOKIE = $HTTP_COOKIE_VARS;	// For CoMPaTiBiLiTy with PHP 4.0
if (!isset($_FILES)) $_FILES = $HTTP_POST_FILES;	// For CoMPaTiBiLiTy with PHP 4.0
if (!isset($_ENV)) $_ENV = $HTTP_ENV_VARS;		// For CoMPaTiBiLiTy with PHP 4.0

?>

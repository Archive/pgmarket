<?php

include ("config.inc.php");
include ("common.inc.php");

$DOC_TITLE="Credit Card Status";
include ($CFG["dirroot"]."header.php");

$t = new Template();
$t->set_file('page', 'templates/eway_confirm.ihtml');
include($CFG['localelangdir']  . "global-common.inc.php");
include($CFG['localelangdir'] .  "global-admin.inc.php");
include($CFG['localelangdir'] .  "global-shopping.inc.php");
$t->set_var('wwwroot', $CFG["wwwroot"]);

$t->set_block('page', 'transaction_error', 'transaction_error_blk');
$t->set_var('transaction_error_blk', '');
$t->set_block('page', 'transaction_ok', 'transaction_ok_blk');
$t->set_var('transaction_ok_blk', '');

if (isset($_POST['ewayTrxnNumber'])) {
	$order_id = htmlentities($_POST['ewayTrxnNumber']);
} else {
	$order_id = 'undefined';
}
if (isset($_POST['ewayTrxnReference'])) {
	$trxn_reference = htmlentities($_POST['ewayTrxnReference']);
} else {
	$trxn_reference = 'undefined';
}
$t->set_var(array(
	'order_id' => $order_id,
	'trxn_reference' => $trxn_reference,
	));

if ($_POST['ewayTrxnStatus'] == 'True') {
	if (isset($_POST['eWAYAuthCode'])) {
		$authcode = htmlentities($_POST['eWAYAuthCode']);
	} else {
		$authcode = 'undefined';
	}
	validate_order($order_id, $authcode);
	$t->set_var('auth_code',$authcode);
		
	$t->parse('transaction_ok_blk','transaction_ok');
} else {
	$t->set_var('response_text', 'unknown');
    if (isset($_POST['eWAYresponseCode'])) {
		$t->set_var('response_text', htmlentities($_POST['eWAYresponseCode']));
	}
	$t->parse('transaction_error_blk','transaction_error');
}

$t->pparse("out", "page");
include ($CFG["dirroot"] . "footer.php");
pgm_session_close($PGM_SESSION, $session_name);

################ Functions
function validate_order($orderid, $authcode)
{
	$qid = new DB_Sql("UPDATE orderscc SET state_id=2, codaut='$authcode' WHERE id = '$orderid'");
}

?>

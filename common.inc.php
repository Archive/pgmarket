<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ($CFG["libdir"] . "stdlib.inc.php");
include ($CFG["libdir"] . "phplib/db_" . $CFG["dbms"] . ".inc.php");	// PHPLib...
include ($CFG["libdir"] . "pgm_" . $CFG["dbms"] . ".inc.php");		// PGM extension...
include ($CFG["libdir"] . $CFG["dbms"] . "_common.inc.php");
include ($CFG["libdir"] . "pgm_common.inc.php");
include ($CFG["libdir"] . "pgm_session.inc.php");
include ($CFG["libdir"] . "pgm_users.inc.php");
include ($CFG["libdir"] . "pgm_cart.inc.php");
include ($CFG["libdir"] . "pgm_spot.inc.php");
include ($CFG["libdir"] . "phplib/template.inc.php");
include ($CFG["localedir"] . $CFG["base_lang"] . "/base_lang.inc.php");

$PGM_SESSION = array();
pgm_session_open($PGM_SESSION, $session_name);

include ($CFG["libdir"] . "misc_settings.inc.php");

if (!isset($PGM_SESSION["CART"])) {
	$PGM_SESSION["CART"] = new PGM_Cart();
}

if ($CFG["only_one_delivery_zone"]) {
	$PGM_SESSION["zone_id"] = 1;
}

if (!isset($PGM_SESSION["frames_used"])) {
	$PGM_SESSION["frames_used"] = $CFG["frames_used_by_default"];
}
if ($PGM_SESSION["frames_used"]) {
	$PGM_SESSION["catbrowser_target"] = "bodyframe";
} else {
	$PGM_SESSION["catbrowser_target"] = "main";
}

if (!isset($PGM_SESSION["layersmenu_used"])) {
	$PGM_SESSION["layersmenu_used"] = $CFG["layersmenu_used_by_default"];
}

if (!isset($PGM_SESSION["lang"])) {
	if ($CFG["use_http_accept_language"] && isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])) {
		$foolang = explode(",", $_SERVER["HTTP_ACCEPT_LANGUAGE"]);
		if (count($foolang) > 0) {
			reset($foolang);
			while (list($key, $val) = each($foolang)) {
				$foovect = explode(";", $val);
				$accept_language = str_replace(" ", "", $foovect[0]);
				$accept_language = substr($accept_language, 0, 2);
				if (in_array($accept_language, $CFG["available_languages"])) {
					$PGM_SESSION["lang"] = $accept_language;
					break;
				}
			}
		}
	}
	if (!isset($PGM_SESSION["lang"])) {
		$PGM_SESSION["lang"] = $CFG["base_lang"];
	}
}
if (!isset($PGM_SESSION["i18n"])) {
	$PGM_SESSION["i18n"] = $CFG["default_i18n"];
}
$CFG["localelangdir"] = $CFG["localedir"] . $PGM_SESSION["lang"] . "/";
$CFG["globalerror"] = $CFG["localelangdir"] . "global-error.inc.php";

$ME = qualified_me();
if (!nvl($norefererflag, 0)) {
	$PGM_SESSION["REFERER"] = nvl($PGM_SESSION["NEW_REFERER"], "");
	$PGM_SESSION["REFERER_AND_QUERY"] = nvl($PGM_SESSION["NEW_REFERER_AND_QUERY"], "");
	$PGM_SESSION["NEW_REFERER"] = $ME;
	$PGM_SESSION["NEW_REFERER_AND_QUERY"] = $ME;
	if (!isset($_SERVER["QUERY_STRING"])) $_SERVER["QUERY_STRING"] = "";	// workaround for a MS IIS bug
	if ($_SERVER["QUERY_STRING"] != "") {
		$PGM_SESSION["NEW_REFERER_AND_QUERY"] .= "?" . $_SERVER["QUERY_STRING"];
	}
	if ($PGM_SESSION["REFERER"] == "") {
		$PGM_SESSION["REFERER"] = $PGM_SESSION["NEW_REFERER"];
	}
	if ($PGM_SESSION["REFERER_AND_QUERY"] == "") {
		$foobar = str_replace("&amp;", "&", $PGM_SESSION["NEW_REFERER_AND_QUERY"]);
		$foobar = str_replace("&", "&amp;", $foobar);
		$PGM_SESSION["REFERER_AND_QUERY"] = $foobar;
	}
}

?>

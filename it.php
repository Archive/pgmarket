<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
include ("config.inc.php");
include ("common.inc.php");
$PGM_SESSION["lang"] = "it";
$PGM_SESSION["CART"]->calc_grandtotal();
include ($CFG["libdir"] . "referer_and_query_lang.inc.php");
pgm_session_close($PGM_SESSION, $session_name);
if (strip_querystring($REFERER_AND_QUERY) == qualified_me()) {
	$url = $CFG["firstpage"];
} else {
	$url = $REFERER_AND_QUERY;
}
if ($PGM_SESSION["frames_used"]) { ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
</head>
<body>
<script language="JavaScript" type="text/javascript">
<!--
if (window.name == "bodyframe") {
parent.categories.location.reload();
parent.bodyframe.location.href="<?php echo $url; ?>";
} else window.location.href="<?php echo $url; ?>";
// -->
</script>
</body>
</html>
<?php
} else {
	redirect("$url");
	die;
}
?>

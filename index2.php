<?
@include("../application.inc.php");
@include("application.inc.php");

$DOC_TITLE = "PgMarket_Home_Page";

include("$CFG->dirroot/header.php");

$qid = new PGM_Sql;
get_new_products(&$qid);
$t = new Template;
$t->set_file(array(
		"page" => "$CFG->dirroot/info/templates/index.ihtml",
		"new_products"	=> "$CFG->dirroot/info/templates/new_products.ihtml"
	));
require($CFG->localedir . $SESSION["lang"] . "/global-common.inc.php");
require($CFG->localedir . $SESSION["lang"] . "/global-admin.inc.php");
$t->set_var(array(
		"DOC_TITLE"			=> ov($t->get_var($DOC_TITLE)),
		"wwwroot"			=> $CFG->wwwroot,
		"images"			=> $CFG->images,
		"firstpage"			=> $CFG->firstpage,
		));

$t->set_block("page", "there_are_new_products", "there_are_new_products_blck");
if (!($qid->num_rows() > 0))
	$t->set_var(array("there_are_new_products_blck" => ""));
else {
	$t->set_block("new_products", "new_products_product", "new_products_products");
	$t->set_block("new_products_product", "new_product_image", "new_product_image_blck");
	$t->set_var(array("new_products_products" => ""));
        $t->set_var("productswww", $CFG->productswww);
	while($qid->next_record()) {
		$t->set_var("new_product_image_blck", "");
		$t->set_var(array(
				"r_id"		=> $qid->f('id'),
				"r_name"	=> ov($qid->f('name')),
				"r_description"	=> ovwbr($qid->f('description')),
				"r_price"	=> $qid->f('price'),
				"r_fprice"	=> formatted_price($qid->f('price'))
		));
		if (file_exists("$CFG->productsdir" . $qid->f("id") . ".thumb.jpg")) {
			$t->set_var(array(
				"imagefilename"	=> $qid->f("id"),
				"thumbwidth"	=> $qid->f("thumbwidth"),
				"thumbheight"	=> $qid->f("thumbheight")
			));
			$t->parse("new_product_image_blck", "new_product_image", true);
			}
		$t->parse("new_products_products", "new_products_product", true);
	}
	$t->parse("there_are_new_products_blck", "there_are_new_products", true);
}

$t->pparse("out", array("new_products", "page"));
include("$CFG->dirroot/footer.php");

/******************************************************************************
 * FUNCTIONS
 *****************************************************************************/

function load_new_products(&$qid) {
/* loads the list of new products */
	global $CFG;
	global $SESSION;
	global $weightunity;
	include ($CFG->globalerror);

	$qid = db_query("
			SELECT p.*, b.name AS bname, i.iva
			FROM products p, brands b, iva i
			WHERE p.brand_id = b.id AND p.iva_id = i.id AND new_level > 0
			ORDER BY new_level ASC
			");
}

?>

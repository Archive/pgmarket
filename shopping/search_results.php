<?php
/* (c) 2000-2002 Marco Pratesi <pratesi@telug.it> */

if ($SETTING["color_used"])
	$qid_col = new PGM_Sql;

$t = new Template();
$t->set_file(array("page" => "templates/search_results.ihtml"));
include ("$CFG->localelangdir" . "/global-common.inc.php");
include ("$CFG->localelangdir" . "/global-shopping.inc.php");
include ("$CFG->localelangdir" . "/global-admin.inc.php");
$t->set_var("wwwroot", $CFG->wwwroot);
$t->set_var("productswww", $CFG->productswww);
$t->set_var("ME", $ME);
$t->set_var("id", $id);

$t->set_block("page", "num_pages", "num_pages_blck");
$t->set_var("num_pages_blck", "");
$summary_report = $t->get_var("Search_Results") . ": " . $num["rows"];
$t->set_var("summary_report", $summary_report);
$t->set_block("page", "products", "products_blck");
$t->set_var("products_blck", "");
if ($qid->num_rows() > 0) {
	// Let us define blocks nested inside a product_row...
	// We indent the code to highlight the blocks' hierarchical nesting...
	$t->set_block("products", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("products", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($num["pages"] > 1) {
		$summary_report .= " " . $t->get_var("on") . " " . $num["pages"] . " " . $t->get_var("pages");
		$num_pages = $num["pages"];
		include ("$CFG->libdir" . "/pages_links.php");
		$t->set_var("pages_links", $pages_links);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	include("product_list.php");
	show_productlist($qid,&$t);
}

$t->parse("products_blck", "products", true);
$t->pparse("out", "page");

?>

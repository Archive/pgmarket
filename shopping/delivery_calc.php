<?php
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");

$DOC_TITLE = "DC_Selection";
$qid = new PGM_Sql();

if (!isset($PGM_SESSION["back"])) {
	$PGM_SESSION["back"] = $PGM_SESSION["REFERER_AND_QUERY"];
}
if (!isset($_POST["zone_id"])) {
	include ($CFG["dirroot"] . "header.php");
	$t = new Template();
	$t->set_file("page", "templates/zone_selection_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var("ME", $ME);
	$t->set_block("page", "delivery_zone_option", "delivery_zone_options");
	$t->set_var("delivery_zone_options", "");
	get_zones_names($qid);
	while ($qid->next_record()) {
		$t->set_var(array(
			"frm_user_zone_id"		=> $qid->f("zone_id"),
			"frm_user_zone_selected"	=> ($qid->f("zone_id") == nvl($PGM_SESSION["zone_id"], "")) ? "selected" : "",
			"frm_user_zone_name"		=> ($qid->f("lname") != "") ? ov($qid->f("lname")) : $qid->f("name")
		));
		$t->parse("delivery_zone_options", "delivery_zone_option", true);
	}
	$t->pparse("out", "page");
	include ($CFG["dirroot"] . "footer.php");
	pgm_session_close($PGM_SESSION, $session_name);
} else {
	$PGM_SESSION["zone_id"] = $_POST["zone_id"];
	$PGM_SESSION["CART"]->calc_grandtotal();
	$back = $PGM_SESSION["back"];
	unset($PGM_SESSION["back"]);
	pgm_session_close($PGM_SESSION, $session_name);
	redirect($back);
	die;
}

?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ("../config.inc.php");
include ("../common.inc.php");

$id = nvl($_GET["id"], 1);
if (!isset($_GET["page_number"])) {
	$page_number = 1;
} else {
	$page_number = max(1, abs(intval($_GET["page_number"])));
}

echo "page numer $page_number id is $id   or ".$_GET["page_number"]."<br>";
$DOC_TITLE = "Shopping_Catalog";
$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
include ($CFG["dirroot"] . "header.php");

build_category_path($id, $category_path);

$catdesc = get_category_description($id);

/* get the name and description of all the sub-categories under this one */
$qid_c = new PGM_Sql();
//get_subcategories($qid_c, $id);
$qid_c->pgm_lang_join_query(
	$CFG["base_lang"],
	array("categories c", "categories_i18n l"),
	array(array("c.id", "l.category_id", 1, $PGM_SESSION["lang"])),
	array(array("l.name AS lname")),
	"c.id, c.name, c.special_level",
	"",
	"parent_id = '$id' AND id > 1",
	"special_level, name",
	array()
);
/* get all the products under this category */
$qid_p = new PGM_Sql();
//get_products_under_category($qid_p, $id, $page_number, $num);
$qid_p->query("
	SELECT COUNT(p.id) AS cnt
	FROM products p, products_categories pc
	WHERE p.id = pc.product_id AND pc.category_id = '$id'
");
$qid_p->next_record();
include ($CFG["libdir"] . "pgm_pager.inc.php");
if (!isset($_GET["page_number"])) {
	$page_number = 1;
} else {
	$page_number = max(1, abs(intval($_GET["page_number"])));
}
$result = pgm_pager($ME, $qid_p->f("cnt"), $PGM_SESSION["products_per_catalog_page"], $page_number);
$limit = $result["last"] - $result["first"] + 1;
$qid_p->pgm_lang_join_query(
	$CFG["base_lang"],
	array("products p", "products_i18n l"),
	array(array("p.id", "l.product_id", 1, $PGM_SESSION["lang"])),
	array(array("l.name AS lname",  "l.description AS ldescription")),
	"p.id, p.code, p.name AS name, p.price, p.discount, p.discqty, p.weight, p.special_flag, p.special_level, p.description, p.thumbtype, p.thumbwidth, p.thumbheight, pc.category_id, b.name AS bname, i.iva, p.new_flag, p.new_level, p.nostock_flag",
	"products_categories pc, brands b, iva i",
	"p.id = pc.product_id AND p.brand_id = b.id AND p.iva_id = i.id AND pc.category_id = '$id'",
	"special_level DESC, name",
	array($result["first"], $limit)
);

if ($CFG["color_used"]) {
	$qid_col = new PGM_Sql();
}

$t = new Template();
$t->set_file("page", "templates/index.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-shopping.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->set_var(array(
	"wwwroot"	=> $CFG["wwwroot"],
	"productswww"	=> $CFG["productswww"],
	"id"		=> $id,
	"category_path"	=> $category_path,
	"catdesc" => $catdesc,
	"QSID"		=> (SID == "") ? "" : "?" . SID
));

$t->set_block("page", "noframes", "noframes_blck");
$t->set_var("noframes_blck", "");
$t->set_block("page", "noframes2", "noframes2_blck");
$t->set_var("noframes2_blck", "");
if (!$PGM_SESSION["frames_used"] || has_priv("admin")) {
	$t->parse("noframes_blck", "noframes", true);
	$t->set_block("noframes2", "sub_categories", "sub_categories_blck");
	$t->set_block("sub_categories", "category_row", "category_rows");
	$t->set_var("sub_categories_blck", "");
	$t->set_block("category_row", "category_cell", "category_cells");
	$t->set_block("category_row", "no_category_cell", "no_category_cells");
	$t->set_var("category_rows", "");
	$t->set_block("noframes2", "adminnoframes2", "adminnoframes2_blck");
	$t->set_var("adminnoframes2_blck", "");
	$t->set_var("no_category_cells", "");
	if ($qid_c->num_rows() != 0)
	{
	$i = 0;
		while ($qid_c->next_record()) {
			$foobar = ($qid_c->f("lname") != "") ? $qid_c->f("lname") : $qid_c->f("name");
			if (has_priv("admin")) {
				$t->set_var(array(
					"c_special_level"	=> $qid_c->f("special_level") . " - ",
					"category_id"		=> $qid_c->f("id"),
					"cname"			=> ov($foobar)
				));
			} else {
				$t->set_var(array(
					"c_special_level"	=> "",
					"category_id"		=> $qid_c->f("id"),
					"cname"			=> ov($foobar)
				));
			}
		        #if ($i % 2 == 0) {
		        #  $t->parse("category_cells", "category_cell", false);
	        	#} else {
	        	  $t->parse("category_cells", "category_cell", true);
	        	#}
	        	if ($i % 2 == 1) {
	        	$t->parse("category_rows", "category_row", true); 
	        	  $t->set_var("category_cells", "");
	        	}
	        	$i++;
		}
	        // Complete row if we are in the middle of one
	        if ($i % 2 == 1) {
	          $t->parse("category_rows", "category_row", true);
	        }
	        $t->parse("sub_categories_blck", 'sub_categories', true);
	}
	if (has_priv("admin")) {
		$t->parse("adminnoframes2_blck", "adminnoframes2", true);
	}
	if ((!$PGM_SESSION["frames_used"]) && (!$PGM_SESSION["layersmenu_used"])) {
		include ($CFG["libdir"] . $CFG["dbms"] . "_treemenu_queries.inc.php");
		include ($CFG["libdir"] . "phplayersmenu/layersmenu.inc.php");
		include ($CFG["libdir"] . "phplayersmenu/layersmenu-noscript.inc.php");
		build_catbrowser($categories, 0);
		if ($categories != "") {
			$tree_mid = new XLayersMenu();
			$tree_mid->set_separator("^");
			$tree_mid->set_dirroot($CFG["dirroot"]);
			$tree_mid->set_libdir($CFG["libdir"] . "phplayersmenu/");
			$tree_mid->set_libwww($CFG["libwww"] . "phplayersmenu/");
			$tree_mid->set_imgdir($CFG["imagesdir"]);
			$tree_mid->set_imgwww($CFG["imageswww"]);
			$tree_mid->set_menu_structure_string($categories);
			$tree_mid->parse_menu_structure("navigation");
			$tree_mid->new_tree_menu("navigation");
			$t->set_var("treemenu", $tree_mid->get_tree_menu("navigation"));
		} else {
			$t->set_var("treemenu", "");
		}
		$t->parse("noframesnolayersmenu_blck", "noframesnolayersmenu", true);
	}
	$t->parse("noframes2_blck", "noframes2", true);
}
$t->set_block("page", "products", "products_blck");
$t->set_var("products_blck", "");
// Let us define blocks nested inside a product_row...
// We indent the code to highlight the blocks' hierarchical nesting...
	$t->set_block("products", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("products", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}

include("product_list.php");
show_productlist($qid_p,&$t);
$t->parse("products_blck", "products", true);

$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

function get_category_description($id) {
  $qid_tc = new PGM_Sql;

  $qid_tc->Query("SELECT description from categories WHERE id = '$id'");
  if ($qid_tc->num_rows() > 0) {
    $qid_tc->next_record();
    return $qid_tc->f('description');
  }
  return "";
}
?>

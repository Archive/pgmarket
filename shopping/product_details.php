<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ("../config.inc.php");
include ("../common.inc.php");

$id = nvl($_GET["id"], "");
$product_id = nvl($_GET["product_id"], "");

$qid = new PGM_Sql();
$ao_qid = new PGM_Sql();
if ($CFG["color_used"]) {
	$qid_col = new PGM_Sql();
}

if ($product_id != "") {
	if ($id != "") {	// category not specified
		$qid->query("SELECT category_id FROM products_categories WHERE product_id = '$product_id' AND category_id = '$id'");
		if (!($qid->num_rows())) {
			pgm_session_close($PGM_SESSION, $session_name);
			redirect($CFG["firstpage"]);
			die;
		}
	} else {
		$qid->query("SELECT category_id FROM products_categories WHERE product_id = '$product_id'");
		if ($qid->num_rows()) {
			$qid->next_record();
			$id = $qid->f("category_id");
		} else {
			pgm_session_close($PGM_SESSION, $session_name);
			redirect($CFG["firstpage"]);
			die;
		}
	}
} else {
	pgm_session_close($PGM_SESSION, $session_name);
	redirect($CFG["firstpage"]);
	die;
}

//get_product_details($qid, $product_id);
$qid->pgm_lang_join_query(
	$CFG["base_lang"],
	array("products p", "products_i18n l"),
	array(array("p.id", "l.product_id", 1, $PGM_SESSION["lang"])),
	array(array("l.name AS lname",  "l.extended_description AS lextended_description")),
	"p.id AS product_id, p.code, p.name AS name, p.price, p.discount, p.discqty, p.weight, p.extended_description, p.imagetype, p.imagewidth, p.imageheight, p.thumbwidth, p.thumbheight, b.name AS bname, i.iva, p.nostock_flag, p.special_flag",
	"brands b, iva i",
	"p.brand_id = b.id AND p.iva_id = i.id AND p.id = '$product_id'",
	"",
	array()
);

if ($qid->num_rows() > 0) {
	$qid->next_record();
	$prod = true;
} else {
	$prod = false;
}

$DOC_TITLE = $prod ? "Product_Details" : "Product_Not_Found";

include ($CFG["dirroot"] . "header.php");

$t = new Template();
$t->set_file("page", "templates/product_details.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-shopping.inc.php");
$t->set_block("page", "product", "product_blck");
$t->set_var(array("product_blck" => ""));
$t->set_block("page", "noproduct", "noproduct_blck");
$t->set_var(array("noproduct_blck" => ""));
if ($prod) {
	build_category_path($id, $category_path);
	$t->set_var("category_path", $category_path);

	$t->set_block("product", "brand_used", "brand_used_blck");
	$t->set_var("brand_used_blck", "");
	/*$t->set_block("product", "discount_price", "discount_price_blck");
	$t->set_var("discount_price_blck", "");*/
 	$t->set_block("product", "user_discount_used", "user_discount_used_blck");
 	$t->set_var("user_discount_used_blck", "");
	$t->set_block("product", "iva_used", "iva_used_blck");
	$t->set_var("iva_used_blck", "");
	$t->set_block("product", "in_stock", "in_stock_blck");
	$t->set_var("in_stock_blck", "");
	$t->set_block("product", "no_stock", "no_stock_blck");
	$t->set_var("no_stock_blck", "");
	$t->set_block("product", "discount_block", "discount_block_blck");
	$t->set_var("discount_block_blck", "");
		$t->set_block("discount_block", "discqty_block", "discqty_block_blck");
		$t->set_var("discqty_block_blck", "");
	$t->set_block("product", "weight_used", "weight_used_blck");
	$t->set_var("weight_used_blck", "");
	$t->set_block("product", "color_used", "color_used_blck");
	$t->set_var("color_used_blck", "");
	$t->set_block("product", "prodimage", "prodimage_blck");
	$t->set_var("prodimage_blck", "");
	$t->set_block("product", "also_ordered", "also_ordered_blck");
	$t->set_var('also_ordered_blck','');
	$t->set_block("also_ordered", "also_ordered_row", "also_ordered_rows");
	$t->set_var('also_ordered_rows','');
	$t->set_var(array(
		"category_id"		=> $id,
		"product_id"		=> $qid->f("product_id"),
		"product_code"		=> $qid->f("code"),
		"name"			=> ($qid->f("lname") != "") ? ov($qid->f("lname")) : ov($qid->f("name")),
		"fprice"		=> formatted_price($qid->f("price")),
		"extended_description"	=> ($qid->f("lextended_description") != "") ? ovwbr($qid->f("lextended_description")) : ovwbr($qid->f("extended_description")),
		"imagetype"		=> $qid->f("imagetype"),
		"productitemcount"	=> $PGM_SESSION["CART"]->get_product_qty($qid->f("product_id"))
	));
	if ($CFG["brand_used"] && $qid->f("bname") != "") {
		$t->set_var("bname", ov($qid->f("bname")));
		$t->parse("brand_used_blck", "brand_used", true);
	}
	if ($CFG["user_discount_used"] && is_logged_in() && $PGM_SESSION["user"]["user_discount"] > 0 && $qid->f("discount") == 0) {
		$t->set_var(array(
			"user_discount"		=> $PGM_SESSION["user"]["user_discount"],
			"discounted_price"	=> formatted_price((1.0-$PGM_SESSION["user"]["user_discount"]/100.0)*$qid->f("price"))
		));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
	}
	if ($CFG["iva_used"]) {
		$t->set_var("iva", ov($qid->f("iva")));
		$t->parse("iva_used_blck", "iva_used", true);
	}
	if ($qid->f("discount") > 0) {
		$t->set_var("discount", $qid->f("discount"));
		if ($qid->f("discqty") > 1) {
			$t->set_var("discqty", $qid->f("discqty"));
			$t->parse("discqty_block_blck", "discqty_block", true);
		} else {
			$t->set_var(array(
				"fprice"     => '<strike>'.formatted_price($qid->f("price")).'</strike>',
				"discprice"        => formatted_price(round($qid->f("price") * (100 - $qid->f("discount")) / 100,2)),
			));
			/*$t->parse("discount_price_blck", "discount_price");*/
		}
		$t->parse("discount_block_blck", "discount_block", true);
	}
	if ($qid->f("nostock_flag")) {
		$t->parse("no_stock_blck", "no_stock", true);
	} else {
		$t->parse("in_stock_blck", "in_stock", true);
	}
	if ($CFG["weight_used"]) {
		$t->set_var("fweight", formatted_weight($qid->f("weight")));
		$t->parse("weight_used_blck", "weight_used", true);
	}
	if ($CFG["color_used"]) {
		$foobar = get_product_color_options($product_id, $color_options, 1);
		if ($foobar > 0) {
			$t->set_var("color_options", $color_options);
			$t->parse("color_used_blck", "color_used", true);
		}
	}
	if ($CFG["images_on_file_system"] && file_exists($CFG["productsdir"] . $qid->f("product_id") . "." . $qid->f("imagetype"))) {
		$t->set_var(array(
			"imagewidth"    => $qid->f("imagewidth"),
			"imageheight"   => $qid->f("imageheight")
		));
		if (has_priv("admin")) {
			$t->set_var("imagesrc", $CFG["wwwroot"] . "admin/getimage.php?image=" . $qid->f("product_id") . "." . $qid->f("imagetype") . "&amp;type=" . $qid->f("imagetype"));
		} else {
			$t->set_var("imagesrc", $CFG["productswww"] . $qid->f("product_id") . "." . $qid->f("imagetype"));
		}
		$t->parse("prodimage_blck", "prodimage", true);
	} else if (!$CFG["images_on_file_system"] && $qid->f("imagetype") != "") {
		$t->set_var(array(
			"imagewidth"    => $qid->f("imagewidth"),
			"imageheight"   => $qid->f("imageheight")
		));
		if (has_priv("admin")) {
			$t->set_var("imagesrc", $CFG["wwwroot"] . "admin/dbgetimage.php?id=" . $qid->f("product_id"));
		} else {
			$t->set_var("imagesrc", $CFG["wwwroot"] . "dbgetimage.php?id=" . $qid->f("product_id"));
		}
		$t->parse("prodimage_blck", "prodimage", true);
	}
    print_also_ordered(&$ao_qid, &$t, $product_id);
	$t->parse("product_blck", "product", true);
} else {
	$t->parse("noproduct_blck", "noproduct", true);
}

$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

function print_also_ordered(&$qid, &$t, $id)
{
  $sql = "SELECT count(1) AS count, oi.product_id AS id , p.name  as name FROM order_items oi, products p 
    WHERE oi.product_id = p.id AND oi.order_id IN 
	(SELECT order_id FROM order_items WHERE product_id = $id) 
	AND NOT product_id = $id GROUP BY oi.product_id, p.name ORDER BY count DESC LIMIT 5";
	$qid->query($sql);
	if ($qid->num_rows() < 1) {
		return;
	}
	while ($qid->next_record()) {
		$t->set_var(array(
			'ao_id' => $qid->f('id'),
			'ao_name' => $qid->f('name')
		));
		$t->parse('also_ordered_rows','also_ordered_row','true');
	}
	$t->parse('also_ordered_blck', 'also_ordered', true);
}

?>

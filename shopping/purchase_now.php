<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
include ($CFG["libdir"] . "pgm_orders.inc.php");
include ($CFG["libdir"] . "pgm_validate.inc.php");

$DOC_TITLE = "Checkout";

include ($CFG["dirroot"] . "header.php");

$t = new Template();
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-shopping.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");

if ($PGM_SESSION["CART"]->num_items() == 0) {	// the shopping cart is empty
	$t->set_file("page", "templates/empty_shopping_cart.ihtml");
} else {
	$t->set_var("ME", $ME);
	if ($_SERVER["REQUEST_METHOD"] == "POST") {	// form has been submitted
		$frm = $_POST;
		$errors = array();
		$errormsg = "";
		validate_customer($frm["customer"], $errors, $errormsg);
		validate_contact($frm["contact"], $errors, $errormsg);
		validate_address($frm["address"], $errors, $errormsg);
		validate_state($frm["state"], $errors, $errormsg);
		validate_zip_code($frm["zip_code"], $errors, $errormsg);
		validate_city($frm["city"], $errors, $errormsg);
		validate_country($frm["country"], $errors, $errormsg);
		validate_comments($frm["comments"], $errors, $errormsg);
		$t->set_var(array(
			"customer"	=> ov($frm["customer"]),
			"contact"	=> ov($frm["contact"]),
			"address"	=> ov($frm["address"]),
			"state"		=> ov($frm["state"]),
			"zip_code"	=> ov($frm["zip_code"]),
			"city"		=> ov($frm["city"]),
			"country"	=> print_country($frm["country"]),
			"comments"	=> ov($frm["comments"])
		));

		if ($errormsg == "") {
			save_orderinfo($_POST);
			$products_list = array();
			$products_list = $PGM_SESSION["CART"]->get_items_array();
			$items_count = count($products_list);

			$t->set_file("page", "templates/purchase_confirmation.ihtml");
			$t->set_var("wwwroot", $CFG["wwwroot"]);
			$t->set_block("page", "weight_used", "weight_used_blck");
			$t->set_var("weight_used_blck", "");
			$t->set_block("page", "weight_notused", "weight_notused_blck");
			$t->set_var("weight_notused_blck", "");
			if ($CFG["weight_used"]) {
				$t->parse("weight_used_blck", "weight_used", true);
				$t->set_var("cart_weight", formatted_weight($PGM_SESSION["CART"]->get_weight()));
			} else {
				$t->parse("weight_notused_blck", "weight_notused", true);
			}
			$t->set_block("page", "product_row", "product_rows");
				$t->set_block("product_row", "iva_used", "iva_used_blck");
				$t->set_block("product_row", "color_used", "color_used_blck");
			$t->set_var("product_rows", "");
//			while ($qid->next_record()) {
			for ($cnt=0; $cnt<$items_count; $cnt++) {
				$t->set_var("iva_used_blck", "");
				$t->set_var("color_used_blck", "");
				$t->set_var(array(
					"product_id"	=> $products_list[$cnt]["id"],
					"color_id"	=> $products_list[$cnt]["color_id"],
					"pname"		=> ($products_list[$cnt]["lname"] != "") ? ov($products_list[$cnt]["lname"]) : ov($products_list[$cnt]["name"]),
					"pcolor"	=> ($products_list[$cnt]["color_lname"] != "") ? ov($products_list[$cnt]["color_lname"]) : ov($products_list[$cnt]["color_name"]),
					"fprice"	=> formatted_price($products_list[$cnt]["price"]),
					"qty"		=> $products_list[$cnt]["qty"],
					"fsubtotal"	=> formatted_price($products_list[$cnt]["subtotal"])
				));
/*
				if ($CFG["iva_used"]) {
					$t->set_var("iva", $products_list[$cnt]["iva"]);
					$t->parse("iva_used_blck", "iva_used", true);
				}
*/
				if($products_list[$cnt]["color_id"] <> 1) {
					$t->parse("color_used_blck", "color_used", true);
				}
				$t->parse("product_rows", "product_row", true);
			}
			$t->set_block("page", "user_discount_used", "user_discount_used_blck");
			$t->set_var("user_discount_used_blck", "");
			if ($CFG["user_discount_used"] && is_logged_in() && $PGM_SESSION["user"]["user_discount"] > 0) {
				$t->set_var(array(
					"fnot_discounted_total"	=> formatted_price($PGM_SESSION["CART"]->get_total()),
					"user_discount"		=> $PGM_SESSION["user"]["user_discount"]
				));
				$t->parse("user_discount_used_blck", "user_discount_used", true);
			}
			$t->set_block("page", "delivery_used", "delivery_used_blck");
			$t->set_var("delivery_used_blck", "");
			if ($CFG["delivery_used"]) {
				$t->set_var(array(
					"ftotal"	=> formatted_price($PGM_SESSION["CART"]->get_discounted_total()),
					"fdelivery"	=> formatted_price($PGM_SESSION["CART"]->get_delivery())
				));
				$t->parse("delivery_used_blck", "delivery_used", true);
			}
			$t->set_var(array(
				"fgrandtotal"	=> formatted_price($PGM_SESSION["CART"]->get_grandtotal()),
				"address"	=> ovwbr($frm["address"]),
				"comments"	=> owbr(trim($frm["comments"]) == "" ? $t->get_var("None_Female_Singular") : $frm["comments"])
			));
		} else {
			$t->set_file("page", "templates/purchase_now.ihtml");
			$t->set_var(array(
				"err_customer"	=> errmsg2($errors["customer"]),
				"err_contact"	=> errmsg2($errors["contact"]),
				"err_address"	=> errmsg2($errors["address"]),
				"err_state"	=> errmsg2($errors["state"]),
				"err_zip_code"	=> errmsg2($errors["zip_code"]),
				"err_city"	=> errmsg2($errors["city"]),
				"err_country"	=> errmsg2($errors["country"]),
				"err_comments"	=> errmsg2($errors["comments"])
			));
		}
	} else {
		$t->set_file("page", "templates/purchase_now.ihtml");
		$qid = new PGM_Sql();
		$qid->query("
			SELECT
				 firstname
				,lastname
				,phone
				,address
				,state
				,zip_code
				,city
				,country
			FROM users
			WHERE username = '" . $PGM_SESSION["user"]["username"] . "'
		");
		$qid->next_record();
		$t->set_var(array(
			"customer"	=> ov($qid->f("firstname") . " " . $qid->f("lastname")),
			"contact"	=> ov($qid->f("phone")),
			"address"	=> ov($qid->f("address")),
			"state"		=> ov($qid->f("state")),
			"zip_code"	=> ov($qid->f("zip_code")),
			"city"		=> ov($qid->f("city")),
			"country"	=> ov($qid->f("country")),
			"comments"	=> ""
		));
		$t->set_var($qid->f("country")."_selected", "selected");
		$t->set_var($qid->f("state")."_selected", "selected");
		$t->set_var(array(
			"err_customer"	=> "",
			"err_contact"	=> "",
			"err_address"	=> "",
			"err_state"	=> "",
			"err_zip_code"	=> "",
			"err_city"	=> "",
			"err_country"	=> "",
			"err_comments"	=> "",
		));

		if ($info = load_orderinfo()) {
			$t->set_var(array(
				"customer"	=> ov($info["customer"]),
				"contact"	=> ov($info["contact"]),
				"address"	=> ov($info["address"]),
				"state"	=> ov($info["state"]),
				"zip_code"	=> ov($info["zip_code"]),
				"city"		=> ov($info["city"]),
				"country"	=> ov($info["country"]),
				"comments"	=> ov($info["comments"])
			));
		}
	}

	include ($CFG["dirroot"] . "form_header.php");
}

$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>

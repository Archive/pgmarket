<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

if ($CFG["color_used"]) {
	$qid_col = new PGM_Sql();
}

$t = new Template();
$t->set_file("page", "templates/search_results.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-shopping.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->set_var(array(
	"wwwroot"	=> $CFG["wwwroot"],
	"productswww"	=> $CFG["productswww"],
	"ME"		=> $ME
));

$summary_report = $t->get_var("Search_Results") . ": " . $result["num_items"];
$t->set_var("summary_report", $summary_report);
$t->set_block("page", "products", "products_blck");
$t->set_var("products_blck", "");
if ($qid->num_rows() > 0) {
	// Let us define blocks nested inside a product_row...
	// We indent the code to highlight the blocks' hierarchical nesting...
	$t->set_block("products", "product_row", "product_rows");
	$t->set_block("products", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("products", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$summary_report .= " " . $t->get_var("on") . " " . $result["num_pages"] . " " . $t->get_var("pages");
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	include('product_list.php');
	show_productlist(&$qid, &$t);
	/*
		$t->set_block("product_row", "brand_used", "brand_used_blck");
		$t->set_block("product_row", "adminprod", "adminprod_blck");
			$t->set_block("adminprod", "special_flag", "special_flag_blck");
		$t->set_block("product_row", "prodthumb", "prodthumb_blck");
		$t->set_block("product_row", "nothumb", "nothumb_blck");
		$t->set_block("product_row", "user_discount_used", "user_discount_used_blck");
		$t->set_block("product_row", "iva_used", "iva_used_blck");
		$t->set_block("product_row", "discount_block", "discount_block_blck");
			$t->set_block("discount_block", "discqty_block", "discqty_block_blck");
		$t->set_block("product_row", "weight_used", "weight_used_blck");
		$t->set_block("product_row", "color_used", "color_used_blck");
	// ... end of the blocks :)
	$t->set_var("product_rows", "");	// Now let us reset the product rows container...
	while ($qid->next_record()) {
	// Now let us reset the blocks contents...
	// We indent the code to highlight the blocks' hierarchical nesting...
		$t->set_var("brand_used_blck", "");
		$t->set_var("adminprod_blck", "");
			$t->set_var("special_flag_blck", "");
		$t->set_var("prodthumb_blck", "");
		$t->set_var("nothumb_blck", "");
		$t->set_var("user_discount_used_blck", "");
		$t->set_var("iva_used_blck", "");
		$t->set_var("discount_block_blck", "");
			$t->set_var("discqty_block_blck", "");
		$t->set_var("weight_used_blck", "");
		$t->set_var("color_used_blck", "");
	// ... now we have reset all nested blocks :)
		$t->set_var(array(
			"product_id"	=> $qid->f("id"),
			"product_code"	=> $qid->f("code"),
			"pname"		=> ($qid->f("lname") != "") ? ov($qid->f("lname")) : ov($qid->f("name")),
			"fprice"	=> formatted_price($qid->f("price")),
			"description"	=> ($qid->f("ldescription") != "") ? ovwbr($qid->f("ldescription")) : ovwbr($qid->f("description")),
			"productitemcount"	=> $PGM_SESSION["CART"]->get_product_qty($qid->f("id"))
		));
		if ($CFG["brand_used"] && $qid->f("bname") != "") {
			$t->set_var("bname", ov($qid->f("bname")));
			$t->parse("brand_used_blck", "brand_used", true);
		}
		if (has_priv("admin")) {
			$t->set_var("p_special_level", $qid->f("special_level"));
			if ($qid->f("special_flag")) {
				$t->parse("special_flag_blck", "special_flag", true);
			}
			$t->parse("adminprod_blck", "adminprod", true);
		}
		if ($CFG["images_on_file_system"] && file_exists($CFG["productsdir"] . $qid->f("id") . ".thumb." . $qid->f("thumbtype"))) {
			$t->set_var(array(
				"thumbwidth"	=> $qid->f("thumbwidth"),
				"thumbheight"	=> $qid->f("thumbheight")
			));
			if (has_priv("admin")) {
				$t->set_var("thumbsrc", $CFG["wwwroot"] . "admin/getimage.php?image=" . $qid->f("id") . ".thumb." . $qid->f("thumbtype") . "&amp;type=" . $qid->f("thumbtype"));
			} else {
				$t->set_var("thumbsrc", $CFG["productswww"] . $qid->f("id") . ".thumb." . $qid->f("thumbtype"));
			}
			$t->parse("prodthumb_blck", "prodthumb", true);
		} else if (!$CFG["images_on_file_system"] && $qid->f("thumbtype") != "") {
			$t->set_var(array(
				"thumbwidth"	=> $qid->f("thumbwidth"),
				"thumbheight"	=> $qid->f("thumbheight")
			));
			if (has_priv("admin")) {
				$t->set_var("thumbsrc", $CFG["wwwroot"] . "admin/dbgetthumb.php?id=" . $qid->f("id"));
			} else {
				$t->set_var("thumbsrc", $CFG["wwwroot"] . "dbgetthumb.php?id=" . $qid->f("id"));
			}
			$t->parse("prodthumb_blck", "prodthumb", true);
		} else {
			$t->parse("nothumb_blck", "nothumb", true);
		}
		if ($CFG["user_discount_used"] && is_logged_in() && $PGM_SESSION["user"]["user_discount"] > 0) {
			$t->set_var(array(
				"user_discount"		=> $PGM_SESSION["user"]["user_discount"],
				"discounted_price"	=> formatted_price((1.0-$PGM_SESSION["user"]["user_discount"]/100.0)*$qid->f("price"))
			));
			$t->parse("user_discount_used_blck", "user_discount_used", true);
		}
		if ($CFG["iva_used"]) {
			$t->set_var("iva", $qid->f("iva"));
			$t->parse("iva_used_blck", "iva_used", true);
		}
		if ($qid->f("discount") > 0) {
			$t->set_var("discount", $qid->f("discount"));
			if ($qid->f("discqty") > 1) {
				$t->set_var("discqty", $qid->f("discqty"));
				$t->parse("discqty_block_blck", "discqty_block", true);
			}
			$t->parse("discount_block_blck", "discount_block", true);
		}
		if ($CFG["weight_used"]) {
			$t->set_var("fweight", formatted_weight($qid->f("weight")));
			$t->parse("weight_used_blck", "weight_used", true);
		}
		if ($CFG["color_used"]) {
			$foobar = get_product_color_options($qid->f("id"), $color_options, 1);
			if ($foobar > 0) {
				$t->set_var("color_options", $color_options);
				$t->parse("color_used_blck", "color_used", true);
			}
		}
		$t->parse("product_rows", "product_row", true);
	}
		*/
	$t->parse("products_blck", "products", true);
}

$t->pparse("out", "page");

?>

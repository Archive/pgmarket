<?php
// navigation.php (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

$norefererflag = 1;
include ("../config.inc.php");
include ("../common.inc.php");
include ($CFG["libdir"] . $CFG["dbms"] . "_treemenu_queries.inc.php");
include ($CFG["libdir"] . "phplayersmenu/layersmenu.inc.php");
include ($CFG["libdir"] . "phplayersmenu/layersmenu-noscript.inc.php");
$t = new Template();
$t->set_file("page", "templates/navigation.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
build_catbrowser($categories, 1);
if ($categories != "") {
	$tree_mid = new XLayersMenu();
	$tree_mid->set_separator("^");
	$tree_mid->set_dirroot($CFG["dirroot"]);
	$tree_mid->set_libdir($CFG["libdir"] . "phplayersmenu/");
	$tree_mid->set_libwww($CFG["libwww"] . "phplayersmenu/");
	$tree_mid->set_imgdir($CFG["imagesdir"]);
	$tree_mid->set_imgwww($CFG["imageswww"]);
	$tree_mid->set_menu_structure_string($categories);
	$tree_mid->parse_menu_structure("navigation");
	$tree_mid->new_tree_menu("navigation");
	$t->set_var("treemenu", $tree_mid->get_tree_menu("navigation"));
} else {
	$t->set_var("treemenu", "");
}
$t->set_var("wwwroot", $CFG["wwwroot"]);
$t->pparse("out", "page");

pgm_session_close($PGM_SESSION, $session_name);

?>

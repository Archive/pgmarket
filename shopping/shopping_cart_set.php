<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ("../config.inc.php");
include ("../common.inc.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$product_id = nvl($_GET["product_id"], "");
	$color_id = nvl($_GET["color_id"], 1);
	$qty = nvl($_GET["qty"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$product_id = nvl($_POST["product_id"], "");
	$color_id = nvl($_POST["color_id"], 1);
	$qty = nvl($_POST["qty"], "");
}

$qty = intval($qty);
if ($qty > 0) { // && $qty < 1000) {
	$PGM_SESSION["CART"]->set_item(array($product_id, $color_id), $qty);
	$PGM_SESSION["CART"]->calc_grandtotal();
}

if (!empty($PGM_SESSION["REFERER_AND_QUERY"])) {
	pgm_session_close($PGM_SESSION, $session_name);
	redirect($PGM_SESSION["REFERER_AND_QUERY"]);
	die;
} else {
	pgm_session_close($PGM_SESSION, $session_name);
	redirect($CFG["firstpage"]);
	die;
}

?>

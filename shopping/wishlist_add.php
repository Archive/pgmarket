<?php
/* cart_add.php (c) 2000 Ying Zhang (ying@zippydesign.com)
/* (c) 2000-2002 Marco Pratesi <pratesi@telug.it> */

/******************************************************************************
 * MAIN
 *****************************************************************************/

include ("../config.inc.php");
include ("../common.inc.php");


if (!is_logged_in())
{ 
	$DOC_TITLE="Add to Wishlish - Needs Login";
	include ($CFG["dirroot"] . "header.php");
	$t = new Template();
	$t->set_file(array("page" => "templates/wishlist_noperms.ihtml"));
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-shopping.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("wwwroot", $CFG['wwwroot']);
	$t->set_var("productswww", $CFG['productswww']);
	$t->set_var("ME", $ME);
	if (! empty($HTTP_REFERER))
	$t->set_var('wants_url', "$HTTP_REFERER");
	else
	$t->set_var('wants_url',$CFG['firstpage']);
	$t->pparse("out", "page");
	include ($CFG["dirroot"] . "footer.php");
	
	pgm_session_close($PGM_SESSION, $session_name);
	return;
}
include ('../lib/wishlist.inc.php');

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$product_id = nvl($_GET["product_id"], "");
	$color_id = nvl($_GET["color_id"], 1);
	$qty = nvl($_GET["qty"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$product_id = nvl($_POST["product_id"], "");
	$color_id = nvl($_POST["color_id"], 1);
	$qty = nvl($_POST["qty"], "");
}

if ($qty == "") {
	$qty = 1;
} else {
	$qty = intval($qty);
}
if ($qty > 0) { // && $qty < 1000) {
	wishlist_add($product_id, 0, $qty);
}

if (!empty($PGM_SESSION["REFERER_AND_QUERY"])) {
	pgm_session_close($PGM_SESSION, $session_name);
	redirect($PGM_SESSION["REFERER_AND_QUERY"]);
	die;
} else {
	pgm_session_close($PGM_SESSION, $session_name);
	redirect($CFG["firstpage"]);
	die;
}
?>

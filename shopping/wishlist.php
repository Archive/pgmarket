<?php
/* cart_view.php (c) 2000 Ying Zhang (ying@zippydesign.com)
/* (c) 2000-2001 Marco Pratesi <pratesi@telug.it> */
/* (c) 2001 Fabio Molinari <fabio.m@mclink.it> */

/******************************************************************************
 * MAIN
 *****************************************************************************/

include ("../config.inc.php");
include ("../common.inc.php");
include ($CFG["libdir"] . "wishlist.inc.php");

$qid_user = new PGM_Sql;

$owns_wishlist = FALSE;
if (empty($username)) {
	require_login();
	$username = $PGM_SESSION['user']['username'];
	$owns_wishlist = TRUE;
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$product_id = nvl($_GET["product_id"], "");
	$color_id = nvl($_GET["color_id"], 1);
	$qty = nvl($_GET["qty"], "");
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$mode = nvl($_POST["mode"], "");
}

switch ($mode) {
	case "remove" :
		wishlist_remove($product_id, $color_id);
		break;

		case "empty" :
			wishlist_empty();
			break;

}
$DOC_TITLE = "Wishlist";
include ($CFG["dirroot"] . "header.php");

$products_list = array();

$t = new Template();
$t->set_file(array("page" => "templates/wishlist.ihtml"));
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-shopping.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->set_var("wwwroot", $CFG["wwwroot"]);
$t->set_var("ME", $ME);

$t->set_block('page', 'unfound_user', 'unfound_user_blck');
$t->set_var('unfound_user_blck', '');
$t->set_block('page', 'blocked_wishlist', 'blocked_wishlist_blck');
$t->set_var('blocked_wishlist_blck', '');
$t->set_block('page', 'found_user', 'found_user_blck');
$t->set_var('found_user_blck', '');
$t->set_block("found_user", "wishlist_empty", "wishlist_empty_blck");
$t->set_var("wishlist_empty_blck", "");
$t->set_block("found_user", "wishlist_notempty", "wishlist_notempty_blck");
$t->set_var("wishlist_notempty_blck", "");
$t->set_block('wishlist_notempty', 'can_empty', 'can_empty_blck');
$t->set_var('can_empty_blck', '');
$t->set_block('wishlist_notempty', 'can_empty2', 'can_empty_blck2');
$t->set_var('can_empty_blck2', '');

$qid_user->Query("SELECT firstname, lastname FROM users WHERE username = '$username'");
if ($qid_user->num_rows() == 0) {
	$t->parse('unfound_user_blck','unfound_user');
} else {
	$qid_user->next_record();
	// Security checks go in here
	// temp - oply myself can see the wishlist
	if (!$owns_wishlist) {
		$t->parse('blocked_wishlist_blck','blocked_wishlist');
	} else {
		$t->set_var('fullname', $qid_user->f('firstname') . ' '. $qid_user->f('lastname'));

		$products_list = wishlist_get_products_list($username);

		if (sizeof($products_list) == 0)  {
			$t->set_var('wishlist_empty', 'Your wishlist is empty');
			$t->parse("wishlist_empty_blck", "wishlist_empty", true);
		} else {
			$t->set_block("wishlist_notempty", "weight_used_th", "weight_used_th_blck");
			$t->set_var("weight_used_th_blck", "");
			if ($CFG["weight_used"]) {
				$t->parse("weight_used_th_blck", "weight_used_th", true);
			}
			$t->set_block("wishlist_notempty", "wishlist_row", "wishlist_rows");
			$t->set_block("wishlist_row", "thumb_exists", "thumb_exists_blck");
			$t->set_block("wishlist_row", "discount_block", "discount_block_blck");
			$t->set_block("wishlist_row", "discqty_block", "discqty_block_blck");
			$t->set_block("wishlist_row", "iva_used", "iva_used_blck");
			$t->set_block("wishlist_row", "weight_used", "weight_used_blck");
			$t->set_block("wishlist_row", "color_used", "color_used_blck");
			$t->set_block('wishlist_row', 'can_remove', 'can_remove_blck');
			$t->set_block('wishlist_row', 'no_stock', 'no_stock_blck');
			$t->set_block('wishlist_row', 'in_stock', 'in_stock_blck');
			$t->set_var("wishlist_rows", "");
			if ($owns_wishlist) { 
				$t->parse('can_empty_blck', 'can_empty');
				$t->parse('can_empty_blck2', 'can_empty2');
			}
			for ($cnt=0; $cnt<count($products_list); $cnt++) {
				$t->set_var("thumb_exists_blck", "");
				$t->set_var("discount_block_blck", "");
				$t->set_var("discqty_block_blck", "");
				$t->set_var("iva_used_blck", "");
				$t->set_var("weight_used_blck", "");
				$t->set_var("color_used_blck", "");
				$t->set_var('can_remove_blck','');
				$t->set_var('in_stock_blck','');
				$t->set_var('no_stock_blck','');
				$t->set_var(array(
					"product_id"	=> $products_list[$cnt]["id"],
					"color_id"	=> $products_list[$cnt]["color_id"],
					"pname"		=> ($products_list[$cnt]["lname"] != "") ? ov($products_list[$cnt]["lname"]) : ov($products_list[$cnt]["name"]),
					//"pcolor"	=> ($products_list[$cnt]["color_lname"] != "") ? ov($products_list[$cnt]["color_lname"]) : ov($products_list[$cnt]["color_name"]),
					"price"		=> $products_list[$cnt]["price"],
					"fprice"	=> formatted_price($products_list[$cnt]["price"]),
					"desired"		=> $products_list[$cnt]["desired"],
					"subtotal"	=> $products_list[$cnt]["subtotal"],
					"fsubtotal"	=> formatted_price($products_list[$cnt]["subtotal"]),
					"colspan4"	=> 4,
					"colspan5"	=> 5,
					"colspan7"	=> 7
				));
				if ($CFG["images_on_file_system"] && file_exists($CFG["productsdir"] . $products_list[$cnt]["id"] . ".thumb." . $products_list[$cnt]["thumbtype"])) {
					$t->set_var(array(
						"thumbsrc"	=> $CFG["productswww"] . $products_list[$cnt]["id"]  . ".thumb." . $products_list[$cnt]["thumbtype"],
						"thumbwidth"	=> $products_list[$cnt]["thumbwidth"],
						"thumbheight"	=> $products_list[$cnt]["thumbheight"]
					));
					$t->parse("thumb_exists_blck", "thumb_exists", true);
				} else if (!$CFG["images_on_file_system"] && $products_list[$cnt]["thumbtype"] != "") {
					$t->set_var(array(
						"thumbsrc"	=> $CFG["wwwroot"] . "dbgetthumb.php?id=" . $products_list[$cnt]["id"],
						"thumbwidth"	=> $products_list[$cnt]["thumbwidth"],
						"thumbheight"	=> $products_list[$cnt]["thumbheight"]
					));
					$t->parse("thumb_exists_blck", "thumb_exists", true);
				}
				if ($products_list[$cnt]["discount"] > 0) {
					$t->set_var("discount", $products_list[$cnt]["discount"]);
					if ($products_list[$cnt]["discqty"] > 1) {
						$t->set_var("discqty", $products_list[$cnt]["discqty"]);
						$t->parse("discqty_block_blck", "discqty_block", true);
					}
					$t->parse("discount_block_blck", "discount_block", true);
				}
				if ($CFG["iva_used"]) {
					$t->set_var("iva", $products_list[$cnt]["iva"]);
					$t->parse("iva_used_blck", "iva_used", true);
				}
				if ($CFG["weight_used"]) {
					$t->set_var(array(
						"weight"	=> $products_list[$cnt]["weight"],
						"fweight"	=> formatted_weight($products_list[$cnt]["weight"]),
						"colspan5"	=> $t->get_var(colspan5) + 1,
						"colspan7"	=> $t->get_var(colspan7) + 1
					));
					$t->parse("weight_used_blck", "weight_used", true);
				}
				if($products_list[$cnt]["color_id"] <> 0)
					$t->parse("color_used_blck", "color_used", true);
				if ($owns_wishlist) 
					$t->parse('can_remove_blck','can_remove', true);
				if ($products_list[$cnt]['nostock_flag'] ) {
					$t->parse("no_stock_blck", "no_stock", true);
				} else {
					$t->parse("in_stock_blck", "in_stock", true);
				}

				$t->parse("wishlist_rows", "wishlist_row", true);
			}	// end of while
			$t->set_block("wishlist_notempty", "weight_used_total", "weight_used_total_blck");
			$t->set_block("wishlist_notempty", "user_discount_used", "user_discount_used_blck");
			$t->set_block("wishlist_notempty", "delivery_used", "delivery_used_blck");
			$t->set_var("delivery_used_blck", "");
			$t->set_block("wishlist_notempty", "delivery_calc", "delivery_calc_blck");
			$t->set_var("delivery_calc_blck", "");
			$t->parse("wishlist_notempty_blck", "wishlist_notempty", true);
		}
		$t->parse('found_user_blck','found_user',true);
	}
}
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>

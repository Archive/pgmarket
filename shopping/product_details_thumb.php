<?php
/* product_details.php (c) 2000 Ying Zhang (ying@zippydesign.com)
/* (c) 2000-2002 Marco Pratesi <pratesi@telug.it> */

/******************************************************************************
 * MAIN
 *****************************************************************************/

include ("../application.inc.php");

$qid = new PGM_Sql;
if ($SETTING["color_used"])
	$qid_col = new PGM_Sql;

if (!empty($product_id)) {
	if (empty($id)) {	// category not specified
		$qid->Query("SELECT category_id FROM products_categories WHERE product_id = '$product_id'");
		if ($qid->num_rows()) {
			$qid->next_record();
			$id = $qid->f("category_id");
		} else {
			redirect("$CFG->firstpage");
			die;
		}
	} else {
		$qid->Query("SELECT category_id FROM products_categories WHERE product_id = '$product_id' AND category_id = '$id'");
		if (!($qid->num_rows())) {
			redirect("$CFG->firstpage");
			die;
		}
	}
} else {
	redirect("$CFG->firstpage");
	die;
}

get_product_details($qid, $product_id);

if ($qid->num_rows() > 0) {
	$qid->next_record();
	$prod = true;
} else {
	$prod = false;
}

$DOC_TITLE = $prod ? "Product_Details" : "Product_Not_Found";

include ("$CFG->dirroot" . "/header.php");

$t = new Template();
$t->set_file(array("page" => "templates/product_details_thumb.ihtml"));
include ("$CFG->localelangdir" . "/global-common.inc.php");
include ("$CFG->localelangdir" . "/global-shopping.inc.php");
$t->set_block("page", "product", "product_blck");
$t->set_var(array("product_blck" => ""));
$t->set_block("page", "noproduct", "noproduct_blck");
$t->set_var(array("noproduct_blck" => ""));
if ($prod) {
	$qid_bcp = new PGM_Sql;
	build_category_path($qid_bcp, $id, $category_path);
	$t->set_var("category_path", $category_path);

	$t->set_block("product", "brand_used", "brand_used_blck");
	$t->set_var("brand_used_blck", "");
	$t->set_block("product", "user_discount_used", "user_discount_used_blck");
	$t->set_var("user_discount_used_blck", "");
	$t->set_block("product", "iva_used", "iva_used_blck");
	$t->set_var("iva_used_blck", "");
	$t->set_block("product", "discount_block", "discount_block_blck");
	$t->set_var("discount_block_blck", "");
		$t->set_block("discount_block", "discqty_block", "discqty_block_blck");
		$t->set_var("discqty_block_blck", "");
	$t->set_block("product", "weight_used", "weight_used_blck");
	$t->set_var("weight_used_blck", "");
	$t->set_block("product", "adminprodimage", "adminprodimage_blck");
	$t->set_var("adminprodimage_blck", "");
	$t->set_block("product", "notadminprodimage", "notadminprodimage_blck");
	$t->set_var("notadminprodimage_blck", "");
	$t->set_block("product", "color_used", "color_used_blck");
	$t->set_var("color_used_blck", "");
	$appoggio = $CART->productitemcount($qid->f("product_id"));
	$appoggio = ($appoggio == "") ? 0 : $appoggio;
	$t->set_var(array(
		"category_id"		=> $id,
		"product_id"		=> $qid->f("product_id"),
		"product_code"		=> $qid->f("code"),
		"name"			=> ($qid->f("lname") != "") ? ov($qid->f("lname")) : ov($qid->f("name")),
		"price"			=> $qid->f("price"),
		"fprice"		=> formatted_price($qid->f("price")),
		"extended_description"	=> ($qid->f("lextended_description") != "") ? ovwbr($qid->f("lextended_description")) : ovwbr($qid->f("extended_description")),
		"imagetype"		=> $qid->f("imagetype"),
		"productitemcount"	=> $appoggio
	));
	if ($SETTING["brand_used"] && $qid->f("bname") != "") {
		$t->set_var("bname", ov($qid->f("bname")));
		$t->parse("brand_used_blck", "brand_used", true);
	}
	if ($SETTING["user_discount_used"] && is_logged_in() && $SESSION["user"]["user_discount"] > 0) {
		$t->set_var(array(
			"user_discount"		=> $SESSION["user"]["user_discount"],
			"discounted_price"	=> formatted_price((1.0-$SESSION["user"]["user_discount"]/100.0)*$qid->f("price"))
		));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
	}
	if ($SETTING["iva_used"]) {
		$t->set_var("iva", ov($qid->f("iva")));
		$t->parse("iva_used_blck", "iva_used", true);
	}
	if ($qid->f("discount") > 0) {
		$t->set_var("discount", $qid->f("discount"));
		if ($qid->f("discqty") > 1) {
			$t->set_var("discqty", $qid->f("discqty"));
			$t->parse("discqty_block_blck", "discqty_block", true);
		}
		$t->parse("discount_block_blck", "discount_block", true);
	}
	if ($SETTING["weight_used"]) {
		$t->set_var(array(
			"weight"	=> $qid->f("weight"),
			"fweight"	=> formatted_weight($qid->f("weight"))
		));
		$t->parse("weight_used_blck", "weight_used", true);
	}
	if (file_exists("$CFG->productsdir" . $qid->f("product_id") . ".thumb.jpg")) {
		$t->set_var(array(
			"imagefilename" => $qid->f("product_id"),
			"thumbwidth"    => $qid->f("thumbwidth"),
			"thumbheight"   => $qid->f("thumbheight")
		));
		if (has_priv("admin")) {
			$t->set_var("wwwroot", $CFG->wwwroot);
			$t->parse("adminprodimage_blck", "adminprodimage", true);
		} else {
			$t->set_var("productswww", $CFG->productswww);
			$t->parse("notadminprodimage_blck", "notadminprodimage", true);
		}
	}
	if ($SETTING["color_used"]) {
		get_product_color_options($qid_col, $product_id, $color_options, 1);
		if ($qid_col->num_rows() > 0) {
			$t->set_var("color_options", $color_options);
			$t->parse("color_used_blck", "color_used", true);
		}
	}
	$t->parse("product_blck", "product", true);
} else
	$t->parse("noproduct_blck", "noproduct", true);

$t->pparse("out", "page");

include ("$CFG->dirroot" . "/footer.php");

?>

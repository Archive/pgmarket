<?php
/* outofstock.php (c) 2002 Craig Small <csmall@small.dropbear.id.au> */

include '../application.inc.php';
$qid = new PGM_Sql;

if (!empty($product_id)) {
	if (! is_numeric($product_id)) {
		redirect("$CFG->firstpage");
	}
	if (empty($category_id) || ! is_numeric($category_id)) {	// category not specified
		$qid->Query("SELECT category_id FROM products_categories WHERE product_id = '$product_id'");
		if ($qid->num_rows()) {
			$qid->next_record();
			$category_id = $qid->f("category_id");
		} else {
			redirect("$CFG->firstpage");
			die;
		}
	} else {
		$qid->Query("SELECT category_id FROM products_categories WHERE product_id = '$product_id' AND category_id = '$category_id'");
		if (!($qid->num_rows())) {
			redirect("$CFG->firstpage");
			die;
		}
	}
} else {
	redirect("$CFG->firstpage");
	die;
}

get_product_details($qid, $product_id);

if ($qid->num_rows() > 0) {
	$qid->next_record();
	$prod = true;
} else {
	$prod = false;
}

/* form has been submitted, try to send the message */
if ($HTTP_SERVER_VARS['REQUEST_METHOD'] == 'POST' && isset($HTTP_POST_VARS)) {
	$frm = $HTTP_POST_VARS;
	$errormsg = validate_form($frm, $errors);

	if (empty($errormsg)) {
		send_form($frm,$qid);

		$DOC_TITLE = 'Request Sent';
		include ("$CFG->dirroot" . '/header.php');
		$t = new Template();
		$t->set_file(array('page' => 'templates/outofstock_success.ihtml'));
		$t->set_var('firstpage', "$CFG->firstpage");
		$t->pparse('out', 'page');
		include ("$CFG->dirroot".'/footer.php');
		die;
	}
} else if (is_logged_in()) {
	$user_qid = new PGM_Sql;
	$frm = load_user_profile($user_qid);
	$frm["name"] = $frm["firstname"] . " " . $frm["lastname"];
}

$DOC_TITLE = $prod ? "Product_Details" : "Product_Not_Found";
$DOC_TITLE = 'Out of Stock Notification';
include ("$CFG->dirroot" . "/header.php");
include ("$CFG->dirroot" . "/form_header.php");

$t = new Template();
$t->set_file(array("page" => "templates/outofstock_form.ihtml"));
include ("$CFG->localelangdir" . "/global-common.inc.php");
$t->set_var("ME", $ME);
$t->set_var("star", return_star());

$t->set_block('page', 'noimage', 'noimage_blck');
$t->set_var('noimage_blck');
$t->set_block('page', 'notadminprodimage', 'notadminprodimage_blck');
$t->set_var('notadminprodimage_blck', '');
$t->set_block('page', 'discount_price', 'discount_price_blck');
$t->set_var('discount_price_blck', '');
$t->set_block('page', 'iva_used', 'iva_used_blck');
$t->set_var('iva_used_blck', '');
$t->set_block('page', 'discount_block', 'discount_block_blck');
$t->set_var('discount_block_blck', '');
$t->set_block('discount_block', 'discqty_block', 'discqty_block_blck');
$t->set_var('discqty_block_blck', '');
$t->set_block('page', 'weight_used', 'weight_used_blck');
$t->set_var('weight_used_blck', '');

$t->set_var(array(
	"pname"			=> ($qid->f("lname") != "") ? ov($qid->f("lname")) : ov($qid->f("name")),
	"description"	=> ($qid->f("lextended_description") != "") ? ovwbr($qid->f("lextended_description")) : ovwbr($qid->f("extended_description")),
	"imagetype"		=> $qid->f("imagetype"),
	"price"			=> $qid->f("price"),
	"fprice"		=> formatted_price($qid->f("price")),
	'productswww'	=> "$CFG->productswww",
	"frm_name"		=> ov($frm["name"]),
	"errors_name"		=> errmsg($errors->name),
	"frm_phone"		=> ov($frm["phone"]),
	"errors_phone"		=> errmsg($errors->phone),
	"frm_email"		=> ov($frm["email"]),
	"errors_email"		=> errmsg($errors->email),
	"frm_message"		=> ov($frm["message"]),
	"errors_message"	=> errmsg($errors->message),
	'product_id'		=> $product_id,
	'category_id'				=> $category_id,
));
	if ($SETTING["user_discount_used"] && is_logged_in() && $SESSION["user"]["user_discount"] > 0) {
		$t->set_var(array(
			"user_discount"		=> $SESSION["user"]["user_discount"],
			"discounted_price"	=> formatted_price((1.0-$SESSION["user"]["user_discount"]/100.0)*$qid->f("price"))
		));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
	}
	if ($SETTING["iva_used"]) {
		$t->set_var("iva", ov($qid->f("iva")));
		$t->parse("iva_used_blck", "iva_used", true);
	}
	if ($qid->f("discount") > 0) {
		$t->set_var("discount", $qid->f("discount"));
		if ($qid->f("discqty") > 1) {
			$t->set_var("discqty", $qid->f("discqty"));
			$t->parse("discqty_block_blck", "discqty_block", true);
		} else {
			$t->set_var(array(
				"discprice"     => formatted_price($qid->f("price")),
				"fprice"        => formatted_price(round($qid->f("price") * (100 - $qid->f("discount")) / 100,2)),
			));
			$t->parse("discount_price_blck", "discount_price");
		}
		$t->parse("discount_block_blck", "discount_block", true);
	}
if (file_exists("$CFG->productsdir" . $qid->f("product_id") . "." . $qid->f("imagetype"))) {
	$t->set_var(array(
		"imagefilename" => $qid->f("product_id"),
		"imagewidth"    => $qid->f("imagewidth"),
		"imageheight"   => $qid->f("imageheight")
	));
	$t->parse('notadminprodimage_blck','notadminprodimage', true);
}
$t->pparse("out", "page");

include ("$CFG->dirroot" . "/footer.php");

/******************************************************************************
 * FUNCTIONS
 *****************************************************************************/

function validate_form(&$frm, &$errors) {
/* validate the contact form, and return the error messages in a string.  if
 * the string is empty, then there are no errors */

	global $CFG, $SESSION;

	$errors = new Object;
	$msg = "";

	validate_name($frm['name'], $errors, $msg);
	validate_email($frm['email'], $errors, $msg);

	return $msg;
}

function send_form(&$frm,&$qid) {
	global $CFG, $SESSION;
	global $contact, $yes;
	include ("$CFG->globalerror");

	$emailbody = "The following person has asked to be notified when this out of stock product\n"
	            ."will be next available.\n\n";
	$emailbody .= $contact["NameOrCompanyName"] . stripslashes($frm['name'])
	            . "\n\n" . "Product Name: " . ov($qid->f("name")) 
				. "\n           " . $qid->f('description')
	            . "\n\n" . $contact['Phone'] . $frm["phone"]
				. "\n" . $contact['EMail'] . $frm['email']
				. "\n" . $contact["Message"] . "\n\n" . stripslashes($frm['message']) . "\n";
	mail ( 
	    "$CFG->seller_fullname <$CFG->seller_email>",
		"Out of Stock Product",
		$emailbody,
		"From: " . $frm['email']
	);
}




?>

<?php
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");

$qid = new PGM_Sql();
$qid_zone = new PGM_Sql();

$DOC_TITLE = "DeliveryCosts_Table";
include ($CFG["dirroot"] . "header.php");

$id = nvl($PGM_SESSION["zone_id"], 1);

$qid->query("
	SELECT maxqty, cost, type
	FROM delivery_costs
	WHERE zone_id = '$id'
	ORDER BY maxqty
");

$t = new Template();
$t->set_file("page", "templates/delivery_costs_list.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->set_block("page", "dc_row", "dc_rows");
$t->set_var("dc_rows", "");
while ($qid->next_record()) {
	$t->set_var(array(
		"dc_id"		=> $id,
		"dc_fmaxqty"	=> formatted_weight($qid->f("maxqty")),
		"dc_fcost"	=> formatted_price($qid->f("cost")),
		"dc_type"	=> ($qid->f("type") == "T") ? $t->get_var("DC_Fixed") : $t->get_var("DC_Unitary")
	));
	$t->parse("dc_rows", "dc_row", true);
}
get_zone_name($qid_zone, $id);
$qid_zone->next_record();
$zone_name = ($qid_zone->f("lname") != "") ? ov($qid_zone->f("lname")) : ov($qid_zone->f("name"));
$t->set_var("zone_name", $zone_name);
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>

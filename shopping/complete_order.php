<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
include ($CFG["libdir"] . "pgm_orders.inc.php");

$paybutton = nvl($_POST["paybutton"], "");

$qid_user = new PGM_Sql;
$qid_order = new PGM_Sql;
$qid_items = new PGM_Sql;
$qid_user = new PGM_Sql;

/* this page will do the CC authentication, so we want to try to prevent
 * people from entering here casually */
$cart_is_empty = ($PGM_SESSION["CART"]->num_items() == 0);
$order = load_orderinfo();
if ($cart_is_empty || !$order) {
	pgm_session_close($PGM_SESSION, $session_name);
	redirect($CFG["firstpage"]);
	die;
}

/*

Se si tratta del pulsante del ContrAssegno, semplicemente e` gia` tutto finito
e bisogna solamente fare cio` che e` descritto nell'if sottostante;
forse si potrebbe mettere qualche controllo sul valore di ritorno di $orderid .

Se si tratta del pulsante relativo alla Carta di Credito, bisogna preparare
le variabili necessarie per preparare la form da sottomettere
al Payment Authorization Service (parte da aggiungere), che corrisponde
a un opportuno template.

*/

$t = new Template();
include ($CFG["localelangdir"] . "global-common.inc.php");
if ($paybutton == $t->get_var("Cash_On_Delivery")) {	// si tratta di un pagamento in contrassegno
	$DOC_TITLE = "Order_Completed_Successfully";
} else if ($paybutton == $t->get_var("Credit_Card")) {
	$DOC_TITLE = "submission_to_banca_sella";
}
include($CFG["dirroot"] . "header.php");

$t = new Template();
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-shopping.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");

if ($paybutton == $t->get_var("Cash_On_Delivery")) { // si tratta di un pagamento in contrassegno

	/* we will create the order in our database, then try to authorize the
 	* payment.  if all was successful, the user's order will have been
 	* completed.
 	*/
	$emailbody = "";
	$orderid = create_order_ca($order, $emailbody,$paymethod);
	send_email_order($qid_user, $orderid, 'A', $emailbody);

//	echo "<pre>" . $emailbody . "</pre>";	// for debugging...

	/* clear out the shopping cart, so the user doesn't accidentally re-submit
 	* and purchase twice!! */
	$PGM_SESSION["CART"]->init();
	$PGM_SESSION["CART"]->calc_grandtotal();
	clear_orderinfo();
	$PGM_SESSION["wantsurl"] = "";
	generate_invoice('A', $orderid, $PGM_SESSION["user"]["username"]);

} else if ($paybutton == $t->get_var("Credit_Card")) {

/////////////////////////////////////////////////////////////////////////
// A PART OF THE CODE HAS BEEN CUT AWAY BECAUSE IT STRICTLY DEPENDS
// ON THE PARTICULAR PAYMENT AUTHORIZATION SERVICE YOU DECIDE TO USE;
// HENCE, IN GENERAL, THE PART OF CODE CUT AWAY WILL NOT BE USEFUL TO YOU
/////////////////////////////////////////////////////////////////////////

	$emailbody = "";
	$orderid = create_order_cc($order, $emailbody);
	send_email_order($qid_user, $orderid, 'C', $emailbody);
	if (getenv('SERVER_NAME') == 'www.smallpiecesoftime.com.au') {
	  $result_url = 'http://www.smallpiecesoftime.com.au/eway_confirm.php';
	} else {
	  $result_url = 'http://dev.smallpiecesoftime.com.au:8101/eway_confirm.php';
	}
        $t->set_file(array("page" => "templates/creditcard.ihtml"));
	$t->set_var(array(
			"order_id"	=> $orderid,
			"f_amount"	=> formatted_price($PGM_SESSION["CART"]->get_grandtotal()),
			"cents_amount"	=> round($PGM_SESSION["CART"]->get_grandtotal(),2) * 100,
			'result_url'	=> $result_url,
			'customer' => $order["customer"],
			'address' => $order['address'].' '. $order['city'].' '. $order['state'].' '. print_country($order['country']),
			'email' => $qid_user->f('email'),
			'zip_code' => $order['zip_code']
			));
	$t->pparse("out", "page");
	// xlz - think this should be here
	$PGM_SESSION["CART"]->init();
	$PGM_SESSION["CART"]->calc_grandtotal();
	clear_orderinfo();
	$PGM_SESSION["wantsurl"] = "";
}

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);


/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

/**
* This function saves the order info into the database for a cash on delivery order.
*
* It stores an entry in the corresponding orders table, and then
* it stores the shopping cart content into the order_items table;
* finally, it prepares the body of the e-mail message to be sent
* to the market seller.
* It returns the order id.
*
* @param array $order the order informations
* @param string $emailbody the body of the e-mail message
* @return string
*/
function create_order_ca(&$order, &$emailbody,$paymethod) {
	global $_SERVER;
	global $PGM_SESSION;
	global $adminday, $adminmonth, $ordersdb;

	switch($paymethod) {
	 case 'phone':
	   $ournotes = 'Payment by Phone\n';
	   break;
	 case 'mail':
	   $ournotes = 'Payment by Mail\n';
	   break;
	 case 'fax':
	   $ournotes = 'Payment by Fax\n';
	   break;
	 case 'dd':
	   $ournotes = 'Payment via Direct Deposit\n';
	   break;
	 default:
	   $ournotes = 'Payment by $paymethod\n';
	   break;
	}

	/* build the custinfo string */
	$custinfo =
		  $ordersdb["Customer"] . " " . $order["customer"] . "\n"
		. $ordersdb["Phone"]    . " " . $order["contact"] . "\n"
		. $ordersdb["Address"]  . "\n" . $order["address"] . "\n"
		. $order["city"] . " " . strtoupper($order["state"]) . " " . $order["zip_code"] . "\n"
		. print_country($order["country"]) . "\n";

	$emailbody = $order['address'] . "\n". $order['city']." "
			. strtoupper($order['state']) ." ". $order['zip_code']."\n"
			. print_country($order['country']) . "\n\n"
			. "and your order is for the following:\n\n";
			

	/* save order information first */

//	$Date = $myday[date("w")] . date(" j ") . $mymonth[date("n")] . date(" Y H:i:s");
	$Dateusec = gettimeofday();
	$unixtime = $Dateusec["sec"];
	$year = date("Y");
	$month = date("n");
	$day = date("j");
	$weekday = date("w");
	$FromIP = $_SERVER["REMOTE_ADDR"];
	$Dateusec = gettimeofday();

	$grandtotal = $PGM_SESSION["CART"]->get_grandtotal();
	$delivery = $PGM_SESSION["CART"]->get_delivery();
	
	$qid = new PGM_Sql();
	$qid->begin();

	$qid->query("
		INSERT INTO ordersca (
			username
			,unixtime
			,year
			,month
			,day
			,weekday
			,fromip
			,custinfo
			,notes
			,amount
			,user_discount_id
			,user_discount
			,delivery
			,state_id
			,ournotes
		) VALUES (
			'" . $PGM_SESSION["user"]["username"] . "'
			,'$unixtime'
			,'$year'
			,'$month'
			,'$day'
			,'$weekday'
			,'$FromIP'
			,'$custinfo'
			,'" . $order["comments"] . "'
			,'$grandtotal'
			,'" . $PGM_SESSION["user"]["user_discount_id"] . "'
			,'" . $PGM_SESSION["user"]["user_discount"] . "'
			,'$delivery'
			,'0'
			,'$ournotes'
		)
	");

	$seqqid = new PGM_Sql;
	$seqqid->Query("SELECT currval('orders_id_seq')");
	$idrow = $seqqid->next_record();
	$orderid = $seqqid->f('currval');

	$order_comments = stripslashes ($order['comments']);

	/* now add the shopping cart items into the order_items table */
	$products_list = array();
	$products_list = $PGM_SESSION["CART"]->get_items_array();
	for ($cnt=0; $cnt<count($products_list); $cnt++) {
	 	$color = $products_list[$cnt]["color_name"];
		if ($products_list[$cnt]["color_lname"] != "") {
			$color .= " (" . $products_list[$cnt]["color_lname"] . ")";
		}


		$product_id = $products_list[$cnt]["id"];
		$color_id = addslashes($products_list[$cnt]["color_id"]);
		$name = addslashes($products_list[$cnt]["name"]);
		$price = $products_list[$cnt]["price"];
		$discount = $products_list[$cnt]["discount"];
		$discqty = $products_list[$cnt]["discqty"];
		$iva = $products_list[$cnt]["iva"];
		$weight = $products_list[$cnt]["weight"];
		$qty = $products_list[$cnt]["qty"];
		$qid->query("
			INSERT INTO order_items (
				 order_id
				,product_id
				,color_id
				,name
				,color
				,price
				,discount
				,discqty
				,iva
				,weight
				,qty
			) VALUES (
				 '$orderid'
				,'$product_id'
				,'$color_id'
				,'$name'
				,'$color'
				,'$price'
				,'$discount'
				,'$discqty'
				,'$iva'
				,'$weight'
				,'$qty'
			)
		");

		$productname = stripslashes($products_list[$cnt]['name']);
			$productname .= " - $color ";

		$prodprice = $products_list[$cnt]["price"] * (100 - $products_list[$cnt]["discount"] ) / 100;
		$emailbody .= sprintf("%3d %-40s %8s ea %8s\n",
			$products_list[$cnt]['qty'],
			$productname,
			formatted_price_text($prodprice),
			formatted_price_text($prodprice *
				$products_list[$cnt]['qty']));
	}

	$qid->commit();


	$emailbody .= "\n";
	$emailbody .= sprintf("    %-40s             %8s\n", 
	     $ordersdb["Delivery"]. ':',  formatted_price_text ($delivery));
	$emailbody .= sprintf("    %-40s             %8s\n", 
	     $ordersdb["TOTAL"] . ':', formatted_price_text ($grandtotal));

	return $orderid;
}

/**
* This function concerns credit card orders; it is analogous to the corresponding function used for cash on delivery orders.
*/
function create_order_cc(&$order, &$emailbody) {
	global $_SERVER;
	global $PGM_SESSION;
	global $ordersdb;

	$passwordric = '';
	/* build the custinfo string */
	$custinfo =
		  $ordersdb["Customer"] . " " . $order["customer"] . "\n"
		. $ordersdb["Phone"]    . " " . $order["contact"] . "\n"
		. $ordersdb["Address"]  . "\n" . $order["address"] . "\n"
		. $order["city"] . " " . strtoupper($order["state"]) . " " . $order["zip_code"] . "\n"
		. print_country($order["country"]) . "\n";

	$emailbody = $order['address'] . "\n". $order['city']." "
			. strtoupper($order['state']) ." ". $order['zip_code']."\n"
			. print_country($order['country']) . "\n\n"
			. "and your order is for the following:\n\n";
	/* save order information first */

//	$Date = $myday[date("w")] . date(" j ") . $mymonth[date("n")] . date(" Y H:i:s");
	$Dateusec = gettimeofday();
	$unixtime = $Dateusec["sec"];
	$year = date("Y");
	$month = date("n");
	$day = date("j");
	$weekday = date("w");
	$FromIP = $_SERVER["REMOTE_ADDR"];
	$Dateusec = gettimeofday();

	$grandtotal = $PGM_SESSION["CART"]->get_grandtotal();
	$delivery = $PGM_SESSION["CART"]->get_delivery();

	$qid = new PGM_Sql();
	$qid->begin();

	$qid->query("
		INSERT INTO orderscc (
			username
			,unixtime
			,year
			,month
			,day
			,weekday
			,fromip
			,custinfo
			,notes
			,amount
			,user_discount_id
			,user_discount
			,delivery
			,state_id
			,otpric
		) VALUES (
			'" . $PGM_SESSION["user"]["username"] . "'
			,'$unixtime'
			,'$year'
			,'$month'
			,'$day'
			,'$weekday'
			,'$FromIP'
			,'$custinfo'
			,'" . $order["comments"] . "'
			,'$grandtotal'
			,'" . $PGM_SESSION["user"]["user_discount_id"] . "'
			,'" . $PGM_SESSION["user"]["user_discount"] . "'
			,'$delivery'
			,'0'
			,'$passwordric'
		)
	");

  
	$seqqid = new PGM_Sql;
	$seqqid->Query("SELECT currval('orders_id_seq')");
	$idrow = $seqqid->next_record();
	$orderid = $seqqid->f('currval');

	/* now add the shopping cart items into the order_items table */
	$products_list = array();
	$products_list = $PGM_SESSION["CART"]->get_items_array();
	for ($cnt=0; $cnt<count($products_list); $cnt++) {
		$color = $products_list[$cnt]["color_name"];
		if ($products_list[$cnt]["color_lname"] != "") {
			$color .= " (" . $products_list[$cnt]["color_lname"] . ")";
		}

		$product_id = $products_list["$cnt"]["id"];
		$color_id = addslashes($products_list[$cnt]["color_id"]);
		$name = addslashes($products_list[$cnt]["name"]);
		$price = $products_list[$cnt]["price"];
		$discount = $products_list[$cnt]["discount"];
		$discqty = $products_list[$cnt]["discqty"];
		$iva = $products_list[$cnt]["iva"];
		$weight = $products_list[$cnt]["weight"];
		$qty = $products_list[$cnt]["qty"];
		$qid->query("
			INSERT INTO order_items (
				 order_id
				,product_id
				,color_id
				,name
				,color
				,price
				,discount
				,discqty
				,iva
				,weight
				,qty
			) VALUES (
				 '$orderid'
				,'$product_id'
				,'$color_id'
				,'$name'
				,'$color'
				,'$price'
				,'$discount'
				,'$discqty'
				,'$iva'
				,'$weight'
				,'$qty'
			)
		");
		$productname = stripslashes($products_list[$cnt]['name']);
		if ($color != "") 
			$productname .= " - $color ";

		$prodprice = $products_list[$cnt]["price"] * (100 - $products_list[$cnt]["discount"] ) / 100;
		$emailbody .= sprintf("%3d %-40s %8s ea %8s\n",
			$products_list[$cnt]['qty'],
			$productname,
			formatted_price_text($prodprice),
			formatted_price_text($prodprice *
				$products_list[$cnt]['qty']));
	}

	$qid->commit();

	$emailbody .= "\n";
	$emailbody .= sprintf("    %-40s             %8s\n", 
	     $ordersdb['Delivery']. ':',  formatted_price_text ($delivery));
	$emailbody .= sprintf("    %-40s             %8s\n", 
	     $ordersdb['TOTAL'] . ':', formatted_price_text ($grandtotal));

	return $orderid;
}


function send_email_order(&$qid_user, $orderid, $type, $emaildetails)
{
	global $PGM_SESSION, $CFG;

	$qid_user->Query("
	SELECT firstname,lastname,email FROM users WHERE username = '".$PGM_SESSION["user"]["username"]."'");
	$qid_user->next_record();

	$var = array();
	$var['orderid'] = $orderid;
	$var['fullname'] = $qid_user->f("firstname") . " " . $qid_user->f("lastname");
	$var['support'] = $CFG['support'];
	$var['myname'] = $CFG['myname'];
	$var['emaildetails'] = $emaildetails;
	if ($type = 'A') {
		$var['order_type_name'] = 'other methods';
		$var['link_url'] = 'http://www.smallpiecesoftime.com.au/users/ordersca.php';
	} else {
		$var['order_type_name'] = 'credit cards';
		$var['link_url'] = 'http://www.smallpiecesoftime.com.au/users/orderscc.php';
	}

$emailbody = "
Dear $var[fullname],

Thankyou for recently placing an order with Small Pieces of Time.  Your order
number is $var[orderid], which you can use to refer to this order in future.

Your order will be delivered to the following address:

$var[emaildetails]

Most orders are dispached within 5 working days of payment.

You can look at the current status of all your orders payed by 
$var[order_type_name] by going to this link 
$var[link_url]

Thankyou for choosing Small Pieces of Time for your scrapbooking needs.

Kellie Jean Small

Small Pieces of Time Pty. Ltd. ABN 20 077 233 366
http://www.smallpiecesoftime.com.au/   kellie@smallpiecesoftime.com.au
PO Box 434 Narellan NSW 2567 AUSTRALIA
Telephone/Fax: (02) 4648 4899
";
	$appoggio = $qid_user->f("email");
	$headers = "From: ".$CFG['seller_fullname'] . ' <'.$CFG['seller_email'] . ">\n";
	$headers .= "Errors-To: ". $CFG['support'] ."\n";
	$headers .= "CC: ". $CFG['seller_fullname'] ." <". $CFG['seller_email'] .">\n";
	mail(
		"$var->fullname <$appoggio>",
		"Order Number: $orderid",
		$emailbody,
		$headers
	);

}
?>

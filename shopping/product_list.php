<?php
function show_productlist($qid_p,&$t)
{
	global $PGM_SESSION, $CFG;
	include ($CFG['localelangdir'] . "/global-common.inc.php");
	include ($CFG['localelangdir'] . "/global-shopping.inc.php");
	include ($CFG['localelangdir'] . "/global-admin.inc.php");
	$t->set_file(array("productlist" => "templates/product_list.ihtml"));
	$t->set_block("productlist", "products_array", "products_array_blck");
		$t->set_var("products_array_blck", "");
	$t->set_block("productlist", "no_products", "no_products_blck");
	$t->set_var("no_products_blck", "");
		$t->set_block("products_array", "products_row", "products_rows");
		$t->set_block("products_row", "product_cell", "product_cells");
		$t->set_block("products_row", "no_product_cell", "no_product_cells");
		$t->set_block("product_cell", "brand_used", "brand_used_blck");
		$t->set_block("product_cell", "adminprod", "adminprod_blck");
		$t->set_block("product_cell", "prodthumb", "prodthumb_blck");
		$t->set_block("product_cell", "nothumb", "nothumb_blck");
		$t->set_block("adminprod", "special_flag", "special_flag_blck");
		$t->set_block("adminprod", "new_flag", "new_flag_blck");
		$t->set_block("adminprod", "nostock_flag", "nostock_flag_blck");
		$t->set_block("product_cell", "notadminprod", "notadminprod_blck");
		$t->set_block("product_cell", "brand_used", "brand_used_blck");
		$t->set_block("product_cell", "iva_used", "iva_used_blck");
		$t->set_block("product_cell", "user_discount_used", "user_discount_used_blck");
		$t->set_block("product_cell", "in_stock", "in_stock_blck");
		$t->set_block("product_cell", "no_stock", "no_stock_blck");
		$t->set_block("product_cell", "discount_block", "discount_block_blck");
			$t->set_block("discount_block", "discqty_block", "discqty_block_blck");
		$t->set_block("product_cell", "weight_used", "weight_used_blck");
		$t->set_block("in_stock", "color_used", "color_used_blck");
	$t->set_var("products_cells", "");
	$t->set_var("products_rows", "");
	if ($qid_p->num_rows() == 0) {
		$t->parse("no_products_blck", "no_products", true);
	} else {
	$i = 0;
	while ($qid_p->next_record()) {
// Now let us reset the blocks contents...
// We indent the code to highlight the blocks' hierarchical nesting...
               $t->set_var("products_cells", "");
	       $t->set_var("no_product_cells", "");
		$t->set_var("brand_used_blck", "");
		$t->set_var("adminprod_blck", "");
		$t->set_var("special_flag_blck", "");
		$t->set_var("new_flag_blck", "");
		$t->set_var("prodthumb_blck", "");
		$t->set_var("nothumb_blck", "");
		$t->set_var("user_discount_used_blck", "");
		$t->set_var("nostock_flag_blck", "");
		$t->set_var("iva_used_blck", "");
		$t->set_var("in_stock_blck", "");
		$t->set_var("no_stock_blck", "");
		$t->set_var("discount_block_blck", "");
			$t->set_var("discqty_block_blck", "");
		$t->set_var("weight_used_blck", "");
		$t->set_var("color_used_blck", "");
// ... now we have reset all nested blocks :)
		$t->set_var(array(
			"id"		=> $qid_p->f('category_id'),
			"product_id"	=> $qid_p->f("id"),
			"product_code"	=> $qid_p->f("code"),
			"pname"		=> ($qid_p->f("lname") != "") ? ov($qid_p->f("lname")) : ov($qid_p->f("name")),
			"fprice"	=> formatted_price($qid_p->f("price")),
			"description"	=> ($qid_p->f("ldescription") != "") ? ovwbr($qid_p->f("ldescription")) : ovwbr($qid_p->f("description")),
			"productitemcount"	=> $PGM_SESSION["CART"]->get_product_qty($qid_p->f("id"))
		));
		if (has_priv("admin")) {
			$t->set_var("p_special_level", $qid_p->f("special_level"));
			if ($qid_p->f("special_flag")) {
				$t->parse("special_flag_blck", "special_flag", true);
			}
			$t->set_var("p_new_level", $qid_p->f("new_level"));
			if ($qid_p->f("new_flag")) {
				$t->parse("new_flag_blck", "new_flag", true);
			}
			if ($qid_p->f("nostock_flag")) {
				$t->parse("nostock_flag_blck", "nostock_flag", true);
			}
			$t->parse("adminprod_blck", "adminprod", true);
		}
		if ($CFG["images_on_file_system"] && file_exists($CFG["productsdir"] . $qid_p->f("id") . ".thumb." . $qid_p->f("thumbtype"))) {
			$t->set_var(array(
				"thumbwidth"	=> $qid_p->f("thumbwidth"),
				"thumbheight"	=> $qid_p->f("thumbheight")
			));
			if (has_priv("admin")) {
				$t->set_var("thumbsrc", $CFG["wwwroot"] . "admin/getimage.php?image=" . $qid_p->f("id") . ".thumb." . $qid_p->f("thumbtype") . "&amp;type=" . $qid_p->f("thumbtype"));
			} else {
				$t->set_var("thumbsrc", $CFG["productswww"] . $qid_p->f("id") . ".thumb." . $qid_p->f("thumbtype"));
			}
			$t->parse("prodthumb_blck", "prodthumb", true);
		} else if (!$CFG["images_on_file_system"] && $qid_p->f("thumbtype") != "") {
			$t->set_var(array(
				"thumbwidth"	=> $qid_p->f("thumbwidth"),
				"thumbheight"	=> $qid_p->f("thumbheight")
			));
			if (has_priv("admin")) {
				$t->set_var("thumbsrc", $CFG["wwwroot"] . "admin/dbgetthumb.php?id=" . $qid_p->f("id"));
			} else {
				$t->set_var("thumbsrc", $CFG["wwwroot"] . "dbgetthumb.php?id=" . $qid_p->f("id"));
			}
			$t->parse("prodthumb_blck", "prodthumb", true);
		} else {
			$t->parse("nothumb_blck", "nothumb", true);
		}
		if ($CFG["brand_used"] && $qid_p->f("bname") != "") {
			$t->set_var("bname", ov($qid_p->f("bname")));
			$t->parse("brand_used_blck", "brand_used", true);
		}
		if ($CFG["user_discount_used"] && is_logged_in() && $PGM_SESSION["user"]["user_discount"] > 0) {
			$t->set_var(array(
				"user_discount"		=> $PGM_SESSION["user"]["user_discount"],
				"discounted_price"	=> formatted_price((1.0-$PGM_SESSION["user"]["user_discount"]/100.0)*$qid_p->f("price"))
			));
			$t->parse("user_discount_used_blck", "user_discount_used", true);
		}
		if ($CFG["iva_used"]) {
			$t->set_var("iva", $qid_p->f("iva"));
			$t->parse("iva_used_blck", "iva_used", true);
		}
		if ($qid_p->f("discount") > 0) {
			$t->set_var("discount", $qid_p->f("discount"));
			if ($qid_p->f("discqty") > 1) {
				$t->set_var("discqty", $qid_p->f("discqty"));
				$t->parse("discqty_block_blck", "discqty_block", true);
			}
			$t->parse("discount_block_blck", "discount_block", true);
		}
		if ($qid_p->f("nostock_flag")) {
			$t->parse("no_stock_blck", "no_stock", true);
		} else {
			$t->parse("in_stock_blck", "in_stock", true);
		}
		if ($CFG["weight_used"]) {
			$t->set_var("fweight", formatted_weight($qid_p->f("weight")));
			$t->parse("weight_used_blck", "weight_used", true);
		}
		if ($CFG["color_used"]) {
			$foobar = get_product_color_options($qid_p->f("id"), $color_options, 1);
			if ($foobar > 0) {
				$t->set_var("color_options", $color_options);
				$t->parse("color_used_blck", "color_used", true);
			}
		}
		if ($i % 2 == 0) {
		  $t->parse("product_cells", "product_cell", false);
		} else {
		  $t->parse("product_cells", "product_cell", true);
		}
		if ($i % 2 == 1) {
		$t->parse("products_rows", "products_row", true); 
		  $t->set_var("products_cells", "");
		}
		$i++;
	}
	// Complete row if we are in the middle of one
	if ($i % 2 == 1) {
	  $t->parse("products_rows", "products_row", true);
	}
	$t->parse("productlist", "products_array", true);
	}


	//$t->pparse("out", "productlist");
}
?>

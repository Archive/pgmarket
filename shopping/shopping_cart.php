<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$product_id = nvl($_GET["product_id"], "");
	$color_id = nvl($_GET["color_id"], 1);
	$qty = nvl($_GET["qty"], "");
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$mode = nvl($_POST["mode"], "");
}

switch ($mode) {
	case "remove" :
		$PGM_SESSION["CART"]->delete_items(array($product_id, $color_id));
		$PGM_SESSION["CART"]->calc_grandtotal();
		break;

	case "empty" :
		$PGM_SESSION["CART"]->init();
		$PGM_SESSION["CART"]->calc_grandtotal();
		break;

	case "recalc" :
		$cart_count = count($_POST["product_id"]);
		for ($i=0; $i<$cart_count; $i++) {
			$PGM_SESSION["CART"]->set_item(array($_POST["product_id"][$i], $_POST["color_id"][$i]), $_POST["qty"][$i]);
		}
		$PGM_SESSION["CART"]->calc_grandtotal();
		break;
}

$DOC_TITLE = "Shopping_Cart";
include ($CFG["dirroot"] . "header.php");

$products_list = array();
$products_list = $PGM_SESSION["CART"]->get_items_array();
$items_count = count($products_list);

$t = new Template();
$t->set_file("page", "templates/shopping_cart.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-shopping.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->set_var("wwwroot", $CFG["wwwroot"]);
$t->set_var("ME", $ME);

$t->set_block("page", "shopping_cart_empty", "shopping_cart_empty_blck");
$t->set_var("shopping_cart_empty_blck", "");
$t->set_block("page", "shopping_cart_notempty", "shopping_cart_notempty_blck");
$t->set_var("shopping_cart_notempty_blck", "");
if ($items_count == 40) {
	$t->parse("shopping_cart_empty_blck", "shopping_cart_empty", true);
} else {
	$t->set_block("shopping_cart_notempty", "weight_used_th", "weight_used_th_blck");
	$t->set_var("weight_used_th_blck", "");
	if ($CFG["weight_used"]) {
		$t->parse("weight_used_th_blck", "weight_used_th", true);
	}
	$t->set_block("shopping_cart_notempty", "cart_row", "cart_rows");
		$t->set_block("cart_row", "thumb_exists", "thumb_exists_blck");
		$t->set_block("cart_row", "discount_block", "discount_block_blck");
		$t->set_block("cart_row", "discqty_block", "discqty_block_blck");
		$t->set_block("cart_row", "iva_used", "iva_used_blck");
		$t->set_block("cart_row", "weight_used", "weight_used_blck");
		$t->set_block("cart_row", "color_used", "color_used_blck");
	$t->set_var("cart_rows", "");
	for ($cnt=0; $cnt<$items_count; $cnt++) {
		$t->set_var("thumb_exists_blck", "");
		$t->set_var("discount_block_blck", "");
		$t->set_var("discqty_block_blck", "");
		$t->set_var("iva_used_blck", "");
		$t->set_var("weight_used_blck", "");
		$t->set_var("color_used_blck", "");
		$t->set_var(array(
			"product_id"	=> $products_list[$cnt]["id"],
			"color_id"	=> $products_list[$cnt]["color_id"],
			"pname"		=> ($products_list[$cnt]["lname"] != "") ? ov($products_list[$cnt]["lname"]) : ov($products_list[$cnt]["name"]),
			"pcolor"	=> ($products_list[$cnt]["color_lname"] != "") ? ov($products_list[$cnt]["color_lname"]) : ov($products_list[$cnt]["color_name"]),
			"fprice"	=> formatted_price($products_list[$cnt]["price"]),
			"qty"		=> $products_list[$cnt]["qty"],
			"fsubtotal"	=> formatted_price($products_list[$cnt]["subtotal"]),
			"colspan4"	=> 4,
			"colspan5"	=> 5,
			"colspan7"	=> 7
		));
		if ($CFG["images_on_file_system"] && file_exists($CFG["productsdir"] . $products_list[$cnt]["id"] . ".thumb." . $products_list[$cnt]["thumbtype"])) {
			$t->set_var(array(
				"thumbsrc"	=> $CFG["productswww"] . $products_list[$cnt]["id"]  . ".thumb." . $products_list[$cnt]["thumbtype"],
				"thumbwidth"	=> $products_list[$cnt]["thumbwidth"],
				"thumbheight"	=> $products_list[$cnt]["thumbheight"]
			));
			$t->parse("thumb_exists_blck", "thumb_exists", true);
		} else if (!$CFG["images_on_file_system"] && $products_list[$cnt]["thumbtype"] != "") {
			$t->set_var(array(
				"thumbsrc"	=> $CFG["wwwroot"] . "dbgetthumb.php?id=" . $products_list[$cnt]["id"],
				"thumbwidth"	=> $products_list[$cnt]["thumbwidth"],
				"thumbheight"	=> $products_list[$cnt]["thumbheight"]
			));
			$t->parse("thumb_exists_blck", "thumb_exists", true);
		}
		if ($products_list[$cnt]["discount"] > 0) {
			$t->set_var("discount", $products_list[$cnt]["discount"]);
			if ($products_list[$cnt]["discqty"] > 1) {
				$t->set_var("discqty", $products_list[$cnt]["discqty"]);
				$t->parse("discqty_block_blck", "discqty_block", true);
			}
			$t->parse("discount_block_blck", "discount_block", true);
		}
		if ($CFG["iva_used"]) {
			$t->set_var("iva", $products_list[$cnt]["iva"]);
			$t->parse("iva_used_blck", "iva_used", true);
		}
		if ($CFG["weight_used"]) {
			$t->set_var(array(
				"fweight"	=> formatted_weight($products_list[$cnt]["weight"]),
				"colspan5"	=> $t->get_var("colspan5") + 1,
				"colspan7"	=> $t->get_var("colspan7") + 1
			));
			$t->parse("weight_used_blck", "weight_used", true);
		}
		if($products_list[$cnt]["color_id"] <> 1) {
			$t->parse("color_used_blck", "color_used", true);
		}
		$t->parse("cart_rows", "cart_row", true);
	}	// end of while
	$t->set_block("shopping_cart_notempty", "weight_used_total", "weight_used_total_blck");
	$t->set_var("weight_used_total_blck", "");
	if ($CFG["weight_used"]) {
		$t->set_var("cart_fweight", formatted_weight($PGM_SESSION["CART"]->get_weight()));
		$t->parse("weight_used_total_blck", "weight_used_total", true);
	}
	$t->set_block("shopping_cart_notempty", "user_discount_used", "user_discount_used_blck");
	$t->set_var("user_discount_used_blck", "");
	if ($CFG["user_discount_used"] && is_logged_in() && $PGM_SESSION["user"]["user_discount"] > 0) {
		$t->set_var(array(
			"fnot_discounted_total"	=> formatted_price($PGM_SESSION["CART"]->get_total()),
			"user_discount"		=> $PGM_SESSION["user"]["user_discount"]
		));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
	}
	$t->set_block("shopping_cart_notempty", "delivery_used", "delivery_used_blck");
	$t->set_var("delivery_used_blck", "");
	$t->set_block("shopping_cart_notempty", "delivery_calc", "delivery_calc_blck");
	$t->set_var("delivery_calc_blck", "");
	if ($CFG["delivery_used"]) {
		$t->set_var(array(
			"cart_fweight"	=> formatted_weight($PGM_SESSION["CART"]->get_weight()),
			"ftotal"	=> formatted_price($PGM_SESSION["CART"]->get_discounted_total()),
			"fdelivery"	=> formatted_price($PGM_SESSION["CART"]->get_delivery())
		));
		if (is_logged_in() || $CFG["only_one_delivery_zone"]) {
			$t->parse("delivery_used_blck", "delivery_used", true);
		} else {
			$t->parse("delivery_calc_blck", "delivery_calc", true);
		}
	}
	$t->set_var("fgrandtotal", formatted_price($PGM_SESSION["CART"]->get_grandtotal()));
	$t->parse("shopping_cart_notempty_blck", "shopping_cart_notempty", true);
}
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>

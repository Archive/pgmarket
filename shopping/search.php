<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");
include ($CFG["libdir"] . $CFG["dbms"] . "_search_queries.inc.php");

$PGM_SESSION["stringtsf"] = nvl($_GET["stringtsf"], "");
$PGM_SESSION["concatenation"] = nvl($_GET["concatenation"], "");
$PGM_SESSION["case_sensitive"] = nvl($_GET["case_sensitive"], "");
if (!isset($_GET["page_number"])) {
	$page_number = 1;
} else {
	$page_number = max(1, abs(intval($_GET["page_number"])));
}

$foobar = ereg_replace ("\+", "", $PGM_SESSION["stringtsf"]);
if (empty($foobar)) {
	pgm_session_close($PGM_SESSION, $session_name);
	redirect($CFG["firstpage"]);
	die;
}

$qid = new PGM_Sql();
find_products($qid, $PGM_SESSION, $page_number, $result);

$DOC_TITLE = "Search_Results";
$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
include ($CFG["dirroot"] . "header.php");
if ($result["num_items"] > $PGM_SESSION["max_results"]) {
	$t = new Template();
	$t->set_file("page", "templates/" . $PGM_SESSION["lang"] . "/too_many_results.ihtml");
	$t->set_var("numresults", $result["num_items"]);
	$t->pparse("out", "page");
} else {
	include ("search_results.inc.php");
}

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>

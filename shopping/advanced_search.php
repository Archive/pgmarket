<?php
// (C) 2000 Fabio Molinari <fabio.m@mclink.it>
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");

$DOC_TITLE = "AdvancedSearch";
include ($CFG["dirroot"] . "header.php");
include ($CFG["libdir"] . $CFG["dbms"] . "_search_queries.inc.php");

if (!isset($_GET["from_menu"])) {
/* Form has been submitted, let's go searching. */

	if (!isset($_GET["page_number"])) {
		$page_number = 1;

		if (cvl($_GET["product_name_min"])
		 && cvl($_GET["product_name_max"])
		 && cvl($_GET["product_name_in"])
		 && cvl($_GET["brand_id"], 1)
		 && cvl($_GET["description_min"])
		 && cvl($_GET["description_max"])
		 && cvl($_GET["name_in"])
		 && cvl($_GET["price_min"])
		 && cvl($_GET["price_max"])
		 && cvl($_GET["discount_min"])
		 && cvl($_GET["discount_max"])
		 && cvl($_GET["discqty_min"])
		 && cvl($_GET["discqty_max"])
		 && cvl($_GET["weight_min"])
		 && cvl($_GET["weight_max"])
		) {
			$errors["emptyform"] = true;
		} else {
			$PGM_SESSION["product_name_min"]	= nvl($_GET["product_name_min"], "");
			$PGM_SESSION["product_name_max"]	= nvl($_GET["product_name_max"], "");
			$PGM_SESSION["product_name_in"]	= nvl($_GET["product_name_in"], "");
			$PGM_SESSION["brand_id"]		= nvl($_GET["brand_id"], "");
			$PGM_SESSION["prod_concatenation"]	= nvl($_GET["prod_concatenation"], "");
			$PGM_SESSION["prod_case_sensitive"]	= nvl($_GET["prod_case_sensitive"], 0);
			$PGM_SESSION["description_min"]	= nvl($_GET["description_min"], "");
			$PGM_SESSION["description_max"]	= nvl($_GET["description_max"], "");
			$PGM_SESSION["name_in"]		= nvl($_GET["name_in"], "");
			$PGM_SESSION["desc_concatenation"]	= nvl($_GET["desc_concatenation"], "");
			$PGM_SESSION["desc_case_sensitive"]	= nvl($_GET["desc_case_sensitive"], 0);
			$PGM_SESSION["price_min"]		= nvl($_GET["price_min"], "");
			$PGM_SESSION["price_max"]		= nvl($_GET["price_max"], "");
			$PGM_SESSION["discount_min"]	= nvl($_GET["discount_min"], "");
			$PGM_SESSION["discount_max"]	= nvl($_GET["discount_max"], "");
			$PGM_SESSION["discqty_min"]		= nvl($_GET["discqty_min"], "");
			$PGM_SESSION["discqty_max"]		= nvl($_GET["discqty_max"], "");
			$PGM_SESSION["weight_min"]		= nvl($_GET["weight_min"], "");
			$PGM_SESSION["weight_max"]		= nvl($_GET["weight_max"], "");
			$errormsg = validate_advanced_search_form($PGM_SESSION, $errors);
		}
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	if (nvl($errormsg, "") == "" && !nvl($errors["emptyform"], false)) {
		$qid = new PGM_Sql();
		advanced_find_products($qid, $PGM_SESSION, $page_number, $result);
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		if ($result["num_items"] > $PGM_SESSION["max_results"]) {
			$t = new Template();
			$t->set_file("page", "templates/" . $PGM_SESSION["lang"] . "/too_many_results.ihtml");
			$t->set_var("numresults", $result["num_items"]);
			$t->pparse("out", "page");
		} else {
			include ("search_results.inc.php");
		}
		include ($CFG["dirroot"] . "footer.php");
		pgm_session_close($PGM_SESSION, $session_name);
		die;
	}
}

$t = new Template();
$t->set_file("page", "templates/advanced_search_form.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-shopping.inc.php");
//$t->set_var("wwwroot", $CFG["wwwroot"]);
//$t->set_var("productswww", $CFG["productswww"]);
$t->set_var("ME", $ME);
if (empty($errors)) {
	$t->set_var("there_are_errors", "");
}
$t->set_var(array(
	"session_product_name_in"	=> ov(nvl($PGM_SESSION["product_name_in"], "")),
	"session_prod_concatenation_or"	=> nvl($PGM_SESSION["prod_concatenation"], "") == "AND" ? "" : "checked",
	"session_prod_concatenation_and"=> nvl($PGM_SESSION["prod_concatenation"], "") == "AND" ? "checked" : "",
	"session_prod_case_sensitive"	=> nvl($PGM_SESSION["prod_case_sensitive"], 0) ? "checked" : "",
	"session_name_in"		=> ov(nvl($PGM_SESSION["name_in"], "")),
	"session_desc_concatenation_or"	=> nvl($PGM_SESSION["desc_concatenation"], "") == "AND" ? "" : "checked",
	"session_desc_concatenation_and"=> nvl($PGM_SESSION["desc_concatenation"], "") == "AND" ? "checked" : "",
	"session_desc_case_sensitive"	=> nvl($PGM_SESSION["desc_case_sensitive"], 0) ? "checked" : "",
	"session_price_min"		=> ov(nvl($PGM_SESSION["price_min"], "")),
	"errors_price_min"		=> errmsg(nvl($errors["price_min"], "")),
	"session_price_max"		=> ov(nvl($PGM_SESSION["price_max"], "")),
	"errors_price_max"		=> errmsg(nvl($errors["price_max"], "")),
	"session_discount_min"		=> ov(nvl($PGM_SESSION["discount_min"], "")),
	"errors_discount_min"		=> errmsg(nvl($errors["discount_min"], "")),
	"session_discount_max"		=> ov(nvl($PGM_SESSION["discount_max"], "")),
	"errors_discount_max"		=> errmsg(nvl($errors["discount_max"], "")),
	"session_discqty_min"		=> ov(nvl($PGM_SESSION["discqty_min"], "")),
	"errors_discqty_min"		=> errmsg(nvl($errors["discqty_min"], "")),
	"session_discqty_max"		=> ov(nvl($PGM_SESSION["discqty_max"], "")),
	"errors_discqty_max"		=> errmsg(nvl($errors["discqty_max"], ""))
));
$t->set_block("page", "brand_used", "brand_used_blck");
$t->set_var("brand_used_blck", "");
if ($CFG["brand_used"]) {
	$t->set_block("brand_used", "brand_row", "brand_rows");
	$t->set_var("brand_rows", "");
	$qid = new PGM_Sql();
	$qid->query("SELECT id, name FROM brands ORDER BY name");
	$PGM_SESSION["brands"] = array();
	$PGM_SESSION["brand_id"] = nvl($PGM_SESSION["brand_id"], 1);
	while ($qid->next_record()) {
		$t->set_var(array(
			"brand_id"	=> $qid->f("id"),
			"session_brand_selected" => ($PGM_SESSION["brand_id"] == $qid->f("id")) ? "selected" : "",
			"brand_name"	=> $qid->f("name")
		));
		$t->parse("brand_rows", "brand_row", true);
	}
	$t->parse("brand_used_blck", "brand_used", true);
}
$t->set_block("page", "weight_used", "weight_used_blck");
$t->set_var("weight_used_blck", "");
if ($CFG["weight_used"]) {
	$t->set_var(array(
		"session_weight_min"	=> ov(nvl($PGM_SESSION["weight_min"], "")),
		"errors_weight_min"	=> errmsg(nvl($errors["weight_min"], "")),
		"session_weight_max"	=> ov(nvl($PGM_SESSION["weight_max"], "")),
		"errors_weight_max"	=> errmsg(nvl($errors["weight_max"], ""))
	));
	$t->parse("weight_used_blck", "weight_used", true);
}
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function validate_advanced_search_form(&$frm, &$errors) {
	global $CFG;
	include ($CFG["libdir"] . "pgm_validate.inc.php");

	$errors = array();
	$errormsg = "";

/*
	if(!empty($frm["product_name_min"])) {
		validate_product_name($frm["product_name_min"], $errors, $errormsg);
		if ($errors["product_name"]) $errors["product_name_min"] = true;
	}
	if(!empty($frm["product_name_max"])) {
		validate_product_name($frm["product_name_max"], $errors, $errormsg);
		if ($errors["product_name"]) $errors["product_name_max"] = true;
	}
	if(!empty($frm["description_min"])) {
		validate_product_description($frm["description_min"], $errors, $errormsg);
		if ($errors["product_description"]) $errors["description_min"] = true;
	}
	if(!empty($frm["description_max"])) {
		validate_product_description($frm["description_max"], $errors, $errormsg);
		if ($errors["product_description"]) $errors["description_max"] = true;
	}
*/
	if(!empty($frm["price_min"])) {
		validate_price($frm["price_min"], $errors, $errormsg);
		if (nvl($errors["price"], false)) $errors["price_min"] = true;
	}
	if(!empty($frm["price_max"])) {
		validate_price($frm["price_max"], $errors, $errormsg);
		if (nvl($errors["price"], false)) $errors["price_max"] = true;
	}
	if(!empty($frm["discount_min"])) {
		validate_searched_discount($frm["discount_min"], $errors, $errormsg);
		if (nvl($errors["discount"], false)) $errors["discount_min"] = true;
	}
	if(!empty($frm["discount_max"])) {
		validate_searched_discount($frm["discount_max"], $errors, $errormsg);
		if (nvl($errors["discount"], false)) $errors["discount_max"] = true;
	}
	if(!empty($frm["discqty_min"])) {
		validate_searched_discqty($frm["discqty_min"], $errors, $errormsg);
		if (nvl($errors["discqty"], false)) $errors["discqty_min"] = true;
	}
	if(!empty($frm["discqty_max"])) {
		validate_searched_discqty($frm["discqty_max"], $errors, $errormsg);
		if (nvl($errors["discqty"], false)) $errors["discqty_max"] = true;
	}
	if(!empty($frm["weight_min"])) {
		validate_weight($frm["weight_min"], $errors, $errormsg);
		if (nvl($errors["weight"], false)) $errors["weight_min"] = true;
	}
	if(!empty($frm["weight_max"])) {
		validate_weight($frm["weight_max"], $errors, $errormsg);
		if (nvl($errors["weight"], false)) $errors["weight_max"] = true;
	}

	return $errormsg;
}

?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

//$REFERER = get_referer();
//$REFERER_AND_QUERY = get_full_referer();
$REFERER = $PGM_SESSION["REFERER"];
$REFERER_AND_QUERY = $PGM_SESSION["REFERER_AND_QUERY"];
if (strpos($REFERER_AND_QUERY, "from_menu=1") === false && strpos($REFERER_AND_QUERY, "search") === false) {
	if ($REFERER == $REFERER_AND_QUERY) {
		$REFERER_AND_QUERY = $REFERER . "?from_menu=1";
	} else {
		$REFERER_AND_QUERY = $REFERER_AND_QUERY . "&amp;from_menu=1";
	}
}

?>

<?php

/**
* Prepares an import form to allow to add/update tuples using data read from a text file to be uploaded
* @return void
*/
function print_import_form($what="update_imp") {
	global $CFG, $ME;

	$frm["submit_caption"] = "Import_Button_Label";
	$frm["newmode"] = $what;

	$t = new Template();
	$t->set_file("page", $CFG["dirroot"] . "admin/templates/data_import_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("ME", $ME);
	$t->set_var(array(
		"frm_title"		=> nvl($t->get_var($frm["newmode"]), ""),
		"frm_newmode"		=> $frm["newmode"],
		"max_file_size_import"	=> $CFG["max_file_size_import"],
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_data_import_end_template() {
	global $PGM_SESSION;
	global $CFG, $ME;

	include ($CFG["dirroot"] . "form_header.php");
	$t = new Template();
	$t->set_file("page", $CFG["dirroot"] . "admin/templates/data_import_end.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("ME", $ME);
	$t->set_var("backurl", $PGM_SESSION["goback"]["request_uri"]);
	$t->pparse("out", "page");
}

function import_orders_states($submitted=true) {
	global $_FILES;
	global $CFG;

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$import_file = $CFG["productsdir"] . "orders_states.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// ... and let's see if the state already exists ...
		$qid->query("SELECT id FROM orders_states WHERE name = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's create it ...
			$qid->query("
				INSERT INTO orders_states (name, description)
				VALUES ('$fldarr[0]', '$fldarr[1]')
			");
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$sid = $qid->f("id");
			// ... and update ...
			$qid->query("
				UPDATE orders_states SET description = '$fldarr[1]'
				WHERE id = '$sid'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();

	// The End
	print_data_import_end_template();
}

function import_categories($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["libdir"] . $CFG["dbms"] . "_layersmenu_queries.inc.php");
	include ($CFG["globalerror"]);

	// Let's open a transaction
	$qid = new PGM_Sql();
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "categories.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// Empty parent ==> "Top"
		if (strlen(trim($fldarr[0])) == 0) {
			$fldarr[0] = "Top";
		}
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the parent category exists ...
		$qid->query("SELECT id FROM categories WHERE name = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's return an error ...
			$foobar = $line_num + 1;
			$errormsg .= "<li> $fldarr[1] - $file_line " . $foobar . $Err_ImpCat . " - $fldarr[0]'";
			$fldarr[0] = 1;
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$fldarr[0] = $qid->f("id");
		}
		// ... let's see if the category already exists ...
		$qid->query("SELECT id FROM categories WHERE name = '$fldarr[1]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's create it ...
			$qid->query("
				INSERT INTO categories (parent_id, name, special_level, description)
				VALUES ('$fldarr[0]', '$fldarr[1]', '$fldarr[2]', '$fldarr[3]')
			");
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$cid = $qid->f("id");
			// ... and update ...
			$qid->query("
				UPDATE categories SET
					 parent_id = '$fldarr[0]'
					,special_level = '$fldarr[2]'
					,description = '$fldarr[3]'
				WHERE id = '$cid'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_categories_i18n($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["libdir"] . $CFG["dbms"] . "_layersmenu_queries.inc.php");
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	//* Let's open a transaction
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "categories_i18n.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// Empty parent ==> "Top"
		if (strlen(trim($fldarr[0])) == 0) {
			$fldarr[0] = "Top";
		}
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the category exists ...
		$qid->query("SELECT id FROM categories WHERE name = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's return an error ...
			$foobar = $line_num + 1;
			$errormsg .= "<li> $fldarr[0] - $file_line " . $foobar . $Err_ImpCat_i18n;
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$cid = $qid->f("id");

			$qid->query("
				DELETE FROM categories_i18n
				WHERE category_id = '$cid' AND lang = '$fldarr[1]'
			");
			$qid->query("
				INSERT INTO categories_i18n (category_id, lang, name)
				VALUES ('$cid', '$fldarr[1]', '$fldarr[2]')
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_brands($submitted=true) {
	global $_FILES;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();

	$import_file = $CFG["productsdir"] . "brands.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// ... and let's see if the brand already exists ...
		$qid->query("SELECT id FROM brands WHERE name = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			/* ... if it doesn't exist let's create it ... */
			$qid->query("
				INSERT INTO brands (name, description)
				VALUES ('$fldarr[0]', '$fldarr[1]')
			");
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$bid = $qid->f("id");
			// ... and update ...
			$qid->query("
				UPDATE brands SET description = '$fldarr[1]'
				WHERE id = '$bid'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_iva($submitted=true) {
	global $_FILES;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();

	$import_file = $CFG["productsdir"] . "iva.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the percentage already exists ...
		$qid->query("SELECT id FROM iva WHERE iva = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's create it
			$qid->query("
				INSERT INTO iva (iva, description)
				VALUES ('$fldarr[0]', '$fldarr[1]')
			");
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$iid = $qid->f("id");
			// ... and update ...
			$qid->query("
				UPDATE iva SET description = '$fldarr[1]'
				WHERE id = '$iid'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_colors($submitted=true) {
	global $_FILES;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();

	$import_file = $CFG["productsdir"] . "colors.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// ... and let's see if the color already exists ...
		$qid->query("SELECT id FROM colors WHERE name = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's create it ...
			$qid->query("
				INSERT INTO colors (name, description)
				VALUES ('$fldarr[0]', '$fldarr[1]')
			");
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$sid = $qid->f("id");
			// ... and update ...
			$qid->query("
				UPDATE colors SET description = '$fldarr[1]'
				WHERE id = '$sid'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();

	// The End
	print_data_import_end_template();
}

function import_colors_i18n($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "colors_i18n.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the color exists ...
		$qid->query("SELECT id FROM colors WHERE name = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's return an error ...
			$foobar = $line_num + 1;
			$errormsg .= "<li> $fldarr[0] - $file_line " . $foobar . $Err_ImpColor_i18n;
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$cid = $qid->f("id");

			$qid->query("
				DELETE FROM colors_i18n
				WHERE color_id = '$cid' AND lang = '$fldarr[1]'
			");
			$qid->query("
				INSERT INTO colors_i18n (color_id, lang, name)
				VALUES ('$cid', '$fldarr[1]', '$fldarr[2]')
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

/**
* Imports products using data provided in an uploaded text file
* @param boolean $submitted it is true if file to be imported has been
*   uploaded via HTTP, it is false if file has been uploaded another way,
*   e.g. via FTP
* @return void
*/
function import_products($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "products.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
//		$fldarr = split("\",\"",ereg_replace("^\"|\"..$|\".$","",$line));
		$fldarr = split("\t",ereg_replace("\n$","",$line));
//echo "<b>" . $line_num . "</b>:" . $line . "<br>";

		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		if($CFG["brand_used"]) {
			if ($fldarr[2] == "") {
				// brand not defined, set the brand_id to 1
				$fldarr[2] = 1;
			} else {
				// Let's see if the brand exists ...
				$qid->query("SELECT id FROM brands WHERE name = '$fldarr[2]'");
				if ($qid->num_rows() == 0) {
					/* ... if it doesn't exist let's return an error ... */
					$foobar = $line_num + 1;
					$errormsg .= "<li> '$fldarr[2]' - $file_line " . $foobar . $Err_ImpBrand;
					$fldarr[2] = 0;
				} else {
					// ... otherwise let's substitute it with its id
					$qid->next_record();
					$fldarr[2] = $qid->f("id");
				}
			}
		} else {
			$fldarr[2] = 1;
		}
		if($CFG["iva_used"]) {
			if ($fldarr[4] == "") {
				// iva not defined, set it to 0 (iva_id = 1)
				$fldarr[4] = 1;
			} else {
				// Let's see if the iva exists ...
				$qid->query("SELECT id FROM iva WHERE iva = '$fldarr[4]'");
				if ($qid->num_rows() == 0) {
					// ... if it doesn't exist let's return an error ...
					$foobar = $line_num + 1;
					$errormsg .= "<li> '$fldarr[4]' - $file_line " . $foobar . $Err_ImpIVA;
					$fldarr[4] = 0;
				} else {
					// ... otherwise let's substitute it with its id
					$qid->next_record();
					$fldarr[4] = $qid->f("id");
				}
			}
		} else {
			$fldarr[4] = 1;
		}

		if ($fldarr[11] == "") {
			// if not specified, the extended_description is assumed identical to the description
			$fldarr[11] = $fldarr[10];
		}

		// ... and let's see if the product already exists ...
		$qid->query("SELECT id FROM products WHERE code = '$fldarr[0]' ORDER BY id DESC");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist, let's create it in the products table ...
			$qid->query("
				INSERT INTO products (
					 code
					,name
					,brand_id
					,price
					,iva_id
					,discount
					,discqty
					,weight
					,special_flag
					,special_level
					,description
					,extended_description
				)
				VALUES (
					 '$fldarr[0]'
					,'$fldarr[1]'
					,'$fldarr[2]'
					,'$fldarr[3]'
					,'$fldarr[4]'
					,'$fldarr[5]'
					,'$fldarr[6]'
					,'$fldarr[7]'
					,'$fldarr[8]'
					,'$fldarr[9]'
					,'$fldarr[10]'
					,'$fldarr[11]'
				)
			");
		} else {
			// ... otherwise let's update it in the products table ...
			$qid->next_record();
			$updated_product_id = $qid->f("id");
			$qid->query("
				UPDATE products SET
					 code = '$fldarr[0]'
					,name = '$fldarr[1]'
					,brand_id = '$fldarr[2]'
					,price = '$fldarr[3]'
					,iva_id = '$fldarr[4]'
					,discount = '$fldarr[5]'
					,discqty = '$fldarr[6]'
					,weight = '$fldarr[7]'
					,special_flag = '$fldarr[8]'
					,special_level = '$fldarr[9]'
					,description = '$fldarr[10]'
					,extended_description = '$fldarr[11]'
				WHERE id = '$updated_product_id'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_products_i18n($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "products_i18n.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// Empty parent ==> "Top"
		if (strlen(trim($fldarr[0])) == 0) {
			$fldarr[0] = "Top";
		}
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the product exists ...
		$qid->query("SELECT id FROM products WHERE name = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's return an error ...
			$foobar = $line_num + 1;
			$errormsg .= "<li> '$fldarr[0]' - $file_line " . $foobar . $Err_ImpProd_i18n;
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$pid = $qid->f("id");

			$qid->query("
				DELETE FROM products_i18n
				WHERE product_id = '$pid' AND lang = '$fldarr[1]'
			");
			$qid->query("
				INSERT INTO products_i18n (
					 product_id
					,lang
					,name
					,description
					,extended_description
				)
				VALUES (
					 '$pid'
					,'$fldarr[1]'
					,'$fldarr[2]'
					,'$fldarr[3]'
					,'$fldarr[4]'
				)
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_products_categories($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "products_categories.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the product exists ...
		$qid->query("SELECT id FROM products WHERE code = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's return an error ...
			$foobar = $line_num + 1;
			$errormsg .= "<li>" . $foobar . $Err_ImpProd . $fldarr[0] . "'.";
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$pid = $qid->f("id");

			// ... let's see if the category exists ...
			$qid->query("SELECT id FROM categories WHERE name = '$fldarr[1]'");

			if ($qid->num_rows() == 0) {
				// ... if it doesn't exist let's return an error ...
				$foobar = $line_num + 1;
				$errormsg .= "<li>" . $foobar . $Err_ImpCat . $fldarr[1] . "'.";
			} else {
				// ... otherwise let's save its ID ...
				$qid->next_record();
				$cid = $qid->f("id");

				// ... let's see if the association already exists ...
				$qid->query("SELECT category_id FROM products_categories WHERE product_id = '$pid' AND category_id = '$cid'");
				if ($qid->num_rows() == 0) {
					// ... if it doesn't exist let's create it ...
					$qid->query("
						INSERT INTO products_categories (product_id, category_id)
						VALUES ('$pid', '$cid')
					");
				}
			}
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_products_colors($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "products_colors.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the product exists ...
		$qid->query("SELECT id FROM products WHERE code = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's return an error ...
			$foobar = $line_num + 1;
			$errormsg .= "<li>" . $foobar . $Err_ImpProd . $fldarr[0] . "'.";
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$pid = $qid->f("id");

			// ... let's see if the color exists ...
			$qid->query("SELECT id FROM colors WHERE name = '$fldarr[1]'");

			if ($qid->num_rows() == 0) {
				// ... if it doesn't exist let's return an error ...
				$foobar = $line_num + 1;
				$errormsg .= "<li>" . $foobar . $Err_ImpColor . $fldarr[0] . "'.";
			} else {
				// ... otherwise let's save its ID ...
				$qid->next_record();
				$cid = $qid->f("id");

				// ... let's see if the association already exists ...
				$qid->query("SELECT color_id FROM products_colors WHERE product_id = '$pid' AND color_id = '$cid'");
				if ($qid->num_rows() == 0) {
					// ... if it doesn't exist let's create it ...
					$qid->query("
						INSERT INTO products_colors (product_id, color_id)
						VALUES ('$pid', '$cid')
					");
				}
			}
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_user_discounts($submitted=true) {
	global $_FILES;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();

	$import_file = $CFG["productsdir"] . "user_discounts.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the percentage already exists ...
		$qid->query("SELECT id FROM user_discounts WHERE discount = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's create it
			$qid->query("
				INSERT INTO user_discounts (discount, name, description)
				VALUES ('$fldarr[0]', '$fldarr[1]', '$fldarr[2]')
			");
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$did = $qid->f("id");
			// ... and update ...
			$qid->query("
				UPDATE user_discounts SET
					 name = '$fldarr[1]'
					,description = '$fldarr[2]'
				WHERE id = '$did'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_zones($submitted=true) {
	global $_FILES;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();

	$import_file = $CFG["productsdir"] . "zones.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// ... and let's see if the zone already exists ...
		$qid->query("SELECT id FROM zones WHERE code = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's create it ...
			$qid->query("
				INSERT INTO zones (code, name, description)
				VALUES ('$fldarr[0]', '$fldarr[1]', '$fldarr[2]')
			");
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$sid = $qid->f("id");
			// ... and update ...
			$qid->query("
				UPDATE zones SET
					 code = '$fldarr[0]'
					,name = '$fldarr[1]'
					,description = '$fldarr[2]'
				WHERE id = '$sid'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_zones_i18n($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "zones_i18n.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if the zone exists ...
		$qid->query("SELECT id FROM zones WHERE code = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's return an error ...
			$foobar = $line_num + 1;
			$errormsg .= "<li> '$fldarr[0]' - $file_line " . $foobar . $Err_ImpZone;
		} else {
			// ... otherwise let's save its ID ...
			$qid->next_record();
			$zid = $qid->f("id");

			$qid->query("
				DELETE FROM zones_i18n
				WHERE zone_id = '$zid' AND lang = '$fldarr[1]'
			");
			$qid->query("
				INSERT INTO zones_i18n (zone_id, lang, name)
				VALUES ('$zid', '$fldarr[1]', '$fldarr[2]')
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_delivery_costs($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "delivery_costs.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		// Let's see if a zone with this code exists ...
		$qid->query("SELECT id FROM zones WHERE code = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's warn ...
			$foobar = $line_num + 1;
			$errormsg .= "<li> '$fldarr[0]' - $file_line " . $foobar . $Err_ImpZone;
		} else {
			// ... if it exists let's save the id ...
			$qid->next_record();
			$fldarr[0] = $qid->f("id"); // zone_id
			// ... let's see if the delivery cost already exists ...
			$qid->query("
				SELECT zone_id, maxqty
				FROM delivery_costs
				WHERE zone_id = '$fldarr[0]' AND maxqty = '$fldarr[1]'
			");
			if ($qid->num_rows() == 0) {
				// ... if it doesn't exist let's create it ...
				$qid->query("
					INSERT INTO delivery_costs (
						 zone_id
						,maxqty
						,cost
						,type
					) VALUES (
						 '$fldarr[0]'
						,'$fldarr[1]'
						,'$fldarr[2]'
						,'$fldarr[3]')
					");
			} else {
				// ... otherwise let's save its Key ...
				$qid->next_record();
				$k1 = $qid->f("zone_id");
				$k2 = $qid->f("maxqty");
				// ... and update ...
				$qid->query("
					UPDATE delivery_costs SET
						 cost = '$fldarr[2]'
						,type = '$fldarr[3]'
					WHERE zone_id = '$k1' AND maxqty = '$k2'
				");
			}
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

function import_users($submitted=true) {
	global $_FILES;
	global $CFG;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$errormsg = "";

	$import_file = $CFG["productsdir"] . "users.txt";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		// dangerous characters quoting
		for ($i=0; $i<count($fldarr); $i++) {
			$fldarr[$i] = addslashes($fldarr[$i]);
		}

		if($CFG["user_discount_used"]) {
			if (strlen(trim($fldarr[4])) == 0) {
				// user discount not defined, set the user_discount_id to 1
				$fldarr[4] = 1;
			} else {
				// Let's see if the user discount exists ...
				$qid->query("SELECT id FROM user_discounts WHERE discount = '$fldarr[4]'");
				if ($qid->num_rows() == 0) {
					/* ... if it doesn't exist let's return an error ... */
					$foobar = $line_num + 1;
					$errormsg .= "<li> $fldarr[0] - $file_line " . $foobar . $Err_ImpUserDiscount . " - '$fldarr[4]'";
					$fldarr[4] = 0;
				} else {
					// ... otherwise let's substitute it with its id
					$qid->next_record();
					$fldarr[4] = $qid->f("id");
				}
			}
		} else $fldarr[4] = 0;

		if (strlen(trim($fldarr[5])) == 0) {
			// delivery zone not defined, set the zone_id to 1 (default delivery zone)
			$fldarr[5] = 1;
		} else {
			// let's check if the zone exists...
			$qid->query("SELECT id FROM zones WHERE code = '$fldarr[5]'");
			if ($qid->num_rows() == 0) {
				// If not let's warn the admin ...
				$foobar = $line_num + 1;
				$errormsg .= "<li> $fldarr[0] - $file_line " . $foobar . " - '" . $fldarr[5] ."'" . $Err_ImpZone;
				$fldarr[5] = 1;
			} else {
				$qid->next_record();
				$fldarr[5] = $qid->f("id");
			}
		}

		// Let's see if the user already exists ...
		$qid->query("SELECT username FROM users WHERE username = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			// ... if it doesn't exist let's create it in the users table ...
			$qid->query("
				INSERT INTO users (
					 username
					,password
					,usertype
					,priv
					,user_discount_id
					,zone_id
					,firstname
					,lastname
					,fiscalcode
					,address
					,number
					,zip_code
					,city
					,country
					,phone
					,fax
					,mobilephone
					,email
					,authdata
					,acceptadvert
					,notes
					,ournotes
					,unixtime
					,year
					,month
					,day
					,weekday
					,fromip
				)
				VALUES (
					'$fldarr[0]'
					,'" . md5($fldarr[1]) ."'
					,'$fldarr[2]'
					,'$fldarr[3]'
					,'$fldarr[4]'
					,'$fldarr[5]'
					,'$fldarr[6]'
					,'$fldarr[7]'
					,'$fldarr[8]'
					,'$fldarr[9]'
					,'$fldarr[10]'
					,'$fldarr[11]'
					,'$fldarr[12]'
					,'$fldarr[13]'
					,'$fldarr[14]'
					,'$fldarr[15]'
					,'$fldarr[16]'
					,'$fldarr[17]'
					,'$fldarr[18]'
					,'$fldarr[19]'
					,'$fldarr[20]'
					,'$fldarr[21]'
					,'$fldarr[22]'
					,'$fldarr[23]'
					,'$fldarr[24]'
					,'$fldarr[25]'
					,'$fldarr[26]'
					,'$fldarr[27]'
				)
			");
		} else {
			// ... otherwise let's update it in the products table ...
			$qid->query("
				UPDATE users SET
					usertype = '$fldarr[2]'
					,priv = '$fldarr[3]'
					,user_discount_id = '$fldarr[4]'
					,zone_id = '$fldarr[5]'
					,firstname = '$fldarr[6]'
					,lastname = '$fldarr[7]'
					,fiscalcode = '$fldarr[8]'
					,address = '$fldarr[9]'
					,number = '$fldarr[10]'
					,zip_code = '$fldarr[13]'
					,city = '$fldarr[11]'
					,country = '$fldarr[12]'
					,phone = '$fldarr[14]'
					,fax = '$fldarr[15]'
					,mobilephone = '$fldarr[16]'
					,email = '$fldarr[17]'
					,authdata = '$fldarr[18]'
					,acceptadvert = '$fldarr[19]'
					,notes = '$fldarr[20]'
					,ournotes = '$fldarr[21]'
					,unixtime = '$fldarr[22]'
					,year = '$fldarr[23]'
					,month = '$fldarr[24]'
					,day = '$fldarr[25]'
					,weekday = '$fldarr[26]'
					,fromip = '$fldarr[27]'
				WHERE username = '$fldarr[0]'
			");
		}
	}
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	print_data_import_end_template();
}

/**
* Imports SQL DELETE and INSERT commands dumped through the suited script; this allows to synchronize data (for involved tables) with another PgMarket installation
* @return void
*/
function import_tables_data($submitted=true) {
	global $_FILES;
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();
	$limit_clause = prepare_limit_clause(1,0);
	$qid->query("SELECT id FROM products ORDER BY id DESC $limit_clause");
	$qid->next_record();
	$max_product_id = $qid->f("id");
	$import_file = $CFG["productsdir"] . "mydata.sql";
	if ($submitted) {
		if (!move_uploaded_file($_FILES["userfile"]["tmp_name"], $import_file)) {
			error($import_file_upload_failed);
		}
	}
	$fcontents = file ($import_file);
	// Traverse the file
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$line = ereg_replace(chr(10), "", $line);
		if ($line != "") {
			$qid->query($line);
		}
	}
	update_sequences();	// needed for PostgreSQL
	$qid->query("SELECT id FROM products ORDER BY id ASC");
	$products_ids = array();
	while ($qid->next_record()) {
		$products_ids[] = $qid->f("id");
	}
	if (count($products_ids) > 0 && $products_ids[count($products_ids)-1] > $max_product_id) {
		$max_product_id = $products_ids[count($products_ids)-1];
	}
	$qid->commit();
	ignore_user_abort(1);
	if ($submitted) {
/*
	if ($CFG["unix"] {
*/
		// Note: This function may not work on Windows systems.
		unlink ($import_file);
/*
	} else {
		system ($CFG["rmf"] . " " . $import_file, $result);
	}
*/
	}
	for ($i=1; $i<=$max_product_id; $i++) {
		if (!in_array($i, $products_ids)) {
/*
			if ($CFG["unix"]) {
*/
				if (file_exists($CFG["productsdir"] . $i . ".gif"))
					unlink ($CFG["productsdir"] . $i . ".gif");
				if (file_exists($CFG["productsdir"] . $i . ".jpg"))
					unlink ($CFG["productsdir"] . $i . ".jpg");
				if (file_exists($CFG["productsdir"] . $i . ".png"))
					unlink ($CFG["productsdir"] . $i . ".png");
				if (file_exists($CFG["productsdir"] . $i . ".thumb.gif"))
					unlink ($CFG["productsdir"] . $i . ".thumb.gif");
				if (file_exists($CFG["productsdir"] . $i . ".thumb.jpg"))
					unlink ($CFG["productsdir"] . $i . ".thumb.jpg");
				if (file_exists($CFG["productsdir"] . $i . ".thumb.png"))
					unlink ($CFG["productsdir"] . $i . ".thumb.png");
/*
			} else {
				system ($CFG["rmf"] . " " .  $CFG["productsdir"] . $i . ".*", $result);
			}
*/
		}
	}
	ignore_user_abort(0);
	
	// The End
	print_data_import_end_template();
	if ($PGM_SESSION["frames_used"]) { ?>
	<script language="JavaScript" type="text/javascript">
	<!--
	parent.categories.location.reload();
	// -->
	</script>
	<?php }
}

?>

<?php

$DBMS_FALSE = "FALSE";
$DBMS_TRUE = "TRUE";

function prepare_limit_clause($limit, $offset) {
	if ($limit == 0) {
		$limit = 1;	// workaround needed for PostgreSQL 7.0
	}
	return "LIMIT " . $limit . ", " . $offset;
}

?>

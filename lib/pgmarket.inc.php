<?php
/* mymarket.php (c) 2000 Ying Zhang (ying@zippydesign.com)
/* (c) 2000-2002 Marco Pratesi <pratesi@telug.it> */
/* (c) 2001 Fabio Molinari <fabio.m@mclink.it> */

function is_logged_in() {
/* this function will return true if the user has logged in.  a user is logged
 * in if the $SESSION["user"] is set (by the login.php page) and also if the
 * remote IP address matches what we saved in the session ($SESSION["ip"])
 * from login.php -- this is not a robust or secure check by any means, but it
 * will do for now */

	global $SESSION, $REMOTE_ADDR;
	return isset($SESSION)
		&& isset($SESSION["user"]);
// commented to avoid problems with proxies
//		&& isset($SESSION["ip"])
//		&& $SESSION["ip"] == $REMOTE_ADDR;
}

function load_user_profile(&$qid) {
/* load up the user's details */

	global $SESSION;
	
	$username = $SESSION["user"]["username"];
//	$qid = new PGM_Sql;
	$qid->Query("
		SELECT firstname, lastname, usertype, priv, zone_id,
			fiscalcode, address, state, zip_code, city, country,
			phone, fax, mobilephone, email, authdata, acceptadvert, notes
		FROM users
		WHERE username = '$username'
	");
	$qid->next_record();
	$arr = array();
	$arr["firstname"] = $qid->f("firstname");
	$arr["lastname"] = $qid->f("lastname");
	$arr["usertype"] = $qid->f("usertype");
	$arr["priv"] = $qid->f("priv");
	$arr["zone_id"] = $qid->f("zone_id");
	$arr["fiscalcode"] = $qid->f("fiscalcode");
	$arr["address"] = $qid->f("address");
	$arr["state"] = $qid->f("state");
	$arr["zip_code"] = $qid->f("zip_code");
	$arr["city"] = $qid->f("city");
	$arr["country"] = $qid->f("country");
	$arr["phone"] = $qid->f("phone");
	$arr["fax"] = $qid->f("fax");
	$arr["mobilephone"] = $qid->f("mobilephone");
	$arr["email"] = $qid->f("email");
	$arr["authdata"] = $qid->f("authdata");
	$arr["acceptadvert"] = $qid->f("acceptadvert");
	$arr["notes"] = $qid->f("notes");

	return $arr;
}

function require_login() {
/* this function checks to see if the user is logged in.  if not, it will show
 * the login screen before allowing the user to continue */

	global $CFG, $SESSION;
	if (! is_logged_in()) {
		$SESSION["wantsurl"] = qualified_me();
		redirect("$CFG->wwwroot" . "/login.php");
	}
}

function require_priv($priv) {
/* this function checks to see if the user has the privilege $priv.  if not,
 * it will display an Insufficient Privileges page and stop */

	global $CFG, $SESSION;

	if ($SESSION["user"]["priv"] != $priv) {
		$t = new Template();
		$t->set_file(array("page" => "$CFG->templatedir" . "insufficient_privileges.ihtml"));
		include ("$CFG->localelangdir" . "/global-common.inc.php");
		include ("$CFG->localelangdir" . "/global-admin.inc.php");
		$t->set_var("wwwroot", $CFG->wwwroot);
		$t->pparse("out", "page");
		die;
	}
}

function has_priv($priv) {
/* returns true if the user has the privilege $priv */

	global $SESSION;
	return $SESSION["user"]["priv"] == $priv;
}

function generate_password($length=10) {
// Returns a randomly generated password of length $length

	$password = "";

	srand((double) microtime() * 1000000);
	$l = 0;
	while ($l<$length) {
		$r = rand(48,122);
		if (!((58<=$r && $r<=64) || (91<=$r && $r<=96))) {
			$password .= chr($r);
			$l++;
		}
	}

	return $password;
}

function errmsg($errorvar) {
/* if $errorvar is set, then returns an error marker << */
	if ($errorvar)
		return "<font color=#ff0000>&lt;&lt;</font>";
}

function errmsg2($errorvar) {
/* like errmsg(), but returns the marker >> */
	if ($errorvar)
		return "<font color=#ff0000>&gt;&gt;</font>";
}

function username_exists(&$qid, $username) {
/* returns the true if the username exists */

//	$qid = new PGM_Sql;
	$qid->begin();
	$qid->Query("SELECT 1 FROM users WHERE username = '$username'");
	$appoggio = $qid->num_rows();
	$qid->commit();
	return $appoggio;
}

function reset_user_password(&$qid, $username) {
/* resets the password for the user with the username $username, and sends it
 * to him/her via email */

	global $CFG;
	global $SESSION;

//	$qid = new PGM_Sql;
	$qid->begin();

	/* load up the user record */
	$qid->Query("
		SELECT username, priv, firstname, lastname, email
		FROM users
		WHERE username = '$username'
	");
	$qid->next_record();
	if ($qid->f("priv") == "admin" || !has_priv("admin")) {
		return 1;
	}

	/* reset the password */
	$newpassword = generate_password();

	/* email the user with the new account information */
	$var = new Object;
	$var->username = $qid->f("username");
	$var->fullname = $qid->f("firstname") . " " . $qid->f("lastname");
	$var->newpassword = $newpassword;
	$var->support = "$CFG->support";
	$var->myname = "$CFG->myname";

	$emailbody = read_template("$CFG->templatedir" . $SESSION["lang"] . "/reset_password.ihtml", $var);

	$appoggio = $qid->f("email");
	$qid->Query("
		UPDATE users
		SET password = '" . md5($newpassword) ."'
		WHERE username = '$username'
	");
	mail(
		"$var->fullname <$appoggio>",
		"$CFG->myname" . " Account Information",
		$emailbody,
		"From: $var->support"
	);

	$qid->commit();

	return 0;
}

//function chop_ccnum($ccnum) {
///* this function returns the the first and last 4 digits of the credit card number
// * and the expiry date.  it is mainly used when we want to display the credit
// * card number on the screen etc. but we don't want to reveal the whole thing
// */
// 
//	return substr($ccnum, 0, 4) . "..." . substr($ccnum, -4);
//}

function save_orderinfo(&$frm) {
/* this function saves the order information into the session variable
 * $SESSION["orderinfo"].  it is used in the purchase confirmation stage */

	global $SESSION;	

	$order = new Object();
	$order->customer = $frm["customer"];
	$order->contact = $frm["contact"];
	$order->address = $frm["address"];
	$order->state = $frm["state"];
	$order->zip_code = $frm["zip_code"];
	$order->city = $frm["city"];
	$order->country = $frm["country"];
//	$order->creditcard = $frm["creditcard"];
//	$order->expiry = $frm["expiry"];
	$order->comments = $frm["comments"];
	
	$SESSION["orderinfo"] = $order;
}

function load_orderinfo() {
/* this function is the counterpart to save_orderinfo.  it is used to
 * retrieve the order information in the complete order page */

	global $SESSION;	

	if (empty($SESSION["orderinfo"])) {
		return false;
	} else {
		return $SESSION["orderinfo"];
	}
}

function clear_orderinfo() {
/* this function is called to clear the orderinfo session variable, it should
 * be used after an order was successfully completed */

	global $SESSION;
	unset($SESSION["orderinfo"]);
}

function return_star() {
	return "<font color=\"#ff0000\">*</font>";
}

function validate_user_discount($user_discount, &$errors, &$msg) {

	global $CFG;
	include ($CFG->globalerror);

	if ($user_discount != "") {
		eregi ("[0-9]{0,}[.]{0,1}[0-9]{0,}", $user_discount, $matched_user_discount);
		if ($user_discount != $matched_user_discount[0] || $user_discount == ".") {
			$errors->user_discount = true;
			$msg .= "<li>".$not_valid_user_discount;
		} else if ($user_discount < 0 || $user_discount > 100) {
			$errors->user_discount = true;
			$msg .= "<li>".$user_discount_not_between_0_and_100;
		}
	}
}

function validate_user_discount_name($user_discount_name, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

/*
	$stripped_user_discount_name = str_replace(" ", "", $user_discount_name);

	if ($user_discount_name == "" || $stripped_user_discount_name == "") {
		$errors->user_discount_name = true;
		$msg .= "<li>".$empty_user_discount_name;
	} else */ if (strlen($user_discount_name) > $SETTING["user_discount_name_length"]) {
		$errors->user_discount_name = true;
		$msg .= "<li>".$user_discount_name_too_long;
	}
}

function validate_user_discount_description($user_discount_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($user_discount_description) > $SETTING["user_discount_description_length"]) {
		$errors->user_discount_description = true;
		$msg .= "<li>".$user_discount_description_too_long;
	}
}

function validate_zone_code($zone_code, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_zone_code = str_replace(" ", "", $zone_code);

	if ($zone_code == "" || $stripped_zone_code == "") {
		$errors->zone_code = true;
		$msg .= "<li>".$empty_zone_code;
	} else if (strlen($zone_code) > $SETTING["zone_code_length"]) {
		$errors->zone_code = true;
		$msg .= "<li>".$zone_code_too_long;
	}
}

function validate_zone_name($zone_name, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_zone_name = str_replace(" ", "", $zone_name);

	if ($zone_name == "" || $stripped_zone_name == "") {
		$errors->zone_name = true;
		$msg .= "<li>".$empty_zone_name;
	} else if (strlen($zone_name) > $SETTING["zone_name_length"]) {
		$errors->zone_name = true;
		$msg .= "<li>".$zone_name_too_long;
	}
}

function validate_zone_description($zone_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($zone_description) > $SETTING["zone_description_length"]) {
		$errors->zone_description = true;
		$msg .= "<li>".$zone_description_too_long;
	}
}

function validate_username($username, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_username = str_replace(" ", "", $username);

	if ($username == "" || $stripped_username == "") {
		$errors->username = true;
		$msg .= "<li>".$empty_username;
	} else if (strlen($username) > $SETTING["username_length"]) {
		$errors->username = true;
		$msg .= "<li>".$username_too_long;
	} else if (strlen($username) < $SETTING["username_length_min"]) {
		$errors->username = true;
		$msg .= "<li>".$username_too_short;
	} else {
		eregi ("[[:alnum:]]+", $username, $matched_username);
		if ($username != $matched_username[0]) {
			$errors->username = true;
			$msg .= "<li>".$not_valid_username;
		}
	}
}

function validate_password($password, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_password = str_replace(" ", "", $password);
//	eregi ("[a-z0-9]{32}", $password, $matched_password);
	preg_match ("/[a-z0-9]{32}/", $password, $matched_password);

	// md5("") = d41d8cd98f00b204e9800998ecf8427e
	if ($password == "d41d8cd98f00b204e9800998ecf8427e") {
		$errors->password = true;
		$msg .= "<li>".$empty_password;
	}
	if ($password == "" || strlen($password) != 32
		|| $stripped_password == "" || strlen($stripped_password) != 32
		|| $password != $matched_password[0]) {
		$errors->password = true;
		$msg = $please_enable_javascript.$msg;
	}
}

function validate_firstname($firstname, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($firstname == "") {
		$errors->firstname = true;
		$msg .= "<li>".$empty_firstname;
	} else if (strlen($firstname) > $SETTING["firstname_length"]) {
		$errors->firstname = true;
		$msg .= "<li>".$firstname_too_long;
	}
}

function validate_ragionesociale($ragionesociale, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($ragionesociale == "") {
		$errors->firstname = true;
		$msg .= "<li>".$empty_ragionesociale;
	} else if (strlen($ragionesociale) > $SETTING["firstname_length"]) {
		$errors->firstname = true;
		$msg .= "<li>".$ragionesociale_too_long;
	}
}

function validate_name($name, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($name == "") {
		$errors->name = true;
		$msg .= "<li>".$empty_name;
	} else if (strlen($name) > $SETTING["name_length"]) {
		$errors->name = true;
		$msg .= "<li>".$name_too_long;
	}
}

function validate_lastname($lastname, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($lastname == "") {
		$errors->lastname = true;
		$msg .= "<li>".$empty_lastname;
	} else if (strlen($lastname) > $SETTING["lastname_length"]) {
		$errors->lastname = true;
		$msg .= "<li>".$lastname_too_long;
	}
}

function validate_customer($customer, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($customer == "") {
		$errors->customer = true;
		$msg .= "<li>".$empty_customer;
	} else if (strlen($customer) > $SETTING["firstname_length"] + $SETTING["lastname_length"] + 1) {
		$errors->customer = true;
		$msg .= "<li>".$customer_too_long;
	}
}

function validate_fiscalcode($fiscalcode, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$even["0"] = 1;
	$even["1"] = 0;
	$even["2"] = 5;
	$even["3"] = 7;
	$even["4"] = 9;
	$even["5"] = 13;
	$even["6"] = 15;
	$even["7"] = 17;
	$even["8"] = 19;
	$even["9"] = 21;
	$even["A"] = 1;
	$even["B"] = 0;
	$even["C"] = 5;
	$even["D"] = 7;
	$even["E"] = 9;
	$even["F"] = 13;
	$even["G"] = 15;
	$even["H"] = 17;
	$even["I"] = 19;
	$even["J"] = 21;
	$even["K"] = 2;
	$even["L"] = 4;
	$even["M"] = 18;
	$even["N"] = 20;
	$even["O"] = 11;
	$even["P"] = 3;
	$even["Q"] = 6;
	$even["R"] = 8;
	$even["S"] = 12;
	$even["T"] = 14;
	$even["U"] = 16;
	$even["V"] = 10;
	$even["W"] = 22;
	$even["X"] = 25;
	$even["Y"] = 24;
	$even["Z"] = 23;

	$odd["0"] = 0;
	$odd["1"] = 1;
	$odd["2"] = 2;
	$odd["3"] = 3;
	$odd["4"] = 4;
	$odd["5"] = 5;
	$odd["6"] = 6;
	$odd["7"] = 7;
	$odd["8"] = 8;
	$odd["9"] = 9;
	$odd["A"] = 0;
	$odd["B"] = 1;
	$odd["C"] = 2;
	$odd["D"] = 3;
	$odd["E"] = 4;
	$odd["F"] = 5;
	$odd["G"] = 6;
	$odd["H"] = 7;
	$odd["I"] = 8;
	$odd["J"] = 9;
	$odd["K"] = 10;
	$odd["L"] = 11;
	$odd["M"] = 12;
	$odd["N"] = 13;
	$odd["O"] = 14;
	$odd["P"] = 15;
	$odd["Q"] = 16;
	$odd["R"] = 17;
	$odd["S"] = 18;
	$odd["T"] = 19;
	$odd["U"] = 20;
	$odd["V"] = 21;
	$odd["W"] = 22;
	$odd["X"] = 23;
	$odd["Y"] = 24;
	$odd["Z"] = 25;

	if ($fiscalcode == "") {
		$errors->fiscalcode = true;
		$msg .= "<li>".$empty_fiscalcode;
	} else if (strlen($fiscalcode) != $SETTING["fiscalcode_length"]) {
		$errors->fiscalcode = true;
		$msg .= "<li>".$bad_fiscalcode_length;
	} else {
		$fiscalcode = strtoupper($fiscalcode);
		eregi ("[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}",
			$fiscalcode, $matched_fiscalcode);
		if ($fiscalcode != $matched_fiscalcode[0]) {
			$errors->fiscalcode = true;
			$msg .= "<li>".$not_valid_fiscalcode;
		} else {
			$total = 0;
			for($i=0;$i<15;$i+=2) {
				$total += $even["$fiscalcode[$i]"];
			}
			for($i=1;$i<14;$i+=2) {
				$total += $odd["$fiscalcode[$i]"];
			}
			$check = chr($total-26*(int)($total/26)+65);
			if ($check != $fiscalcode[15]) {
				$errors->fiscalcode = true;
				$msg .= "<li>".$not_valid_fiscalcode;
			}
		}
	}
}

function validate_partitaiva($partitaiva, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($partitaiva == "") {
		$errors->fiscalcode = true;
		$msg .= "<li>".$empty_partitaiva;
	} else if (strlen($partitaiva) != $SETTING["partitaiva_length"]) {
		$errors->fiscalcode = true;
		$msg .= "<li>".$bad_partitaiva_length;
	} else {
		eregi ("[0-9]{11}", $partitaiva, $matched_partitaiva);
		if ($partitaiva != $matched_partitaiva[0]) {
			$errors->fiscalcode = true;
			$msg .= "<li>".$not_valid_partitaiva;
		}
	}
}

function validate_address($address, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($address == "") {
		$errors->address = true;
		$msg .= "<li>".$empty_address;
	} else if (strlen($address) > $SETTING["address_length"]) {
		$errors->address = true;
		$msg .= "<li>".$address_too_long;
	}
}

function validate_number($number, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($number) > $SETTING["number_length"]) {
		$errors->number = true;
		$msg .= "<li>".$number_too_long;
	}
}

function validate_state($state, &$errors, &$msg) {

	global $CFG, $SESSION;
	include ($CFG->globalerror);

	if ($state == "") {
		$errors->state = true;
		$msg .= "<li>".$empty_state;
	}
}

function validate_cap($zip_code, &$errors, &$msg, $length=5) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($zip_code == "") {
		$errors->zip_code = true;
		$msg .= "<li>".$empty_zip_code;
	} else {
		eregi ("[0-9]{4,5}", $zip_code, $matched_zip_code);
		if ((string) $zip_code != (string) $matched_zip_code[0]) {
			if (strlen($zip_code) != $SETTING["zip_code_length"]) {
				$errors->zip_code = true;
				$msg .= "<li>".$zip_code_length_is_wrong;
			} else {
				$errors->zip_code = true;
				$msg .= "<li>".$zip_code_notint;
			}
		} else if ($zip_code <= 0) {
			$errors->zip_code = true;
			$msg .= "<li>".$zip_code_is_zero;
		}
	}
}

function validate_zip_code($zip_code, &$errors, &$msg, $length=5) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($zip_code == "") {
		$errors->zip_code = true;
		$msg .= "<li>".$empty_zip_code;
	} else if (strlen($zip_code) > $SETTING["zip_code_length_max"]) {
		$errors->zip_code = true;
		$msg .= "<li>".$zip_code_too_long;
	}
}

function validate_city($city, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($city == "") {
		$errors->city = true;
		$msg .= "<li>".$empty_city;
	} else if (strlen($city) > $SETTING["city_length"]) {
		$errors->city = true;
		$msg .= "<li>".$city_too_long;
	}
}

function validate_country($country, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($country == "") {
		$errors->country = true;
		$msg .= "<li>".$empty_country;
	} else if (strlen($country) > $SETTING["country_length"]) {
		$errors->country = true;
		$msg .= "<li>".$country_too_long;
	}
}

function validate_phone($phone, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($phone == "") {
		$errors->phone = true;
		$msg .= "<li>".$empty_phone;
	} else if (strlen($phone) > $SETTING["phone_length"]) {
		$errors->phone = true;
		$msg .= "<li>".$phone_too_long;
	} else if (strlen($phone) < $SETTING["phone_length_min"]) {
		$errors->phone = true;
		$msg .= "<li>".$phone_too_short;
	} else {
		$phone = ereg_replace (" ", "", $phone);
		$phone = ereg_replace ("\+", "", $phone);
		$phone = ereg_replace ("\-", "", $phone);
		$phone = ereg_replace ("\(", "", $phone);
		$phone = ereg_replace ("\)", "", $phone);
		$phone = ereg_replace ("\/", "", $phone);
		eregi ("[0-9]+", $phone, $matched_phone);
		if ($phone != $matched_phone[0]) {
			$errors->phone = true;
			$msg .= "<li>".$not_valid_phone;
		}
	}
}

function validate_contact($contact, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($contact == "") {
		$errors->contact = true;
		$msg .= "<li>".$empty_contact;
	} else if (strlen($contact) > $SETTING["phone_length"]) {
		$errors->contact = true;
		$msg .= "<li>".$contact_too_long;
	} else if (strlen($contact) < $SETTING["phone_length_min"]) {
		$errors->contact = true;
		$msg .= "<li>".$contact_too_short;
	} else {
		$contact = ereg_replace (" ", "", $contact);
		$contact = ereg_replace ("\+", "", $contact);
		$contact = ereg_replace ("\-", "", $contact);
		$contact = ereg_replace ("\(", "", $contact);
		$contact = ereg_replace ("\)", "", $contact);
		$contact = ereg_replace ("\/", "", $contact);
		eregi ("[0-9]+", $contact, $matched_contact);
		if ($contact != $matched_contact[0]) {
			$errors->contact = true;
			$msg .= "<li>".$not_valid_contact;
		}
	}
}

function validate_fax($fax, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($fax) > $SETTING["fax_length"]) {
		$errors->fax = true;
		$msg .= "<li>".$fax_too_long;
	} else if (strlen($fax) > 0 && strlen($fax) < $SETTING["fax_length_min"]) {
		$errors->fax = true;
		$msg .= "<li>".$fax_too_short;
	} else {
		$fax = ereg_replace (" ", "", $fax);
		$fax = ereg_replace ("\+", "", $fax);
		$fax = ereg_replace ("\-", "", $fax);
		$fax = ereg_replace ("\(", "", $fax);
		$fax = ereg_replace ("\)", "", $fax);
		$fax = ereg_replace ("\/", "", $fax);
		eregi ("[0-9]+", $fax, $matched_fax);
		if ($fax != $matched_fax[0]) {
			$errors->fax = true;
			$msg .= "<li>".$not_valid_fax;
		}
	}
}

function validate_mobilephone($mobilephone, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($mobilephone) > $SETTING["mobilephone_length"]) {
		$errors->mobilephone = true;
		$msg .= "<li>".$mobilephone_too_long;
	} else if (strlen($mobilephone) > 0 && strlen($mobilephone) < $SETTING["mobilephone_length_min"]) {
		$errors->mobilephone = true;
		$msg .= "<li>".$mobilephone_too_short;
	} else {
		$mobilephone = ereg_replace (" ", "", $mobilephone);
		$mobilephone = ereg_replace ("\+", "", $mobilephone);
		$mobilephone = ereg_replace ("\-", "", $mobilephone);
		$mobilephone = ereg_replace ("\(", "", $mobilephone);
		$mobilephone = ereg_replace ("\)", "", $mobilephone);
		$mobilephone = ereg_replace ("\/", "", $mobilephone);
		eregi ("[0-9]+", $mobilephone, $matched_mobilephone);
		if ($mobilephone != $matched_mobilephone[0]) {
			$errors->mobilephone = true;
			$msg .= "<li>".$not_valid_mobilephone;
		}
	}
}

function validate_email($email, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($email == "") {
		$errors->email = true;
		$msg .= "<li>".$empty_email;
	} else if (strlen($email) > $SETTING["email_length"]) {
		$errors->email = true;
		$msg .= "<li>".$email_too_long;
	} else {
		eregi ("(^[a-z0-9\.\_-]{1,})\@([a-z0-9\.-]{1,})\.([a-z]{2,3})$", $email, $matched_email);
		if ($email != $matched_email[0]) {
			$errors->email = true;
			$msg .= "<li>".$not_valid_email;
		}
	}
}

function validate_notes($notes, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($notes) > $SETTING["notes_length"]) {
		$errors->notes = true;
		$msg .= "<li>".$notes_too_long;
	}
}

function validate_message($message, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($message == "") {
		$errors->message = true;
		$msg .= "<li>".$empty_message;
	} else if (strlen($message) > $SETTING["message_length"]) {
		$errors->message = true;
		$msg .= "<li>".$message_too_long;
	}
}

function validate_ournotes($ournotes, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($ournotes) > $SETTING["ournotes_length"]) {
		$errors->ournotes = true;
		$msg .= "<li>".$ournotes_too_long;
	}
}

function validate_comments($comments, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($comments) > $SETTING["comments_length"]) {
		$errors->comments = true;
		$msg .= "<li>".$comments_too_long;
	}
}

function validate_lang(&$qid, $lang, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($lang == "") {
		$errors->lang = true;
		$msg .= "<li>".$empty_lang;
	} else if (strlen($lang) > $SETTING["lang_length"]) {
		$errors->lang = true;
		$msg .= "<li>".$lang_too_long;
	} else {
		// according to glibc locales...
		eregi ("^[a-zA-Z0-9\.\_\@]{1,}", $lang, $matched_lang);
		if ($lang != $matched_lang[0]) {
			$errors->lang = true;
			$msg .= "<li>".$not_valid_lang;
		}
		$qid->Query("SELECT lang FROM languages WHERE lang = '$lang'");
		if ($qid->num_rows() > 0) {
			$errors->lang = true;
			$msg .= "<li>".$the_lang.$lang.$already_exists;
		}
	}
}

function validate_category_name($category_name, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_category_name = str_replace(" ", "", $category_name);

	if ($category_name == "" || $stripped_category_name == "") {
		$errors->category_name = true;
		$msg .= "<li>".$empty_category_name;
	} else if (strlen($category_name) > $SETTING["category_name_length"]) {
		$errors->category_name = true;
		$msg .= "<li>".$category_name_too_long;
	}
}

function validate_category_description($category_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($category_description) > $SETTING["category_description_length"]) {
		$errors->category_description = true;
		$msg .= "<li>".$category_description_too_long;
	}
}

function validate_brand_name($brand_name, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_brand_name = str_replace(" ", "", $brand_name);

	if ($brand_name == "" || $stripped_brand_name == "") {
		$errors->brand_name = true;
		$msg .= "<li>".$empty_brand_name;
	} else if (strlen($brand_name) > $SETTING["brand_name_length"]) {
		$errors->brand_name = true;
		$msg .= "<li>".$brand_name_too_long;
	}
}

function validate_brand_description($brand_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($brand_description) > $SETTING["brand_description_length"]) {
		$errors->brand_description = true;
		$msg .= "<li>".$brand_description_too_long;
	}
}

function validate_iva($iva, &$errors, &$msg) {

	global $CFG;
	include ($CFG->globalerror);

	if ($iva != "") {
		eregi ("[0-9]{0,}[.]{0,1}[0-9]{0,}", $iva, $matched_iva);
		if ($iva != $matched_iva[0] || $iva == ".") {
			$errors->iva = true;
			$msg .= "<li>".$not_valid_iva;
		} else if ($iva < 0 || $iva > 100) {
			$errors->iva = true;
			$msg .= "<li>".$iva_not_between_0_and_100;
		}
	}
}

function validate_iva_description($iva_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($iva_description) > $SETTING["iva_description_length"]) {
		$errors->iva_description = true;
		$msg .= "<li>".$iva_description_too_long;
	}
}

function validate_color_name($color_name, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_color_name = str_replace(" ", "", $color_name);

	if ($color_name == "" || $stripped_color_name == "") {
		$errors->color_name = true;
		$msg .= "<li>".$empty_color_name;
	} else if (strlen($color_name) > $SETTING["color_name_length"]) {
		$errors->color_name = true;
		$msg .= "<li>".$color_name_too_long;
	}
}

function validate_color_description($color_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($color_description) > $SETTING["color_description_length"]) {
		$errors->color_description = true;
		$msg .= "<li>".$color_description_too_long;
	}
}

function validate_product_code($product_code, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

/*
	if ($product_code == "") {
		$errors->product_code = true;
		$msg .= "<li>".$product_code_is_empty;
	} else {
*/
/*
		if ($product_code != str_replace(" ", "", $product_code)) {
			$errors->product_code = true;
			$msg .= "<li>".$product_code_wspace;
		}
		if ($product_code != str_replace("&", "", $product_code)) {
			$errors->product_code = true;
			$msg .= "<li>".$product_code_wampersand;
		}
		if ($product_code != str_replace("?", "", $product_code)) {
			$errors->product_code = true;
			$msg .= "<li>".$product_code_wquestionm;
		}
*/
		if (strlen($product_code) > $SETTING["product_code_length"]) {
			$errors->product_code = true;
			$msg .= "<li>".$product_code_too_long;
		}
/*	} */
}

//function product_id_exists(&$qid, $product_id, &$errors, &$msg) {
//
//	global $CFG, $SETTING;
//	include ($CFG->globalerror);
//
////	$qid = new PGM_Sql;
//	$qid->Query("SELECT id FROM products WHERE id = '$product_id'");
//
//	if ($qid->num_rows() > 0) {
//		$errors->product_exists = true;
//		$msg .= "<li>".$the_product_id.$product_id.$already_exists;
//	}
////	$qid->close();
//}

function validate_product_name($product_name, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_product_name = str_replace(" ", "", $product_name);

	if ($product_name == "" || $stripped_product_name == "") {
		$errors->product_name = true;
		$msg .= "<li>".$empty_product_name;
	} else if (strlen($product_name) > $SETTING["product_name_length"]) {
		$errors->product_name = true;
		$msg .= "<li>".$product_name_too_long;
	}
}

function validate_price($price, &$errors, &$msg) {

	global $CFG, $SESSION;
	include ($CFG->globalerror);

	if ($price == "") {
		$errors->price = true;
		$msg .= "<li>".$empty_price;
	} else if ($SESSION["price_must_be_integer"] && ((string) ((int) $price) != "$price" || (string) $price != str_replace(".", "", $price))) {
		$errors->price = true;
		$msg .= "<li>".$price_notint;
	} else if (!$SESSION["price_must_be_integer"] && (string) ((float) $price) != "$price") {
		$errors->price = true;
		$msg .= "<li>".$price_notfloat;
	} else if ($price <= 0) {
		$errors->price = true;
		$msg .= "<li>".$price_is_zero;
	}
}

function validate_cost($cost, &$errors, &$msg) {

	global $CFG, $SESSION;
	include ($CFG->globalerror);

	if ($cost == "") {
		$errors->cost = true;
		$msg .= "<li>".$empty_cost;
	} else if ($SESSION["price_must_be_integer"] && ((string) ((int) $cost) != "$cost" || (string) $cost != str_replace(".", "", $cost))) {
		$errors->cost = true;
		$msg .= "<li>".$cost_notint;
	} else if (!$SESSION["price_must_be_integer"] && (string) ((float) $cost) != "$cost") {
		$errors->cost = true;
		$msg .= "<li>".$cost_notfloat;
	} else if ($cost < 0) {
		$errors->cost = true;
		$msg .= "<li>".$cost_is_lower_than_zero;
	}
}

function validate_discount($discount, $discqty, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($discount.$discqty != "") {
		if (($discount != 0) && ($discount == "" || $discqty == "")) {
			$errors->discount = true;
			$errors->discqty = true;
			$msg .= "<li>".$discount_not_completely_defined;
		}
		if ($discount != "") {
			eregi ("[0-9]{0,}[.]{0,1}[0-9]{0,}", $discount, $matched_discount);
			if ($discount != $matched_discount[0] || $discount == ".") {
				$errors->discount = true;
				$msg .= "<li>".$not_valid_discount;
			} else if ($discount < 0 || $discount > 100) {
				$errors->discount = true;
				$msg .= "<li>".$discount_not_between_0_and_100;
			}
		}
		if ($discqty != "" && ((string) ((int) $discqty) != "$discqty")) {
			$errors->discqty = true;
			$msg .= "<li>".$discqty_notint;
		} else if ($discqty < 0) {
			$errors->discqty = true;
			$msg .= "<li>".$discqty_is_negative;
		} else if ($discqty > $SETTING["discqtymax"]) {
			$errors->discqty = true;
			$msg .= "<li>".$discqty_is_too_large;
		}
	}
}

function validate_weight($weight, &$errors, &$msg) {

	global $CFG, $SESSION;
	global $weightunity;
	include ($CFG->globalerror);

	if ($weight == "") {
		$errors->weight = true;
		$msg .= "<li>".$empty_weight;
	} else {
		eregi ("[0-9]{0,}[.]{0,1}[0-9]{0,}", $weight, $matched_weight);
		if ($weight != $matched_weight[0] || $weight == ".") {
			$errors->weight = true;
			$msg .= "<li>".$not_valid_weight;
		} else if ($weight <= 0) {
			$errors->weight = true;
			$msg .= "<li>".$weight_is_zero;
		} else if ($weight > $SESSION["weightmax"]) {
			$errors->weight = true;
			$msg .= "<li>".$weight_over_max;
		}
	}
}

function validate_special_level($special_level, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

/*
	if ($special_level == "") {
		$errors->special_level = true;
		$msg .= "<li>".$special_level_empty;
	} else if ((string) ((int) $special_level) != "$special_level") {
*/
	if ((string) ((int) $special_level) != "$special_level" && $special_level != "") {
		$errors->special_level = true;
		$msg .= "<li>".$special_level_notint;
	}
}

function validate_new_level($new_level, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ((string) ((int) $new_level) != "$new_level" && $new_level != "") {
		$errors->new_level = true;
		$msg .= "<li>".$new_level_notint;
	}
}

function validate_product_description($product_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($product_description) > $SETTING["product_description_length"]) {
		$errors->product_description = true;
		$msg .= "<li>".$product_description_too_long;
	}
}

function validate_product_extended_description($product_extended_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($product_extended_description) > $SETTING["product_extended_description_length"]) {
		$errors->product_extended_description = true;
		$msg .= "<li>".$product_extended_description_too_long;
	}
}

function validate_order_state_name($order_state_name, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	$stripped_order_state_name = str_replace(" ", "", $order_state_name);

	if ($order_state_name == "" || $stripped_order_state_name == "") {
		$errors->order_state_name = true;
		$msg .= "<li>".$empty_order_state_name;
	} else if (strlen($order_state_name) > $SETTING["order_state_name_length"]) {
		$errors->order_state_name = true;
		$msg .= "<li>".$order_state_name_too_long;
	}
}

function validate_order_state_description($order_state_description, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($order_state_description) > $SETTING["order_state_description_length"]) {
		$errors->order_state_description = true;
		$msg .= "<li>".$order_state_description_too_long;
	}
}

function validate_searched_discount($discount, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	eregi ("[0-9]{0,}[.]{0,1}[0-9]{0,}", $discount, $matched_discount);
	if ($discount != $matched_discount[0] || $discount == ".") {
		$errors->discount = true;
		$msg .= "<li>".$not_valid_discount;
	} else if ($discount < 0 || $discount > 100) {
		$errors->discount = true;
		$msg .= "<li>".$discount_not_between_0_and_100;
	}
}

function validate_searched_discqty($discqty, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if ($discqty != "" && ((string) ((int) $discqty) != "$discqty")) {
		$errors->discqty = true;
		$msg .= "<li>".$discqty_notint;
	} else if ($discqty < 0) {
		$errors->discqty = true;
		$msg .= "<li>".$discqty_is_negative;
	} else if ($discqty > $SETTING["discqtymax"]) {
		$errors->discqty = true;
		$msg .= "<li>".$discqty_is_too_large;
	}
}

function validate_priv($priv, &$errors, &$msg) {

	global $CFG, $SETTING;
	include ($CFG->globalerror);

	if (strlen($priv) > $SETTING["priv_length"]) {
		$errors->priv = true;
		$msg .= "<li>".$priv_too_long;
	}
	eregi ("[[:alnum:]]+", $priv, $matched_priv);
	if ($priv != $matched_priv[0]) {
		$errors->priv = true;
		$msg .= "<li>".$not_valid_priv;
	}
}


// Generate an invoice given an and type.
function generate_invoice($type, $id, $username) {

	global $CFG, $ME;
	global $SESSION;
	global $weightunity;
	global $qid_order;
	global $qid_items;
	global $qid_user;
//	include ($CFG->globalerror);

        if ($type == 'A') {
	  $qid_order->Query("
		SELECT o.*, os.name AS state_name
		FROM ordersca o, orders_states os
		WHERE o.state_id = os.id AND o.id = '$id'
	  ");
	} else {
	  $qid_order->Query("
		SELECT o.*, os.name AS state_name
		FROM orderscc o, orders_states os
		WHERE o.state_id = os.id AND o.id = '$id'
	  ");
	}
	if ($qid_order->num_rows() < 1) {
		$t = new Template();
		$t->set_file(array("page" => "users/templates/no_such_order.ihtml"));
		require($CFG->localedir . $SESSION["lang"] . "/global-common.inc.php");
		$t->pparse("out", "page");
//		include("users/templates/no_such_order.ihtml");
		return;
	} else {
		$qid_order->next_record();
	}


	get_order_items(&$qid_items, $id);

	$appoggio = $qid_order->f("username");
	$qid_user->Query("
		SELECT usertype, fiscalcode, email
		FROM users
		WHERE username = '$appoggio'
	");
	$qid_user->next_record();

	$t = new Template();
	$t->set_file(array("page" => $CFG->templatedir."/invoice.ihtml"));
	require("$CFG->localelangdir" . "/global-common.inc.php");
	require("$CFG->localelangdir" . "/global-names.inc.php");
	$t->set_var("ME", $ME);
	$t->set_block("page", "popup_header", "popup_header_blck");
	$t->set_var("popup_header_blck", "");
	$t->set_block("page", "popup_footer", "popup_footer_blck");
	$t->set_var("popup_footer_blck", "");
	$t->set_block("page", "is_not_popup", "is_not_popup_blck");
	$t->set_var("is_not_popup_blck", "");
	$t->set_block("page", "have_discount", "have_discount_blck");
	$t->set_var(array("have_discount_blck" => ""));
	$subtotal = $qid_order->f("amount") - $qid_order->f("delivery");
	if ($qid_order->f("user_discount") > 0)
	{
	  $discount_amount = round($subtotal * $qid_order->f("user_discount") / (100 - $qid_order->f("user_discount")),2);
	  $t->set_var("order_fuserdisc", formatted_price($discount_amount));
	  $t->parse("have_discount_blck", "have_discount");
	}
	$subtotal += $discount_amount;
	$t->set_var(array(
			"wwwroot"	=> $CFG->wwwroot,
			"order_id"		=> ov($id),
			"order_state_name"	=> $qid_order->f("state_name"),
			"order_date"		=> $userday[$qid_order->f("weekday")] . " " . $qid_order->f("day") . " " . $usermonth[$qid_order->f("month")] . " " . $qid_order->f("year"),
			"order_amount"		=> $qid_order->f("amount"),
			"order_famount"		=> formatted_price($qid_order->f("amount")),
			"order_subtotal"		=> formatted_price($subtotal),
			"order_custinfo"	=> nl2br(ov($qid_order->f("custinfo"))),
			"user_fiscalcode"	=> $qid_user->f("fiscalcode"),
			"user_email"		=> $qid_user->f("email"),
			"order_notes"		=> nl2br(ov($qid_order->f("notes"))),
			"order_delivery"		=> formatted_price($qid_order->f("delivery")),
	));

	if (me() == "/users/orderwindow.php") {
		$t->parse("popup_header_blck", "popup_header", true);
		$t->parse("popup_footer_blck", "popup_footer", true);
	} else {
	  $t->parse("is_not_popup_blck", "is_not_popup", true);
	}
	$t->set_block("page", "item_row", "item_rows");
	$t->set_var("item_rows", "");
	$t->set_block("page", "payment_info", "payment_info_blck");
	$t->set_var("payment_info_blck", "");
	$t->set_block("page", "bank_auth", "bank_auth_blck");
	$t->set_var("bank_auth_blck", "");
	while ($qid_items->next_record()) {
		$discount = ($qid_items->f("qty") >= $qid_items->f("purchase_discqty")) ? $qid_items->f("purchase_discount") : 0;
		$total = $qid_items->f("purchase_price")*$qid_items->f("qty")*(1.0+$qid_items->f("purchase_iva")/100.0)*(1.0-$discount/100.0);
		$t->set_var(array(
			"prod_curr_price"	=> $qid_items->f("curr_price"),
			"prod_fcurr_price"	=> formatted_price($qid_items->f("curr_price")),
			"prod_purchase_price"	=> $qid_items->f("purchase_price"),
			"prod_fpurchase_price"	=> formatted_price($qid_items->f("purchase_price")),
			"prod_qty"		=> $qid_items->f("qty"),
 			"prod_total"		=> $total,
 			"prod_ftotal"		=> formatted_price($total),
			"prod_product_id"	=> $qid_items->f("product_id"),
			"prod_name"		=> $qid_items->f("name")
		));
		$t->parse("item_rows", "item_row", true);
	}
	$t->set_block("page", "delivery_used", "delivery_used_blck");
	$t->set_var(array("delivery_used_blck" => ""));
	if ($SESSION["delivery_used"]) {
		$t->set_var(array(
			"order_delivery"	=> $qid_order->f("delivery"),
			"order_fdelivery"	=> formatted_price($qid_order->f("delivery"))
		));
		$t->parse("delivery_used_blck", "delivery_used", true);
	}
	if ($type == 'C' && $qid_order->f("state_id") == 2) {
	  $t->set_var('bank_auth_number', $qid_order->f("codaut"));
	  $t->parse("bank_auth_blck", "bank_auth", true);
	}
	if ($qid_order->f("state_id") == 0) {
	  $t->parse("payment_info_blck", "payment_info", true);
	}
	$t->pparse("out", "page");
//	include("templates/order_details.ihtml");
}

function add_order_notes($qid, $orderid, $type, $notes)
{
  if ($type == 'C') 
  	$table = 'orderscc';
  else
  	$table = 'ordersca';

  $qid->query("UPDATE $table SET ournotes=( SELECT COALESCE(ournotes || '$notes\n', '$notes\n') FROM $table WHERE id = $orderid) WHERE id=$orderid");
  }
?>

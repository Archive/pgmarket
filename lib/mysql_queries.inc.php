<?php
/* (c) 2000 Ying Zhang (ying@zippydesign.com)
/* (c) 2000-2002 Marco Pratesi <pratesi@telug.it> */
/* (c) 2000 Oleg V. Kalashnikov <oleg@ok.gnu.zp.ua> */
/* (c) 2000-2001 Fabio Molinari <fabio.m@mclink.it> */

$DBMS_FALSE = "0";
$DBMS_TRUE = "1";

function prepare_limit_clause($limit, $offset) {
	return "LIMIT " . $offset . ", " . $limit;
}

function get_special_products(&$qid) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id
			,p.name
			,l.name AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.special_level
			,p.description
			,l.description AS ldescription
		FROM products p LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[lang]'), brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND special_flag = 1
		ORDER BY special_level ASC
	");
}

function get_products_under_category(&$qid, $id, &$page_number, &$num) {

	global $SESSION;

	$qid->Query("
		SELECT COUNT(p.id) AS cnt
		FROM products p, products_categories pc
		WHERE p.id = pc.product_id AND pc.category_id = '$id'
	");
	$qid->next_record();
	$num["rows"] = $qid->f("cnt");
	$num["pages"] = max(1, ceil($num["rows"]/$SESSION["products_per_catalog_page"]));
	$page_number = min($page_number, $num["pages"]);
	$first = ($page_number-1)*$SESSION["products_per_catalog_page"];
	$last = min($num["rows"]-1, $page_number*$SESSION["products_per_catalog_page"]-1);
	$limit = $last - $first + 1;
	$limit_clause = prepare_limit_clause($limit, $first);

	$qid->Query("
		SELECT
			 p.id
			,p.code
			,p.name AS name
			,l.name AS lname
			,p.price
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.description
			,l.description AS ldescription
			,p.thumbwidth
			,p.thumbheight
			,pc.category_id
			,b.name AS bname
			,i.iva
		FROM products p LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[lang]'), products_categories pc, brands b, iva i
		WHERE p.id = pc.product_id AND p.brand_id = b.id AND p.iva_id = i.id AND pc.category_id = '$id'
		ORDER BY special_level ASC
		$limit_clause
	");
}

function get_color_name(&$qid, $color_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id AS color_id
			,c.name
			,l.name AS lname
		FROM colors c LEFT JOIN colors_i18n l ON (c.id = l.color_id AND l.lang = '$SESSION[lang]')
		WHERE id = '$color_id'
	");
}

function get_product_color_options(&$qid, $product_id, &$output, $options=1) {

	global $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname
		FROM colors c LEFT JOIN colors_i18n l ON (c.id = l.color_id AND l.lang = '$SESSION[lang]'), products_colors pc
		WHERE pc.color_id = c.id AND pc.product_id = '$product_id'
		ORDER BY name
	");

	if ($options) {
		$output = "<option value=\"\"></option>\n";
		while ($qid->next_record()) {
			$appoggio = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$output .= "<option value=\"" . ov($qid->f("id")) . "\">" . ov($appoggio) . "</option>\n";
		}
	} else {
		while ($qid->next_record()) {
			$appoggio = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$output .= "<li>" . ov($appoggio) . "</li>\n";
		}
		$output = "<ul>" . $output . "</ul>";
	}
}

function get_product_details(&$qid, $product_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id AS product_id
			,p.code
			,p.name AS name
			,l.name AS lname
			,p.price
			,p.discount
			,p.discqty
			,p.weight
			,p.extended_description
			,l.extended_description AS lextended_description
			,p.imagetype
			,p.imagewidth
			,p.imageheight
			,p.thumbwidth
			,p.thumbheight
			,b.name AS bname
			,i.iva
		FROM products p LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[lang]'), brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND p.id = '$product_id'
	");
}

function build_category_path(&$qid, $id=1, &$category_path) {
// puts in $category_path the path of the product categories,
// starting from the category specified by $id and ending with the top

	global $CFG, $SESSION;

	while ($id > 1) {
		$qid->Query("
			SELECT c.parent_id, c.name, l.name AS lname
			FROM categories c LEFT JOIN categories_i18n l ON (c.id = l.category_id AND l.lang = '$SESSION[lang]')
			WHERE c.id = '$id'
		");
		if ($qid->num_rows()) {
			$qid->next_record();
			$parent = $qid->f("parent_id");
			$name = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$name = "<a href='$CFG->wwwroot/shopping/index.php?id=$id'>" . stripslashes($name) . "</a>";
			$category_path = " &gt; " . $name . $category_path;
		} else {
			$parent = 1;
			$name = "";
		}
		$id = $parent;
	}
	$category_path = "<a href='$CFG->wwwroot/shopping/index.php?id=1'>Top</a>" . $category_path;
}

function get_subcategories(&$qid, $id) {

	global $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM categories c LEFT JOIN categories_i18n l ON (c.id = l.category_id AND l.lang = '$SESSION[lang]')
		WHERE parent_id = '$id' AND id > 1
		ORDER BY special_level, name
	");
}

function build_catbrowser(&$qid, &$output, $parent=1, $indent=".") {

	global $CFG, $SESSION, $HTTP_SERVER_VARS;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM categories c LEFT JOIN categories_i18n l ON (c.id = l.category_id AND l.lang = '$SESSION[lang]')
		WHERE parent_id = '$parent' AND id <> 1
		ORDER BY special_level, name
	");

	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}

	for ($i=0; $i<$numr; $i++) {
		$output .= $indent . stripslashes($cat[$i]["name"]) . "^" . "$CFG->wwwroot" . "/shopping/?" . $HTTP_SERVER_VARS["QUERY_STRING"] . "&id=" . $cat[$i]["id"] . "^" . $SESSION["catbrowser_target"] . "\n";
		if ($cat[$i]["id"] != $parent)
			build_catbrowser($qid, $output, $cat[$i]["id"], $indent . ".");
	}
}

function build_catbrowser_js(&$qid, &$output, $parent=1, $indent=".") {

	global $CFG, $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM categories c LEFT JOIN categories_i18n l ON (c.id = l.category_id AND l.lang = '$SESSION[lang]')
		WHERE parent_id = '$parent' AND id <> 1
		ORDER BY special_level, name
	");
	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}

	for ($i=0; $i<$numr; $i++) {
			$output .= $indent . $cat[$i]["name"] . "^" . "$CFG->wwwroot" . "/shopping/index.php?id=" . $cat[$i]["id"] . "\n";
		if ($cat[$i]["id"] != $parent)
			build_catbrowser_js($qid, $output, $cat[$i]["id"], $indent . ".");
	}
}

function get_cart_items_info(&$qid) {

	global $SESSION, $CART;

	$in_clause = $CART->get_productid_list();
	if (empty($in_clause))
		return false;

	$qid->Query("
		SELECT
			 p.id
			,p.name AS name
			,l.name AS lname
			,p.thumbwidth
			,p.thumbheight
		FROM products p LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[lang]')
		WHERE p.id IN ($in_clause)
	");

	return $qid->num_rows();
}

function get_order_items(&$qid, $id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 oi.product_id
			,oi.name AS oiname
			,oi.color
			,p.name
			,l.name AS lname
			,p.price AS curr_price
			,oi.price AS purchase_price
			,oi.discount AS purchase_discount
			,oi.discqty AS purchase_discqty
			,oi.iva AS purchase_iva
			,oi.weight AS purchase_weight
			,oi.qty
		FROM order_items oi LEFT JOIN products p ON (oi.product_id = p.id) LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[lang]')
		WHERE order_id = '$id'
	");
}

function find_products(&$qid, $frm, &$page_number, &$num) {

	global $DBMS_FALSE, $DBMS_TRUE;

	$l_search_condition = "";

	$stringtsf_stripped = ereg_replace ("\+", "", $frm["stringtsf"]);

	$l_array_of_elements = explode(" ", $stringtsf_stripped);
	foreach($l_array_of_elements as $l_search_element) {
		if ($l_search_element != "") 
			if ($frm["case_sensitive"])
				$l_search_condition .=
					"( POSITION('$l_search_element' IN p.name) > 0 " .
					"OR POSITION('$l_search_element' IN l.name) > 0 " .
					"OR POSITION('$l_search_element' IN p.description) > 0 " .
					"OR POSITION('$l_search_element' IN l.description) > 0 " .
					"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
					"OR POSITION('$l_search_element' IN l.extended_description) > 0 " .
					") " . $frm["concatenation"] . " ";
			else
				$l_search_condition .=
					"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.name)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.extended_description)) > 0 " .
					") " . $frm["concatenation"] . " ";
	}

	if ($frm["concatenation"] == "OR")
		$l_search_condition.="$DBMS_FALSE";
	else
		$l_search_condition.="$DBMS_TRUE";

	return find_products_query($qid, $l_search_condition, $page_number, $num);
}

function advanced_find_products(&$qid, $frm, &$page_number, &$num) {

	global $DBMS_FALSE, $DBMS_TRUE;
	global $SESSION;

	$l_search_condition = "";

	// Prepare for searching words in the product name
	$prod_stripped = ereg_replace ("\+", "", $frm["product_name_in"]);
	if (!empty($prod_stripped)) {
		$prod_concatenation = $frm["prod_concatenation"];
		$prod_case_sensitive = $frm["prod_case_sensitive"];

		$l_array_of_elements = explode(" ",$prod_stripped);
		foreach($l_array_of_elements as $l_search_element) {
			if ($l_search_element != "") {
				if ($prod_case_sensitive)
					$prod_l_search_condition .=
						"( POSITION('$l_search_element' IN p.name) > 0 " .
						"OR POSITION('$l_search_element' IN l.name) > 0 " .
						") $prod_concatenation ";
				else
					$prod_l_search_condition .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.name)) > 0 " .
						") $prod_concatenation ";
			}
		}

		if ($prod_concatenation == "OR")
			$prod_l_search_condition.="$DBMS_FALSE";
		else
			$prod_l_search_condition.="$DBMS_TRUE";
	}
	if (isset($prod_l_search_condition)) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "( " . $prod_l_search_condition . " )";
	}

	// Matching the brand
	if (!empty($frm["brand_id"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= " p.brand_id = '$frm[brand_id]' ";
	}

	// Prepare for searching words in the product description
	$desc_stripped = ereg_replace ("\+", "", $frm["name_in"]);
	if (!empty($desc_stripped)) {
		$desc_concatenation = $frm["desc_concatenation"];
		$desc_case_sensitive = $frm["desc_case_sensitive"];

		$l_array_of_elements = explode(" ",$desc_stripped);
		foreach($l_array_of_elements as $l_search_element) {
			if ($l_search_element != "") {
				if ($desc_case_sensitive)
					$desc_l_search_condition .=
						"( POSITION('$l_search_element' IN p.description) > 0 " .
						"OR POSITION('$l_search_element' IN l.description) > 0 " .
						"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
						"OR POSITION('$l_search_element' IN l.extended_description) > 0 " .
						") $desc_concatenation ";
				else
					$desc_l_search_condition .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.extended_description)) > 0 " .
						") $desc_concatenation ";
			}
		}

		if ($desc_concatenation == "OR")
			$desc_l_search_condition.="$DBMS_FALSE";
		else
			$desc_l_search_condition.="$DBMS_TRUE";
	}
	if (isset($desc_l_search_condition)) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(" . $desc_l_search_condition . ")";
	}

	// Prepare for searching products with prices within a limited range
	if (!empty($frm["price_min"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(price >= '$frm[price_min]')";
	}
	if (!empty($frm["price_max"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(price <= '$frm[price_max]')";
	}

	// Prepare for searching products with discount within a limited range
	if (!empty($frm["discount_min"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(discount >= '$frm[discount_min]')";
	}
	if (!empty($frm["discount_max"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(discount <= '$frm[discount_max]')";
	}

	// Prepare for searching products with discount qty within a limited range
	if (!empty($frm["discqty_min"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(discqty >= '$frm[discqty_min]')";
	}
	if (!empty($frm["discqty_max"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(discqty <= '$frm[discqty_max]')";
	}

	// Prepare for searching products with weight within a limited range
	if (!empty($frm["weight_min"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(weight >= '$frm[weight_min]')";
	}
	if (!empty($frm["weight_max"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		$l_search_condition .= "(weight <= '$frm[weight_max]')";
	}

	return find_products_query($qid, $l_search_condition, $page_number, $num);
}

function find_products_query(&$qid, $l_search_condition, &$page_number, &$num) {

	global $SESSION;

	$qid->Query("
		SELECT COUNT(p.id) AS cnt
		FROM products p LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[lang]'), brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition)
	");
	$qid->next_record();
	$num["rows"] = $qid->f("cnt");
	$num["pages"] = max(1, ceil($num["rows"]/$SESSION["products_per_catalog_page"]));
	$page_number = min($page_number, $num["pages"]);
	$first = ($page_number-1)*$SESSION["products_per_catalog_page"];
	$last = min($num["rows"]-1, $page_number*$SESSION["products_per_catalog_page"]-1);
	$limit = $last - $first + 1;
	$limit_clause = prepare_limit_clause($limit, $first);

	$qid->Query("
		SELECT
			 p.id
			,p.code
			,p.name
			,l.name AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.description
			,l.description AS ldescription
			,p.thumbwidth
			,p.thumbheight
		FROM products p LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[lang]'), brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition)
		ORDER BY name, bname
		$limit_clause
	");
}

function get_zone_name(&$qid, $zone_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 z.id AS zone_id
			,z.name
			,l.name AS lname
		FROM zones z LEFT JOIN zones_i18n l ON (z.id = l.zone_id AND l.lang = '$SESSION[lang]')
		WHERE id = '$zone_id'
	");
}

function get_zones_names(&$qid) {

	global $SESSION;

	$qid->Query("
		SELECT
			 z.id AS zone_id
			,z.name
			,l.name AS lname
		FROM zones z LEFT JOIN zones_i18n l ON (z.id = l.zone_id AND l.lang = '$SESSION[lang]')
		ORDER BY name
	");
}

?>

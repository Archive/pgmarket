<?php
/* (c) 2000 Ying Zhang (ying@zippydesign.com)
/* (c) 2000-2002 Marco Pratesi <pratesi@telug.it> */
/* (c) 2000 Oleg V. Kalashnikov <oleg@ok.gnu.zp.ua> */
/* (c) 2000-2001 Fabio Molinari <fabio.m@mclink.it> */

/* We use UNIONs and not OUTER JOINS to support PostgreSQL < 7.1 */

$DBMS_FALSE = "FALSE";
$DBMS_TRUE = "TRUE";

function prepare_limit_clause($limit, $offset) {
	if ($limit == 0)
		$limit = 1;	// workaround needed for PostgreSQL 7.0
	return "LIMIT " . $limit . ", " . $offset;
}

function get_special_products(&$qid) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id
			,p.name
			,l.name AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.special_level
			,p.description
			,l.description AS ldescription
		FROM products p, products_i18n l, brands b, iva i
		WHERE p.id = l.product_id AND l.lang = '$SESSION[lang]' AND p.brand_id = b.id AND p.iva_id = i.id AND p.special_flag = 1
		UNION ALL
		SELECT
			 p.id
			,p.name
			,'' AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.special_level
			,p.description
			,'' AS ldescription
		FROM products p, brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND p.special_flag = 1
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[lang]'
		)
		ORDER BY special_level ASC
");
}

function get_new_products(&$qid) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id
			,p.name
			,l.name AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.new_level
			,p.description
			,l.description AS ldescription
		FROM products p, products_i18n l, brands b, iva i
		WHERE p.id = l.product_id AND l.lang = '$SESSION[lang]' AND p.brand_id = b.id AND p.iva_id = i.id AND p.new_flag = 1
		UNION ALL
		SELECT
			 p.id
			,p.name
			,'' AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.new_level
			,p.description
			,'' AS ldescription
		FROM products p, brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND p.new_flag = 1
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[lang]'
		)
		ORDER BY new_level ASC
");
}

function get_products_under_category(&$qid, $id, &$page_number, &$num) {

	global $SESSION;

	$qid->Query("
		SELECT COUNT(p.id) AS cnt
		FROM products p, products_categories pc
		WHERE p.id = pc.product_id AND pc.category_id = '$id'
	");
	$qid->next_record();
	$num["rows"] = $qid->f("cnt");
	$num["pages"] = max(1, ceil($num["rows"]/$SESSION["products_per_catalog_page"]));
	$page_number = min($page_number, $num["pages"]);
	$first = ($page_number-1)*$SESSION["products_per_catalog_page"];
	$last = min($num["rows"]-1, $page_number*$SESSION["products_per_catalog_page"]-1);
	$limit = $last - $first + 1;
	$limit_clause = prepare_limit_clause($limit, $first);

	$qid->Query("
		SELECT
			 p.id
			,p.code
			,p.name AS name
			,l.name AS lname
			,p.price
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.nostock_flag
			,p.new_level
			,p.description
			,l.description AS ldescription
			,p.thumbwidth
			,p.thumbheight
			,pc.category_id
			,b.name AS bname
			,i.iva
		FROM products p, products_i18n l, products_categories pc, brands b, iva i
		WHERE p.id = l.product_id AND l.lang = '$SESSION[lang]'
			AND p.id = pc.product_id AND p.brand_id = b.id
			AND p.iva_id = i.id AND pc.category_id = '$id'
		UNION ALL
		SELECT
			 p.id
			,p.code
			,p.name AS name
			,'' AS lname
			,p.price
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.nostock_flag
			,p.new_level
			,p.description
			,'' AS ldescription
			,p.thumbwidth
			,p.thumbheight
			,pc.category_id
			,b.name AS bname
			,i.iva
		FROM products p, products_categories pc, brands b, iva i
		WHERE p.id = pc.product_id AND p.brand_id = b.id AND p.iva_id = i.id AND pc.category_id = '$id'
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[lang]'
		)
		ORDER BY special_level ASC, name ASC
		$limit_clause
	");
}

function get_color_name(&$qid, $color_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id AS color_id
			,c.name
			,l.name AS lname
		FROM colors c, colors_i18n l
		WHERE c.id = l.color_id AND l.lang = '$SESSION[lang]' AND c.id = '$color_id'
		UNION ALL
		SELECT
			 c.id AS color_id
			,c.name
			,'' AS lname
		FROM colors c
		WHERE c.id = '$color_id'
		AND c.id NOT IN (
			SELECT color_id FROM colors_i18n WHERE lang = '$SESSION[lang]'
		)
	");
}

function get_product_color_options(&$qid, $product_id, &$output, $options=1) {

	global $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname
		FROM products_colors pc, colors c, colors_i18n l
		WHERE pc.color_id = c.id AND c.id = l.color_id AND l.lang = '$SESSION[lang]' AND pc.product_id = '$product_id'
		UNION ALL
		SELECT c.id, c.name, '' AS lname
		FROM products_colors pc, colors c
		WHERE pc.color_id = c.id AND pc.product_id = '$product_id'
		AND c.id NOT IN (
			SELECT color_id FROM colors_i18n WHERE lang = '$SESSION[lang]'
		)
		ORDER BY name
	");

	if ($options) {
		$output = "<option value=\"\"></option>\n";
		while ($qid->next_record()) {
			$appoggio = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$output .= "<option value=\"" . ov($qid->f("id")) . "\">" . ov($appoggio) . "</option>\n";
		}
	} else {
		while ($qid->next_record()) {
			$appoggio = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$output .= "<li>" . ov($appoggio) . "</li>\n";
		}
		$output = "<ul>" . $output . "</ul>";
	}
}

function get_product_details(&$qid, $product_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id AS product_id
			,p.code
			,p.name AS name
			,l.name AS lname
			,p.price
			,p.discount
			,p.discqty
			,p.weight
			,p.extended_description
			,l.extended_description AS lextended_description
			,p.imagetype
			,p.imagewidth
			,p.imageheight
			,p.thumbwidth
			,p.thumbheight
			,p.nostock_flag
			,b.name AS bname
			,i.iva
		FROM products p, products_i18n l, brands b, iva i
		WHERE p.id = l.product_id AND l.lang = '$SESSION[lang]'
			AND p.brand_id = b.id AND p.iva_id = i.id AND p.id = '$product_id'
		UNION ALL
		SELECT
			 p.id AS product_id
			,p.code
			,p.name AS name
			,'' AS lname
			,p.price
			,p.discount
			,p.discqty
			,p.weight
			,p.extended_description
			,'' AS lextended_description
			,p.imagetype
			,p.imagewidth
			,p.imageheight
			,p.thumbwidth
			,p.thumbheight
			,p.nostock_flag
			,b.name AS bname
			,i.iva
		FROM products p, brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND p.id = '$product_id'
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[lang]'
		)
	");
}

function build_category_path(&$qid, $id=1, &$category_path) {
// puts in $category_path the path of the product categories,
// starting from the category specified by $id and ending with the top

	global $CFG, $SESSION;

	while ($id > 1) {
		$qid->Query("
			SELECT c.parent_id, c.name, l.name AS lname
			FROM categories c, categories_i18n l
			WHERE c.id = l.category_id AND l.lang = '$SESSION[lang]' AND c.id = '$id'
			UNION ALL
			SELECT c.parent_id, c.name, '' AS lname
			FROM categories c
			WHERE c.id = '$id'
			AND c.id NOT IN (
				SELECT category_id FROM categories_i18n WHERE lang = '$SESSION[lang]'
			)
		");
		if ($qid->num_rows()) {
			$qid->next_record();
			$parent = $qid->f("parent_id");
			$name = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$name = "<a href='$CFG->wwwroot/shopping/$id.php'>" . stripslashes($name) . "</a>";
			$category_path = " &gt; " . $name . $category_path;
		} else {
			$parent = 1;
			$name = "";
		}
		$id = $parent;
	}
	$category_path = "<a href='$CFG->wwwroot/shopping/1.php'>Top</a>" . $category_path;
}

function get_subcategories(&$qid, $id) {

	global $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM categories c, categories_i18n l
		WHERE c.id = l.category_id AND l.lang = '$SESSION[lang]' AND c.parent_id = '$id' AND c.id > 1
		UNION ALL
		SELECT c.id, c.name, '' AS lname, c.special_level
		FROM categories c
		WHERE c.parent_id = '$id' AND c.id > 1
		AND c.id NOT IN (
			SELECT category_id FROM categories_i18n WHERE lang = '$SESSION[lang]'
		)
		ORDER BY special_level, name
	");
}

function build_catbrowser(&$qid, &$output, $parent=1, $indent=".") {

	global $SESSION;

	$qid->Query("
		CREATE TEMPORARY TABLE tmp_categories ()
		INHERITS (categories)
	");
	$qid->Query("
		INSERT INTO tmp_categories
		SELECT * FROM categories WHERE id <> 1
	");
	$qid->Query("
		CREATE TEMPORARY TABLE tmp_categories_i18n ()
		INHERITS (categories_i18n)
	");
	$qid->Query("
		INSERT INTO tmp_categories_i18n
		SELECT * FROM categories_i18n WHERE lang = '$SESSION[lang]' AND category_id <> 1
	");
	pg_build_catbrowser($qid, $output, $parent=1, $indent=".");
	$qid->Query("DROP TABLE tmp_categories");
	$qid->Query("DROP TABLE tmp_categories_i18n");
}

function pg_build_catbrowser(&$qid, &$output, $parent=1, $indent=".") {

	global $CFG, $SESSION, $HTTP_SERVER_VARS;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM tmp_categories c, tmp_categories_i18n l
		WHERE c.id = l.category_id AND c.parent_id = '$parent'
		UNION ALL
		SELECT c.id, c.name, '' AS lname, c.special_level
		FROM tmp_categories c
		WHERE c.parent_id = '$parent'
		AND c.id NOT IN (
			SELECT category_id FROM tmp_categories_i18n
		)
		ORDER BY special_level, name
	");

	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}
	for ($i=0; $i<$numr; $i++) {
		$appoggio = $cat[$i]["id"];
		$qid->Query("
			DELETE FROM tmp_categories_i18n
			WHERE category_id = $appoggio
		");
	}
	$qid->Query("
		DELETE FROM tmp_categories
		WHERE parent_id = '$parent'
	");

	for ($i=0; $i<$numr; $i++) {
		$output .= $indent . stripslashes($cat[$i]["name"]) . "^" . "$CFG->wwwroot" . "/shopping/" . $cat[$i]["id"] . ".php?". $HTTP_SERVER_VARS["QUERY_STRING"] . "^" . $SESSION["catbrowser_target"] . "\n";
		if ($cat[$i]["id"] != $parent)
			pg_build_catbrowser($qid, $output, $cat[$i]["id"], $indent . ".");
	}
}

function build_catbrowser_js(&$qid, &$output, $parent=1, $indent=".") {

	global $SESSION;

	$qid->Query("
		CREATE TEMPORARY TABLE tmp_categories ()
		INHERITS (categories)
	");
	$qid->Query("
		INSERT INTO tmp_categories
		SELECT * FROM categories WHERE id <> 1
	");
	$qid->Query("
		CREATE TEMPORARY TABLE tmp_categories_i18n ()
		INHERITS (categories_i18n)
	");
	$qid->Query("
		INSERT INTO tmp_categories_i18n
		SELECT * FROM categories_i18n WHERE lang = '$SESSION[lang]' AND category_id <> 1
	");
	pg_build_catbrowser_js($qid, $output, $parent=1, $indent=".");
	$qid->Query("DROP TABLE tmp_categories");
	$qid->Query("DROP TABLE tmp_categories_i18n");
}

function pg_build_catbrowser_js(&$qid, &$output, $parent=1, $indent=".") {

	global $CFG, $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM tmp_categories c, tmp_categories_i18n l
		WHERE c.id = l.category_id AND c.parent_id = '$parent'
		UNION ALL
		SELECT c.id, c.name, '' AS lname, c.special_level
		FROM tmp_categories c
		WHERE c.parent_id = '$parent'
		AND c.id NOT IN (
			SELECT category_id FROM tmp_categories_i18n
		)
		ORDER BY special_level, name
	");
	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}
	for ($i=0; $i<$numr; $i++) {
		$appoggio = $cat[$i]["id"];
		$qid->Query("
			DELETE FROM tmp_categories_i18n
			WHERE category_id = $appoggio
		");
	}
	$qid->Query("
		DELETE FROM tmp_categories
		WHERE parent_id = '$parent' AND id <> 1
	");

	for ($i=0; $i<$numr; $i++) {
			$output .= $indent . $cat[$i]["name"] . "^" . "$CFG->wwwroot" . "/shopping/" . $cat[$i]["id"] . ".php\n";
		if ($cat[$i]["id"] != $parent)
			pg_build_catbrowser_js($qid, $output, $cat[$i]["id"], $indent . ".");
	}
}

function get_cart_items_info(&$qid) {

	global $SESSION, $CART;

	$in_clause = $CART->get_productid_list();
	if (empty($in_clause))
		return false;

	$qid->Query("
		SELECT
			 p.id
			,p.name AS name
			,l.name AS lname
			,p.thumbwidth
			,p.thumbheight
		FROM products p, products_i18n l
		WHERE p.id = l.product_id AND l.lang = '$SESSION[lang]' AND p.id IN ($in_clause)
		UNION ALL
		SELECT
			 p.id
			,p.name AS name
			,'' AS lname
			,p.thumbwidth
			,p.thumbheight
		FROM products p
		WHERE p.id IN ($in_clause)
		AND p.id NOT IN (
			SELECT product_id
			FROM products_i18n
			WHERE lang = '$SESSION[lang]'
		)
	");

	return $qid->num_rows();
}

function get_order_items(&$qid, $id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 oi.product_id
			,oi.name AS oiname
			,oi.color
			,p.name
			,l.name AS lname
			,p.price AS curr_price
			,oi.price AS purchase_price
			,oi.discount AS purchase_discount
			,oi.discqty AS purchase_discqty
			,oi.iva AS purchase_iva
			,oi.weight AS purchase_weight
			,oi.qty
		FROM order_items oi, products p, products_i18n l
		WHERE oi.product_id = p.id AND oi.order_id = '$id' AND p.id = l.product_id AND l.lang = '$SESSION[lang]'
		UNION ALL
		SELECT
			 oi.product_id
			,oi.name AS oiname
			,oi.color
			,p.name
			,'' AS lname
			,p.price AS curr_price
			,oi.price AS purchase_price
 			,oi.discount AS purchase_discount
			,oi.discqty AS purchase_discqty
			,oi.iva AS purchase_iva
			,oi.weight AS purchase_weight
			,oi.qty
		FROM order_items oi, products p
		WHERE oi.product_id = p.id AND oi.order_id = '$id'
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[lang]'
		)
		UNION ALL
		SELECT
			 oi.product_id
			,oi.name AS oiname
			,oi.color
			,'' as name
			,'' as lname
			,0 as curr_price
			,oi.price AS purchase_price
			,oi.discount AS purchase_discount
			,oi.discqty AS purchase_discqty
			,oi.iva AS purchase_iva
			,oi.weight AS purchase_weight
			,oi.qty
		FROM order_items oi
		WHERE oi.order_id = '$id'
		AND oi.product_id NOT IN (
			SELECT id FROM products
		)
	");
}

function find_products(&$qid, $frm, &$page_number, &$num) {

	global $DBMS_FALSE, $DBMS_TRUE;

	$l_search_condition = "";
	$l_search_condition_for_union = "";

	$stringtsf_stripped = ereg_replace ("\+", "", $frm["stringtsf"]);

	$l_array_of_elements = explode(" ", $stringtsf_stripped);
	foreach($l_array_of_elements as $l_search_element) {
		if ($l_search_element != "") 
			if ($frm["case_sensitive"]) {
				$l_search_condition .=
					"( POSITION('$l_search_element' IN p.name) > 0 " .
					"OR POSITION('$l_search_element' IN l.name) > 0 " .
					"OR POSITION('$l_search_element' IN p.description) > 0 " .
					"OR POSITION('$l_search_element' IN l.description) > 0 " .
					"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
					"OR POSITION('$l_search_element' IN l.extended_description) > 0 " .
					") " . $frm["concatenation"] . " ";
				$l_search_condition_for_union .=
					"( POSITION('$l_search_element' IN p.name) > 0 " .
					"OR POSITION('$l_search_element' IN p.description) > 0 " .
					"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
					") " . $frm["concatenation"] . " ";
			} else {
				$l_search_condition .=
					"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.name)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.extended_description)) > 0 " .
					") " . $frm["concatenation"] . " ";
				$l_search_condition_for_union .=
					"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
					") " . $frm["concatenation"] . " ";
			}
	}

	if ($frm["concatenation"] == "OR") {
		$l_search_condition .= "$DBMS_FALSE";
		$l_search_condition_for_union .= "$DBMS_FALSE";
	} else {
		$l_search_condition .= "$DBMS_TRUE";
		$l_search_condition_for_union .= "$DBMS_TRUE";
	}

	return find_products_query($qid, $l_search_condition, $l_search_condition_for_union, $page_number, $num);
}

function advanced_find_products(&$qid, $frm, &$page_number, &$num) {

	global $DBMS_FALSE, $DBMS_TRUE;

	$l_search_condition = "";
	$l_search_condition_for_union = "";

	// Prepare for searching words in the product name
	$prod_stripped = ereg_replace ("\+", "", $frm["product_name_in"]);
	if (!empty($prod_stripped)) {
		$prod_concatenation = $frm["prod_concatenation"];
		$prod_case_sensitive = $frm["prod_case_sensitive"];

		$l_array_of_elements = explode(" ",$prod_stripped);
		foreach($l_array_of_elements as $l_search_element) {
			if ($l_search_element != "") {
				if ($prod_case_sensitive) {
					$prod_l_search_condition .=
						"( POSITION('$l_search_element' IN p.name) > 0 " .
						"OR POSITION('$l_search_element' IN l.name) > 0 " .
						") $prod_concatenation ";
					$prod_l_search_condition_for_union .=
						"( POSITION('$l_search_element' IN p.name) > 0 " .
						") $prod_concatenation ";
				} else {
					$prod_l_search_condition .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.name)) > 0 " .
						") $prod_concatenation ";
					$prod_l_search_condition_for_union .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
						") $prod_concatenation ";
				}
			}
		}

		if ($prod_concatenation == "OR") {
			$prod_l_search_condition .= "$DBMS_FALSE";
			$prod_l_search_condition_for_union .= "$DBMS_FALSE";
		} else {
			$prod_l_search_condition .= "$DBMS_TRUE";
			$prod_l_search_condition_for_union .= "$DBMS_TRUE";
		}
	}
	if (isset($prod_l_search_condition)) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "( " . $prod_l_search_condition . " )";
		$l_search_condition_for_union .= "( " . $prod_l_search_condition_for_union . " )";
	}

	// Matching the brand
	if (!empty($frm["brand_id"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= " brand_id = '$frm[brand_id]' ";
		$l_search_condition_for_union .= " brand_id = '$frm[brand_id]' ";
	}

	// Prepare for searching words in the product description
	$desc_stripped = ereg_replace ("\+", "", $frm["name_in"]);
	if (!empty($desc_stripped)) {
		$desc_concatenation = $frm["desc_concatenation"];
		$desc_case_sensitive = $frm["desc_case_sensitive"];

		$l_array_of_elements = explode(" ",$desc_stripped);
		foreach($l_array_of_elements as $l_search_element) {
			if ($l_search_element != "") {
				if ($desc_case_sensitive) {
					$desc_l_search_condition .=
						"( POSITION('$l_search_element' IN p.description) > 0 " .
						"OR POSITION('$l_search_element' IN l.description) > 0 " .
						"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
						"OR POSITION('$l_search_element' IN l.extended_description) > 0 " .
						") $desc_concatenation ";
					$desc_l_search_condition_for_union .=
						"( POSITION('$l_search_element' IN p.description) > 0 " .
						"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
						") $desc_concatenation ";
				} else {
					$desc_l_search_condition .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.extended_description)) > 0 " .
						") $desc_concatenation ";
					$desc_l_search_condition_for_union .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
						") $desc_concatenation ";
				}
			}
		}

		if ($desc_concatenation == "OR") {
			$desc_l_search_condition .= "$DBMS_FALSE";
			$desc_l_search_condition_for_union .= "$DBMS_FALSE";
		} else {
			$desc_l_search_condition .= "$DBMS_TRUE";
			$desc_l_search_condition_for_union .= "$DBMS_TRUE";
		}
	}
	if (isset($desc_l_search_condition)) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(" . $desc_l_search_condition . ")";
		$l_search_condition_for_union .= "(" . $desc_l_search_condition_for_union . ")";
	}

	// Prepare for searching products with prices within a limited range
	if (!empty($frm["price_min"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(price >= '$frm[price_min]')";
		$l_search_condition_for_union .= "(price >= '$frm[price_min]')";
	}
	if (!empty($frm["price_max"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(price <= '$frm[price_max]')";
		$l_search_condition_for_union .= "(price <= '$frm[price_max]')";
	}

	// Prepare for searching products with discount within a limited range
	if (!empty($frm["discount_min"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(discount >= '$frm[discount_min]')";
		$l_search_condition_for_union .= "(discount >= '$frm[discount_min]')";
	}
	if (!empty($frm["discount_max"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(discount <= '$frm[discount_max]')";
		$l_search_condition_for_union .= "(discount <= '$frm[discount_max]')";
	}

	// Prepare for searching products with discount qty within a limited range
	if (!empty($frm["discqty_min"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(discqty >= '$frm[discqty_min]')";
		$l_search_condition_for_union .= "(discqty >= '$frm[discqty_min]')";
	}
	if (!empty($frm["discqty_max"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(discqty <= '$frm[discqty_max]')";
		$l_search_condition_for_union .= "(discqty <= '$frm[discqty_max]')";
	}

	// Prepare for searching products with weight within a limited range
	if (!empty($frm["weight_min"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(weight >= '$frm[weight_min]')";
		$l_search_condition_for_union .= "(weight >= '$frm[weight_min]')";
	}
	if (!empty($frm["weight_max"])) {
		if (!empty($l_search_condition))
			$l_search_condition .= " AND ";
		if (!empty($l_search_condition_for_union))
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(weight <= '$frm[weight_max]')";
		$l_search_condition_for_union .= "(weight <= '$frm[weight_max]')";
	}

	return find_products_query($qid, $l_search_condition, $l_search_condition_for_union, $page_number, $num);
}

function find_products_query(&$qid, $l_search_condition, $l_search_condition_for_union, &$page_number, &$num) {

	global $SESSION;

	$num["rows"] = 0;
	$qid->Query("
		SELECT COUNT(p.id) AS cnt
		FROM products p, products_i18n l, brands b, iva i
		WHERE p.id = l.product_id AND l.lang = '$SESSION[lang]' AND p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition)
	");
	$qid->next_record();
	$num["rows"] += $qid->f("cnt");
	$qid->Query("
		SELECT COUNT(p.id) AS cnt
		FROM products p, brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition_for_union)
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[lang]'
		)
	");
	$qid->next_record();
	$num["rows"] += $qid->f("cnt");
	$num["pages"] = max(1, ceil($num["rows"]/$SESSION["products_per_catalog_page"]));
	$page_number = min($page_number, $num["pages"]);
	$first = ($page_number-1)*$SESSION["products_per_catalog_page"];
	$last = min($num["rows"]-1, $page_number*$SESSION["products_per_catalog_page"]-1);
	$limit = $last - $first + 1;
	$limit_clause = prepare_limit_clause($limit, $first);

	$qid->Query("
		SELECT
			 p.id
			,p.code
			,p.name
			,l.name AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.new_flag
			,p.new_level
			,p.nostock_flag
			,p.description
			,l.description AS ldescription
			,p.thumbwidth
			,p.thumbheight
		FROM products p, products_i18n l, brands b, iva i
		WHERE p.id = l.product_id AND l.lang = '$SESSION[lang]' AND p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition)
		UNION ALL
		SELECT
			 p.id
			,p.code
			,p.name
			,'' AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.new_flag
			,p.new_level
			,p.nostock_flag
			,p.description
			,'' AS ldescription
			,p.thumbwidth
			,p.thumbheight
		FROM products p, brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition_for_union)
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[lang]'
		)
		ORDER BY name, bname
		$limit_clause
	");
}

function get_zone_name(&$qid, $zone_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 z.id AS zone_id
			,z.name
			,l.name AS lname
		FROM zones z, zones_i18n l
		WHERE z.id = l.zone_id AND l.lang = '$SESSION[lang]' AND z.id = '$zone_id'
		UNION ALL
		SELECT
			 z.id AS zone_id
			,z.name
			,'' AS lname
		FROM zones z
		WHERE z.id = '$zone_id'
		AND z.id NOT IN (
			SELECT zone_id FROM zones_i18n WHERE lang = '$SESSION[lang]'
		)
	");
}

function get_zones_names(&$qid) {

	global $SESSION;

	$qid->Query("
		SELECT
			 z.id AS zone_id
			,z.name
			,l.name AS lname
		FROM zones z, zones_i18n l
		WHERE z.id = l.zone_id AND l.lang = '$SESSION[lang]'
		UNION ALL
		SELECT
			 z.id AS zone_id
			,z.name
			,'' AS lname
		FROM zones z
		WHERE z.id NOT IN (
			SELECT zone_id FROM zones_i18n WHERE lang = '$SESSION[lang]'
		)
		ORDER BY name
	");
}

?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

/**
* It finds the path of the product categories, starting from the given category and ending with the Top
* @param integer $id the category id for which the category path
*   has to be found
* @param string $category_path the string that stores the resulting
*   category path
* @returns void
*/
function build_category_path($id=1, &$category_path) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	while ($id > 1) {
		$qid->pgm_lang_join_query(
			$CFG["base_lang"],
			array("categories c", "categories_i18n l"),
			array(array("c.id", "l.category_id", 1, $PGM_SESSION["lang"])),
			array(array("l.name AS lname")),
			"c.parent_id, c.name",
			"",
			"c.id = '$id'",
			"",
			array()
		);
		if ($qid->num_rows()) {
			$qid->next_record();
			$parent = $qid->f("parent_id");
			$name = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$name = "<a href=\"" . $CFG["wwwroot"] . "shopping/$id.php\">" . stripslashes($name) . "</a>";
			$category_path = " &gt; " . $name . $category_path;
		} else {
			$parent = 1;
			$name = "";
		}
		$id = $parent;
	}
	$category_path = "<a href=\"" . $CFG["wwwroot"] . "shopping/1.php\">Top</a>" . $category_path;
}

function get_product_color_options($product_id, &$output, $options=1) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("colors c", "colors_i18n l"),
		array(array("c.id", "l.color_id", 1, $PGM_SESSION["lang"])),
		array(array("l.name AS lname")),
		"c.id, c.name",
		"products_colors pc",
		"pc.color_id = c.id AND pc.product_id = '$product_id'",
		"name",
		array()
	);

	if ($options) {
		$output = "<option value=\"1\"></option>\n";
		while ($qid->next_record()) {
			$foobar = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$output .= "<option value=\"" . ov($qid->f("id")) . "\">" . ov($foobar) . "</option>\n";
		}
	} else {
		$output = "";
		while ($qid->next_record()) {
			$foobar = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
			$output .= "<li>" . ov($foobar) . "</li>\n";
		}
		$output = "<ul>" . $output . "</ul>";
	}

	return $qid->num_rows();
}

function build_color_options(&$output, &$preselected) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("colors c", "colors_i18n l"),
		array(array("c.id", "l.color_id", 1, $PGM_SESSION["lang"])),
		array(array("l.name AS lname")),
		"c.id, c.name",
		"",
		"c.id > 1",
		"name",
		""
	);
	$count_preselected = count($preselected);
	while ($qid->next_record()) {
		$foobar = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
		if ($count_preselected > 0) {
			$selected = in_array($qid->f("id"), $preselected) ? "selected" : "";
		} else {
			$selected = "";
		}
		$output .= "<option value=\"" . ov($qid->f("id")) . "\" $selected>" . ov($foobar);
	}
}

function get_zone_name(&$qid, $zone_id) {
	global $PGM_SESSION;
	global $CFG;

	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("zones z", "zones_i18n l"),
		array(array("z.id", "l.zone_id", 1, $PGM_SESSION["lang"])),
		array(array("l.name AS lname")),
		"z.id AS zone_id, z.name",
		"",
		"id = '$zone_id'",
		"",
		array()
	);
}

function get_zones_names(&$qid) {
	global $PGM_SESSION;
	global $CFG;

	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("zones z", "zones_i18n l"),
		array(array("z.id", "l.zone_id", 1, $PGM_SESSION["lang"])),
		array(array("l.name AS lname")),
		"z.id AS zone_id, z.name",
		"",
		"",
		"name",
		array()
	);
}

function refresh_cache() {
	global $PGM_SESSION;
	global $CFG;
	include_once ($CFG["libdir"] . "phplayersmenu/layersmenu.inc.php");

	$save_lang = $PGM_SESSION["lang"];

	$qid = new PGM_Sql("SELECT lang FROM languages");
	$languages = array();
	$languages[0] = $CFG["base_lang"];
	$i = 0;
	while ($qid->next_record()) {
		$i++;
		$languages[$i] = $qid->f("lang");
	}
	$num_languages = count($languages);

	$qid->query("DELETE FROM cache");

	for ($i=0; $i<$num_languages; $i++) {
		$PGM_SESSION["lang"] = $languages[$i];
		build_catbrowser_js($categories);
		if ($categories != "") {
			$mid = new LayersMenu(100);
			$mid->set_separator("^");
			$mid->set_dirroot($CFG["dirroot"]);
			$mid->set_libdir($CFG["libdir"] . "phplayersmenu/");
			$mid->set_libwww($CFG["libwww"] . "phplayersmenu/");
			$mid->set_tpldir($CFG["dirroot"] . "templates/");
			$mid->set_imgdir($CFG["imagesdir"]);
			$mid->set_imgwww($CFG["imageswww"]);
			$mid->set_forward_arrow_img("forward-gnome.png");
			$mid->set_vertical_menu_tpl("layersmenu-vertical_menu.ihtml");
			$mid->set_sub_menu_tpl("layersmenu-sub_menu.ihtml");
			$mid->set_menu_structure_string($categories);
			$mid->parse_menu_structure("vermenu");
			$mid->new_vertical_menu("vermenu");
			$qid->query("
				INSERT INTO cache (
					 lang
					,header
					,first_level_menu
					,footer
				)
				VALUES (
					 '" . $PGM_SESSION["lang"] . "'
					,'" . addslashes($mid->make_header()) . "'
					,'" . addslashes($mid->get_menu("vermenu")) . "'
					,'" . addslashes($mid->make_footer()) . "'
				)
			");
		}
	}

	$PGM_SESSION["lang"] = $save_lang;
}

function return_star() {
	return "<font color=\"#ff0000\">*</font>";
}

/* if $errorvar is set, then returns an error marker << */
function errmsg($errorvar) {
	if ($errorvar) {
		return "<font color=\"#ff0000\">&lt;&lt;</font>";
	}
}

/* like errmsg(), but returns the marker >> */
function errmsg2($errorvar) {
	if ($errorvar) {
		return "<font color=\"#ff0000\">&gt;&gt;</font>";
	}
}

function getmicrotime(){
	list($usec, $sec) = explode(" ", microtime());
	return ((float) $usec + (float) $sec);
}
?>

<?php
/**
* PGM extension to the PHPLib DB_Sql class for PostgreSQL
*
* (C) 2002 Marco Pratesi (marco@pgmarket.net)
* Released under the GNU LGPL
*/
class PGM_Sql extends DB_Sql {

/**
* Method to be invoked to begin a transaction.
* @access public
* @return void
*/
function begin() {
	ignore_user_abort(1);
	$this->query("BEGIN");
}

/**
* Method to be invoked to end a transaction.
* @access public
* @return void
*/
function commit() {
	$this->query("COMMIT");
	ignore_user_abort(0);
}

/**
* This method addresses LEFT JOINs, especially the ones involving I18N tables.
*
* It allows to provide both DB astraction (as an example, really different
* queries have to be used on PostgreSQL <= 7.0 and on MySQL 3.23 to perform
* LEFT JOINs) and to simplify (and, hence, lighten) the query
* when LEFT JOINs with I18N tables are not needed, i.e.,
* when the current language coincides with the base language,
* i.e. the language of the base, non I18N, tables.
* Note: no check is performed about correctness/coherence of passed arguments.
* @access public
* @param string $base_lang The base language (of the shop);
*   example: $CFG["base_lang"]
* @param array $left_join_table N-elements array of strings,
*   each one identifying a table and its respective alias;
*   this array lists tables involved in LEFT JOINs for the query at hand;
*   example: array("products p", "products_i18n l")
* @param array $left_join_fields (N-1)-elements array of arrays;
*   the i-th array concerns the LEFT JOIN between the i-th and the (i+1)-th
*   tables of the $left_join_table array;
*   each array consists of 4 fields, i.e. the left and the right field
*   involved in the LEFT JOIN, a boolean field (0 or 1) indicating if
*   the language is involved in the LEFT JOIN clause, and a string indicating
*   the I18N language; the resulting query is simpler if this last field,
*   i.e. the I18N language, is equal to the base language, i.e. $base_lang;
*   example: array(array("p.id", "l.product_id", 1, $SESSION["lang"]))
* @param array $left_join_wanted_field (N-1)-elements array of arrays;
*   the i-th array concerns fields and respective aliases
*   of the (i+1)-th table of the $left_join_table array;
*   each array lists fields that have to be returned by the query;
*   example: array(array("l.name AS lname",  "l.description AS ldescription"))
* @param string $other_fields it lists other fields that must be used
*   in the SELECT clause, both for tables that are involved in the LEFT JOINs
*   and for tables that are not;
*   example: "p.id, p.name, b.name AS bname, p.price, i.iva, p.discount, p.discqty, p.weight, p.special_level, p.description, p.thumbtype, p.thumbwidth, p.thumbheight"
* @param string $other_tables it lists other tables (and respective aliases)
*   that are not involved in the LEFT JOINs but must be used in the FROM clause;
*   example: "brands b, iva i"
* @param string $additional_where_clause conditions to be used in the WHERE
*   clause in addition to the LEFT JOINs conditions;
*   example: "p.brand_id = b.id AND p.iva_id = i.id AND special_flag = 1"
* @param string $order_by_clause the ORDER BY clause; it can be an empty string;
*   example: "special_level ASC"
* @param array $limits two-fields array used for the LIMIT clause;
*   it provides $limit_offset and $limit_numitems, i.e. the first element
*   to be returned by the query and the maximum number of elements
*   to be returned by the query; it can be an empty array;
*   example: array()
*   example: array($first, $limit)
* @return Query_ID
*/
function pgm_lang_join_query(
	$base_lang,
	$left_join_table,
	$left_join_fields,
	$left_join_wanted_field,
	$other_fields,
	$other_tables,
	$additional_where_clause,
	$order_by_clause,
	$limits
) {
	$num_join_tables = count($left_join_table);

	for ($i=0; $i<$num_join_tables; $i++) {
		$foobar = explode(" ", $left_join_table[$i]);
		$left_join_table_name[$i] = $foobar[0];
		$left_join_table_alias[$i] = $foobar[1];
	}
	for ($i=0; $i<$num_join_tables-1; $i++) {
		$left_join_left_field[$i] = $left_join_fields[$i][0];
		$left_join_right_field[$i] = $left_join_fields[$i][1];
		$lang_array[$i] = $left_join_fields[$i][2];
		if ($lang_array[$i]) {
			$lang[$i] = $left_join_fields[$i][3];
		}
		$counter = count($left_join_wanted_field[$i]);
		for ($cnt=0; $cnt<$counter; $cnt++) {
			$foobar = explode(" AS ", $left_join_wanted_field[$i][$cnt]);
			$left_join_wanted_field_name[$i][$cnt] = $foobar[0];
			$left_join_wanted_field_alias[$i][$cnt] = $foobar[1];
		}
	}

	$union_query = "";

	$lang_array[$num_join_tables-1] = "";
	$lang[$num_join_tables-1] = "";
	for ($query_count=0; $query_count<$num_join_tables; $query_count++) {

		if (!($lang_array[$query_count] && $lang[$query_count] == $base_lang))
		{
		$select_clause[$query_count] = "";
		for ($i=0; $i<$query_count; $i++) {
			$counter = count($left_join_wanted_field[$i]);
			for ($cnt=0; $cnt<$counter; $cnt++) {
				$select_clause[$query_count] .= ", '' AS " . $left_join_wanted_field_alias[$i][$cnt];
			}
		}
		for ($i=$query_count; $i<$num_join_tables-1; $i++) {
			$counter = count($left_join_wanted_field[$i]);
			for ($cnt=0; $cnt<$counter; $cnt++) {
				$select_clause[$query_count] .= ", " . $left_join_wanted_field[$i][$cnt];
			}
		}
		if ($other_fields != "") {
			$select_clause[$query_count] .= ", " . $other_fields;
		}
		$select_clause[$query_count] = "SELECT " . substr($select_clause[$query_count], 2);

		$from_clause[$query_count] = " FROM " . $left_join_table[0];
		$where_clause[$query_count] = "";
		for ($i=$query_count+1; $i<$num_join_tables; $i++) {
			$from_clause[$query_count] .= ", " . $left_join_table[$i];
			$foobar = ($left_join_table_alias[$i] != "") ? $left_join_table_alias[$i] : $left_join_table_name[$i];
			$where_clause[$query_count] .= " AND " . $left_join_left_field[$i-1] . " = " . $left_join_right_field[$i-1];
			if ($lang_array[$i-1]) {
				$where_clause[$query_count] .= " AND " . $foobar . ".lang = '" . $lang[$i-1] . "'";
			}
		}
		if ($other_tables != "") {
			$from_clause[$query_count] .= ", " . $other_tables;
		}
		if ($where_clause[$query_count] != "") {
			$where_clause[$query_count] = substr($where_clause[$query_count], 5);
		}
		if ($where_clause[$query_count] != "" && $additional_where_clause != "") {
			$where_clause[$query_count] = $where_clause[$query_count] . " AND " . $additional_where_clause;
		} else if ($where_clause[$query_count] == "") {
			$where_clause[$query_count] = $additional_where_clause;
		}
		if ($query_count>0) {
			if ($where_clause[$query_count] != "") {
				$where_clause[$query_count] .= " AND ";
			}
			$where_clause[$query_count] .= $left_join_left_field[$query_count-1] . " NOT IN ( SELECT " . $left_join_right_field[$query_count-1] . " FROM " . $left_join_table[$query_count];
			if ($lang_array[$query_count-1]) {
				$where_clause[$query_count] .= " WHERE " . $left_join_table_alias[$query_count] . ".lang = '" . $lang[$query_count-1] . "'";
			}
			$where_clause[$query_count] .= " )";
		}
		$where_clause[$query_count] = " WHERE " . $where_clause[$query_count];

		$query[$query_count] = $select_clause[$query_count] . "\n" . $from_clause[$query_count] . "\n" . $where_clause[$query_count] . "\n";
		$union_query .= "UNION ALL\n" . $query[$query_count];
		}
	}
	$union_query = substr($union_query, 10);

	if ($order_by_clause != "") {
		$order_by_clause = " ORDER BY " . $order_by_clause;
	}

	if (count($limits) == 2) {
		if ($limits[1] == 0) {
			$limits[1] = 1;	// workaround needed for PostgreSQL 7.0
		}
		$limit_clause = " LIMIT " . $limits[1] . ", " . $limits[0];
	} else {
		$limit_clause = "";
	}

	$union_query .= $order_by_clause . "\n" . $limit_clause;
	//echo "union query is $union_query<br>";
	return $this->query($union_query);
}

} /* END OF CLASS */

?>

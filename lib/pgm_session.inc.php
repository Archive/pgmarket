<?php

function pgm_session_open(&$PGM_SESSION, $session_name = "pgm_example_session") {
    if (isset($_SERVER["HTTP_USER_AGENT"])) {
        $spiders = array('Googlebot','WebCrawler','TurnitinBot','Slurp', 'Scooter', 'Szukacz');
        foreach($spiders as $Val)
        {
            if (eregi($Val, $_SERVER["HTTP_USER_AGENT"]))
            {
				define('SID','');
                return;
            }
        } 
	}
	if ($session_name == "") {
		$session_name = "pgm_example_session";
	}
	session_start();
	if (!isset($_SESSION)) {	// For compatibility with PHP 4.0
		session_register("PGM_SESSION");
		if (!isset($PGM_SESSION)) {
			$PGM_SESSION = array();
		}
/*
		global $HTTP_SESSION_VARS;
		if (isset($HTTP_SESSION_VARS[$session_name])) {
			$PGM_SESSION = $HTTP_SESSION_VARS[$session_name];
		}
*/
	} else {
		$PGM_SESSION = array();
		if (isset($_SESSION[$session_name])) {
			$PGM_SESSION = $_SESSION[$session_name];
		}
	}
}

function pgm_session_close(&$PGM_SESSION, $session_name = "pgm_example_session") {
	if ($session_name == "") {
		$session_name = "pgm_example_session";
	}
	if (!isset($_SESSION)) {	// For compatibility with PHP 4.0
/*
		global $HTTP_SESSION_VARS;
		$HTTP_SESSION_VARS[$session_name] = array();
		$HTTP_SESSION_VARS[$session_name] = $PGM_SESSION;
*/
	} else {
		$_SESSION[$session_name] = array();
		$_SESSION[$session_name] = $PGM_SESSION;
	}
}

function pgm_session_id() {
	return session_id();
}

?>

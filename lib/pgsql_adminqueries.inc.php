<?php
/* (c) 2000-2001 Marco Pratesi <pratesi@telug.it> */
/* (c) 2000-2001 Fabio Molinari <fabio.m@mclink.it> */

/* We use UNIONs and not OUTER JOINS to support PostgreSQL < 7.1 */

$DBMS_FALSE = "FALSE";
$DBMS_TRUE = "TRUE";

function update_sequences(&$qid) {
	$tables = array("user_discounts", "zones", "categories", "brands", "iva", "colors", "products", "orders_states");
	foreach ($tables as $table) {
		$max = get_table_id_max($qid, $table);
		if ($max == 0)
			$max = 1;
		$qid->Query("SELECT setval('$table" . "_id_seq', $max);");
	}
}

function get_table_id_max(&$qid, $table) {
	$qid->Query("SELECT id FROM $table ORDER BY id DESC LIMIT 1,0");
	$qid->next_record();
	return $qid->f("id");
}

function get_category_info(&$qid, $id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id
			,c.name
			,l.name AS lname
			,c.special_level
			,c.description
		FROM categories c, categories_i18n l
		WHERE c.id = l.category_id AND l.lang = '$SESSION[i18n]' AND c.id = '$id'
		UNION ALL
		SELECT
			 c.id
			,c.name
			,'' AS lname
			,c.special_level
			,c.description
		FROM categories c
		WHERE c.id = '$id'
		AND c.id NOT IN (
			SELECT category_id FROM categories_i18n WHERE lang = '$SESSION[i18n]'
		)
	");
}

function get_allcategories_info(&$qid, $limit_clause) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id
			,c.name
			,l.name AS lname
			,c.special_level
			,c.description
			,parent.name AS parent
		FROM categories c, categories_i18n l, categories parent
		WHERE c.id = l.category_id AND c.parent_id = parent.id AND l.lang = '$SESSION[i18n]' AND c.id > 1
		UNION ALL
		SELECT
			 c.id
			,c.name
			,'' AS lname
			,c.special_level
			,c.description
			,parent.name AS parent
		FROM categories c, categories parent
		WHERE c.parent_id = parent.id AND c.id > 1
		AND c.id NOT IN (
			SELECT category_id FROM categories_i18n WHERE lang = '$SESSION[i18n]'
		)
		ORDER BY name, special_level
		$limit_clause
	");
}

function build_category_tree(&$qid, &$output, &$preselected, $parent=1, $indent="") {

	global $SESSION;

	$qid->Query("
		CREATE TEMPORARY TABLE tmp_categories ()
		INHERITS (categories)
	");
	$qid->Query("
		INSERT INTO tmp_categories
		SELECT * FROM categories
	");
	$qid->Query("
		CREATE TEMPORARY TABLE tmp_categories_i18n ()
		INHERITS (categories_i18n)
	");
	$qid->Query("
		INSERT INTO tmp_categories_i18n
		SELECT * FROM categories_i18n WHERE lang = '$SESSION[lang]' AND category_id <> 1
	");
	pg_build_category_tree($qid, $output, $preselected, $parent=1, $indent="");
	$qid->Query("DROP TABLE tmp_categories");
	$qid->Query("DROP TABLE tmp_categories_i18n");
}

function pg_build_category_tree(&$qid, &$output, &$preselected, $parent=1, $indent="") {
/* recursively go through the category tree, starting at a parent, and
 * drill down, printing options for a selection list box.  preselected
 * items are marked as being selected.  this is not an efficient algorithm
 * because it has to issue one query per category!!  it's only used because it
 * is easy to understand.
 */

	global $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM tmp_categories c, tmp_categories_i18n l
		WHERE c.id = l.category_id AND c.parent_id = '$parent'
		UNION ALL
		SELECT c.id, c.name, '' AS lname, c.special_level
		FROM tmp_categories c
		WHERE c.parent_id = '$parent'
		AND c.id NOT IN (
			SELECT category_id FROM tmp_categories_i18n
		)
		ORDER BY special_level, name
	");

	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}
	for ($i=0; $i<$numr; $i++) {
		$appoggio = $cat[$i]["id"];
		$qid->Query("
			DELETE FROM tmp_categories_i18n
			WHERE category_id = $appoggio
		");
	}
	$qid->Query("
		DELETE FROM tmp_categories
		WHERE parent_id = '$parent'
	");

	for ($i=0; $i<$numr; $i++) {
		$selected = in_array($cat[$i]["id"], $preselected) ? "selected" : "";
		$output .= "<option value=\"" . ov($cat[$i]["id"]) . "\" $selected>" . $indent . ov($cat[$i]["name"]);
		if ($cat[$i]["id"] != $parent) {
			pg_build_category_tree($qid, $output, $preselected, $cat[$i]["id"], $indent."&nbsp;&nbsp;");
		}
	}
}

function get_color_info(&$qid, $color_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id AS color_id
			,c.name
			,l.name AS lname
			,c.description
		FROM colors c, colors_i18n l
		WHERE c.id = l.color_id AND l.lang = '$SESSION[i18n]' AND c.id = '$color_id'
		UNION ALL
		SELECT
			 c.id AS color_id
			,c.name
			,'' AS lname
			,c.description
		FROM colors c
		WHERE c.id = '$color_id'
		AND c.id NOT IN (
			SELECT color_id FROM colors_i18n WHERE lang = '$SESSION[i18n]'
		)
	");
}

function get_allcolors_info(&$qid, $limit_clause) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id AS color_id
			,c.name
			,l.name AS lname
			,c.description
		FROM colors c, colors_i18n l
		WHERE c.id = l.color_id AND l.lang = '$SESSION[i18n]' AND c.id > 0
		UNION ALL
		SELECT
			 c.id AS color_id
			,c.name
			,'' AS lname
			,c.description
		FROM colors c
		WHERE c.id > 0
		AND c.id NOT IN (
			SELECT color_id FROM colors_i18n WHERE lang = '$SESSION[i18n]'
		)
		ORDER BY name
		$limit_clause
	");
}

function build_color_options(&$qid, &$output, &$preselected) {

	global $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname
		FROM colors c, colors_i18n l
		WHERE c.id = l.color_id AND l.lang = '$SESSION[lang]' AND c.id > 0
		UNION ALL
		SELECT c.id, c.name, '' AS lname
		FROM colors c
		WHERE c.id > 0
		AND c.id NOT IN (
			SELECT color_id FROM colors_i18n WHERE lang = '$SESSION[lang]'
		)
		ORDER BY name
	");

	$count_preselected = count($preselected);
	while ($qid->next_record()) {
		$appoggio = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
		if ($count_preselected > 0)
			$selected = in_array($qid->f("id"), $preselected) ? "selected" : "";
		else
			$selected = "";
		$output .= "<option value=\"" . ov($qid->f("id")) . "\" $selected>" . ov($appoggio);
	}
}

function get_product_info(&$qid, $product_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id AS product_id
			,p.name
			,l.name AS lname
			,p.description
			,l.description AS ldescription
			,p.extended_description
			,l.extended_description AS lextended_description
		FROM products p, products_i18n l
		WHERE p.id = l.product_id AND l.lang = '$SESSION[i18n]' AND p.id = '$product_id'
		UNION ALL
		SELECT
			 p.id AS product_id
			,p.name
			,'' AS lname
			,p.description
			,'' AS ldescription
			,p.extended_description
			,'' AS lextended_description
		FROM products p
		WHERE p.id = '$product_id'
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[i18n]'
		)
	");
}

function get_allproducts_info_i18n(&$qid, $limit_clause) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id AS product_id
			,p.name
			,l.name AS lname
			,p.special_level
			,p.description
		FROM products p, products_i18n l
		WHERE p.id = l.product_id AND l.lang = '$SESSION[i18n]'
		UNION ALL
		SELECT
			 p.id AS product_id
			,p.name
			,'' AS lname
			,p.special_level
			,p.description
		FROM products p
		WHERE p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '$SESSION[i18n]'
		)
		ORDER BY name, special_level
		$limit_clause
	");
}

function get_zone_info(&$qid, $zone_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 z.id AS zone_id
			,z.name
			,l.name AS lname
			,z.description
		FROM zones z, zones_i18n l
		WHERE z.id = l.zone_id AND l.lang = '$SESSION[i18n]' AND z.id = '$zone_id'
		UNION ALL
		SELECT
			 z.id AS zone_id
			,z.name
			,'' AS lname
			,z.description
		FROM zones z
		WHERE z.id = '$zone_id'
		AND z.id NOT IN (
			SELECT zone_id FROM zones_i18n WHERE lang = '$SESSION[i18n]'
		)
	");
}

function get_allzones_info(&$qid, $limit_clause) {

	global $SESSION;

	$qid->Query("
		SELECT
			 z.id AS zone_id
			,z.name
			,l.name AS lname
			,z.description
		FROM zones z, zones_i18n l
		WHERE z.id = l.zone_id AND l.lang = '$SESSION[i18n]'
		UNION ALL
		SELECT
			 z.id AS zone_id
			,z.name
			,'' AS lname
			,z.description
		FROM zones z
		WHERE z.id NOT IN (
			SELECT zone_id FROM zones_i18n WHERE lang = '$SESSION[i18n]'
		)
		ORDER BY name
		$limit_clause
	");
}

?>

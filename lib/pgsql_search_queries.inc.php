<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

// We use UNIONs and not OUTER JOINS to support PostgreSQL < 7.1

function find_products(&$qid, $frm, &$page_number, &$result) {
	global $DBMS_FALSE, $DBMS_TRUE;

	$l_search_condition = "";
	$l_search_condition_for_union = "";

	$stringtsf_stripped = ereg_replace ("\+", "", $frm["stringtsf"]);

	$l_array_of_elements = explode(" ", $stringtsf_stripped);
	reset($l_array_of_elements);
	while (list(, $l_search_element) = each($l_array_of_elements)) {
		if ($l_search_element != "") {
			if (nvl($frm["case_sensitive"], 0)) {
				$l_search_condition .=
					"( POSITION('$l_search_element' IN p.name) > 0 " .
					"OR POSITION('$l_search_element' IN l.name) > 0 " .
					"OR POSITION('$l_search_element' IN p.description) > 0 " .
					"OR POSITION('$l_search_element' IN l.description) > 0 " .
					"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
					"OR POSITION('$l_search_element' IN l.extended_description) > 0 " .
					") " . $frm["concatenation"] . " ";
				$l_search_condition_for_union .=
					"( POSITION('$l_search_element' IN p.name) > 0 " .
					"OR POSITION('$l_search_element' IN p.description) > 0 " .
					"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
					") " . $frm["concatenation"] . " ";
			} else {
				$l_search_condition .=
					"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.name)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(l.extended_description)) > 0 " .
					") " . $frm["concatenation"] . " ";
				$l_search_condition_for_union .=
					"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
					"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
					") " . $frm["concatenation"] . " ";
			}
		}
	}

	if ($frm["concatenation"] == "OR") {
		$l_search_condition .= "$DBMS_FALSE";
		$l_search_condition_for_union .= "$DBMS_FALSE";
	} else {
		$l_search_condition .= "$DBMS_TRUE";
		$l_search_condition_for_union .= "$DBMS_TRUE";
	}

	return find_products_query($qid, $l_search_condition, $l_search_condition_for_union, $page_number, $result);
}

function advanced_find_products(&$qid, $frm, &$page_number, &$result) {
	global $DBMS_FALSE, $DBMS_TRUE;

	$l_search_condition = "";
	$l_search_condition_for_union = "";
	$prod_l_search_condition = "";
	$prod_l_search_condition_for_union = "";
	$desc_l_search_condition = "";	
	$desc_l_search_condition_for_union = "";	

	// Prepare for searching words in the product name
	$prod_stripped = ereg_replace ("\+", "", $frm["product_name_in"]);
	if ($prod_stripped != "") {
		$prod_concatenation = $frm["prod_concatenation"];
		$prod_case_sensitive = $frm["prod_case_sensitive"];

		$l_array_of_elements = explode(" ",$prod_stripped);
		reset($l_array_of_elements);
		while (list(, $l_search_element) = each($l_array_of_elements)) {
			if ($l_search_element != "") {
				if ($prod_case_sensitive) {
					$prod_l_search_condition .=
						"( POSITION('$l_search_element' IN p.name) > 0 " .
						"OR POSITION('$l_search_element' IN l.name) > 0 " .
						") $prod_concatenation ";
					$prod_l_search_condition_for_union .=
						"( POSITION('$l_search_element' IN p.name) > 0 " .
						") $prod_concatenation ";
				} else {
					$prod_l_search_condition .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.name)) > 0 " .
						") $prod_concatenation ";
					$prod_l_search_condition_for_union .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.name)) > 0 " .
						") $prod_concatenation ";
				}
			}
		}

		if ($prod_concatenation == "OR") {
			$prod_l_search_condition .= "$DBMS_FALSE";
			$prod_l_search_condition_for_union .= "$DBMS_FALSE";
		} else {
			$prod_l_search_condition .= "$DBMS_TRUE";
			$prod_l_search_condition_for_union .= "$DBMS_TRUE";
		}
	}
	if ($prod_l_search_condition != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "( " . $prod_l_search_condition . " )";
		$l_search_condition_for_union .= "( " . $prod_l_search_condition_for_union . " )";
	}

	// Matching the brand
	if ($frm["brand_id"] > 1) {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= " brand_id = '" . $frm["brand_id"] . "' ";
		$l_search_condition_for_union .= " brand_id = '" . $frm["brand_id"] . "' ";
	}

	// Prepare for searching words in the product description
	$desc_stripped = ereg_replace ("\+", "", $frm["name_in"]);
	if ($desc_stripped != "") {
		$desc_concatenation = $frm["desc_concatenation"];
		$desc_case_sensitive = $frm["desc_case_sensitive"];

		$l_array_of_elements = explode(" ",$desc_stripped);
		reset($l_array_of_elements);
		while (list(, $l_search_element) = each($l_array_of_elements)) {
			if ($l_search_element != "") {
				if ($desc_case_sensitive) {
					$desc_l_search_condition .=
						"( POSITION('$l_search_element' IN p.description) > 0 " .
						"OR POSITION('$l_search_element' IN l.description) > 0 " .
						"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
						"OR POSITION('$l_search_element' IN l.extended_description) > 0 " .
						") $desc_concatenation ";
					$desc_l_search_condition_for_union .=
						"( POSITION('$l_search_element' IN p.description) > 0 " .
						"OR POSITION('$l_search_element' IN p.extended_description) > 0 " .
						") $desc_concatenation ";
				} else {
					$desc_l_search_condition .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(l.extended_description)) > 0 " .
						") $desc_concatenation ";
					$desc_l_search_condition_for_union .=
						"( POSITION(LOWER('$l_search_element') IN LOWER(p.description)) > 0 " .
						"OR POSITION(LOWER('$l_search_element') IN LOWER(p.extended_description)) > 0 " .
						") $desc_concatenation ";
				}
			}
		}

		if ($desc_concatenation == "OR") {
			$desc_l_search_condition .= "$DBMS_FALSE";
			$desc_l_search_condition_for_union .= "$DBMS_FALSE";
		} else {
			$desc_l_search_condition .= "$DBMS_TRUE";
			$desc_l_search_condition_for_union .= "$DBMS_TRUE";
		}
	}
	if ($desc_l_search_condition != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(" . $desc_l_search_condition . ")";
		$l_search_condition_for_union .= "(" . $desc_l_search_condition_for_union . ")";
	}

	// Prepare for searching products with prices within a limited range
	if ($frm["price_min"] != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(price >= '" . $frm["price_min"] . "')";
		$l_search_condition_for_union .= "(price >= '" . $frm["price_min"] . "')";
	}
	if ($frm["price_max"] != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(price <= '" . $frm["price_max"] . "')";
		$l_search_condition_for_union .= "(price <= '" . $frm["price_max"] . "')";
	}

	// Prepare for searching products with discount within a limited range
	if ($frm["discount_min"] != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(discount >= '" . $frm["discount_min"] . "')";
		$l_search_condition_for_union .= "(discount >= '" . $frm["discount_min"] . "')";
	}
	if ($frm["discount_max"] != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(discount <= '" . $frm["discount_max"] . "')";
		$l_search_condition_for_union .= "(discount <= '" . $frm["discount_max"] . "')";
	}

	// Prepare for searching products with discount qty within a limited range
	if ($frm["discqty_min"] != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(discqty >= '" . $frm["discqty_min"] . "')";
		$l_search_condition_for_union .= "(discqty >= '" . $frm["discqty_min"] . "')";
	}
	if ($frm["discqty_max"] != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(discqty <= '" . $frm["discqty_max"] . "')";
		$l_search_condition_for_union .= "(discqty <= '" . $frm["discqty_max"] . "')";
	}

	// Prepare for searching products with weight within a limited range
	if ($frm["weight_min"] != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(weight >= '" . $frm["weight_min"] . "')";
		$l_search_condition_for_union .= "(weight >= '" . $frm["weight_min"] . "')";
	}
	if ($frm["weight_max"] != "") {
		if ($l_search_condition != "")
			$l_search_condition .= " AND ";
		if ($l_search_condition_for_union != "")
			$l_search_condition_for_union .= " AND ";
		$l_search_condition .= "(weight <= '" . $frm["weight_max"] . "')";
		$l_search_condition_for_union .= "(weight <= '" . $frm["weight_max"] . "')";
	}

	return find_products_query($qid, $l_search_condition, $l_search_condition_for_union, $page_number, $result);
}

function find_products_query(&$qid, $l_search_condition, $l_search_condition_for_union, &$page_number, &$result) {
	global $PGM_SESSION;
	global $CFG, $ME;

	$num_rows = 0;
	$qid->query("
		SELECT COUNT(p.id) AS cnt
		FROM products p, products_i18n l, brands b, iva i
		WHERE p.id = l.product_id AND l.lang = '" . $PGM_SESSION["lang"] . "' AND p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition)
	");
	$qid->next_record();
	$num_rows += $qid->f("cnt");
	$qid->query("
		SELECT COUNT(p.id) AS cnt
		FROM products p, brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition_for_union)
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '" . $PGM_SESSION["lang"] . "'
		)
	");
	$qid->next_record();
	$num_rows += $qid->f("cnt");
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $num_rows, $PGM_SESSION["products_per_catalog_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);

	$qid->query("
		SELECT
			 p.id
			,p.code
			,p.name
			,l.name AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.description
			,l.description AS ldescription
			,p.thumbtype
			,p.thumbwidth
			,p.thumbheight
		FROM products p, products_i18n l, brands b, iva i
		WHERE p.id = l.product_id AND l.lang = '" . $PGM_SESSION["lang"] . "' AND p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition)
		UNION ALL
		SELECT
			 p.id
			,p.code
			,p.name
			,'' AS lname
			,b.name AS bname
			,p.price
			,i.iva
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.description
			,'' AS ldescription
			,p.thumbtype
			,p.thumbwidth
			,p.thumbheight
		FROM products p, brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND ($l_search_condition_for_union)
		AND p.id NOT IN (
			SELECT product_id FROM products_i18n WHERE lang = '" . $PGM_SESSION["lang"] . "'
		)
		ORDER BY name, bname
		$limit_clause
	");
}

?>

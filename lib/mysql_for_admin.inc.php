<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

/**
* This is an empty function for MySQL; it is foreseen only because it is needed for PostgreSQL
* @return void
*/
function update_sequences() {
}

/**
* For the documentation, look at the corresponding function provided for PostgreSQL
*/
function build_category_tree(&$output, &$preselected, $parent=1, $indent="") {
	global $PGM_SESSION;

	$qid = new PGM_Sql();
	$qid->query("DROP TABLE IF EXISTS tmp_categories");
	$qid->query("DROP TABLE IF EXISTS tmp_categories_i18n");
	$qid->query("
		CREATE TEMPORARY TABLE tmp_categories
		SELECT * FROM categories
	");
	$qid->query("
		CREATE TEMPORARY TABLE tmp_categories_i18n
		SELECT * FROM categories_i18n WHERE lang = '" . $PGM_SESSION["lang"] . "' AND category_id <> 1
	");
	my_build_category_tree($qid, $output, $preselected, $parent=1, $indent="");
}

/**
* For the documentation, look at the corresponding function provided for PostgreSQL
*/
function my_build_category_tree(&$qid, &$output, &$preselected, $parent=1, $indent="") {
	global $PGM_SESSION;

	$qid->query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM tmp_categories c LEFT JOIN tmp_categories_i18n l ON (c.id = l.category_id AND l.lang = '" . $PGM_SESSION["lang"] . "')
		WHERE parent_id = '$parent'
		ORDER BY special_level, name
	");

	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}
	for ($i=0; $i<$numr; $i++) {
		$qid->query("
			DELETE FROM tmp_categories_i18n
			WHERE category_id = '" . $cat[$i]["id"] . "'
		");
	}
	$qid->query("
		DELETE FROM tmp_categories
		WHERE parent_id = '$parent'
	");

	for ($i=0; $i<$numr; $i++) {
		$selected = in_array($cat[$i]["id"], $preselected) ? "selected" : "";
		$output .= "<option value=\"" . ov($cat[$i]["id"]) . "\" $selected>" . $indent . ov($cat[$i]["name"]);
		if ($cat[$i]["id"] != $parent) {
			my_build_category_tree($qid, $output, $preselected, $cat[$i]["id"], $indent."&nbsp;&nbsp;");
		}
	}
}

?>

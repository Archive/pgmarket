<?php

function wishlist_get_userinfo($username, &$userinfo) {

	global $PGM_SESSION;

	$sql = "SELECT firstname, lastname, city, state FROM users WHERE
	        username = '$username'";
	// Check if they allow third parties to view this if it isnt ourselves

	if ($PGM_SESSION['user']['username'] != $username) {
		$sql .= " AND publicwl = 1";
	}
	$qid = new DB_Sql($sql);
	if ($qid->num_rows() < 1) {
		return false;
	}
	$qid->next_record();
	$userinfo['firstname'] = $qid->f('firstname');
	$userinfo['lastname'] = $qid->f('lastname');
	$userinfo['city'] = $qid->f('city');
	$userinfo['state'] = $qid->f('state');

	return true;
}




/* Add an item to your wishlist */
function wishlist_add($product_id, $colorid='0', $qty=1) {
	
	global $PGM_SESSION;

	if (wishlist_productitemcount($product_id, $colorid) > 0) {
		$sql = "UPDATE wishlist_items SET desired = desired + $qty WHERE 
	            username = '".$PGM_SESSION['user']['username']."' AND
			   product_id = $product_id AND color_id = $colorid";
	} else {
		$sql = "INSERT INTO wishlist_items (username,product_id,color_id,desired)
		        VALUES ( '".$PGM_SESSION['user']['username']."', $product_id, $colorid, $qty)";
	}
	$qid = new DB_Sql($sql);
		
}

/* set the quantity of the item in your wishlist */
function wishlist_set($productid, $colorid=0, $qty) {
}


/* Remove item from your wishlist if it exists */
function wishlist_remove($productid, $colorid=0) {
	
	global $PGM_SESSION;

	$sql = "DELETE FROM wishlist_items WHERE product_id = $productid AND
	        username = '".$PGM_SESSION['user']['username']."'";
	$qid = new DB_Sql($sql);

}

/* Remove all items from the wishlist */
function wishlist_empty() {
	
	global $PGM_SESSION;

	$sql = "DELETE FROM wishlist_items WHERE 
	        username = '".$PGM_SESSION['user']['username']."'";
	$qid = new DB_Sql($sql);

}

/* Return the total number of items in your wishlist */
function wishlist_itemcount() {

	global $PGM_SESSION;

	$count = 0;

	$sql = "SELECT COUNT(*) AS prodcount FROM wishlist_items WHERE username ='"
	       . $PGM_SESSION['user']['username'] ."'";
	$qid = new DB_Sql($sql);
	$qid->next_record();
	return $qid->f('prodcount');
}

	

/* Number of a certani type of prooducts */
function wishlist_productitemcount($productid, $colorid="0") {
	global $PGM_SESSION;

	$sql = "SELECT count(*) FROM wishlist_items WHERE product_id=$productid AND color_id=$colorid AND username='".$PGM_SESSION['user']['username']."'";

	$qid = new DB_Sql($sql);
	
	if ($qid->num_rows() < 1) {
		return 0;
	}
	$qid->next_record();
	return intval($qid->f('count'));
}

function wishlist_get_products_list($username) {
	global $PGM_SESSION;

	$products_list = array();

	$sql = "SELECT
			 p.id AS product_id
			,p.name AS name
			,l.name AS lname
			,p.price AS price
			,p.thumbtype
			,p.thumbwidth
			,p.thumbheight
			,p.discount
			,p.discqty
			,p.nostock_flag AS nostock_flag
			,w.desired AS desired
		FROM products p, products_i18n l, wishlist_items w
		WHERE p.id = l.product_id AND l.lang = '$PGM_SESSION[lang]' 
		AND p.id = w.product_id
		AND w.username ='$username'
		UNION ALL
		SELECT
			 p.id AS product_id
			,p.name AS name
			,'' AS lname
			,p.price AS price
			,p.thumbtype
			,p.thumbwidth
			,p.thumbheight
			,p.discount
			,p.discqty
			,p.nostock_flag AS nostock_flag
			,w.desired AS desired
		FROM products p, wishlist_items w
		WHERE p.id = w.product_id
		AND p.id NOT IN (
			SELECT product_id
			FROM products_i18n
			WHERE lang = '$PGM_SESSION[lang]'
		)
		AND w.username ='$username'
	";
	$qid = new DB_Sql($sql);
	$cnt=0;
	while ($qid->next_record()) {
		$products_list[$cnt]['id'] = $qid->f('product_id');
		$products_list[$cnt]['color_id'] = $qid->f('color_id');
		$products_list[$cnt]['name'] = $qid->f('name');
		$products_list[$cnt]['lname'] = $qid->f('lname');
		$products_list[$cnt]['price'] = $qid->f('price');
		$products_list[$cnt]['discount'] = $qid->f('discount');
		$products_list[$cnt]['discqty'] = $qid->f('discqty');
		$products_list[$cnt]['nostock_flag'] = $qid->f('nostock_flag');
		$products_list[$cnt]['thumbtype'] = $qid->f('thumbtype');
		$products_list[$cnt]['thumbwidth'] = $qid->f('thumbwidth');
		$products_list[$cnt]['thumbheight'] = $qid->f('thumbheight');
		$products_list[$cnt]['desired'] = $qid->f('desired');
		$products_list[$cnt]['entered_date'] = $qid->f('entered_date');
		$products_list[$cnt]['subtotal'] = $qid->f('price') * $qid->f('desired');
		$cnt++;
	}
	return $products_list;
}

function wishlist_get_productid_list($username) {

	global $PGM_SESSION;

	$productid_list = '';

    $sql = "SELECT product_id FROM wishlist_items WHERE username ='$username'";
	$qid = new DB_Sql($sql);

	while($qid->next_record) {
		$productid_list .= ",'".$$qid->f('product_id')."'";
	}
	return substr($productid_list,1);
}



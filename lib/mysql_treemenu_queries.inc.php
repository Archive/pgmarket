<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

function build_catbrowser(&$output, $use_targets=1, $parent=1, $indent=".^") {
	global $_GET, $_SERVER;
	global $PGM_SESSION;

	$output = "";
	$qid = new PGM_Sql();
	$qid->query("DROP TABLE IF EXISTS tmp_categories");
	$qid->query("DROP TABLE IF EXISTS tmp_categories_i18n");
	$qid->query("
		CREATE TEMPORARY TABLE tmp_categories
		SELECT * FROM categories
	");
	$qid->query("
		CREATE TEMPORARY TABLE tmp_categories_i18n
		SELECT * FROM categories_i18n WHERE lang = '" . $PGM_SESSION["lang"] . "' AND category_id <> 1
	");
	if ($use_targets) {
		if (!isset($_SERVER["QUERY_STRING"])) $_SERVER["QUERY_STRING"] = "";	// workaround for a MS IIS bug
		$query_string = ($_SERVER["QUERY_STRING"] == "") ? "" : $_SERVER["QUERY_STRING"] . "&amp;";
	} else {
		$query_string = "";
		reset($_GET);
		while (list($key, $value) = each($_GET)) {
			if ($key != "page_number" && $key != "id") {
				$query_string .= "&amp;" . $key . "=" . $value;
			}
		}
		if ($query_string != "") {
			$query_string = substr($query_string, 5) . "&amp;";
		}
	}
	my_build_catbrowser($qid, $output, $use_targets, $query_string, $parent, $indent);
}

function my_build_catbrowser(&$qid, &$output, $use_targets=1, $query_string="", $parent=1, $indent=".^") {
	global $PGM_SESSION;
	global $CFG;

	$qid->query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM tmp_categories c LEFT JOIN tmp_categories_i18n l ON (c.id = l.category_id AND l.lang = '" . $PGM_SESSION["lang"] . "')
		WHERE parent_id = '$parent' AND id <> 1
		ORDER BY special_level, name
	");

	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}
	for ($i=0; $i<$numr; $i++) {
		$qid->query("
			DELETE FROM tmp_categories_i18n
			WHERE category_id = '" . $cat[$i]["id"] . "'
		");
	}
	$qid->query("
		DELETE FROM tmp_categories
		WHERE parent_id = '$parent'
	");

	for ($i=0; $i<$numr; $i++) {
		$foobar = stripsid($CFG["wwwroot"] . "shopping/index.php?" . $query_string . "id=" . $cat[$i]["id"]);
		$output .= $indent . stripslashes($cat[$i]["name"]) . "^" . $foobar;
		if ($use_targets) {
			$output .= "^^^" . $PGM_SESSION["catbrowser_target"];
		}
		$output .= "\n";
		if ($cat[$i]["id"] != $parent) {
			my_build_catbrowser($qid, $output, $use_targets, $query_string, $cat[$i]["id"], "." . $indent);
		}
	}
}

?>

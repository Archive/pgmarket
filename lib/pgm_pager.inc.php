<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

function pgm_pager($url, $num_items, $items_per_page, $page_number, $block_size = 10) {
	global $_GET;

	$query_string = "";
	reset($_GET);
	while (list($key, $value) = each($_GET)) {
		if ($key != "page_number" && $value != "") {
			$query_string .= "&amp;" . $key . "=" . $value;
		}
	}

	$html_output = "";

	$num_pages = max(1, ceil($num_items/$items_per_page));
	$page_number = min($page_number, $num_pages);
	$num_blocks = ceil($num_pages/$block_size);
	$block_number = (int) (($page_number-1)/$block_size);

	if (!($page_number == 1)) {
		$html_output .= "<a href=\"" . $url . "?page_number=1" . $query_string . "\"><b>&lt;&lt;</b></a>";
		$foobar = $page_number - 1;
		$html_output .= "&nbsp;&nbsp;<a href=\"" . $url . "?page_number=" . $foobar . $query_string . "\"><b>&lt;</b></a>";
	} else {
		$html_output .= "<b>&lt;&lt;</b>&nbsp;&nbsp;<b>&lt;</b>";
	}

	if ($block_number > 0) {
		$html_output .= "&nbsp;&nbsp;<b>[</b>";
		for ($i=0; $i<$block_number; $i++) {
			$foobar1 = $i*$block_size + 1;	// page number to be linked
			$foobar2 = ($i+1)*$block_size;
			$html_output .= "&nbsp;<a href=\"" . $url . "?page_number=" . $foobar1 . $query_string . "\">" . $foobar1 . "-" . $foobar2 . "</a>&nbsp;";
		}
		$html_output .= "<b>]</b>";
	}

	$foobar1 = $block_number*$block_size + 1;
	$foobar2 = min(($block_number+1)*$block_size, $num_pages);
	$foobar2++;
	for ($i=$foobar1; $i<$page_number; $i++) {
		$html_output .= "&nbsp;&nbsp;<a href=\"" . $url . "?page_number=" . $i . $query_string . "\">" . $i . "</a>";
	}
	$html_output .= "&nbsp;&nbsp;<b><font color=\"#ff0000\">" . $page_number . "</font></b>";
	for ($i=$page_number+1; $i<$foobar2; $i++) {
		$html_output .= "&nbsp;&nbsp;<a href=\"" . $url . "?page_number=" . $i . $query_string . "\">" . $i . "</a>";
	}

	if ($block_number+1 < $num_blocks) {
		$html_output .= "&nbsp;&nbsp;<b>[</b>";
		for ($i=$block_number+1; $i<$num_blocks; $i++) {
			$foobar1 = $i*$block_size + 1;	// page number to be linked
			$foobar2 = min(($i+1)*$block_size, $num_pages);
			$html_output .= "&nbsp;<a href=\"" . $url . "?page_number=" . $foobar1 . $query_string . "\">" . $foobar1;
			if ($foobar2 > $foobar1) {
				$html_output .= "-" . $foobar2;
			}
			$html_output .= "</a>&nbsp;";
		}
		$html_output .= "<b>]</b>";
	}

	if (!($page_number == $num_pages)) {
		$foobar = $page_number + 1;
		$html_output .= "&nbsp;&nbsp;<a href=\"" . $url . "?page_number=" . $foobar . $query_string . "\"><b>&gt;</b></a>";
		$html_output .= "&nbsp;&nbsp;<a href=\"" . $url . "?page_number=" . $num_pages . $query_string . "\"><b>&gt;&gt;</b></a>";
	} else {
		$html_output .= "&nbsp;&nbsp;<b>&gt;</b>&nbsp;&nbsp;<b>&gt;&gt;</b>";
	}

	$result = array();
	$result["html"] = $html_output;
	$result["num_items"] = $num_items;
	$result["first"] = ($page_number-1)*$items_per_page;
	$result["last"] = min($num_items, $page_number*$items_per_page);
	$result["last"]--;
	$result["page_number"] = $page_number;
	$result["num_pages"] = $num_pages;

	return $result;
}

?>

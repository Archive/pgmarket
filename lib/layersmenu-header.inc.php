<!-- PHP Layers Menu 1.0.8 (c) 2001, 2002 Marco Pratesi <pratesi@telug.it> -->

<?php
$abscissa_step = 100;			// step for the left boundaries of layers
$abscissa_offset = -20;			// to choose the horizontal coordinate start offset for the layers
$rightarrow = " &middot;&middot;&middot;&gt;";
// the following is to support browsers not detecting the mouse position
$ordinata_step = 15;			// estimated value of the number of pixels between links on a layer
$ordinata[1] = 230-$ordinata_step;	// to choose the vertical coordinate start offset for the layers
?>

<script language="JavaScript">
<!--
var thresholdY = 15;		// in pixels; threshold for vertical repositioning of a layer
var ordinata_margin = 20;	// to start the layer a bit above the mouse vertical coordinate
// -->
</script>

<script language="JavaScript" src="<?php echo $CFG->libwww; ?>/layersmenu.js"></script>

<script language="JavaScript">
<!--

<?php

$qid_bcjs = new PGM_Sql;
$qid_bcjs->begin();
build_catbrowser_js($qid_bcjs, $categories);
$qid_bcjs->commit();
//echo $categories;

$moveLayers = "";
$shutdown = "";
$firstleveltable = "";

/* ********************************************************** */
/* Taken from TreeMenu 1.1 - Bjorge Dijkstra (bjorge@gmx.net) */
/* ********************************************************** */
/* with some changes */

/*********************************************/
/*  Read text file with tree structure       */
/*********************************************/

/*********************************************/
/* read file to $tree array                  */
/* tree[x][0] -> tree level                  */
/* tree[x][1] -> item text                   */
/* tree[x][2] -> item link                   */
/* tree[x][3] -> link target                 */
/*********************************************/

$maxlevel=0;
$cnt=1;

while ($categories != "") {
	$beforecr = strcspn($categories, "\n");
	$buffer = substr($categories, 0, $beforecr);
	$categories = substr(stristr($categories, 10), 1);
	$buffer = ereg_replace(chr(13), "", $buffer);	// Microsoft Stupidity Suppression
	$tree[$cnt][0] = strspn($buffer, ".");
	$tmp = rtrim(substr($buffer, $tree[$cnt][0]));
	$node = explode("^", $tmp);
	for ($i=count($node); $i<=2; $i++)
		$node[$i] = "";
	$tree[$cnt][1] = $node[0];
	$layer_label[$cnt] = "L" . $cnt;
	$tree[$cnt][2] = (ereg_replace(" ", "", $node[1]) == "") ? "#" : $node[1];
	$tree[$cnt][3] = $node[2];
	if ($tree[$cnt][0] > $maxlevel) $maxlevel = $tree[$cnt][0];
	$cnt++;
}

$tmpcount = count($tree);
$tree[$tmpcount+1][0] = 0;

/* ******************************************************************** */

for ($i=0; $i<$maxlevel; $i++)
	$abscissa[$i] = $i * $abscissa_step + $abscissa_offset;

for ($cnt=1; $cnt<=$tmpcount; $cnt++) {	// this counter scans all nodes
	// assign the layers name to the current hierarchical level,
	// to keep trace of the route leading to the current node on the tree
	$layername[$tree[$cnt][0]] = $layer_label[$cnt];

	// assign the starting vertical coordinates for all sublevels
	for ($i=$tree[$cnt][0]+1; $i<$maxlevel; $i++)
		$ordinata[$i] = $ordinata[$i-1] + 1.5*$ordinata_step;
	// increment the starting vertical coordinate for the current sublevel
	if ($tree[$cnt][0] < $maxlevel)
		$ordinata[$tree[$cnt][0]] += $ordinata_step;

	if ($tree[$cnt+1][0]>$tree[$cnt][0] && $cnt<$tmpcount) {	// the node is not a leaf, hence it has at least a child
		// initialize the corresponding layer content trought a void string
		$layer[$layer_label[$cnt]] = "";
		// prepare the popUp function related to the children
		echo "function popUp" . $layer_label[$cnt] . "() {\n" . "shutdown();\n";
		for ($i=1; $i<=$tree[$cnt][0]; $i++)
			echo "popUp('" . $layername[$i] . "',true);\n";
		echo "}\n";

		// geometrical parameters are assigned to the new layer, related to the above mentioned children
		$moveLayers .= "setleft('" . $layer_label[$cnt] . "'," . $abscissa[$tree[$cnt][0]] . ");\n";
		$moveLayers .= "settop('" . $layer_label[$cnt] . "'," . $ordinata[$tree[$cnt][0]] . ");\n";
//		$moveLayers .= "setwidth('" . $layer_label[$cnt] . "'," . $abscissa_step . ");\n";

		// the new layer is accounted for in the shutdown() function
		$shutdown .= "popUp('" . $layer_label[$cnt] . "',false);\n"; 
	}

	if ($tree[$cnt+1][0]>$tree[$cnt][0] && $cnt<$tmpcount)	// not a leaf
		$currentarrow = $rightarrow;
	else	// a leaf
		$currentarrow = "";
/* */
	$currentlink = $tree[$cnt][2];
/* */
/*
	if ($tree[$cnt+1][0]>$tree[$cnt][0] && $cnt<$tmpcount)	// not a leaf
		$currentlink = "#";
	else	// a leaf
		$currentlink = $tree[$cnt][2];
*/
	if ($tree[$cnt][3] != "")
		$currenttarget = " target=\"" . $tree[$cnt][3] . "\"";
	else
		$currenttarget = "";
	if ($tree[$cnt][0] > 1) {
	// the hierarchical level is > 1, hence the current node is not a child of the root node
	// handle accordingly the corresponding link, distinguishing if the current node is a leaf or not
		if ($tree[$cnt+1][0]>$tree[$cnt][0] && $cnt<$tmpcount)	// not a leaf
			$onmouseover = " onMouseover=\"moveLayerY('" . $layer_label[$cnt] . "', currentY) ; popUp" . $layer_label[$cnt] . "();";
		else	// a leaf
			$onmouseover = " onMouseover=\"popUp" . $layername[$tree[$cnt][0]-1] . "();";
		$layer[$layername[$tree[$cnt][0]-1]] .= "<a href=\"" . $currentlink . "\"" . $onmouseover . "\"" . $currenttarget . ">" . stripslashes($tree[$cnt][1]) . "</a>" . $currentarrow . "<br>\n";
	} else if ($tree[$cnt][0] == 1) {
	// the hierarchical level is = 1, hence the current node is a child of the root node
	// handle accordingly the corresponding link, distinguishing if the current node is a leaf or not
		if ($tree[$cnt+1][0]>$tree[$cnt][0] && $cnt<$tmpcount)	// not a leaf
			$onmouseover = " onMouseover = \"moveLayerY('" . $layer_label[$cnt] . "', currentY) ; popUp" . $layer_label[$cnt] . "();";
		else	// a leaf
			$onmouseover = " onMouseover=\"shutdown();";
		$firstleveltable .= "<a href=\"" . $currentlink . "\"" . $onmouseover . "\"" . $currenttarget . ">" . stripslashes($tree[$cnt][1]) . "</a>" . $currentarrow . "<br>\n";
	}

}	// end of the "for" cycle scanning all nodes

$shutdown = "function shutdown() {\n" . $shutdown . "}\n" . "if (NS4) {\ndocument.onmousedown = function() { shutdown(); }\n} else {\ndocument.onclick = function() { shutdown(); } \n}\n";

?>

function moveLayers() {
<?php echo $moveLayers; ?>
}
<?php echo $shutdown; ?>

// -->
</script>

<!-- PHP Layers Menu 1.0.8 (c) 2001, 2002 Marco Pratesi <pratesi@telug.it> -->

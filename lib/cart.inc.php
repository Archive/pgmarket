<?php
/* cart.php (c) 2000 Ying Zhang (ying@zippydesign.com)
/* (c) 2000-2001 Marco Pratesi <pratesi@telug.it> */
/* (c) 2001 Fabio Molinari <fabio.m@mclink.it> */

class Cart {
	var $items;		/* array of items */
	var $subtotals;		/* array of subtotals */
	var $not_discounted_total;
	var $total;		/* total of the cart */
	var $weight;		// total weight of the cart
	var $delivery;		// delivery price, it depends on the weight
	var $grandtotal;	// $grandtotal = $total + $delivery

	function Cart() {
	/* object constructor */

		$this->init();
	}

	function init() {
	/* this function is called to initialize (and reset) a shopping cart */

		$this->items = array();
		$this->subtotals = array();
		$this->not_discounted_total = 0;
		$this->total = 0;
		$this->weight = 0;
		$this->delivery = 0;
		$this->grandtotal = 0;
	}

	function add($productid, $colorid=0, $qty) {
	/* add an item to the shopping cart // no, it doesn't --> and update the total price */

		if (isset($productid)) {
			setdefault($this->items[$productid][$colorid]["qty"], 0);
			$this->items[$productid][$colorid]["qty"] += $qty;
		}
	}

	function set($productid, $colorid=0, $qty) {
	/* set the quantity of a product in the cart to a specified value */

		if (isset($productid))
			$this->items[$productid][$colorid]["qty"] = (int) $qty;
	}

	function remove($productid, $colorid=0) {
	/* this function will remove a given product from the cart */

		if (isset($productid))
			unset($this->items[$productid][$colorid]);
		if ($this->productitemcount($productid) == 0)
			unset($this->items[$productid]);
	}

	function cleanup() {
	/* this function will clean up the cart, removing items with invalid product
	 * id's (non-numeric ones) and products with quantities less than 1 */

		foreach ($this->items as $productid => $colorid) {
			foreach ($this->items[$productid] as $colorid => $product)
				if ($product["qty"] < 1)
					unset($this->items[$productid][$colorid]);
			if ($this->productitemcount($productid) == 0)
				unset($this->items[$productid]);
		}
	}

	function itemcount() {
	/* returns the number of individual items in the shopping cart
	 * (note, this takes into account the quantities of the items as well) */

		$count = 0;
		foreach ($this->items as $productid => $colorid)
			foreach ($this->items[$productid] as $colorid => $product)
				$count += $product["qty"];

		return $count;
	}

	function productitemcount($productid) {

		$items = 0;
		if(isset($this->items[$productid]))
			foreach ($this->items[$productid] as $colorid => $product)
				$items += $product["qty"];

		return $items;
	}

	function get_productid_list() {
	/* return a comma delimited list of all the products in the cart, this will
	 * be used for queries, eg. SELECT id, price FROM products WHERE id IN .... */

		$productid_list = "";

		foreach ($this->items as $productid => $qty)
			$productid_list .= ",'" . $productid . "'";

		/* need to strip off the leading comma */
		return substr($productid_list, 1);
	}

	function get_products_list(&$qid, &$products_list) {

		$numr = get_cart_items_info($qid);
		$cnt = 0;
		if ($numr > 0) {
			while ($qid->next_record()) {
				$productid = $qid->f("id");
				foreach ($this->items[$productid] as $colorid => $product) {
					$products_list[$cnt]["product_id"] = $productid;
					$products_list[$cnt]["color_id"] = $colorid;
					$products_list[$cnt]["qty"] = $product["qty"];
					$products_list[$cnt]["code"] = $product["code"];
					$products_list[$cnt]["name"] = $qid->f("name");
					$products_list[$cnt]["lname"] = $qid->f("lname");
					$products_list[$cnt]["price"] = $product["price"];
					$products_list[$cnt]["discount"] = $product["discount"];
					$products_list[$cnt]["discqty"] = $product["discqty"];
					$products_list[$cnt]["weight"] = $product["weight"];
					$products_list[$cnt]["thumbwidth"] = $qid->f("thumbwidth");
					$products_list[$cnt]["thumbheight"] = $qid->f("thumbheight");
					$products_list[$cnt]["iva"] = $product["iva"];
					$products_list[$cnt]["subtotal"] = $product["subtotal"];
					$products_list[$cnt]["color_name"] = $product["color_name"];
					$products_list[$cnt]["color_lname"] = $product["color_lname"];
					$cnt++;
				}
			}
			for ($cnt=0; $cnt<count($products_list); $cnt++) {
				get_color_name($qid, $products_list[$cnt]["color_id"]);
				$qid->next_record();
				$products_list[$cnt]["color_name"] = $qid->f("name");
				$products_list[$cnt]["color_lname"] = $qid->f("lname");
			}
		}
	}

	function recalc_total(&$qid) {

		global $SESSION, $SETTING;

		$this->not_discounted_total = 0;
		$this->total = 0;
		$this->weight = 0;
		$this->delivery = 0;
		$this->grandtotal = 0;

		$in_clause = $this->get_productid_list();
		if (empty($in_clause))
			return;
		$qid->Query("
			SELECT p.id, p.code, p.price, p.discount, p.discqty, p.weight, i.iva
			FROM products p, iva i
			WHERE p.iva_id = i.id AND p.id IN ($in_clause)
		");

		while ($qid->next_record()) {
			$productid = $qid->f("id");
			foreach ($this->items[$productid] as $colorid => $product) {
				$this->items[$productid][$colorid]["code"] = $qid->f("code");
				$this->items[$productid][$colorid]["price"] = $qid->f("price");
				$this->items[$productid][$colorid]["discount"] = $qid->f("discount");
				$this->items[$productid][$colorid]["discqty"] = $qid->f("discqty");
				$this->items[$productid][$colorid]["weight"] = $qid->f("weight");
				$this->items[$productid][$colorid]["iva"] = $qid->f("iva");
				if ($this->productitemcount($productid) >= $this->items[$productid][$colorid]["discqty"])
					$this->items[$productid][$colorid]["subtotal"] = $product["qty"] * $this->items[$productid][$colorid]["price"] * (100 - $this->items[$productid][$colorid]["discount"]) / 100;
				else
					$this->items[$productid][$colorid]["subtotal"] = $product["qty"] * $this->items[$productid][$colorid]["price"];
				if ($SETTING["iva_used"])
					$this->items[$productid][$colorid]["subtotal"] *= (100 + $this->items[$productid][$colorid]["iva"]) / 100;
				$this->total += $this->items[$productid][$colorid]["subtotal"];
				$this->weight += $this->items[$productid][$colorid]["qty"] * $this->items[$productid][$colorid]["weight"];
			}
		}

		$this->not_discounted_total = $this->total;
		if ($SETTING["user_discount_used"] && is_logged_in())
			$this->total *= (100 - $SESSION["user"]["user_discount"])/100;

		if ($SETTING["delivery_used"] && isset($SESSION["zone_id"])) {
			$Zone = $SESSION["zone_id"];
			$Weight = $this->weight;
			$Total = $this->total;
			$qid->Query("
				SELECT cost, type
				FROM delivery_costs
				WHERE zone_id = '$Zone' AND maxqty >= '$Total'
				ORDER BY maxqty ASC
			");
			if ($qid->num_rows() > 0) {
				$qid->next_record();
				$this->delivery = $qid->f("cost") * (($qid->f("type") == "U") ? $Weight : 1);
			}
		}
		$this->grandtotal = $this->total + $this->delivery;
	}
}

?>

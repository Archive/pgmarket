<?php

$PGM_SESSION["products_per_page"] = 10;		// max number of products to be shown per page (admin products page)
$PGM_SESSION["user_discounts_per_page"] = 10;	// max number of user discounts to be shown per page (admin user discounts page)
$PGM_SESSION["zones_per_page"] = 10;		// max number of zones to be shown per page (admin zones page)
$PGM_SESSION["dc_per_page"] = 10;		// max number of delivery costs to be shown per page (admin delivery costs page)
$PGM_SESSION["users_per_page"] = 10;		// max number of users to be shown per page (admin users page)
$PGM_SESSION["categories_per_page"] = 10;	// max number of categories to be shown per page (admin categories page)
$PGM_SESSION["brands_per_page"] = 10;		// max number of brands to be shown per page (admin brands page)
$PGM_SESSION["colors_per_page"] = 10;		// max number of colors to be shown per page (admin colors page)
$PGM_SESSION["iva_per_page"] = 10;		// max number of iva percentages to be shown per page (admin iva page)
$PGM_SESSION["orders_states_per_page"] = 10;		// max number of orders to be shown per page (admin and users orders pages)

?>

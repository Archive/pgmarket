<?php
/* (c) 2000-2001 Marco Pratesi <pratesi@telug.it> */

$DBMS_FALSE = "0";
$DBMS_TRUE = "1";

function update_sequences(&$qid) {
// this is an empty function for MySQL;
// it is foreseen only because it is needed for PostgreSQL
}

function get_category_info(&$qid, $id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id
			,c.name
			,l.name AS lname
			,c.special_level
			,c.description
		FROM categories c LEFT JOIN categories_i18n l ON (c.id = l.category_id AND l.lang = '$SESSION[i18n]')
		WHERE id = '$id'
	");
}

function get_allcategories_info(&$qid, $limit_clause) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id
			,c.name
			,l.name AS lname
			,c.special_level
			,c.description
			,parent.name AS parent
		FROM categories c LEFT JOIN categories_i18n l ON (c.id = l.category_id AND l.lang = '$SESSION[i18n]'), categories parent
		WHERE c.parent_id = parent.id AND c.id > 1
		ORDER BY name, special_level
		$limit_clause
	");
}

function build_category_tree(&$qid, &$output, &$preselected, $parent=1, $indent="") {
/* recursively go through the category tree, starting at a parent, and
 * drill down, printing options for a selection list box.  preselected
 * items are marked as being selected.  this is not an efficient algorithm
 * because it has to issue one query per category!!  it's only used because it
 * is easy to understand.
 */

	global $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM categories c LEFT JOIN categories_i18n l ON (c.id = l.category_id AND l.lang = '$SESSION[lang]')
		WHERE parent_id = '$parent'
		ORDER BY special_level, name
	");

	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}

	for ($i=0; $i<$numr; $i++) {
		$selected = in_array($cat[$i]["id"], $preselected) ? "selected" : "";
		$output .= "<option value=\"" . ov($cat[$i]["id"]) . "\" $selected>" . $indent . ov($cat[$i]["name"]);
		if ($cat[$i]["id"] != $parent) {
			build_category_tree($qid, $output, $preselected, $cat[$i]["id"], $indent."&nbsp;&nbsp;");
		}
	}
}

function get_color_info(&$qid, $color_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id AS color_id
			,c.name
			,l.name AS lname
			,c.description
		FROM colors c LEFT JOIN colors_i18n l ON (c.id = l.color_id AND l.lang = '$SESSION[i18n]')
		WHERE id = '$color_id'
	");
}

function get_allcolors_info(&$qid, $limit_clause) {

	global $SESSION;

	$qid->Query("
		SELECT
			 c.id AS color_id
			,c.name
			,l.name AS lname
			,c.description
		FROM colors c LEFT JOIN colors_i18n l ON (c.id = l.color_id AND l.lang = '$SESSION[i18n]')
		WHERE id > 0
		ORDER BY name
		$limit_clause
	");
}

function build_color_options(&$qid, &$output, &$preselected) {

	global $SESSION;

	$qid->Query("
		SELECT c.id, c.name, l.name AS lname
		FROM colors c LEFT JOIN colors_i18n l ON (c.id = l.color_id AND l.lang = '$SESSION[lang]')
		WHERE c.id > 0
		ORDER BY name
	");

	$count_preselected = count($preselected);
	while ($qid->next_record()) {
		$appoggio = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
		if ($count_preselected > 0)
			$selected = in_array($qid->f("id"), $preselected) ? "selected" : "";
		else
			$selected = "";
		$output .= "<option value=\"" . ov($qid->f("id")) . "\" $selected>" . ov($appoggio);
	}
}

function get_product_info(&$qid, $product_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id AS product_id
			,p.name
			,l.name AS lname
			,p.special_level
			,p.description
			,l.description AS ldescription
			,p.extended_description
			,l.extended_description AS lextended_description
		FROM products p LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[i18n]')
		WHERE id = '$product_id'
	");
}

function get_allproducts_info_i18n(&$qid, $limit_clause) {

	global $SESSION;

	$qid->Query("
		SELECT
			 p.id AS product_id
			,p.name
			,l.name AS lname
			,p.special_level
			,p.description
		FROM products p LEFT JOIN products_i18n l ON (p.id = l.product_id AND l.lang = '$SESSION[i18n]')
		ORDER BY name, special_level
		$limit_clause
	");
}

function get_zone_info(&$qid, $zone_id) {

	global $SESSION;

	$qid->Query("
		SELECT
			 z.id AS zone_id
			,z.name
			,l.name AS lname
			,z.description
		FROM zones z LEFT JOIN zones_i18n l ON (z.id = l.zone_id AND l.lang = '$SESSION[i18n]')
		WHERE id = '$zone_id'
	");
}

function get_allzones_info(&$qid, $limit_clause) {

	global $SESSION;

	$qid->Query("
		SELECT
			 z.id AS zone_id
			,z.name
			,l.name AS lname
			,z.description
		FROM zones z LEFT JOIN zones_i18n l ON (z.id = l.zone_id AND l.lang = '$SESSION[i18n]')
		ORDER BY name
		$limit_clause
	");
}

?>

<!-- PHP Layers Menu 1.0.8 (c) 2001, 2002 Marco Pratesi <pratesi@telug.it> -->

<?php
$cellpadding = 10;
$width = $abscissa_step - $cellpadding;
for ($cnt=1; $cnt<=$tmpcount; $cnt++)
	if (!($tree[$cnt+1][0]<=$tree[$cnt][0])) {
?>

<div id="<?php echo $layer_label[$cnt]; ?>" style="position: absolute; visibility: hidden;">
<table border="2" bordercolor="#004488" cellpadding="0" cellspacing="0" class="nav">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="<?php echo $cellpadding; ?>" width="<?php echo $abscissa_step; ?>">
<tr>
<td class="normal" nowrap>
<b><?php echo stripslashes($tree[$cnt][1]); ?></b>
</td>
</tr>
<tr>
<td class="normal" nowrap>
<?php echo $layer[$layer_label[$cnt]]; ?>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

<?php
}
?>

<script language="JavaScript">
<!--
moveLayers();
loaded = 1;	// to avoid stupid errors of Microsoft browsers
// -->
</script>

<!-- PHP Layers Menu 1.0.8 (c) 2001, 2002 Marco Pratesi <pratesi@telug.it> -->

<?php
/**
* PGM extension to the PHPLib DB_Sql class for MySQL
*
* (C) 2002 Marco Pratesi (marco@pgmarket.net)
* Released under the GNU LGPL
*/
class PGM_Sql extends DB_Sql {

/**
* Method to be invoked to begin a transaction.
* @access public
* @return void
*/
function begin() {
	ignore_user_abort(1);
//	$this->query("BEGIN");
}

/**
* Method to be invoked to end a transaction.
* @access public
* @return void
*/
function commit() {
//	$this->query("COMMIT");
	ignore_user_abort(0);
}

/**
* For comments about this method, refer to the description of the same method for the PostgreSQL class
*/
function pgm_lang_join_query(
	$base_lang,
	$left_join_table,
	$left_join_fields,
	$left_join_wanted_field,
	$other_fields,
	$other_tables,
	$additional_where_clause,
	$order_by_clause,
	$limits
) {
	$num_join_tables = count($left_join_table);

	for ($i=0; $i<$num_join_tables; $i++) {
		$foobar = explode(" ", $left_join_table[$i]);
		$left_join_table_name[$i] = $foobar[0];
		$left_join_table_alias[$i] = $foobar[1];
	}
	for ($i=0; $i<$num_join_tables-1; $i++) {
		$left_join_left_field[$i] = $left_join_fields[$i][0];
		$left_join_right_field[$i] = $left_join_fields[$i][1];
		$lang_array[$i] = $left_join_fields[$i][2];
		if ($lang_array[$i]) {
			$lang[$i] = $left_join_fields[$i][3];
		}
		$counter = count($left_join_wanted_field[$i]);
		for ($cnt=0; $cnt<$counter; $cnt++) {
			$foobar = explode(" AS ", $left_join_wanted_field[$i][$cnt]);
			$left_join_wanted_field_name[$i][$cnt] = $foobar[0];
			$left_join_wanted_field_alias[$i][$cnt] = $foobar[1];
		}
	}

	$select_clause = "";
	for ($i=0; $i<$num_join_tables-1; $i++) {
		$counter = count($left_join_wanted_field[$i]);
		if (!($lang_array[$i] && $lang[$i] == $base_lang)) {
			for ($cnt=0; $cnt<$counter; $cnt++) {
				$select_clause .= ", " . $left_join_wanted_field[$i][$cnt];
			}
		} else {
			for ($cnt=0; $cnt<$counter; $cnt++) {
				$select_clause .= ", '' AS " . $left_join_wanted_field_alias[$i][$cnt];
			}
		}
	}
	if ($other_fields != "") {
		$select_clause .= ", " . $other_fields;
	}
	$select_clause = "SELECT " . substr($select_clause, 2);

	$from_clause = " " . $left_join_table[0];
	for ($i=1; $i<$num_join_tables; $i++) {
		if (!($lang_array[$i-1] && $lang[$i-1] == $base_lang)) {
			$from_clause .= " LEFT JOIN " . $left_join_table[$i];
			$foobar = ($left_join_table_alias[$i] != "") ? $left_join_table_alias[$i] : $left_join_table_name[$i];
			$from_clause .= " ON (" . $left_join_left_field[$i-1] . " = " . $left_join_right_field[$i-1];
			if ($lang_array[$i-1]) {
				$from_clause .= " AND " . $foobar . ".lang = '" . $lang[$i-1] . "'";
			}
			$from_clause .= ")";
		}
	}
	if ($other_tables != "") {
		$from_clause .= ", " . $other_tables;
	}
	$from_clause = " FROM" . $from_clause;

	if ($additional_where_clause != "") {
		$where_clause = " WHERE " . $additional_where_clause;
	} else {
		$where_clause = "";
	}

	if ($order_by_clause != "") {
		$order_by_clause = " ORDER BY " . $order_by_clause;
	}

	if (count($limits) == 2) {
		$limit_clause = " LIMIT " . $limits[0] . ", " . $limits[1];
	} else {
		$limit_clause = "";
	}

	$query = "\n" . $select_clause . "\n" . $from_clause . "\n" . $where_clause . "\n" . $order_by_clause . "\n" . $limit_clause;
	return $this->query($query);
}

} /* END OF CLASS */

?>

<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

/**
* This function checks if the user is logged in
* @return boolean
*/
function is_logged_in() {
	global $PGM_SESSION;
	return (!empty($PGM_SESSION["user"]["username"]));
/*
	global $PGM_SESSION, $HTTP_SERVER_VARS;
	return isset($PGM_SESSION)
		&& isset($PGM_SESSION["user"]);
// commented to avoid problems with proxies
//		&& isset($PGM_SESSION["ip"])
//		&& $PGM_SESSION["ip"] == $HTTP_SERVER_VARS["REMOTE_ADDR"];
*/
}

/**
* Loads up and returns the user's details
* @return array
*/
function load_user_profile() {
	global $PGM_SESSION;
	
	$qid = new PGM_Sql("
		SELECT
			 firstname
			,lastname
			,usertype
			,priv
			,zone_id
			,fiscalcode
			,address
			,state
			,zip_code
			,city
			,country
			,phone
			,fax
			,mobilephone
			,email
			,authdata
			,acceptadvert
			,notes
		FROM users
		WHERE username = '" . $PGM_SESSION["user"]["username"] . "'
	");
	$qid->next_record();
	$arr = array();
	$arr["firstname"] = $qid->f("firstname");
	$arr["lastname"] = $qid->f("lastname");
	$arr["usertype"] = $qid->f("usertype");
	$arr["priv"] = $qid->f("priv");
	$arr["zone_id"] = $qid->f("zone_id");
	$arr["fiscalcode"] = $qid->f("fiscalcode");
	$arr["address"] = $qid->f("address");
	$arr["state"] = $qid->f("state");
	$arr["zip_code"] = $qid->f("zip_code");
	$arr["city"] = $qid->f("city");
	$arr["country"] = $qid->f("country");
	$arr["phone"] = $qid->f("phone");
	$arr["fax"] = $qid->f("fax");
	$arr["mobilephone"] = $qid->f("mobilephone");
	$arr["email"] = $qid->f("email");
	$arr["authdata"] = $qid->f("authdata");
	$arr["acceptadvert"] = $qid->f("acceptadvert");
	$arr["notes"] = $qid->f("notes");

	return $arr;
}

/**
* This function checks if the user is logged in; if not, it redirects to the login page.
* @return void
*/
function require_login() {
	global $PGM_SESSION, $session_name;
	global $CFG;

	if (!is_logged_in()) {
		$PGM_SESSION["wantsurl"] = qualified_me();
		pgm_session_close($PGM_SESSION, $session_name);
		redirect($CFG["wwwroot"] . "login.php");
		die;
	}
}

/**
* This function checks if the user has the requested provilege; if not, it displays an Insufficient Privileges page and stops
* @param string $priv the requested provilege
* @return void
*/
function require_priv($priv) {
	global $PGM_SESSION;
	global $CFG;

	if (nvl($PGM_SESSION["user"]["priv"], "") != $priv) {
		$t = new Template();
		$t->set_file("page", $CFG["dirroot"] . "templates/insufficient_privileges.ihtml");
		include ($CFG["localelangdir"] . "global-common.inc.php");
		include ($CFG["localelangdir"] . "global-admin.inc.php");
		$t->set_var("wwwroot", $CFG["wwwroot"]);
		$t->pparse("out", "page");
		die;
	}
}

/**
* This function returns true if the user has the requested privilege
* @param string $priv the requested provilege
* @return boolean
*/
function has_priv($priv) {
	global $PGM_SESSION;

	if (is_logged_in() && isset($PGM_SESSION["user"]["priv"])) {
		return $PGM_SESSION["user"]["priv"] == $priv;
	} else {
		return false;
	}
}

/**
* Returns a randomly generated password of requested length
* @param integer $length the equested length
* @return string
*/
function generate_password($length=10) {
	$password = "";
	srand((double) microtime() * 1000000);
	$l = 0;
	while ($l<$length) {
		$r = rand(48,122);
		if (!((58<=$r && $r<=64) || (91<=$r && $r<=96))) {
			$password .= chr($r);
			$l++;
		}
	}
	return $password;
}

/**
* Resets the password for a given user and sends it to him/her via email; it returns 0 if all goes right and 1 on error.
* @param string $username the username for which the password has to be reset
* @return int
*/
function reset_user_password($username) {
	global $PGM_SESSION;
	global $CFG;

	// load up the user record
	$qid = new PGM_Sql("
		SELECT username, priv, firstname, lastname, email
		FROM users
		WHERE username = '$username'
	");
	$qid->next_record();
	if ($qid->f("priv") == "admin" || !has_priv("admin")) {
		return 1;
	}

	// reset the password
	$newpassword = generate_password();
	$qid->query("
		UPDATE users
		SET password = '" . md5($newpassword) ."'
		WHERE username = '$username'
	");

	// email the user with the new account information
	$t = new Template();
	$t->set_file("page", $CFG["dirroot"] . "templates/" . $PGM_SESSION["lang"] . "/reset_password.ihtml");
	$t->set_var(array(
		"myname"	=> $CFG["myname"],
		"support"	=> $CFG["support"],
		"fullname"	=> $qid->f("firstname") . " " . $qid->f("lastname"),
		"username"	=> $qid->f("username"),
		"newpassword"	=> $newpassword
	));
	$emailbody = $t->parse("out", "page");
	mail(
		$qid->f("firstname") . " " . $qid->f("lastname") . " <" . $qid->f("email") . ">",
		$CFG["myname"] . " Account Information",
		$emailbody,
		"From: " . $CFG["support"]
	);

	return 0;
}

?>

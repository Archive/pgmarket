<?php
/* (c) 2000-2002 Marco Pratesi <pratesi@telug.it> */

$pager_query_string = $QUERY_STRING;
$pager_query_string = ereg_replace("page_number=[0-9]{1,}.", "", $pager_query_string);
$pager_query_string = ereg_replace(".page_number=[0-9]{1,}", "", $pager_query_string);
$pager_query_string = ereg_replace("page_number=[0-9]{1,}", "", $pager_query_string);
$pager_query_string = addsidtoquery($pager_query_string);
if ($pager_query_string != "")
	$pager_query_string = "&" . $pager_query_string;

$pages_links = "";

if (!($page_number == 1)) {
	$pages_links .= "<a href=\"";
	$pages_links .= $ME . "?page_number=1" . $pager_query_string;
	$pages_links .= "\">";
	$pages_links .= "<b>&lt;&lt;</b>";
	$pages_links .= "</a>&nbsp;&nbsp;";
	$pages_links .= "<a href=\"";
	$pages_links .= $ME . "?page_number=";
	$appoggio = $page_number - 1;
	$pages_links .= $appoggio . $pager_query_string;
	$pages_links .= "\">";
	$pages_links .= "<b>&lt;</b>";
	$pages_links .= "</a>&nbsp;&nbsp;";
	for ($i=1; $i<$page_number; $i++) {
		$pages_links .= "<a href=\"";
		$pages_links .= $ME . "?page_number=" . $i . $pager_query_string;
		$pages_links .= "\">";
		$pages_links .= $i;
		$pages_links .= "</a>&nbsp;&nbsp;";
	}
} else
	$pages_links .= "<b>&lt;&lt;</b>&nbsp;&nbsp;<b>&lt;</b>&nbsp;&nbsp;";
$pages_links .= "<b><font color=\"#ff0000\">" . $page_number . "</font></b>&nbsp;&nbsp;";
if (!($page_number == $num_pages)) {
	for ($i=$page_number+1; $i<=$num_pages; $i++) {
		$pages_links .= "<a href=\"";
		$pages_links .= $ME . "?page_number=" . $i . $pager_query_string;
		$pages_links .= "\">";
		$pages_links .= $i;
		$pages_links .= "</a>&nbsp;&nbsp;";
	}
	$pages_links .= "<a href=\"";
	$pages_links .= $ME . "?page_number=";
	$appoggio = $page_number + 1;
	$pages_links .= $appoggio . $pager_query_string;
	$pages_links .= "\">";
	$pages_links .= "<b>&gt;</b>";
	$pages_links .= "</a>&nbsp;&nbsp;";
	$pages_links .= "<a href=\"";
	$pages_links .= $ME . "?page_number=" . $num_pages . $pager_query_string;
	$pages_links .= "\">";
	$pages_links .= "<b>&gt;&gt;</b>";
	$pages_links .= "</a>";
} else
	$pages_links .= "<b>&gt;</b>&nbsp;&nbsp;<b>&gt;&gt;</b>";

?>

<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

function build_catbrowser_js(&$output, $parent=1, $indent=".^") {
	global $PGM_SESSION;

	$output = "";
	$qid = new PGM_Sql();
	$qid->query("DROP TABLE IF EXISTS tmp_categories");
	$qid->query("DROP TABLE IF EXISTS tmp_categories_i18n");
	$qid->query("
		CREATE TEMPORARY TABLE tmp_categories
		SELECT * FROM categories
	");
	$qid->query("
		CREATE TEMPORARY TABLE tmp_categories_i18n
		SELECT * FROM categories_i18n WHERE lang = '" . $PGM_SESSION["lang"] . "' AND category_id <> 1
	");
	my_build_catbrowser_js($qid, $output, $parent, $indent);
}

function my_build_catbrowser_js(&$qid, &$output, $parent=1, $indent=".^") {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM tmp_categories c LEFT JOIN tmp_categories_i18n l ON (c.id = l.category_id AND l.lang = '" . $PGM_SESSION["lang"] . "')
		WHERE parent_id = '$parent' AND id <> 1
		ORDER BY special_level, name
	");
	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}
	for ($i=0; $i<$numr; $i++) {
		$qid->query("
			DELETE FROM tmp_categories_i18n
			WHERE category_id = '" . $cat[$i]["id"] . "'
		");
	}
	$qid->query("
		DELETE FROM tmp_categories
		WHERE parent_id = '$parent'
	");

	for ($i=0; $i<$numr; $i++) {
		$output .= $indent . $cat[$i]["name"] . "^" . $CFG["wwwroot"] . "shopping/index.php?id=" . $cat[$i]["id"] . "\n";
		if ($cat[$i]["id"] != $parent) {
			my_build_catbrowser_js($qid, $output, $cat[$i]["id"], "." . $indent);
		}
	}
}

?>

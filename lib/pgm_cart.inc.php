<?php
/**
* PGM Cart (C) 2002-2003 Marco Pratesi (marco@pgmarket.net)
*
* Released under the GNU LGPL
*
* Cart (the base class) depends on the DB_Sql class of PHPLib
* PGM_Cart (the extended class) depends also on the PGM_Sql extension
*
* These classes should work also with PHP3,
* apart from (maybe) the handling of pgm_session_id()
*
* The base class is intended as a general purpose cart class
* that could be useful not only for PgMarket, but also
* for development of other e-commerce packages
*
* PostgreSQL command to create the corresponding table...
* \connect - postgres
* CREATE TABLE carts_items (
* 	sid varchar(32) DEFAULT '',
* 	id int4,
* 	color_id int2 DEFAULT 1,
* 	qty int2,
* 	changed varchar(14) DEFAULT '' NOT NULL,
* 	PRIMARY KEY (sid, id, color_id)
* );
*
* MySQL command to create the corresponding table...
* CREATE TABLE carts_items (
* 	sid varchar(32) NOT NULL,
* 	id int,
* 	color_id int DEFAULT 1,
* 	qty int,
* 	changed varchar(14) DEFAULT '' NOT NULL,
* 	UNIQUE (sid, id, color_id)
* );
*
* @package PgMarket
*/
class Cart {

/**
* The name of the class
* @access private
* @var string
*/
var $classname = "Cart";

/**
* Array of fields needed to identify each item in the cart for a given session
*
* It can be noted that the primary key of the cart table consists
* just of these fields + the session identifier field.
* If you want to handle products that require more parameters to be identified,
* simply use a longer array, setting it through the suited method.
*
* @access private
* @var array
*/
var $field         = array("id", "color_id");
/**
* Array of default values for corresponding fields
* @access private
* @var array
*/
var $field_default = array(0, 1);

/**
* Name of the table used to store items in the cart
* @access private
* @var string
*/
var $cart_table    = "carts_items";
/**
* Name of the table field where the session identifier is stored
* @access private
* @var string
*/
var $sid_field     = "sid";
/**
* Name of the table field where is stored the quantity of items in the cart for a given item
* @access private
* @var string
*/
var $qty_field     = "qty";
/**
* Name of the table field where is stored a time stamp identifying the last change for the item
* @access private
* @var string
*/
var $changed_field = "changed";

/**
* Do you want that code execution halts on error?
* @access private
* @var string
*/
var $halt_on_error = "yes";

/**
* Lifetime (in seconds) of cart items.
*
* The garbage collector, when run, removes all cart items older than
* the lifetime chosen here
*
* @access private
* @var integer
*/
var $gc_time  = 1440;
/**
* Garbage collect probability in percent
* @access private
* @var integer
*/
var $gc_probability = 1;

/**
* A multi-dimensional array containing data for items in the cart
* @access private
* @var array
*/
var $item = array();

/**
* The constructor method
* @return void
*/
function Cart() {
	$this->init();
}

/**
* The method to initialize (and reset) the cart
* @access public
* @return boolean
*/
function init() {
	$query =
		"DELETE FROM " . $this->cart_table .
		" WHERE " . $this->sid_field . " = '" . pgm_session_id() . "'";
	$qid = new DB_Sql($query);
	$this->item = array();
	if (!is_array($this->field)) {
		$this->error("init: \$this->field is not an array.");
		return false;
	}
	if (!is_array($this->field_default)) {
		$this->error("init: \$this->field_default is not an array.");
		return false;
	}
	if (count($this->field) == 0) {
		$this->error("init: \$this->field is a zero-length array.");
		return false;
	}
	if (count($this->field_default) == 0) {
		$this->error("init: \$this->field_default is a zero-length array.");
		return false;
	}
	if (count($this->field) != count($this->field_default)) {
		$this->error("init: \$this->field and \$this->field_default have different sizes.");
		return false;
	}
	return true;
}

/**
* The method to set the value of the $fields array
* @access public
* @param array $fields_array the array to set $fields
* @return boolean
*/
function set_fields($fields_array) {
	if (!is_array($fields_array)) {
		$this->error("set_fields: \$fields_array is not an array.");
		return false;
	}
	if (count($fields_array) == 0) {
		$this->error("set_fields: \$fields_array is a zero-length array.");
		return false;
	}
	$this->field = $fields_array;
	return true;
}

/**
* The method to set the value of the $fields_defaults array
* @access public
* @param array $fields_array_defaults the array to set $fields_defaults
* @return boolean
*/
function set_fields_defaults($fields_defaults_array) {
	if (!is_array($fields_defaults_array)) {
		$this->error("set_fields_defaults: \$fields_defaults_array is not an array.");
		return false;
	}
	if (count($fields_defaults_array) == 0) {
		$this->error("set_fields_defaults: \$fields_defaults_array is a zero-length array.");
		return false;
	}
	if (count($this->field) != count($fields_defaults_array)) {
		$this->error("set_fields_defaults: \$fields_defaults_array and \$this->field have different sizes.");
		return false;
	}
	$this->field_defaults = $fields_defaults_array;
	return true;
}

/**
* A method used to prepare a WHERE condition (SQL) based on values passed through an ordered array
*
* It returns false on error
*
* @access private
* @param array $item_array array of values for the WHERE condition
* @return string
*/
function where_condition($item_array) {
	if (!is_array($item_array)) {
		$this->error("where_condition: \$item_array is not an array.");
		return false;
	}
	$cnt_array = count($item_array);
	if ($cnt_array == 0) {
		$this->error("where_condition: \$item_array is a zero-length array.");
		return false;
	}
	$cnt_min = min(count($this->field), $cnt_array);
	$where_condition = $this->sid_field . " = '" . pgm_session_id() . "'";
	for ($i=0; $i<$cnt_min; $i++) {
		$where_condition .= " AND " . $this->field[$i] . " = '" . $item_array[$i] . "'";
	}
	return $where_condition;
}

/**
* Method that extends where_condition()
*
* If the passed array does not specify values for all fields,
* default values are considered for remaining fields.
*
* @access private
* @return string
*/
function extended_where_condition($item_array) {
	$extended_where_condition = $this->where_condition($item_array);
	$cnt_array = count($item_array);
	$cnt = count($this->field);
	for ($i=$cnt_array; $i<$cnt; $i++) {
		$extended_where_condition .= " AND " . $this->field[$i] . " = '" . $this->field_default[$i] . "'";
	}
	return $extended_where_condition;
}

/**
* Returns the quantity of items in the cart for a given item
*
* This method returns the quantity of items in the cart for a given item,
* that is identified by the content of fields passed through an array.
*
* It can either ignore the defaults or not.
*
* If defaults are ignored and the passed array does not specify values
* for all fields, the WHERE condition does not use fields default values
* in the SELECT, hence the returned quantity may concern more than
* a single entry in the cart; this is useful if, as an example, you want
* to know how many items of a given products of whatever color are
* in the cart: simply use this method ignoring defaults and passing
* an array without the field that would specify the color id.
*
* If defaults are not ignored and the passed array does not specify values
* for all fields, the WHERE condition uses fields default values
* in the SELECT, hence the returned quantity concerns one entry in the cart
* at the most.
*
* It returns false on error
*
* @access public
* @param array $item_array array of values to identify items in the cart
* @param boolean $ignore_defaults it indicates if default values of the fields
*   have to be considered or not
* @return integer
*/
function get_qty($item_array, $ignore_defaults=false) {
	if (!is_array($item_array)) {
		$this->error("get_qty: \$item_array is not an array.");
		return false;
	}
	$query =
		"SELECT SUM(" . $this->qty_field . ") AS qty" .
		" FROM " . $this->cart_table;
	if ($ignore_defaults) {
		$query .= " WHERE " . $this->where_condition($item_array);
	} else {
		$query .= " WHERE " . $this->extended_where_condition($item_array);
	}
	$qid = new DB_Sql($query);
	$qid->next_record();
	return ($qid->f("qty") == "") ? 0 : $qid->f("qty");
}

/**
* Returns the quantity of items in the cart for a given product id
*
* Only the product id is considered, other fields are ignored, hence
* the returned quantity may concern more than a single entry in the cart.
*
* @access public
* @param integer $pid the product identifier
* @return integer
*/
function get_product_qty($pid) {
	$query =
		"SELECT SUM(" . $this->qty_field . ") AS qty" .
		" FROM " . $this->cart_table .
		" WHERE " . $this->field[0] . " = '" . $pid . "' AND " . $this->sid_field . " = '" . pgm_session_id() . "'";
	$qid = new DB_Sql($query);
	$qid->next_record();
	return ($qid->f("qty") == "") ? 0 : $qid->f("qty");
}

/**
* Returns the total quantity of items in the cart
* @access public
* @return integer
*/
function num_items() {
	$query =
		"SELECT SUM(" . $this->qty_field . ") AS qty" .
		" FROM " . $this->cart_table .
		" WHERE " . $this->sid_field . " = '" . pgm_session_id() . "'";
	$qid = new DB_Sql($query);
	$qid->next_record();
	return ($qid->f("qty") == "") ? 0 : $qid->f("qty");
}

/**
* Inserts an item in the cart
* @access private
* @param array $item_array array of values to identify the item to be inserted in the cart
* @param integer $qty the quantity of items to be inserted
* @return boolean
*/
function insert_item($item_array, $qty) {
	if (!is_array($item_array)) {
		$this->error("insert_item: \$item_array is not an array.");
		return false;
	}
	$cnt_item_array = count($item_array);
	if ($cnt_item_array == 0) {
		$this->error("insert_item: \$item_array is a zero-length array.");
		return false;
	}
	if ((string) ((int) $qty) != "$qty") {
		$this->error("insert_item: \$qty is not an integer.");
	}
	$cnt = count($this->field);
	$field_list  = "(" . $this->sid_field;
	for ($i=0; $i<$cnt; $i++) {
		$field_list .= ", " . $this->field[$i];
	}
	$field_list .= ", " . $this->qty_field . ", " . $this->changed_field . ")";
	$cnt_min = min($cnt, $cnt_item_array);
	$values = "('" . pgm_session_id() . "'";
	for ($i=0; $i<$cnt_min; $i++) {
		$values .= ", '" . $item_array[$i] . "'";
	}
	for ($i=$cnt_min; $i<$cnt; $i++) {
		$values .= ", '" . $this->field_default[$i] . "'";
	}
	$now = date("YmdHis", time());
	$values .= ", '" . $qty . "', '" . $now . "')";
	$query =
		"INSERT INTO " . $this->cart_table .
		" " . $field_list . " VALUES " . $values;
	$qid = new DB_Sql($query);
	$this->garbage_collection();
	return true;
}

/**
* This method sets the quantity of items for an item in the cart
* @access public
* @param array $item_array array of values to identify the item
* @param integer $qty the quantity of items
* @return void
*/
function set_item($item_array, $qty) {
	if ((string) ((int) $qty) != "$qty") {
		$this->error("set_item: \$qty is not an integer.");
	}
	if ($qty <= 0) {
		$this->delete_items($item_array);
		return;
	}
	if ($this->get_qty($item_array) == 0) {
		$this->insert_item($item_array, $qty);
		return;
	}
	$now = date("YmdHis", time());
	$query =
		"UPDATE " . $this->cart_table .
		" SET " . $this->qty_field . " = '" . $qty . "', " .
			$this->changed_field . " = '" . $now . "'" .
		" WHERE " . $this->extended_where_condition($item_array);
	$qid = new DB_Sql($query);
	$this->garbage_collection();
}

/**
* Deletes items in the cart for a given item
*
* This method deletes the items in the cart for a given item,
* that is identified by the content of fields passed through an array.
*
* It can either ignore the defaults or not.
*
* If defaults are ignored and the passed array does not specify values
* for all fields, the WHERE condition does not use fields default values
* in the SELECT, hence deletion may concern more than a single entry
* in the cart; this is useful if, as an example, you want to remove
* from the cart a given products of whatever color simply use this method
* ignoring defaults and passing an array without the field that would specify
* the color id. 
*               
* If defaults are not ignored and the passed array does not specify values
* for all fields, the WHERE condition uses fields default values
* in the SELECT, hence deletion concerns one entry in the cart at the most.
*
* @access public
* @param array $item_array array of values to identify items in the cart
* @param boolean $ignore_defaults it indicates if default values of the fields
*   have to be considered or not
* @return void
*/
function delete_items($item_array, $ignore_defaults=true) {
	$query = "DELETE FROM " . $this->cart_table;
	if ($ignore_defaults) {
		$query .= " WHERE " . $this->where_condition($item_array);
	} else {
		$query .= " WHERE " . $this->extended_where_condition($item_array);
	}
	$qid = new DB_Sql($query);
}

/**
* Adds to the cart $qty items of a given item
* @access public
* @param array $item_array array of values to identify the item
* @param integer $qty the quantity of items to be added
* @return void
*/
function add_item($item_array, $qty) {
	if ((string) ((int) $qty) != "$qty") {
		$this->error("add_item: \$qty is not an integer.");
	}
	$num_items = $this->get_qty($item_array);
	if ($num_items == 0) {
		$this->insert_item($item_array, $qty);
	} else {
		$this->set_item($item_array, $num_items+$qty);
	}
}

/**
* Returns a list of products ids in the cart
* @access public
* @return string $products_ids_list a comma separated list of products ids in the cart
*/
function get_items_ids_list() {
	$query =
		"SELECT " . $this->field[0] .
		" FROM " . $this->cart_table .
		" WHERE " . $this->sid_field . " = '" . pgm_session_id() . "'" .
		" ORDER BY " . $this->field[0];
	$qid = new DB_Sql($query);
	$products_ids_list = "";
	while ($qid->next_record()) {
		$products_ids_list .= ", '" . $qid->f($this->field[0]) . "'";
	}
	if ($qid->num_rows() > 0) {
		$products_ids_list = substr($products_ids_list, 2);
	}
	return $products_ids_list;
}

/**
* This method rescans the cart table to refresh and return the items array
* @access public
* @return array
*/
function refresh_items_array() {
	$field_list = $this->qty_field;
	$field_cnt = count($this->field);
	for ($i=0; $i<$field_cnt; $i++) {
		$field_list .= ", " . $this->field[$i];
	}
	$query =
		"SELECT " . $field_list .
		" FROM " . $this->cart_table .
		" WHERE " . $this->sid_field . " = '" . pgm_session_id() . "'" .
		" ORDER BY " . $this->field[0];
	$qid = new DB_Sql($query);
	$this->item = array();
	$cnt = 0;
	while ($qid->next_record()) {
		$this->item[$cnt][$this->qty_field] = $qid->f($this->qty_field);
		for ($i=0; $i<$field_cnt; $i++) {
			$this->item[$cnt][$this->field[$i]] = $qid->f($this->field[$i]);
		}
		$cnt++;
	}
	return $this->item;
}

/**
* This method returns the items array
* @access public
* @return array
*/
function get_items_array() {
	return $this->refresh_items_array();
}

/**
* Garbage collector; it removes old entries from the cart table
* @access public
* @return void
*/
function garbage_collection() {
	srand(time());
	if ((rand()%100) < $this->gc_probability) {
		$sqldate = date("YmdHis", time() - 60*$this->gc_time);
		$query =
			"DELETE FROM " . $this->cart_table .
			" WHERE " . $this->changed_field . " < '" . $sqldate . "'";
		$qid = new DB_Sql($query);
	}
}

/**
* Method to handle errors
* @access private
* @param string $errormsg the error message
* @return void
*/
function error($errormsg) {
	print "<b>" . $this->classname . " Error:</b> " . $errormsg . "<br>\n";
	if ($this->halt_on_error == "yes") {
		die("<b>Halted.</b><br>\n");
	}
}

} /* END OF CLASS */

/* ************************************************************ */
/* ************************************************************ */
/* ************************************************************ */

/**
* This class extends the base class to provide a class conceived specifically for PgMarket
* @package PgMarket
*/
class PGM_Cart extends Cart {

//var $products_table = "products";

/**
* The name of the class
* @access private
* @var string
*/
var $classname = "PGM_Cart";

/** 
* Discounted value of the total price of items in the cart
* @access private 
* @var float
*/
var $discounted_total = 0;
/**
* Total price of items in the cart
* @access private
* @var float
*/
var $total = 0;
var $sale_total = 0;
/**
* Total weight of items in the cart
* @access private
* @var float
*/
var $weight = 0;
/**
* Delivery cost
* @access private
* @var float
*/
var $delivery = 0;
/**
* The grandtotal is the sum of the discounted total + the delivery cost
* @access private
* @var float
*/
var $grandtotal = 0;

/**
* The constructor method
* @return void
*/
function PGM_Cart() {	// object constructor
	$this->init();
	$this->set_fields(array("id", "color_id"));
	$this->set_fields_defaults(array(0, 1));
}

/**
* This method computes the total price, the discounted total, and the total weight, and refreshes the items array; it returns the discounted total
* @access public
* @return float
*/
function calc_total() {
	global $PGM_SESSION;
	global $CFG;


	$this->item = array();
	$this->total = 0;
	$this->sale_total = 0;
	$this->discounted_total = 0;
	$this->weight = 0;

	if ($this->num_items() == 0) {
		return 0;
	}

	$qid = new PGM_Sql();
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("carts_items ci", "colors_i18n cl", "products_i18n pl"),
		array(array("ci." . $this->field[1], "cl.color_id", 1, $PGM_SESSION["lang"]), array("ci." . $this->field[0], "pl.product_id", 1, $PGM_SESSION["lang"])),
		array(array("cl.name AS color_lname"), array("pl.name AS lname")),
		"ci." . $this->qty_field . ", ci." . $this->field[0] . ", ci." . $this->field[1] . ", p.code, p.name, p.price, p.discount, p.discqty, p.weight, i.iva, p.thumbtype, p.thumbwidth, p.thumbheight, c.name AS color_name",
		"products p, iva i, colors c",
		"ci." .  $this->field[0] . " = p.id AND p.iva_id = i.id AND ci." .  $this->field[1] . " = c.id AND " . $this->sid_field . " = '" . pgm_session_id() . "'",
		$this->field[0],
		""
	);

	$cnt = 0;
	while ($qid->next_record()) {
		$this->item[$cnt][$this->qty_field] = $qid->f($this->qty_field);
		$this->item[$cnt][$this->field[0]] = $qid->f($this->field[0]);
		$this->item[$cnt][$this->field[1]] = $qid->f($this->field[1]);
		$this->item[$cnt]["code"] = $qid->f("code");
		$this->item[$cnt]["price"] = $qid->f("price");
		$this->item[$cnt]["discount"] = $qid->f("discount");
		$this->item[$cnt]["discqty"] = $qid->f("discqty");
		$this->item[$cnt]["weight"] = $qid->f("weight");
		$this->item[$cnt]["iva"] = $qid->f("iva");
		$this->item[$cnt]["name"] = $qid->f("name");
		$this->item[$cnt]["lname"] = $qid->f("lname");
		$this->item[$cnt]["color_name"] = $qid->f("color_name");
		$this->item[$cnt]["color_lname"] = $qid->f("color_lname");
		$this->item[$cnt]["thumbtype"] = $qid->f("thumbtype");
		$this->item[$cnt]["thumbwidth"] = $qid->f("thumbwidth");
		$this->item[$cnt]["thumbheight"] = $qid->f("thumbheight");
		$this->item[$cnt]["subtotal"] = $this->item[$cnt][$this->qty_field] * $this->item[$cnt]["price"];
		if ($this->get_product_qty($this->item[$cnt][$this->field[0]]) >= $this->item[$cnt]["discqty"]) {
			$this->item[$cnt]["subtotal"] *= (100 - $this->item[$cnt]["discount"]) / 100;
		}
		if ($CFG["iva_used"]) {
			$this->item[$cnt]["subtotal"] *= (100 + $this->item[$cnt]["iva"]) / 100;
		}
		if ($this->get_product_qty($this->item[$cnt][$this->field[0]]) >= $this->item[$cnt]["discqty"] &&  $this->item[$cnt]["discount"] > 0) {
			$this->sale_total += $this->item[$cnt]["subtotal"];
		} else {
			$this->total += $this->item[$cnt]["subtotal"];
		}
		$this->weight += $this->item[$cnt]["weight"]*$this->item[$cnt][$this->qty_field];
		$cnt++;
	}
	$this->discounted_total = $this->total;
	if ($CFG["user_discount_used"] && is_logged_in()) {
		$this->discounted_total *= (100 - $PGM_SESSION["user"]["user_discount"]) / 100;
	}
	$this->total += $this->sale_total;
	$this->discounted_total += $this->sale_total;
	return $this->discounted_total;
}

/**
* Returns the (not discounted) total price
* @access public
* @return float
*/
function get_total() {
	return $this->total;
}

/**
* Returns the discounted total
* @access public
* @return float
*/
function get_discounted_total() {
	return $this->discounted_total;
}

/**
* Returns the total weight
* @access public
* @return float
*/
function get_weight() {
	return $this->weight;
}

/**
* Returns the items array
* @access public
* @return array
*/
function get_items_array() {
	return $this->item;
}

/**
* This method computes and returns the delivery cost
* @access public
* @return float
*/
function calc_delivery() {
	global $PGM_SESSION;
	global $CFG;

	$this->delivery = 0;
	if (!($CFG["delivery_used"]) || !(isset($PGM_SESSION["zone_id"])) ) {
		return $this->delivery;
	}
	$query =
		"SELECT cost, type" .
		" FROM delivery_costs" .
		" WHERE zone_id = '" . $PGM_SESSION["zone_id"] . "' AND maxqty >= '" . $this->total . "'" .
		" ORDER BY maxqty ASC";
	$qid = new PGM_Sql($query);
	if ($qid->num_rows() > 0) {
		$qid->next_record();
		$this->delivery = $qid->f("cost") * (($qid->f("type") == "U") ? $this->weight : 1);
	}
	return $this->delivery;
}

/**
* Returns the delivery cost
* @access public
* @return float
*/
function get_delivery() {
	return $this->delivery;
}

/**
* This method computes and returns the grandtotal
* @access public
* @return float
*/
function calc_grandtotal() {
	$this->grandtotal = $this->calc_total() + $this->calc_delivery();
	return $this->grandtotal;
}

/**
* Returns the grandtotal
* @access public
* @return float
*/
function get_grandtotal() {
	return $this->grandtotal;
}

} /* END OF CLASS */

?>

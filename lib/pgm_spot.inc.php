<?

// Special stuff for smallpieceoftime website

function spoterrorhandler($errno, $errstr, $errfile, $errline) {
	global $_SERVER;

	if (preg_match('/lib\/phplib/', $errfile) ||
	    preg_match('/lib\/pgm_user/', $errfile) ) {
		return;
	}
	$emailbody = "

Uh Oh,

There has been a problem with the website. The error number is $errno and the
message is '$errstr'.
The error occured on line $errline of file $errfile.
URL is $_SERVER[SCRIPT_URI] .
";

	mail('csmall@enc.com.au', 'Website Error', $emailbody);
}

set_error_handler("spoterrorhandler");

function validate_state($state, &$errors, &$msg) {
	global $CFG;
	include ($CFG["globalerror"]);

	if ($state == "") {
		$errors["state"] = true;
		$msg .= "<li>" . $empty_state;
	} else if (strlen($state) > 4) {
		$errors["state"] = true;
		$msg .= "<li>" . $state_too_long;
	}
}


function print_country($code)
{
	$country_names = array(
  		'au' => 'Australia',
		'nz' => 'New Zealand',
		'xx' => 'Other'
	);
	if (isset($country_names[$code])) {
		return $country_names[$code];
	}
	return $code;
}


// Generate an invoice given an and type.
function generate_invoice($type, $id, $username) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["localedir"] . $PGM_SESSION["lang"] . "/global-names.inc.php");

	if ($type == 'C') {
	$qid_order = new PGM_Sql("
		SELECT o.*, os.name AS state_name
		FROM orderscc o, orders_states os
		WHERE o.state_id = os.id AND o.id = '$id' AND o.username = '$username'
	");
	} else {
	$qid_order = new PGM_Sql("
		SELECT o.*, os.name AS state_name
		FROM ordersca o, orders_states os
		WHERE o.state_id = os.id AND o.id = '$id' AND o.username = '$username'
	");
	}
	if ($qid_order->num_rows() < 1) {
		$t = new Template();
		$t->set_file("page", "templates/no_such_order.ihtml");
		include ($CFG["localelangdir"] . "global-common.inc.php");
		$t->pparse("out", "page");
		return;
	} else {
		$qid_order->next_record();
	}

	$qid_items = new PGM_Sql();
	get_order_items($qid_items, $id);

	$qid_user = new PGM_Sql("SELECT usertype, fiscalcode, email FROM users WHERE username = '$username'");
	$qid_user->next_record();

	$t = new Template();
	$t->set_file("page", $CFG["dirroot"]."templates/invoice.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"order_id"		=> ov($id),
		"order_type"		=> ov($type),
		"wwwroot"		=> $CFG['wwwroot'],
		"order_state_name"	=> $qid_order->f("state_name"),
		"order_date"		=> $userday[$qid_order->f("weekday")] . " " . $qid_order->f("day") . " " . $usermonth[$qid_order->f("month")] . " " . $qid_order->f("year"),
		"order_famount"		=> formatted_price($qid_order->f("amount")),
		"order_custinfo"	=> nl2br(ov($qid_order->f("custinfo"))),
		"user_fiscalcode"	=> $qid_user->f("fiscalcode"),
		"user_email"		=> $qid_user->f("email"),
		"order_notes"		=> nl2br(ov($qid_order->f("notes")))
	));
	$t->set_block("page", "weight_used", "weight_used_blck");
	$t->set_var(array("weight_used_blck" => ""));
	if ($CFG["weight_used"]) {
		$t->parse("weight_used_blck", "weight_used", true);
	}
	$t->set_block("page", "item_row", "item_rows");
	$t->set_var("item_rows", "");
		$t->set_block("item_row", "discount_used_row", "discount_used_row_blck");
		$t->set_block("item_row", "iva_used_row", "iva_used_row_blck");
		$t->set_block("item_row", "weight_used_row", "weight_used_row_blck");
		$t->set_block("item_row", "color_used", "color_used_blck");
	$subtotal = 0;
	while ($qid_items->next_record()) {
		$t->set_var("discount_used_row_blck", "");
		$t->set_var("iva_used_row_blck", "");
		$t->set_var("weight_used_row_blck", "");
		$t->set_var("color_used_blck", "");
		$discount = ($qid_items->f("qty") >= $qid_items->f("purchase_discqty")) ? $qid_items->f("purchase_discount") : 0;
		$total = $qid_items->f("purchase_price")*$qid_items->f("qty")*(1.0+$qid_items->f("purchase_iva")/100.0)*(1.0-$discount/100.0);
		$subtotal += $total;
		$t->set_var(array(
			"prod_qty"		=> $qid_items->f("qty"),
			"prod_code"		=> ($qid_items->f("code")==''?'&nbsp;':$qid_items->f("code")),
			"prod_fcurr_price"	=> formatted_price($qid_items->f("curr_price")),
			"prod_fpurchase_price"	=> formatted_price($qid_items->f("purchase_price")),
			"prod_ftotal"		=> formatted_price($total)
		));
		$foobar = ($qid_items->f("lname") != "" && $qid_items->f("name") == $qid_items->f("oiname")) ? ov($qid_items->f("lname")) : ov($qid_items->f("oiname"));
		if ($qid_items->f("name") != $qid_items->f("oiname")) {
			$foobar = "<div class=\"warning\">" . $foobar . "</div>";
		}
		if ($qid_items->f("name") != "") {
			$foobar = "<a href=\"" . $CFG["wwwroot"] . "shopping/product_details.php?product_id=" . $qid_items->f("product_id") . "\">" . $foobar . "</a>";
		}
		$t->set_var("prod_name", $foobar);
		if ($discount > 0) {
			$t->set_var("prod_purchase_discount", $discount);
			$t->parse("discount_used_row_blck", "discount_used_row", true);
		}
		if ($CFG["iva_used"]) {
			$t->set_var("prod_purchase_iva", $qid_items->f("purchase_iva"));
			$t->parse("iva_used_row_blck", "iva_used_row", true);
		}
		if ($CFG["weight_used"]) {
			$t->set_var("prod_fpurchase_weight", formatted_weight ($qid_items->f("purchase_weight")));
			$t->parse("weight_used_row_blck", "weight_used_row", true);
		}
		if ($qid_items->f("color") != "") {
			$t->set_var("prod_color", $qid_items->f("color"));
			$t->parse("color_used_blck", "color_used", true);
		}
		$t->parse("item_rows", "item_row", true);
	}
	$blurb_rowspan = 2;
	$t->set_block("page", "user_discount_used", "user_discount_used_blck");
	$t->set_var(array("user_discount_used_blck" => ""));
	if ($qid_order->f("user_discount") > 0) {
		$blurb_rowspan++;
		$t->set_var("order_user_discount", $qid_order->f("user_discount"));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
	}
	$t->set_block("page", "delivery_used", "delivery_used_blck");
	$t->set_var(array("delivery_used_blck" => ""));
	if ($CFG["delivery_used"]) {
		$blurb_rowspan++;
		$t->set_var("order_fdelivery", formatted_price($qid_order->f("delivery")));
		$t->parse("delivery_used_blck", "delivery_used", true);
	}
	$t->set_var("order_fsubtotal", formatted_price($subtotal));
	$t->set_var("blurb_rowspan", $blurb_rowspan);

	// Create popup stuff
	$t->set_block("page", "popup_header", "popup_header_blck");
	$t->set_block("page", "popup_footer", "popup_footer_blck");
	$t->set_block("page", "is_not_popup", "is_not_popup_blck");
	$t->set_var(array("popup_header_blck" => "",
		"popup_footer_blck" => '',
		'is_not_popup_blck' => ''
		));
	if (me() == "/users/orderwindow.php") {
		$t->parse("popup_header_blck", "popup_header", true);
		$t->parse("popup_footer_blck", "popup_footer", true);
	} else {
	  $t->parse("is_not_popup_blck", "is_not_popup", true);
	}

	// Create bank auth if it is there
	$t->set_block("page", "bank_auth", "bank_auth_blck");
	$t->set_var(array("bank_auth_blck" => ""));
	if ($type == 'C' && $qid_order->f('state_id') == 2) {
	  $t->set_var('bank_auth_number', $qid_order->f("codaut"));
	  $t->parse("bank_auth_blck", "bank_auth", true);
	}
	$t->set_block("page", "payment_info", "payment_info_blck");
	$t->set_var("payment_info_blck", "");
	if ($qid_order->f("state_id") == 0) {
	  $t->parse("payment_info_blck", "payment_info", true);
	}

	$t->pparse("out", "page");
}

function add_order_notes($qid, $orderid, $type, $notes)
{
  if ($type == 'C') 
  	$table = 'orderscc';
  else
  	$table = 'ordersca';

  $qid->query("UPDATE $table SET ournotes=( SELECT COALESCE(ournotes || '$notes\n', '$notes\n') FROM $table WHERE id = $orderid) WHERE id=$orderid");
  }

?>

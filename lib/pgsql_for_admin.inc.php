<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

// We use UNIONs and not OUTER JOINS to support PostgreSQL < 7.1

function update_sequences() {
	$tables = array("user_discounts", "zones", "categories", "brands", "iva", "colors", "products", "orders_states");
	$qid = new PGM_Sql();
	reset($tables);
	while (list(, $table) = each($tables)) {
		$max = get_table_id_max($qid, $table);
		if ($max == 0) {
			$max = 1;
		}
		$qid->query("SELECT setval('$table" . "_id_seq', $max);");
	}
}

function get_table_id_max(&$qid, $table) {
	$qid->query("SELECT id FROM $table ORDER BY id DESC LIMIT 1,0");
	$qid->next_record();
	return $qid->f("id");
}

/**
* It prepares temporary tables and then calls pg_build_category_tree()
* @return void
*/
function build_category_tree(&$output, &$preselected, $parent=1, $indent="") {
	global $PGM_SESSION;

	$qid = new PGM_Sql();
	$qid->begin();
	$qid->query("
		CREATE TEMPORARY TABLE tmp_categories ()
		INHERITS (categories)
	");
	$qid->query("
		INSERT INTO tmp_categories
		SELECT * FROM categories
	");
	$qid->query("
		CREATE TEMPORARY TABLE tmp_categories_i18n ()
		INHERITS (categories_i18n)
	");
	$qid->query("
		INSERT INTO tmp_categories_i18n
		SELECT * FROM categories_i18n WHERE lang = '" . $PGM_SESSION["lang"] . "' AND category_id <> 1
	");
	pg_build_category_tree($qid, $output, $preselected, $parent=1, $indent="");
	$qid->query("DROP TABLE tmp_categories");
	$qid->query("DROP TABLE tmp_categories_i18n");
	$qid->commit();
}

/**
* Builds the HTML code of the category tree
*
* It recursively goes through the category tree, starting at a parent,
* and drills down, printing options for a selection list box.
* Preselected items are marked as being selected.
* This is not an efficient algorithm because it has to issue one query
* per category!!
* It's used because it is easy to understand.
*
* @return void
*/
function pg_build_category_tree(&$qid, &$output, &$preselected, $parent=1, $indent="") {
	global $PGM_SESSION;

	$qid->query("
		SELECT c.id, c.name, l.name AS lname, c.special_level
		FROM tmp_categories c, tmp_categories_i18n l
		WHERE c.id = l.category_id AND c.parent_id = '$parent'
		UNION ALL
		SELECT c.id, c.name, '' AS lname, c.special_level
		FROM tmp_categories c
		WHERE c.parent_id = '$parent'
		AND c.id NOT IN (
			SELECT category_id FROM tmp_categories_i18n
		)
		ORDER BY special_level, name
	");

	$cat = array();
	$numr = $qid->num_rows();
	for ($i=0; $i<$numr; $i++) {
		$qid->next_record();
		$cat[$i]["id"] = $qid->f("id");
		$cat[$i]["name"] = ($qid->f("lname") != "") ? $qid->f("lname") : $qid->f("name");
	}
	for ($i=0; $i<$numr; $i++) {
		$qid->query("
			DELETE FROM tmp_categories_i18n
			WHERE category_id = '" . $cat[$i]["id"] . "'
		");
	}
	$qid->query("
		DELETE FROM tmp_categories
		WHERE parent_id = '$parent'
	");

	for ($i=0; $i<$numr; $i++) {
		$selected = in_array($cat[$i]["id"], $preselected) ? "selected" : "";
		$output .= "<option value=\"" . ov($cat[$i]["id"]) . "\" $selected>" . $indent . ov($cat[$i]["name"]);
		if ($cat[$i]["id"] != $parent) {
			pg_build_category_tree($qid, $output, $preselected, $cat[$i]["id"], $indent."&nbsp;&nbsp;");
		}
	}
}

?>

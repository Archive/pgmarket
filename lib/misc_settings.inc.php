<?php

$PGM_SESSION["products_per_catalog_page"] = 12;	// max number of products to be shown per page (catalog pages, search results)
$PGM_SESSION["max_results"] = 100;		// max number of search results to accept the search request
$PGM_SESSION["orders_per_page"] = 10;	// max number of orders to be shown per page (admin and users orders pages)

$CFG["user_discount_name_length"] = 100;
$CFG["user_discount_description_length"] = 255;
$CFG["zone_code_length"] = 10;
$CFG["zone_name_length"] = 100;
$CFG["zone_description_length"] = 255;
$CFG["username_length"] = 16;
$CFG["username_length_min"] = 6;
$CFG["password_length"] = 32;
$CFG["priv_length"] = 5;
$CFG["firstname_length"] = 64;
$CFG["lastname_length"] = 64;
$CFG["name_length"] = 64;
$CFG["fiscalcode_length"] = 16;
$CFG["partitaiva_length"] = 11;
$CFG["address_length"] = 255;
$CFG["number_length"] = 50;
$CFG["zip_code_length"] = 4;
$CFG["zip_code_length_max"] = 10;
$CFG["zip_code_length_min"] = 4;
$CFG["city_length"] = 100;
$CFG["country_length"] = 100;
$CFG["phone_length"] = 32;
$CFG["phone_length_min"] = 5;
$CFG["fax_length"] = 32;
$CFG["fax_length_min"] = 5;
$CFG["mobilephone_length"] = 32;
$CFG["mobilephone_length_min"] = 5;
$CFG["email_length"] = 128;
$CFG["notes_length"] = 500;
$CFG["message_length"] = 5000;
$CFG["ournotes_length"] = 500;
$CFG["comments_length"] = 500;

$CFG["lang_length"] = 15;
$CFG["category_name_length"] = 100;
$CFG["category_description_length"] = 2000;
$CFG["brand_name_length"] = 100;
$CFG["brand_description_length"] = 255;
$CFG["iva_description_length"] = 255;
$CFG["color_name_length"] = 100;
$CFG["color_description_length"] = 255;
$CFG["product_code_length"] = 25;
$CFG["product_name_length"] = 100;
$CFG["product_description_length"] = 500;
$CFG["product_extended_description_length"] = 2000;
$CFG["discqtymax"] = 1000;
$CFG["order_state_name_length"] = 100;
$CFG["order_state_description_length"] = 255;

?>

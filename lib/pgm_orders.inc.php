<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

function get_order_items(&$qid, $id) {
	global $PGM_SESSION;
	global $CFG;

	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("order_items oi", "products_i18n l", "products p"),
		array(array("oi.product_id", "l.product_id", 1, $PGM_SESSION["lang"]), array("oi.product_id", "p.id", 0)),
		array(array("l.name AS lname"), array("p.name AS name", "p.price AS curr_price", "p.code AS code")),
		"oi.product_id, oi.name AS oiname, oi.color, oi.price AS purchase_price, oi.discount AS purchase_discount, oi.discqty AS purchase_discqty, oi.iva AS purchase_iva, oi.weight AS purchase_weight, oi.qty",
		"",
		"oi.order_id = '$id'",
		"",
		array()
	);
}

/**
* It saves the order information into $PGM_SESSION["orderinfo"]; it is used in the purchase confirmation stage
* @param array $frm array containing order informations
* @return void
*/
function save_orderinfo(&$frm) {
	global $PGM_SESSION;	

	$PGM_SESSION["orderinfo"]["customer"] = $frm["customer"];
	$PGM_SESSION["orderinfo"]["contact"] = $frm["contact"];
	$PGM_SESSION["orderinfo"]["address"] = $frm["address"];
	$PGM_SESSION["orderinfo"]["state"] = $frm["state"];
	$PGM_SESSION["orderinfo"]["zip_code"] = $frm["zip_code"];
	$PGM_SESSION["orderinfo"]["city"] = $frm["city"];
	$PGM_SESSION["orderinfo"]["country"] = $frm["country"];
//	$PGM_SESSION["orderinfo"]["creditcard"] = $frm["creditcard"];
//	$PGM_SESSION["orderinfo"]["expiry"] = $frm["expiry"];
	$PGM_SESSION["orderinfo"]["comments"] = $frm["comments"];
}

/**
* This function is the counterpart to save_orderinfo
*
* It is used to retrieve the order information in the complete order page;
* it returns false if there is no orderinfo.
*
* @return array
*/
function load_orderinfo() {
	global $PGM_SESSION;	

	if (empty($PGM_SESSION["orderinfo"])) {
		return false;
	} else {
		return $PGM_SESSION["orderinfo"];
	}
}

/**
* This function is called to clear the orderinfo session variable; it should be used after an order has been successfully completed.
* @return void
*/
function clear_orderinfo() {
	global $PGM_SESSION;

	$PGM_SESSION["orderinfo"] = array();
	unset($PGM_SESSION["orderinfo"]);
}

?>

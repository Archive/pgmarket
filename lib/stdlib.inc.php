<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

/**
* It sets a given variable to a default value if it is empty
* @param unknown $var the variable at hand
* @param unknown the default value
* @return void
*/
function setdefault(&$var, $default="") {
	if (empty($var)) {
		$var = $default;
	}
}

/**
* It returns true if a given variable is not set or is equal to the default
* @param unknown $var the variable at hand
* @param unknown the default value
* @return boolean
*/
function cvl(&$var, $default="") {
	return (!isset($var) || $var == $default) ? true : false;
}

/**
* If the passed variable is set, it returns its value; otherwise, it returns the default.
* @param unknown $var the variable at hand
* @param unknown the default value
* @return unknown
*/
function nvl(&$var, $default="") {
	return isset($var) ? $var : $default;
}

/**
* If the passed variable is set, it returns it with the HTML characters (like "<", ">", etc.) properly quoted; otherwise, it returns an empty string.
*
* Note that this function must be called with a variable, while,
* for normal strings or functions, o() has to be used.
*
* @param string $var the variable at hand
* @return string
*/
function ov($var) {
	return isset($var) ? stripslashes(htmlSpecialChars(stripslashes($var))) : "";
}

function ovwbr($var) {
	$foobar = nl2br(ov($var));
	return ($foobar == "") ? "&nbsp;" : $foobar;
}

/**
* If the passed variable is not empty, it returns it with the HTML characters (like "<", ">", etc.) properly quoted; otherwise, it returns an empty string.
* @param string $var the variable at hand
* @return string
*/
function o($var) {
	return empty($var) ? "" : htmlSpecialChars(stripslashes($var));
}

function owbr($var) {
	$foobar = nl2br(o($var));
	return ($foobar == "") ? "&nbsp;" : $foobar;
}

/**
* It takes a URL and returns it without the querystring portion
* @param string $url the URL at hand
* @returns string
*/
function strip_querystring($url) {
	if ($commapos = strpos($url, '?')) {
		return substr($url, 0, $commapos);
	} else {
		return $url;
	}
}

/**
* It returns the name of the current script, without the querystring portion.
*
* This function is necessary because PHP_SELF and REQUEST_URI and PATH_INFO
* return different things depending on a lot of things like your OS, Web
* server, the way PHP is compiled (i.e. as a CGI, module, ISAPI, etc.),
* and so on.
*
* @return string
*/
function me() {
	global $_SERVER;

	if (isset($_SERVER["SCRIPT_NAME"])) {
		$me = $_SERVER["SCRIPT_NAME"];
	} else if (isset($_SERVER["REQUEST_URI"])) {
		$me = $_SERVER["REQUEST_URI"];
	} else if (isset($_SERVER["PHP_SELF"])) {
		$me = $_SERVER["PHP_SELF"];
	} else if (isset($_SERVER["PATH_INFO"])) {
		$me = $_SERVER["PATH_INFO"];
	}

	return strip_querystring($me);
}

/**
* Like me(), but it returns a complete URL
* @return string
*/
function qualified_me() {
	global $_SERVER;

	$protocol = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	$this_host = (isset($_SERVER["HTTP_HOST"])) ? $_SERVER["HTTP_HOST"] : $_SERVER["SERVER_NAME"];

	return $protocol . $this_host . me();
}

/**
* It redirects to a given URL using meta tags
* @param string $url the URL where the client has to be redirected
* @param string $message the message to show to the client
* @param integer $delay the redirection delay
* @return void
*/
function redirect($url, $message="", $delay=0) {
	$url = addsidtourl($url);
	echo	"
		<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">
		<html>
		<head>
		<title></title>
		<meta http-equiv='Refresh' content='$delay; url=$url'>
		</head>
		<body>
		";
	if (!empty($message)) {
		echo	"
		<div style=\"font-family: Arial, Sans-serif; font-size: 12pt;\" align=\"center\">$message</div>
		";
	}
	echo	"
		</body>
		</html>
	";
	die;
}

function addsidtourl($url) {
	if (defined(SID) && SID != "") {
		if (strpos($url, SID) === false) {
			if ($url == strip_querystring($url)) {
				$url .= "?" . SID;
			} else {
				$url .= "&amp;" . SID;
			}
		}
	}
	return $url;
}

/*
function addsidtoquery($query) {
	if (SID != "") {
		if (strpos($query, SID) === false) {
			if ($query == "") {
				$query .= SID;
			} else {
				$query .= "&amp;" . SID;
			}
		}
	}
	return $query;
}
*/

function stripsid($string) {
	if (SID != "") {
		if ($pos = strpos($string, '?')) {
			$flag = 1;
			$url = substr($string, 0, $pos);
			if (strlen($string) > $pos+1) {
				$query = substr($string, $pos+1);
			} else {
				$query = "";
			}
		} else {
			$flag = 0;
			$url = "";
			$query = $string;
		}
		while (!(strpos($query, SID) === false)) {
			if ($query == SID) {
				$query = "";
			} else {
				$query = str_replace(SID . "&amp;", "", $query);
				$query = str_replace("&amp;" . SID, "", $query);
				$query = str_replace(SID . "&", "", $query);
				$query = str_replace("&" . SID, "", $query);
			}
		}
		$string = $url;
		if ($query != "" && ($url != "" || $flag)) {
			$string .= "?";
		}
		$string .= $query;
	}
	return $string;
}

function message($string) {
	echo "<div class=\"normal\">" . $string . "</div>";
}

function error($string) {
	echo "<div class=\"warning\">" . $string . "</div>";
}

?>

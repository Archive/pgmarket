<?php
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");
include ($CFG["libdir"] . $CFG["dbms"] . "_for_admin.inc.php");

$DOC_TITLE = "Zones_Management_i18n";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "edit":
		print_edit_zone_i18n_form($id);
		break;

	case "del":
		delete_zone_i18n($id);
		break;

	case "update":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_zone_name($_POST["lname"], $errors, $errormsg);
		if ($errormsg == "") {
			update_zone_i18n($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_zone_i18n_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_zones_i18n();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_zone_i18n_form_template($frm, $errors) {
	global $PGM_SESSION;
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/zone_i18n_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"			=> $frm["newmode"],
		"id"				=> nvl($frm["id"], ""),
		"frm_zone_name"			=> ov($frm["name"]),
		"i18n"				=> ov($PGM_SESSION["i18n"]),
		"frm_zone_lname"		=> ov($frm["lname"]),
		"errors_zone_name"		=> errmsg(nvl($errors["zone_name"], "")),
		"frm_zone_description"		=> ov($frm["description"]),
		"frm_submit_caption"		=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_edit_zone_i18n_form($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
//	get_zone_info($qid, $id);
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("zones z", "zones_i18n l"),
		array(array("z.id", "l.zone_id", 1, $PGM_SESSION["i18n"])),
		array(array("l.name AS lname")),
		"z.id AS zone_id, z.name, z.description",
		"",
		"z.id = '$id'",
		"",
		""
	);
	$qid->next_record();
	$frm["id"] = $id;
	$frm["name"] = $qid->f("name");
	$frm["lname"] = $qid->f("lname");
	$frm["description"] = $qid->f("description");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_zone_i18n_form_template($frm, $errors);
}

function delete_zone_i18n($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("
		DELETE FROM zones_i18n
		WHERE zone_id = '$id' AND lang = '" . $PGM_SESSION["i18n"] . "'
	");
	$qid->query("
		SELECT name FROM zones
		WHERE id = '$id'
	");
	$qid->next_record();
	$zone_name = $qid->f("name");

	$t = new Template();
	$t->set_file("page", "templates/zone_i18n_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"zone_name"	=> ov($zone_name),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_zone_i18n($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		DELETE FROM zones_i18n
		WHERE zone_id = '" . $frm["id"] . "' AND lang = '" . $PGM_SESSION["i18n"] . "'
	");
	$qid->query("
		INSERT INTO zones_i18n (zone_id, lang, name)
		VALUES ('" . $frm["id"] . "', '" . $PGM_SESSION["i18n"] . "', '" . $frm["lname"] . "')
	");
	$qid->query("
		SELECT name FROM zones
		WHERE id = '" . $frm["id"] . "'
	");
	$qid->next_record();

	$t = new Template();
	$t->set_file("page", "templates/zone_i18n_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name"	=> ov($qid->f("name")),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_zones_i18n() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM zones");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["zones_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
//	$limit_clause = prepare_limit_clause($limit, $result["first"]);
//	get_allzones_info($qid, $limit_clause);
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("zones z", "zones_i18n l"),
		array(array("z.id", "l.zone_id", 1, $PGM_SESSION["i18n"])),
		array(array("l.name AS lname")),
		"z.id AS zone_id, z.name, z.description",
		"",
		"",
		"name",
		array($result["first"], $limit)
	);

	$t = new Template();
	$t->set_file("page", "templates/zone_i18n_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_var("i18n", ov($PGM_SESSION["i18n"]));
	$t->set_block("page", "zone_row", "zone_rows");
	$t->set_var("zone_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"		=> $qid->f("zone_id"),
			"name"		=> ov($qid->f("name")),
			"lname"		=> ov($qid->f("lname")),
			"description"	=> ovwbr($qid->f("description"))
		));
		$t->parse("zone_rows", "zone_row", true);
	}
	$t->pparse("out", "page");
}

?>

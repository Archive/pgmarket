<?php
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

include ($CFG["libdir"] . "misc_adminsettings.inc.php");
$t = new Template();
$t->set_file("page", "templates/header.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->set_var(array(
	"DOC_TITLE"			=> $t->get_var($DOC_TITLE),
	"session_user_customername"	=> o($PGM_SESSION["user"]["firstname"] . " " . $PGM_SESSION["user"]["lastname"]),
	"wwwroot"			=> $CFG["wwwroot"],
	"firstpage"			=> $CFG["firstpage"]
));
$t->set_block("page", "brand_used_header", "brand_used_header_blck");
$t->set_var("brand_used_header_blck", "");
if($CFG["brand_used"]) {
	$t->parse("brand_used_header_blck", "brand_used_header", true);
}
$t->set_block("page", "iva_used_header", "iva_used_header_blck");
$t->set_var("iva_used_header_blck", "");
if($CFG["iva_used"]) {
	$t->parse("iva_used_header_blck", "iva_used_header", true);
}
$t->set_block("page", "color_used_header", "color_used_header_blck");
$t->set_var("color_used_header_blck", "");
if($CFG["color_used"]) {
	$t->parse("color_used_header_blck", "color_used_header", true);
}
$t->set_block("page", "user_discount_used_header", "user_discount_used_header_blck");
$t->set_var("user_discount_used_header_blck", "");
if($CFG["user_discount_used"]) {
	$t->parse("user_discount_used_header_blck", "user_discount_used_header", true);
}
$t->set_block("page", "delivery_used_header", "delivery_used_header_blck");
$t->set_var("delivery_used_header_blck", "");
if($CFG["delivery_used"]) {
	$t->parse("delivery_used_header_blck", "delivery_used_header", true);
}
$t->pparse("out", "page");
?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "User_Discounts_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "add":
		print_add_user_discount_form();
		break;

	case "edit":
		if ($id != 1) {
			print_edit_user_discount_form($id);
		}
		break;

	case "del":
		if ($id != 1) {
			delete_user_discount($id);
		}
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_user_discount($_POST["discount"], $errors, $errormsg);
		validate_user_discount_name($_POST["name"], $errors, $errormsg);
		validate_user_discount_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			insert_user_discount($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_User_Discount";
			include ($CFG["dirroot"] . "form_header.php");
			load_user_discount_form_template($frm, $errors);
		}
		break;

	case "update":
		if ($id == 1) {
			break;
		}
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_user_discount($_POST["discount"], $errors, $errormsg);
		validate_user_discount_name($_POST["name"], $errors, $errormsg);
		validate_user_discount_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			update_user_discount($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_user_discount_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_user_discounts();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_user_discount_form_template($frm, $errors) {
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/user_discount_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"discount_id"		=> nvl($frm["id"]),
		"frm_discount"		=> $frm["discount"],
		"errors_discount"	=> errmsg(nvl($errors["discount"], "")),
		"frm_name"		=> ov($frm["name"]),
		"errors_name"		=> errmsg(nvl($errors["discount_name"], "")),
		"frm_description"	=> ov($frm["description"]),
		"errors_description"	=> errmsg(nvl($errors["discount_description"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_add_user_discount_form() {
	// set default values for the reset of the fields
	$frm["newmode"] = "insert";
	$frm["discount"] = "";
	$frm["name"] = "";
	$frm["description"] = "";
	$frm["submit_caption"] = "Add_User_Discount";
	$errors = array();

	load_user_discount_form_template($frm, $errors);
}

function print_edit_user_discount_form($id) {
	$qid = new PGM_Sql("SELECT discount, name, description FROM user_discounts WHERE id = '$id'");
	$qid->next_record();
	$frm["id"] = $id;
	$frm["discount"] = $qid->f("discount");
	$frm["name"] = $qid->f("name");
	$frm["description"] = $qid->f("description");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_user_discount_form_template($frm, $errors);
}

function delete_user_discount($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();
	// select the name of the percentage to be deleted
	$qid->query("SELECT discount, name FROM user_discounts WHERE id = '$id'");
	$qid->next_record();
	$frm["discount"] = $qid->f("discount");
	$frm["name"] = $qid->f("name");
	// delete this percentage
	$qid->query("DELETE FROM user_discounts WHERE id = '$id'");
	// reset to 1 the user_discount_id for all users with this user_discount_id
	$qid->query("UPDATE users SET user_discount_id = 1 WHERE user_discount_id = '$id'");
	$qid->commit();

	$t = new Template();
	$t->set_file("page", "templates/user_discount_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name_and_discount"	=> ($frm["name"] != "") ? ov($frm["name"] . " - " . $frm["discount"]) : ov($frm["discount"]),
		"backurl"		=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function insert_user_discount($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		INSERT INTO user_discounts (
			 discount
			,name
			,description
		)
		VALUES (
			 '" . $frm["discount"] . "'
			,'" . $frm["name"] . "'
			,'" . $frm["description"] . "'
		)
	");

	$t = new Template();
	$t->set_file("page", "templates/user_discount_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name_and_discount"	=> ($frm["name"] != "") ? ov($frm["name"] . " - " . $frm["discount"]) : ov($frm["discount"]),
		"backurl"		=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_user_discount($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		UPDATE user_discounts SET
			 discount = '" . $frm["discount"] . "'
			,name = '" . $frm["name"] . "'
			,description = '" . $frm["description"] . "'
		WHERE id = '" . $frm["id"] . "'
	");

	$t = new Template();
	$t->set_file("page", "templates/user_discount_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_discount"	=> ov($frm["discount"]),
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_user_discounts() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM user_discounts WHERE id > 1");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["user_discounts_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT id, discount, name, description FROM user_discounts WHERE id > 1 ORDER BY discount
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/user_discount_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "user_discount_row", "user_discount_rows");
	$t->set_var("user_discount_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"			=> $qid->f("id"),
			"name_and_discount"	=> ($qid->f("name") != "") ? ov($qid->f("name") . " - " . $qid->f("discount")) : ov($qid->f("discount")),
			"discount"		=> $qid->f("discount"),
			"name"			=> $qid->f("name"),
			"description"		=> ovwbr($qid->f("description"))
		));
		$t->parse("user_discount_rows", "user_discount_row", true);
	}
	$t->pparse("out", "page");
}

?>

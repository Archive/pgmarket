<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");
include ($CFG["libdir"] . "pgm_orders.inc.php");

$DOC_TITLE = "Orders_Log_cc";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "details":
		print_order_details($id);
		break;

	case "edit":
		print_edit_order_form($id);
		break;

	case "del":
		delete_order($id);
//		list_orders();
		break;

	case "update":
		update_order($_POST);
//		list_orders();
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_orders();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function print_order_details($id) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["localedir"] . $PGM_SESSION["lang"] . "/global-names.inc.php");

	$qid_order = new PGM_Sql("
		SELECT o.*, os.name AS state_name
		FROM orderscc o, orders_states os
		WHERE o.state_id = os.id AND o.id = '$id'
	");
	$qid_order->next_record();

	$qid_items = new PGM_Sql();
	get_order_items($qid_items, $id);

	$order_username = $qid_order->f("username");
	$qid_user = new PGM_Sql("
		SELECT usertype, fiscalcode, email
		FROM users
		WHERE username = '$order_username'
	");
	$qid_user->next_record();

	$t = new Template();
	$t->set_file("page", "templates/ordercc_details.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"id"			=> ov($id),
		"order_state_name"	=> $qid_order->f("state_name"),
		"order_date"		=> $userday[$qid_order->f("weekday")] . " " . $qid_order->f("day") . " " . $usermonth[$qid_order->f("month")] . " " . $qid_order->f("year"),
		"order_username"	=> $qid_order->f("username"),
		"order_famount"		=> formatted_price($qid_order->f("amount")),
		"order_custinfo"	=> nl2br(ov($qid_order->f("custinfo"))),
		"user_fiscalcode"	=> $qid_user->f("fiscalcode"),
		"user_email"		=> $qid_user->f("email"),
		"order_notes"		=> nl2br(ov($qid_order->f("notes"))),
		"order_ournotes"	=> nl2br(ov($qid_order->f("ournotes")))
	));
	$t->set_block("page", "usertype_pf", "usertype_pf_blck");
	$t->set_var("usertype_pf_blck", "");
	$t->set_block("page", "usertype_az", "usertype_az_blck");
	$t->set_var("usertype_az_blck", "");
	if ($qid_user->f("usertype") == "pf") {
		$t->parse("usertype_pf_blck", "usertype_pf", true);
	} else {
		$t->parse("usertype_az_blck", "usertype_az", true);
	}
	$t->set_block("page", "weight_used", "weight_used_blck");
	$t->set_var("weight_used_blck", "");
	if ($CFG["weight_used"])
		$t->parse("weight_used_blck", "weight_used", true);
	$t->set_block("page", "item_row", "item_rows");
	$t->set_var("item_rows", "");
 		$t->set_block("item_row", "discount_used_row", "discount_used_row_blck");
		$t->set_block("item_row", "iva_used_row", "iva_used_row_blck");
 		$t->set_block("item_row", "weight_used_row", "weight_used_row_blck");
		$t->set_block("item_row", "color_used", "color_used_blck");
	$t->set_block("page", "cc_verification", "cc_verification_blck");
	$t->set_var("cc_verification_blck", "");
	$t->set_var(array('order_codaut' => $qid_order->f("codaut"),
	                  'order_otpris' => $qid_order->f("otpris")));
	$t->parse('cc_verification_blck', 'cc_verification', true);
	while ($qid_items->next_record()) {
 		$t->set_var("discount_used_row_blck", "");
		$t->set_var("iva_used_row_blck", "");
 		$t->set_var("weight_used_row_blck", "");
		$t->set_var("color_used_blck", "");
 		$discount = ($qid_items->f("qty") >= $qid_items->f("purchase_discqty")) ? $qid_items->f("purchase_discount") : 0;
 		$total = $qid_items->f("purchase_price")*$qid_items->f("qty")*(1.0+$qid_items->f("purchase_iva")/100.0)*(1.0-$discount/100.0);
		$t->set_var(array(
			"prod_qty"		=> $qid_items->f("qty"),
			"prod_fcurr_price"	=> formatted_price($qid_items->f("curr_price")),
			"prod_fpurchase_price"	=> formatted_price($qid_items->f("purchase_price")),
 			"prod_ftotal"		=> formatted_price($total)
		));
		$foobar = ($qid_items->f("lname") != "" && $qid_items->f("name") == $qid_items->f("oiname")) ? ov($qid_items->f("lname")) : ov($qid_items->f("oiname"));
		if ($qid_items->f("name") != $qid_items->f("oiname"))
			$foobar = "<div class=\"warning\">" . $foobar . "</div>";
		if ($qid_items->f("name") != "")
			$foobar = "<a href=\"" . $CFG["wwwroot"] . "admin/products.php?mode=details&amp;product_id=" . $qid_items->f("product_id") . "\">" . $foobar . "</a>";
		$t->set_var("prod_name", $foobar);
		if ($discount > 0) {
			$t->set_var("prod_purchase_discount", $discount);
			$t->parse("discount_used_row_blck", "discount_used_row", true);
		}
		if ($CFG["iva_used"]) {
			$t->set_var("prod_purchase_iva", $qid_items->f("purchase_iva"));
			$t->parse("iva_used_row_blck", "iva_used_row", true);
		}
		if ($CFG["weight_used"]) {
			$t->set_var("prod_fpurchase_weight", formatted_weight($qid_items->f("purchase_weight")));
			$t->parse("weight_used_row_blck", "weight_used_row", true);
		}
		if ($qid_items->f("color") != "") {
			$t->set_var("prod_color", $qid_items->f("color"));
			$t->parse("color_used_blck", "color_used", true);
		}
		$t->parse("item_rows", "item_row", true);
	}
	$t->set_block("page", "user_discount_used", "user_discount_used_blck");
	$t->set_var(array("user_discount_used_blck" => ""));
	if ($qid_order->f("user_discount") > 0) {
		$t->set_var("order_user_discount", $qid_order->f("user_discount"));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
 	}
	$t->set_block("page", "delivery_used", "delivery_used_blck");
	$t->set_var("delivery_used_blck", "");
	if ($CFG["delivery_used"]) {
		$t->set_var("order_fdelivery", formatted_price($qid_order->f("delivery")));
		$t->parse("delivery_used_blck", "delivery_used", true);
	}
	$t->pparse("out", "page");
}

function print_edit_order_form($id) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["localedir"] . $PGM_SESSION["lang"] . "/global-names.inc.php");

	$qid_order = new PGM_Sql("SELECT * FROM orderscc WHERE id = '$id'");
	$qid_order->next_record();

	$foobar = $qid_order->f("username");
	$qid_user = new PGM_Sql("
		SELECT usertype, fiscalcode, email
		FROM users
		WHERE username = '$foobar'
	");
	$qid_user->next_record();

	$t = new Template();
	$t->set_file("page", "templates/ordercc_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_newmode"		=> "update",
		"id"			=> ov($id),
		"frm_date"		=> ov($userday[$qid_order->f("weekday")] . " " . $qid_order->f("day") . " " . $usermonth[$qid_order->f("month")] . " " . $qid_order->f("year")),
		"frm_year"		=> ov($qid_order->f("year")),
		"frm_month"		=> ov($qid_order->f("month")),
		"frm_day"		=> ov($qid_order->f("day")),
		"frm_weekday"		=> ov($qid_order->f("weekday")),
		"frm_username"		=> ov($qid_order->f("username")),
		"frm_famount"		=> formatted_price($qid_order->f("amount")),
		"order_custinfo"	=> $qid_order->f("custinfo"),
		"user_fiscalcode"	=> $qid_user->f("fiscalcode"),
		"user_email"		=> $qid_user->f("email"),
		"order_notes"		=> $qid_order->f("notes"),
		"order_ournotes"	=> $qid_order->f("ournotes"),
		"frm_submit_caption"	=> $t->get_var("Save_Changes")
	));
	$t->set_block("page", "cc_verification", "cc_verification_blck");
	$t->set_var("cc_verification_blck", "");
	$t->set_var(array('order_codaut' => $qid_order->f("codaut"),
	                  'order_otpris' => $qid_order->f("otpris")));
	$t->parse('cc_verification_blck', 'cc_verification', true);
	$t->set_block("page", "order_state_option", "order_state_options");
	$t->set_var("order_state_options", "");
	$qid_orders_states = new PGM_Sql("SELECT * FROM orders_states ORDER BY name");
	while ($qid_orders_states->next_record()) {
		$t->set_var(array(
			"frm_order_state_id"		=> $qid_orders_states->f("id"),
			"frm_order_state_selected"	=> ($qid_orders_states->f("id") == $qid_order->f("state_id")) ? "selected" : "",
			"frm_order_state_name"		=> $qid_orders_states->f("name")
		));
		$t->parse("order_state_options", "order_state_option", true);
	}
	$t->set_block("page", "usertype_pf", "usertype_pf_blck");
	$t->set_var("usertype_pf_blck", "");
	$t->set_block("page", "usertype_az", "usertype_az_blck");
	$t->set_var("usertype_az_blck", "");
	if ($qid_user->f("usertype") == "pf") {
		$t->parse("usertype_pf_blck", "usertype_pf", true);
	} else {
		$t->parse("usertype_az_blck", "usertype_az", true);
	}
	$t->set_block("page", "weight_used", "weight_used_blck");
	$t->set_var("weight_used_blck", "");
	if ($CFG["weight_used"])
		$t->parse("weight_used_blck", "weight_used", true);
	$t->set_block("page", "item_row", "item_rows");
	$t->set_var("item_rows", "");
		$t->set_block("item_row", "discount_used_row", "discount_used_row_blck");
		$t->set_block("item_row", "iva_used_row", "iva_used_row_blck");
		$t->set_block("item_row", "weight_used_row", "weight_used_row_blck");
		$t->set_block("item_row", "color_used", "color_used_blck");
	$qid_items = new PGM_Sql();
	get_order_items($qid_items, $id);
	while ($qid_items->next_record()) {
		$t->set_var("discount_used_row_blck", "");
		$t->set_var("iva_used_row_blck", "");
		$t->set_var("weight_used_row_blck", "");
		$t->set_var("color_used_blck", "");
		$discount = ($qid_items->f("qty") >= $qid_items->f("purchase_discqty")) ? $qid_items->f("purchase_discount") : 0;
		$total = $qid_items->f("purchase_price")*$qid_items->f("qty")*(1.0+$qid_items->f("purchase_iva")/100.0)*(1.0-$discount/100.0);
		$t->set_var(array(
			"prod_qty"		=> $qid_items->f("qty"),
			"prod_fcurr_price"	=> formatted_price($qid_items->f("curr_price")),
			"prod_fpurchase_price"	=> formatted_price($qid_items->f("purchase_price")),
			"prod_ftotal"		=> formatted_price($total)
		));
		$foobar = ($qid_items->f("lname") != "" && $qid_items->f("name") == $qid_items->f("oiname")) ? ov($qid_items->f("lname")) : ov($qid_items->f("oiname"));
		if ($qid_items->f("name") != $qid_items->f("oiname")) {
			$foobar = "<div class=\"warning\">" . $foobar . "</div>";
		}
		if ($qid_items->f("name") != "") {
			$foobar = "<a href=\"" . $CFG["wwwroot"] . "admin/products.php?mode=details&amp;product_id=" . $qid_items->f("product_id") . "\">" . $foobar . "</a>";
		}
		$t->set_var("prod_name", $foobar);
		if ($discount > 0) {
			$t->set_var("prod_purchase_discount", $discount);
			$t->parse("discount_used_row_blck", "discount_used_row", true);
		}
		if ($CFG["iva_used"]) {
			$t->set_var("prod_purchase_iva", $qid_items->f("purchase_iva"));
			$t->parse("iva_used_row_blck", "iva_used_row", true);
		}
		if ($CFG["weight_used"]) {
			$t->set_var("prod_fpurchase_weight", formatted_weight($qid_items->f("purchase_weight")));
			$t->parse("weight_used_row_blck", "weight_used_row", true);
		}
		if ($qid_items->f("color") != "") {
			$t->set_var("prod_color", $qid_items->f("color"));
			$t->parse("color_used_blck", "color_used", true);
		}
		$t->parse("item_rows", "item_row", true);
	}
	$t->set_block("page", "user_discount_used", "user_discount_used_blck");
	$t->set_var(array("user_discount_used_blck" => ""));
	if ($qid_order->f("user_discount") > 0) {
		$t->set_var("order_user_discount", $qid_order->f("user_discount"));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
 	}
	$t->set_block("page", "delivery_used", "delivery_used_blck");
	$t->set_var("delivery_used_blck", "");
	if ($CFG["delivery_used"]) {
		$t->set_var("order_fdelivery", formatted_price($qid_order->f("delivery")));
		$t->parse("delivery_used_blck", "delivery_used", true);
	}
	$t->pparse("out", "page");
}

function delete_order($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();
	$qid->query("DELETE FROM orderscc WHERE id = '$id'");
	$qid->query("DELETE FROM order_items WHERE order_id = '$id'");
	$qid->commit();

	$t = new Template();
	$t->set_file("page", "templates/order_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"id"		=> ov($id),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_order($frm) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("
		UPDATE orderscc SET
			 custinfo = '" .	nvl($frm["custinfo"], "") . "'
			,notes = '" .		nvl($frm["notes"], "") . "'
			,ournotes = '" .	nvl($frm["ournotes"], "") . "'
			,state_id = '" .	nvl($frm["state_id"], "") . "'
		WHERE id = '" . $frm["id"] . "'
	");

	$t = new Template();
	$t->set_file("page", "templates/order_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"id"		=> ov($frm["id"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_orders() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;
	include ($CFG["localedir"] . $PGM_SESSION["lang"] . "/global-names.inc.php");

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM orderscc");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["orders_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT o.*, os.name AS state_name
		FROM orderscc o, orders_states os
		WHERE o.state_id = os.id
		ORDER BY os.name, o.unixtime ASC
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/ordercc_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "order_row", "order_rows");
	$t->set_var("order_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"			=> ov($qid->f("id")),
			"order_state_name"	=> ov($qid->f("state_name")),
			"order_username"	=> ov($qid->f("username")),
			"order_date"		=> ov($userday[$qid->f("weekday")] . " " . $qid->f("day") . " " . $usermonth[$qid->f("month")] . " " . $qid->f("year")),
			"order_custinfo"	=> ovwbr($qid->f("custinfo")),
			"order_famount"		=> formatted_price($qid->f("amount"))
		));
		$t->parse("order_rows", "order_row", true);
	}
	$t->pparse("out", "page");
}

?>

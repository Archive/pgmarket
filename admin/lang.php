<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "Languages";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "add":
		print_add_lang_form();
		break;

	case "edit":
//		if ($id != 0) {
//			print_edit_lang_form($lang);
//		}
// Currently not implemented for safety reasons
		list_langs();
		break;

	case "del":
//		if ($id != 0) {
//			delete_lang($lang);
//		}
// Currently not implemented for safety reasons
		list_langs();
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_lang($_POST["lang"], $errors, $errormsg);
		if ($errormsg == "") {
			insert_lang($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_Lang";
			include ($CFG["dirroot"] . "form_header.php");
			load_lang_form_template($frm, $errors);
		}
		break;

	case "update":
//		if ($id == 0) {
//			break;
//		}
// Currently not implemented for safety reasons
		list_langs();
		break;

	case "choose":
		select_lang($_GET["lang"]);
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_langs();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_lang_form_template($frm, $errors) {
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/lang_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"frm_lang"		=> nvl($frm["lang"], ""),
		"errors_lang"		=> errmsg(nvl($errors["lang"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_add_lang_form() {
	/* set default values for the reset of the fields */
	$frm["newmode"] = "insert";
	$frm["lang"] = "";
	$frm["submit_caption"] = "Add_Lang";
	$errors = array();
	load_lang_form_template($frm, $errors);
}

function print_edit_lang_form($lang) {
// Currently not implemented for safety reasons
}

function delete_lang($lang) {
// Currently not implemented for safety reasons
}

function insert_lang($frm) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("INSERT INTO languages (lang) VALUES ('" . $frm["lang"] . "')");
	$t = new Template();
	$t->set_file("page", "templates/lang_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_lang"	=> ov($frm["lang"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_lang($frm) {
// Currently not implemented for safety reasons
}

function select_lang($lang) {
	global $PGM_SESSION;
	global $CFG;

	$PGM_SESSION["i18n"] = $lang;
	$t = new Template();
	$t->set_file("page", "templates/lang_selected.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_lang"	=> ov($lang),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_langs() {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("SELECT lang FROM languages ORDER BY lang");
	$t = new Template();
	$t->set_file("page", "templates/lang_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("i18n", $PGM_SESSION["i18n"]);
	$t->set_block("page", "lang_row", "lang_rows");
	$t->set_var("lang_rows", "");
	while ($qid->next_record()) {
		$t->set_var("lang", $qid->f("lang"));
		$t->parse("lang_rows", "lang_row", true);
	}
	$t->pparse("out", "page");
}

?>

<?php
include ("../config.inc.php");
$type = ($_GET["type"] == "jpg") ? "jpeg" : $_GET["type"];
header("Content-Type: image/" . $type);
header("Cache-Control: max-age=0");
header("Expires: " . gmdate("D, d M Y H:i:s", time()) . " GMT");
readfile ($CFG["productsdir"] . $_GET["image"]);
?>

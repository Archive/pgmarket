<?php
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "Zones_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "add":
		print_add_zone_form();
		break;

	case "edit":
		print_edit_zone_form($id);
		break;

	case "del":
		if ($id != 1) {
			delete_zone($id);
		}
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_zone_code($_POST["code"], $errors, $errormsg);
		validate_zone_name($_POST["name"], $errors, $errormsg);
		validate_zone_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			insert_zone($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_Zone";
			include ($CFG["dirroot"] . "form_header.php");
			load_zone_form_template($frm, $errors);
		}
		break;

	case "update":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_zone_code($_POST["code"], $errors, $errormsg);
		validate_zone_name($_POST["name"], $errors, $errormsg);
		validate_zone_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			update_zone($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_zone_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_zones();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_zone_form_template($frm, $errors) {
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/zone_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"id"			=> nvl($frm["id"]),
		"frm_code"		=> ov($frm["code"]),
		"errors_code"		=> errmsg(nvl($errors["zone_code"], "")),
		"frm_name"		=> ov($frm["name"]),
		"errors_name"		=> errmsg(nvl($errors["zone_name"], "")),
		"frm_description"	=> ov($frm["description"]),
		"errors_description"	=> errmsg(nvl($errors["zone_description"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_add_zone_form() {
	// set default values for the reset of the fields
	$frm["newmode"] = "insert";
	$frm["code"] = "";
	$frm["name"] = "";
	$frm["description"] = "";
	$frm["submit_caption"] = "Add_Zone";
	$errors = array();

	load_zone_form_template($frm, $errors);
}

function print_edit_zone_form($id) {
	$qid = new PGM_Sql("SELECT code, name, description FROM zones WHERE id = '$id'");
	$qid->next_record();
	$frm["id"] = $id;
	$frm["code"] = $qid->f("code");
	$frm["name"] = $qid->f("name");
	$frm["description"] = $qid->f("description");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_zone_form_template($frm, $errors);
}

function delete_zone($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();
	// find out the zone name
	$qid->query("SELECT code from zones WHERE id = '$id'");
	$qid->next_record();
	$frm["code"] = $qid->f("code");
	// delete this zone
	$qid->query("DELETE FROM zones WHERE id = '$id'");
	// delete this zone from the zones_i18n table
	$qid->query("DELETE FROM zones_i18n WHERE zone_id = '$id'");
	// reset to 1 (default zone) the zone_id for all users with that zone_id
	$qid->query("UPDATE users SET zone_id = 1 WHERE zone_id = '$id'");
	// delete all delivery costs for this zone
	$qid->query("DELETE FROM delivery_costs WHERE zone_id = '$id'");
	$qid->commit();

	$t = new Template();
	$t->set_file("page", "templates/zone_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_code"	=> ov($frm["code"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function insert_zone($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql();
	$qid->lock("zones");
	$qid->query("
		INSERT INTO zones (
			 code
			,name
			,description
		)
		VALUES (
			 '" . $frm["code"] . "'
			,'" . $frm["name"] . "'
			,'" . $frm["description"] . "'
		)
	");
	$qid->query("SELECT id FROM zones ORDER BY id DESC");
	$qid->next_record();
	$inserted_zone_id = $qid->f("id");
	$qid->unlock();

	$t = new Template();
	$t->set_file("page", "templates/zone_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"id"		=> $inserted_zone_id,
		"frm_code"	=> ov($frm["code"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_zone($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		UPDATE zones SET
			 code = '" . $frm["code"] . "'
			,name = '" . $frm["name"] . "'
			,description = '" . $frm["description"] . "'
		WHERE id = '" . $frm["id"] . "'
	");

	$t = new Template();
	$t->set_file("page", "templates/zone_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"id"		=> $frm["id"],
		"frm_code"	=> ov($frm["code"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_zones() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM zones");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["zones_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT * FROM zones ORDER BY name
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/zone_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "zone_row", "zone_rows");
	$t->set_var("zone_rows", "");
		$t->set_block("zone_row", "delete_action", "delete_action_blck");
		$t->set_var("delete_action_blck", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"		=> $qid->f("id"),
			"code"		=> ov($qid->f("code")),
			"name"		=> ov($qid->f("name")),
			"description"	=> ovwbr($qid->f("description"))
		));
		if ($qid->f("id") != 1) {
			$t->parse("delete_action_blck", "delete_action");
		}
		$t->parse("zone_rows", "zone_row", true);
	}
	$t->pparse("out", "page");
}

?>

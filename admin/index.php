<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "Administration";
include ("header.php");

$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);

$t = new Template();
$t->set_file("page", "templates/" . $PGM_SESSION["lang"] . "/index.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
$t->set_var("cfg_myname", $CFG["myname"]);
$t->pparse("out", "page");

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>

<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$username = nvl($_POST["username"], "");
	$frm = $_POST;
	$errormsg = admin_validate_change_password_form($frm, $errors);

	if ($errormsg == "") {
		admin_update_user_password($frm["newresponse"], $frm["username"]);
		$noticemsg = "Password_Change_Successful";
	}
}
if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$username = nvl($_GET["username"], "");
}

$DOC_TITLE = "Change_Password";
include ("header.php");
include ($CFG["dirroot"] . "form_header.php");

$t = new Template();
$t->set_file("page", "templates/change_password_form.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
$t->set_var(array(
	"libwww"		=> $CFG["libwww"],
	"username"		=> $username,
	"errors_newpassword"	=> errmsg(nvl($errors["newpassword"], "")),
	"errors_newpassword2"	=> errmsg(nvl($errors["newpassword2"], ""))
));
$t->pparse("out", "page");

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function admin_validate_change_password_form(&$frm, &$errors) {
	global $CFG;
	include ($CFG["globalerror"]);

	$errors = array();
	$msg = "";

	if ($frm["newresponse"] == "" || $frm["newresponse2"] == "") {	// the browser is not using JavaScript
		$frm["newresponse"] = md5($frm["newpassword"]);
		$frm["newresponse2"] = md5($frm["newpassword2"]);
	}
	// md5("") = d41d8cd98f00b204e9800998ecf8427e
	if ($frm["newresponse"] == "d41d8cd98f00b204e9800998ecf8427e") {
		$errors["newpassword"] = true;
		$msg .= $no_new_password;
	} else if ($frm["newresponse2"] == "d41d8cd98f00b204e9800998ecf8427e") {
		$errors["newpassword2"] = true;
		$msg .= $no_new_password2;
	} else if ($frm["newresponse"] != $frm["newresponse2"]) {
		$errors["newpassword"] = true;
		$errors["newpassword2"] = true;
		$msg .= $new_passwords_dont_match;
	}

	return $msg;
}

function admin_update_user_password($newpassword, $username) {
	global $CFG;

	$qid = new PGM_Sql("
		UPDATE users
		SET password = '$newpassword'
		WHERE username = '$username'
	");
	$qid->query("SELECT priv FROM users WHERE username = '$username'");
	$qid->next_record();

	$foobar = $qid->f("priv");
	if ($foobar == "admin") {
		mail(
			$CFG["seller_fullname"] . " <" . $CFG["seller_email"] . ">",
			"PgMarket root password changed",
			"PgMarket root password changed",
			"From: " . $CFG["support"]
		);
	}
}

?>

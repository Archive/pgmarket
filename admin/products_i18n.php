<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");
include ($CFG["libdir"] . $CFG["dbms"] . "_for_admin.inc.php");

$DOC_TITLE = "Products_Management_i18n";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$product_id = nvl($_GET["product_id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$product_id = nvl($_POST["product_id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "edit":
		print_edit_product_i18n_form($product_id);
		break;

	case "del":
		delete_product_i18n($product_id);
		break;

	case "update":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		if ($_POST["product_lname"] != "") {
			validate_product_name($_POST["product_lname"], $errors, $errormsg);
		}
		validate_product_description($_POST["product_ldescription"], $errors, $errormsg);
		validate_product_extended_description($_POST["product_lextended_description"], $errors, $errormsg);
		if ($errormsg == "") {
			update_product_i18n($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_product_i18n_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_products_i18n();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_product_i18n_form_template($frm, $errors) {
	global $PGM_SESSION;
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/product_i18n_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_newmode"				=> $frm["newmode"],
		"product_id"				=> nvl($frm["product_id"], ""),
		"frm_product_name"			=> ov($frm["product_name"]),
		"i18n"					=> ov($PGM_SESSION["i18n"]),
		"frm_product_lname"			=> ov($frm["product_lname"]),
		"errors_product_name"			=> errmsg(nvl($errors["product_name"], "")),
		"frm_product_description"		=> ov($frm["product_description"]),
		"frm_product_ldescription"		=> ov($frm["product_ldescription"]),
		"errors_product_description"		=> errmsg(nvl($errors["product_description"], "")),
		"frm_product_extended_description"	=> ov($frm["product_extended_description"]),
		"frm_product_lextended_description"	=> ov($frm["product_lextended_description"]),
		"errors_product_extended_description"	=> errmsg(nvl($errors["product_extended_description"], "")),
		"frm_submit_caption"			=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_edit_product_i18n_form($product_id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
//	get_product_info($qid, $product_id);
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("products p", "products_i18n l"),
		array(array("p.id", "l.product_id", 1, $PGM_SESSION["i18n"])),
		array(array("l.name AS lname", "l.description AS ldescription", "l.extended_description AS lextended_description")),
		"p.id AS product_id, p.name, p.special_level, p.description, p.extended_description",
		"",
		"p.id = '$product_id'",
		"",
		""
	);
	$qid->next_record();
	$frm["product_id"] = $product_id;
	$frm["product_name"] = $qid->f("name");
	$frm["product_lname"] = $qid->f("lname");
	$frm["product_description"] = $qid->f("description");
	$frm["product_ldescription"] = $qid->f("ldescription");
	$frm["product_extended_description"] = $qid->f("extended_description");
	$frm["product_lextended_description"] = $qid->f("lextended_description");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_product_i18n_form_template($frm, $errors);
}

function delete_product_i18n($product_id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("
		DELETE FROM products_i18n
		WHERE product_id = '$product_id' AND lang = '" . $PGM_SESSION["i18n"] . "'
	");
	$qid->query("
		SELECT name FROM products
		WHERE id = '$product_id'
	");
	$qid->next_record();
	$prod["name"] = $qid->f("name");

	$t = new Template();
	$t->set_file("page", "templates/product_i18n_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"prod_name"	=> ov($prod["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_product_i18n($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	if ($frm["product_lextended_description"] == "" && $frm["product_ldescription"] != "") {
		$frm["product_lextended_description"] = $frm["product_ldescription"];
	}

	$qid = new PGM_Sql("
		DELETE FROM products_i18n
		WHERE product_id = '" . $frm["product_id"] . "' AND lang = '" . $PGM_SESSION["i18n"] . "'
	");
	$qid->query("
		INSERT INTO products_i18n (
			 product_id
			,lang
			,name
			,description
			,extended_description
		)
		VALUES (
			 '" . $frm["product_id"] . "'
			,'" . $PGM_SESSION["i18n"] . "'
			,'" . $frm["product_lname"] . "'
			,'" . $frm["product_ldescription"] . "'
			,'" . $frm["product_lextended_description"] . "'
		)
	");
	$qid->query("
		SELECT name FROM products
		WHERE id = '" . $frm["product_id"] . "'
	");
	$qid->next_record();

	$t = new Template();
	$t->set_file("page", "templates/product_i18n_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name"	=> ov($qid->f("name")),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_products_i18n() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM products");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["products_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
//	$limit_clause = prepare_limit_clause($limit, $result["first"]);
//	get_allproducts_info_i18n($qid, $limit_clause);
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("products p", "products_i18n l"),
		array(array("p.id", "l.product_id", 1, $PGM_SESSION["i18n"])),
		array(array("l.name AS lname")),
		"p.id AS product_id, p.name, p.special_level, p.description",
		"",
		"",
		"name, special_level",
		array($result["first"], $limit)
	);

	$t = new Template();
	$t->set_file("page", "templates/product_i18n_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_var("i18n", ov($PGM_SESSION["i18n"]));
	$t->set_block("page", "product_row", "product_rows");
	$t->set_var("product_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"product_id"		=> $qid->f("product_id"),
			"product_name"		=> ov($qid->f("name")),
			"product_lname"		=> ov($qid->f("lname")),
			"product_special_level"	=> $qid->f("special_level"),
			"product_description"	=> ovwbr($qid->f("description"))
		));
		$t->parse("product_rows", "product_row", true);
	}
	$t->pparse("out", "page");
}

?>

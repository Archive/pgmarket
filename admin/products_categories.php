<?php
/* (c) 2000-2001 Marco Pratesi <pratesi@telug.it> */
/* (c) 2000-2001 Fabio Molinari <fabio.m@mclink.it> */
/* with contribution for the script structure 
   from categories.php (c) 2000 Ying Zhang (ying@zippydesign.com) */

/******************************************************************************
 * MAIN
 *****************************************************************************/

include ("../application.inc.php");
require_login();
require_priv("admin");

$qid = new PGM_Sql;

$DOC_TITLE = "Products_and_Categories_Management";
include ("header.php");

switch (nvl($mode)) {
	case "import" :
		print_import_products_categories_form();
		break;

	case "update_imp" :
		if ($HTTP_POST_FILES['userfile']['tmp_name'] == "none" || $HTTP_POST_FILES['userfile']['tmp_name'] == "") // "" --> Opera 5
			print_import_products_categories_form();
		else
			import_products_categories($HTTP_POST_FILES);
		break;

	default :
		print_import_products_categories_form();
		break;
}

include ("footer.php");

/******************************************************************************
 * FUNCTIONS
 *****************************************************************************/

function print_import_products_categories_form() {

	global $CFG, $ME, $SETTING;
//	include ($CFG->globalerror);

	/* set values for the form */
	$frm["submit_caption"] = "Import_Button_Label";
	$frm["newmode"] = "update_imp";

	$t = new Template();
	$t->set_file(array("page" => "templates/data_import_form.ihtml"));
	include ("$CFG->localelangdir" . "/global-common.inc.php");
	include ("$CFG->localelangdir" . "/global-admin.inc.php");
	$t->set_var("ME", $ME);
	$t->set_var(array(
		"frm_newmode"			=> $frm["newmode"],
		"session_max_file_size_import"	=> $SETTING["max_file_size_import"],
		"frm_submit_caption"		=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function import_products_categories($HTTP_POST_FILES) {

	global $CFG, $ME, $SESSION;
	include ($CFG->globalerror);
	global $qid;

	/* Let's open a transaction */
	$qid->begin();

	$errormsg = "";

	$fcontents = file ($HTTP_POST_FILES['userfile']['tmp_name']);
	/* Traverse the file */
	while (list ($line_num, $line) = each ($fcontents)) {
		$line = ereg_replace(chr(13), "", $line);	// Microsoft Stupidity Suppression
		$fldarr = split("\t",ereg_replace("\n$","",$line));
		/* dangerous characters quoting */
		for ($i=0; $i<count($fldarr); $i++)
			$fldarr[$i] = addslashes($fldarr[$i]);

		/* Let's see if the product exists ... */
		$qid->Query("SELECT id FROM products WHERE code = '$fldarr[0]'");
		if ($qid->num_rows() == 0) {
			/* ... if it doesn't exist let's return an error ... */
			$appoggio = $line_num + 1;
			$errormsg .= "<li>" . $appoggio . $Err_ImpProd . $fldarr[0] . "'.";
		} else {
			/* ... otherwise let's save its ID ... */
			$qid->next_record();
			$pid = $qid->f("id");

			/* ... let's see if the category exists ... */
			$qid->Query("SELECT id FROM categories WHERE name = '$fldarr[1]'");

			if ($qid->num_rows() == 0) {
				/* ... if it doesn't exist let's return an error ... */
				$appoggio = $line_num + 1;
				$errormsg .= "<li>" . $appoggio . $Err_ImpCat . $fldarr[1] . "'.";
			} else {
				/* ... otherwise let's save its ID ... */
				$qid->next_record();
				$cid = $qid->f("id");

				/* ... let's see if the association already exists ... */
				$qid->Query("SELECT category_id FROM products_categories WHERE product_id = '$pid' AND category_id = '$cid'");
				if ($qid->num_rows() == 0) {
					/* ... if it doesn't exist let's create it ... */
					$qid->Query("
						INSERT INTO products_categories (product_id, category_id)
						VALUES ('$pid', '$cid')
					");
				}
			}
		}
	}

	/* Let's close the transaction */
	$qid->commit();
	
	/* The End */
	include ("$CFG->dirroot" . "/form_header.php");
	$t = new Template();
	$t->set_file(array("page" => "templates/data_import_end.ihtml"));
	include ("$CFG->localelangdir" . "/global-common.inc.php");
	include ("$CFG->localelangdir" . "/global-admin.inc.php");
	$t->set_var("ME", $ME);
	$t->set_var("backurl", $SESSION["goback"]["request_uri"]);
	$t->pparse("out", "page");
}

?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "Users_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$username = nvl($_GET["username"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$username = nvl($_POST["username"], 1);
	$mode = nvl($_POST["mode"], "");
}

if (!isset($_GET["usertype"]) || $_GET["usertype"] == "") {
	$usertype = "pf";
} else {
	$usertype = $_GET["usertype"];
}

switch (nvl($mode)) {
	case "details":
		print_user_details($username);
		break;

	case "add":
		print_add_user_form($usertype);
		break;

	case "edit":
		print_edit_user_form($username);
		break;

	case "del":
		delete_user($username);
//		list_users();
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		include ($CFG["globalerror"]);
		validate_username($_POST["username"], $errors, $errormsg);
		if (username_exists($_POST["username"])) {
			$errors["username"] = true;
			$errormsg = "<li>" . $the_username . " <b>" . ov($_POST["username"]) ."</b> " . $already_exists . $errormsg;
		}
		if ($_POST["response"] == "" || $_POST["response2"] == "") {	// the browser is not using JavaScript
			$_POST["response"] = md5($_POST["password"]);
			$_POST["response2"] = md5($_POST["password2"]);
		}
		if ($_POST["response"] == $_POST["response2"]) {
			validate_password($_POST["response"], $errors, $errormsg);
		} else {
			$errors["password"] = true;
			$errormsg .= "<li>" . $passwords_do_not_match;
		}
/*
		if (email_exists($_POST["email"])) {
			$errors["email"] = true;
			$errormsg .= "<li>" . $the_email_address . " <b>" . ov($_POST["email"]) ."</b> " . $already_exists;
		}
*/
		validate_user_form($_POST, $errors, $errormsg);
		if ($errormsg == "") {
			insert_user($_POST);
//			list_users();
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_User";
			include ($CFG["dirroot"] . "form_header.php");
			load_user_form_template($frm, $errors);
		}
		break;

	case "update":
		$errors = array();
		$errormsg = "";
/*
		if ($_POST["old_email"] != $_POST["email"]
		     && email_exists($_POST["email"])) {
			$errors["email"] = true;
			$errormsg .= "<li>" . $the_email_address . " <b>" . ov($_POST["email"]) ."</b> " . $already_exists;
		}
*/
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_user_form($_POST, $errors, $errormsg);
		if ($errormsg == "") {
			update_user($_POST);
//			list_users();
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_user_form_template($frm, $errors);
		}
		break;

	case "resetpw":
		$return = reset_user_password($username);
		if ($return == 0) {
			$t = new Template();
			$t->set_file("page", "templates/" . $PGM_SESSION["lang"] . "/user_password_reset.ihtml");
			include ($CFG["localelangdir"] . "global-common.inc.php");
			$t->set_var("username", ov($username));
			$t->pparse("out", "page");
		} else {
			include ("templates/" . $PGM_SESSION["lang"] . "/password_not_reset.ihtml");
		}
		list_users();
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_users();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function validate_user_form(&$frm, &$errors, &$errormsg) {
	global $CFG;
//	include ($CFG["libdir"] . "pgm_validate.inc.php");

	validate_priv($frm["priv"], $errors, $errormsg);
	if ($frm["usertype"] == "pf") {
		validate_firstname($frm["firstname"], $errors, $errormsg);
		validate_lastname($frm["lastname"], $errors, $errormsg);
		if (!empty($frm["fiscalcode"])) {
			validate_fiscalcode($frm["fiscalcode"], $errors, $errormsg);
		}
	} else if ($frm["usertype"] == "az") {
		validate_ragionesociale($frm["firstname"], $errors, $errormsg);
		validate_partitaiva($frm["fiscalcode"], $errors, $errormsg);
	}
	validate_address($frm["address"], $errors, $errormsg);
	validate_zip_code($frm["zip_code"], $errors, $errormsg);
	validate_city($frm["city"], $errors, $errormsg);
	validate_state($frm["state"], $errors, $errormsg);
	validate_country($frm["country"], $errors, $errormsg);
	validate_phone($frm["phone"], $errors, $errormsg);
	validate_fax($frm["fax"], $errors, $errormsg);
	validate_mobilephone($frm["mobilephone"], $errors, $errormsg);
	validate_email($frm["email"], $errors, $errormsg);
	validate_notes($frm["notes"], $errors, $errormsg);
	validate_ournotes($frm["ournotes"], $errors, $errormsg);
}

function load_user_form_template($frm, $errors) {
	global $CFG;

	$qid = new PGM_Sql();

	$t = new Template();
	if ($frm["newmode"] == "insert") {
		$t->set_file("page", "templates/user_form_signup-" . $frm["usertype"] . ".ihtml");
		$t->set_var("libwww", $CFG["libwww"]);
	} else {
		$t->set_file("page", "templates/user_form_change_settings-" . $frm["usertype"] . ".ihtml");
	}
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"frm_old_email"		=> $frm["old_email"],
		"frm_username"		=> ov($frm["username"]),
		"errors_username"	=> errmsg(nvl($errors["username"], "")),
		"errors_password"	=> errmsg(nvl($errors["password"], "")),
		"frm_priv"		=> ov($frm["priv"]),
		"errors_priv"		=> errmsg(nvl($errors["priv"], "")),
		"frm_firstname"		=> ov($frm["firstname"]),
		"errors_firstname"	=> errmsg(nvl($errors["firstname"], "")),
		"frm_lastname"		=> ov($frm["lastname"]),
		"errors_lastname"	=> errmsg(nvl($errors["lastname"], "")),
		"frm_fiscalcode"	=> ov($frm["fiscalcode"]),
		"errors_fiscalcode"	=> errmsg(nvl($errors["fiscalcode"], "")),
		"frm_address"		=> ov($frm["address"]),
		"errors_address"	=> errmsg(nvl($errors["address"], "")),
		"errors_state"		=> errmsg(nvl($errors["state"], "")),
		"frm_zip_code"		=> ov($frm["zip_code"]),
		"errors_zip_code"	=> errmsg(nvl($errors["zip_code"], "")),
		"frm_city"		=> ov($frm["city"]),
		"errors_city"		=> errmsg(nvl($errors["city"], "")),
		"errors_country"	=> errmsg(nvl($errors["country"], "")),
		"frm_phone"		=> ov($frm["phone"]),
		"errors_phone"		=> errmsg(nvl($errors["phone"], "")),
		"frm_fax"		=> ov($frm["fax"]),
		"errors_fax"		=> errmsg(nvl($errors["fax"], "")),
		"frm_mobilephone"	=> ov($frm["mobilephone"]),
		"errors_mobilephone"	=> errmsg(nvl($errors["mobilephone"], "")),
		"frm_email"		=> ov($frm["email"]),
		"errors_email"		=> errmsg(nvl($errors["email"], "")),
		"frm_authdata"		=> nvl($frm["authdata"], "") ? "checked" : "" ,
		"frm_acceptadvert"	=> nvl($frm["acceptadvert"], "") ? "checked" : "" ,
		"frm_notes"		=> ov($frm["notes"]),
		"errors_notes"		=> errmsg(nvl($errors["notes"], "")),
		"frm_ournotes"		=> ov($frm["ournotes"]),
		"errors_ournotes"	=> errmsg(nvl($errors["ournotes"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->set_var($frm["state"]."_selected", "selected");
	$t->set_var($frm["country"]."_selected", "selected");
	$t->set_block("page", "user_discount_used", "user_discount_used_blck");
	$t->set_var("user_discount_used_blck", "");
	if($CFG["user_discount_used"]) {
		$qid->query("SELECT id, discount, name FROM user_discounts");
		$t->set_block("user_discount_used", "user_discount_used_option", "user_discount_used_options");
		$t->set_var("user_discount_used_options", "");
		while ($qid->next_record()) {
			$t->set_var(array(
				"frm_user_discount_id"		=> $qid->f("id"),
				"frm_user_discount_selected"	=> ($qid->f("id") == nvl($frm["user_discount_id"], "")) ? "selected" : "",
				"frm_name_and_discount"		=> ($qid->f("name") != "") ? ov($qid->f("name") . " - " . $qid->f("discount") . "%") : ov($qid->f("discount") . "%")
			));
			$t->parse("user_discount_used_options", "user_discount_used_option", true);
		}
		$t->parse("user_discount_used_blck", "user_discount_used", true);
	}
	$t->set_block("page", "delivery_used", "delivery_used_blck");
	$t->set_var("delivery_used_blck", "");
	if ($CFG["delivery_used"] && !($CFG["only_one_delivery_zone"])) {
		$t->set_block("delivery_used", "delivery_zone_option", "delivery_zone_options");
		$t->set_var("delivery_zone_options", "");
		get_zones_names($qid);
		while ($qid->next_record()) {
			$t->set_var(array(
				"frm_user_zone_id"		=> $qid->f("zone_id"),
				"frm_user_zone_selected"	=> ($qid->f("zone_id") == nvl($frm["zone_id"], "")) ? "selected" : "",
				"frm_user_zone_name"		=> ($qid->f("lname") != "") ? ov($qid->f("lname")) : ov($qid->f("name"))
			));
			$t->parse("delivery_zone_options", "delivery_zone_option", true);
		}
		$t->parse("delivery_used_blck", "delivery_used", true);
	}
	$t->pparse("out", "page");
}

function print_user_details($username) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["localedir"] . $PGM_SESSION["lang"] . "/global-names.inc.php");

	$qid = new PGM_Sql("
		SELECT u.*, ud.discount, ud.name AS udname
		FROM users u, user_discounts ud
		WHERE u.user_discount_id = ud.id AND username = '$username'
	");

	$t = new Template();

	if ($qid->num_rows() == 0) {
		$t->set_file("page", "templates/user_not_found.ihtml");
		$t->set_var(array(
			"username"	=> $username,
			"user_not_found"=> $user_not_found
		));
		$t->pparse("out", "page");
		return;
	}

	$qid->next_record();

	$t->set_file("page", "templates/user_details-" . $qid->f("usertype") . ".ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_username"		=> ov($qid->f("username")),
		"frm_priv"		=> ov($qid->f("priv")),
		"frm_firstname"		=> ov($qid->f("firstname")),
		"frm_lastname"		=> ov($qid->f("lastname")),
		"frm_fiscalcode"	=> ov($qid->f("fiscalcode")),
		"frm_address"		=> ovwbr($qid->f("address")),
		"frm_zip_code"		=> ov($qid->f("zip_code")),
		"frm_city"		=> ov($qid->f("city")),
		"frm_phone"		=> ov($qid->f("phone")),
		"frm_fax"		=> ov($qid->f("fax")),
		"frm_mobilephone"	=> ov($qid->f("mobilephone")),
		"frm_email"		=> ov($qid->f("email")),
		"frm_authdata"		=> $qid->f("authdata") ? strtoupper($CFG["yes"]) : "" ,
		"frm_acceptadvert"	=> $qid->f("acceptadvert") ? strtoupper($CFG["yes"]) : "" ,
		"frm_notes"		=> ovwbr($qid->f("notes")),
		"frm_ournotes"		=> ovwbr($qid->f("ournotes")),
		"frm_date"		=> $userday[$qid->f("weekday")] . " " . $qid->f("day") . " " . $usermonth[$qid->f("month")] . " " . $qid->f("year")
	));
	$t->set_var($frm["state"]."_selected", "selected");
	$t->set_var($frm["country"]."_selected", "selected");
	$t->set_block("page", "user_discount_used", "user_discount_used_blck");
	$t->set_var("user_discount_used_blck", "");
	if ($CFG["user_discount_used"]) {
		$t->set_var("frm_udname_and_discount", ($qid->f("udname") != "") ? ov($qid->f("udname") . " - " . $qid->f("discount") . "%") : ov($qid->f("discount") . "%"));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
	}
	$t->set_block("page", "delivery_used", "delivery_used_blck");
	$t->set_var("delivery_used_blck", "");
	if ($CFG["delivery_used"] && !($CFG["only_one_delivery_zone"])) {
		$foobar = $qid->f("zone_id");
		get_zone_name($qid, $foobar);
		$qid->next_record();
		$zone_name = ($qid->f("lname") != "") ? ov($qid->f("lname")) : ov($qid->f("name"));
		$t->set_var("frm_zone", $zone_name);
		$t->parse("delivery_used_blck", "delivery_used", true);
	}
	$t->pparse("out", "page");
}

function print_add_user_form($usertype) {
	// set default values for the reset of the fields
	$frm["usertype"] = $usertype;
	$frm["username"] = "";
	$frm["priv"] = "";
	$frm["firstname"] = "";
	$frm["lastname"] = "";
	$frm["fiscalcode"] = "";
	$frm["address"] = "";
	$frm["state"] = "";
	$frm["zip_code"] = "";
	$frm["city"] = "";
	$frm["country"] = "";
	$frm["phone"] = "";
	$frm["fax"] = "";
	$frm["mobilephone"] = "";
	$frm["email"] = "";
	$frm["authdata"] = 0;
	$frm["acceptadvert"] = 1;
	$frm["notes"] = "";
	$frm["ournotes"] = "";

	$frm["old_email"] = "";
	$frm["newmode"] = "insert";
	$frm["submit_caption"] = "Add_User";
	$errors = array();

	load_user_form_template($frm, $errors);
}

function print_edit_user_form($username) {
	$qid = new PGM_Sql("SELECT * FROM users WHERE username = '$username'");
	$qid->next_record();
	$frm["username"] = $qid->f("username");
	$frm["usertype"] = $qid->f("usertype");
	$frm["priv"] = $qid->f("priv");
	$frm["user_discount_id"] = $qid->f("user_discount_id");
	$frm["zone_id"] = $qid->f("zone_id");
	$frm["firstname"] = $qid->f("firstname");
	$frm["lastname"] = $qid->f("lastname");
	$frm["fiscalcode"] = $qid->f("fiscalcode");
	$frm["address"] = $qid->f("address");
	$frm["state"] = $qid->f("state");
	$frm["zip_code"] = $qid->f("zip_code");
	$frm["city"] = $qid->f("city");
	$frm["country"] = $qid->f("country");
	$frm["phone"] = $qid->f("phone");
	$frm["fax"] = $qid->f("fax");
	$frm["mobilephone"] = $qid->f("mobilephone");
	$frm["email"] = $qid->f("email");
	$frm["authdata"] = $qid->f("authdata");
	$frm["acceptadvert"] = $qid->f("acceptadvert");
	$frm["notes"] = $qid->f("notes");
	$frm["ournotes"] = $qid->f("ournotes");
	$frm["unixtime"] = $qid->f("unixtime");
	$frm["year"] = $qid->f("year");
	$frm["month"] = $qid->f("month");
	$frm["day"] = $qid->f("day");
	$frm["weekday"] = $qid->f("weekday");
	$frm["fromip"] = $qid->f("fromip");

	$frm["old_email"] = $frm["email"];

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_user_form_template($frm, $errors);
}

function delete_user($username) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("SELECT priv FROM users WHERE username = '$username'");
	$qid->next_record();
	if ($qid->f("priv") != "admin") {
		$qid->query("DELETE FROM users WHERE username = '$username'");

		$t = new Template();
		$t->set_file("page", "templates/user_deleted.ihtml");
		include ($CFG["localelangdir"] . "global-common.inc.php");
		include ($CFG["localelangdir"] . "global-admin.inc.php");
		$t->set_var(array(
			"username"	=> ov($username),
			"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
		));
		$t->pparse("out", "page");
	} else {
		include ("templates/" . $PGM_SESSION["lang"] . "/admin_not_deleted.ihtml");
	}
}

function insert_user($frm) {
	global $_SERVER;
	global $PGM_SESSION;
	global $CFG;

//	$date = $myday[date("w")] . date(" j") . "-" . date("n") . "-" . date("Y H:i:s");
	$Dateusec = gettimeofday(); $unixtime = $Dateusec["sec"];
	$year = date("Y"); $month = date("n"); $day = date("j"); $weekday = date("w");
	$fromip = $_SERVER["REMOTE_ADDR"];

	$qid = new PGM_Sql("
		INSERT INTO users (
			 username
			,password
			,usertype
			,priv
			,user_discount_id
			,zone_id
			,firstname
			,lastname
			,fiscalcode
			,address
			,state
			,zip_code
			,city
			,country
			,phone
			,fax
			,mobilephone
			,email
			,authdata
			,acceptadvert
			,notes
			,ournotes
			,unixtime
			,year
			,month
			,day
			,weekday
			,fromip
		)
		VALUES (
			 '" . nvl($frm["username"], "") . "'
			,'" . nvl($frm["response"], "") . "'
			,'" . nvl($frm["usertype"], "") . "'
			,'" . nvl($frm["priv"], "") . "'
			,'" . nvl($frm["user_discount_id"], "") . "'
			,'" . nvl($frm["zone_id"], "") . "'
			,'" . nvl($frm["firstname"], "") . "'
			,'" . nvl($frm["lastname"], "") . "'
			,'" . nvl($frm["fiscalcode"], "") . "'
			,'" . nvl($frm["address"], "") . "'
			,'" . nvl($frm["state"], "") . "'
			,'" . nvl($frm["zip_code"], "") . "'
			,'" . nvl($frm["city"], "") . "'
			,'" . nvl($frm["country"], "") . "'
			,'" . nvl($frm["phone"], "") . "'
			,'" . nvl($frm["fax"], "") . "'
			,'" . nvl($frm["mobilephone"], "") . "'
			,'" . nvl($frm["email"], "") . "'
			,'" . nvl($frm["authdata"], "") . "'
			,'" . nvl($frm["acceptadvert"], "") . "'
			,'" . nvl($frm["notes"], "") . "'
			,'" . nvl($frm["ournotes"], "") . "'
			,'$unixtime'
			,'$year'
			,'$month'
			,'$day'
			,'$weekday'
			,'$fromip'
		)
	");

	$t = new Template();
	$t->set_file("page", "templates/user_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_username"	=> ov($frm["username"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_user($frm) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("
		UPDATE users SET
			 priv = '"		. nvl($frm["priv"], "") . "'
			,user_discount_id = '"	. nvl($frm["user_discount_id"], "") . "'
			,zone_id = '"		. nvl($frm["zone_id"], "") . "'
			,firstname = '"		. nvl($frm["firstname"], "") . "'
			,lastname = '"		. nvl($frm["lastname"], "") . "'
			,fiscalcode = '"	. nvl($frm["fiscalcode"], "") . "'
			,address = '"		. nvl($frm["address"], "") . "'
			,state = '"		. nvl($frm["state"], "") . "'
			,zip_code = '"		. nvl($frm["zip_code"], "") . "'
			,city = '"		. nvl($frm["city"], "") . "'
			,country = '"		. nvl($frm["country"], "") . "'
			,phone = '"		. nvl($frm["phone"], "") . "'
			,fax = '"		. nvl($frm["fax"], "") . "'
			,mobilephone = '"	. nvl($frm["mobilephone"], "") . "'
			,email = '"		. nvl($frm["email"], "") . "'
			,authdata = '"		. nvl($frm["authdata"], "") . "'
			,acceptadvert = '"	. nvl($frm["acceptadvert"], "") . "'
			,notes = '"		. nvl($frm["notes"], "") . "'
			,ournotes = '"		. nvl($frm["ournotes"], "") . "'
		WHERE username = '"		. nvl($frm["username"], "") . "'
	");

	$t = new Template();
	$t->set_file("page", "templates/user_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"username"	=> ov($frm["username"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_users() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(username) AS cnt FROM users WHERE username <> 'root'");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["users_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT username, firstname, lastname, phone, email, authdata, acceptadvert
		FROM users
		WHERE username <> 'root'
		ORDER BY username ASC
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/user_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "user_row", "user_rows");
	$t->set_var("user_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"username"	=> ov($qid->f("username")),
			"firstname"	=> ov($qid->f("firstname")),
			"lastname"	=> $qid->f("lastname") == "" ? "&nbsp;" : ov($qid->f("lastname")),
			"phone"		=> ov($qid->f("phone")),
			"email"		=> ov($qid->f("email")),
			"authdata"	=> $qid->f("authdata") ? strtoupper($CFG["yes"]) : "&nbsp;",
			"acceptadvert"	=> $qid->f("acceptadvert") ? strtoupper($CFG["yes"]) : "&nbsp;"
		));
		$t->parse("user_rows", "user_row", true);
	}
	$t->pparse("out", "page");
}

?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");
include ($CFG["libdir"] . $CFG["dbms"] . "_for_admin.inc.php");
include ($CFG["libdir"] . $CFG["dbms"] . "_layersmenu_queries.inc.php");

$DOC_TITLE = "Data_Import";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "update_imp" :
		include ($CFG["libdir"] . "admin_import_data_files.inc.php");
		if (nvl($_FILES["userfile"]["tmp_name"], "") == "none" || nvl($_FILES["userfile"]["tmp_name"], "") == "") {	// "" --> Opera 5
			include ($CFG["globalerror"]);
			echo "<div class=\"warning\">$current_products_data_will_be_lost</div>";
			print_import_form();
		} else {
			import_tables_data();
			if ($CFG["layersmenu_cached"]) {
				refresh_cache();
			}
		}
		break;

	case "imp_uploaded":
		include ($CFG["libdir"] . "admin_import_data_files.inc.php");
		import_tables_data(false);
		break;

	default :
		include ($CFG["globalerror"]);
		echo "<div class=\"warning\">$current_products_data_will_be_lost</div>";
		include ($CFG["libdir"] . "admin_import_data_files.inc.php");
		print_import_form();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>

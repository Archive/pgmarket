<?php
/* (c) 2001 Marco Pratesi <pratesi@telug.it> */
/* (c) 2000-2001 Fabio Molinari <fabio.m@mclink.it> */
/* with contribution for the script structure 
   from categories.php (c) 2000 Ying Zhang (ying@zippydesign.com) */

/******************************************************************************
 * MAIN
 *****************************************************************************/

include ("../application.inc.php");
require_login();
require_priv("admin");

$qid = new PGM_Sql;

$DOC_TITLE = "Images_Import";
include ("header.php");

switch (nvl($mode)) {
	case "update_impimg" :
		if ($HTTP_POST_FILES['userfile']['tmp_name'] == "none" || $HTTP_POST_FILES['userfile']['tmp_name'] == "") // "" --> Opera 5
			print_new_import_images_form();
		else
			new_import_images($HTTP_POST_FILES);
		break;

	default :
		print_new_import_images_form();
		break;
}

include ("footer.php");

/******************************************************************************
 * FUNCTIONS
 *****************************************************************************/

function print_new_import_images_form() {

	global $CFG, $ME, $SETTING;
//	include ($CFG->globalerror);

	/* set values for the form */
	$frm["submit_caption"] = "Import_Images_Button_Label";
	$frm["newmode"] = "update_impimg";

	$t = new Template();
	$t->set_file(array("page" => "templates/data_import_form.ihtml"));
	include ("$CFG->localelangdir" . "/global-common.inc.php");
	include ("$CFG->localelangdir" . "/global-admin.inc.php");
	$t->set_var("ME", $ME);
	$t->set_var(array(
		"frm_newmode"			=> $frm["newmode"],
		"session_max_file_size_import"	=> $SETTING["max_file_size_import"],
		"frm_submit_caption"		=> $t->get_var($frm["submit_caption"]) . " (.tar.gz, .tgz, .zip)"
	));
	$t->pparse("out", "page");
}

function new_import_images($HTTP_POST_FILES) {

	global $CFG, $ME, $SESSION, $SETTING;
	include ($CFG->globalerror);
	$upload_path = "$CFG->productsdir";
	global $qid;

	/* Let's open a transaction */
	$qid->begin();

	$errormsg = "";

	// Let's unzip the file with images into tempdir ...
	$tmpdir = "$CFG->productsdir" . "/tmp/";
	system ("rm -rf $tmpdir", $result);
	system ("mkdir $tmpdir", $result);
	system ("unzip -qq " . $HTTP_POST_FILES['userfile']['tmp_name'] . " -d " . $tmpdir, $result);
	system ("gzip -dc " . $HTTP_POST_FILES['userfile']['tmp_name'] . " | tar xf - -C " . $tmpdir, $result);
	// Let's manage all files in the tempdir
	$handle = opendir($tmpdir);
//echo "Directory handle: $handle\n";
//echo "Files:\n";
	while ($file = readdir($handle)) {
//echo "$file\n";
//echo strtok($file,'.') . "\n";
		$product_id = strtok($file,'.');
		if (!empty($product_id)) {
//			$appoggio = string_cleanup($pcode);
			$qid->Query("SELECT imagetype FROM products WHERE id = '$product_id' ORDER BY id DESC");
			if ($qid->num_rows() > 0) {
				$qid->next_record();
				$oldtype = $qid->f("imagetype");
				$upload_name = $upload_path . $product_id;
				$size = getimagesize ($tmpdir . $file);
				switch ($size[2]) {
					case 1 :
						$type = "gif";
						$convert = "giftopnm";
						break;
					case 2 :
						$type = "jpg";
						$convert = "djpeg";
						break;
					case 3 :
						$type = "png";
						$convert = "pngtopnm";
						break;
					default :
						$type = "";
						$errormsg .= "<li> $file: $unsupported_image_type";
				}
				if ($type != "") {
					$upload_name_thumb = $upload_name . ".thumb.jpg";
					$upload_name_old = $upload_name . "." . $oldtype;
					$upload_name .= "." . $type;
					if (!copy($tmpdir . $file, $upload_name)) {
						$errormsg .= "<li> $image_upload_failed";
					} else {
						if ($CFG->unix)
							system ("chmod 644 $upload_name", $result);
						if ($SETTING["thumbs_used"]) {
							system ("$CFG->pnmpath/$convert ".$upload_name . " | ".
								"$CFG->pnmpath/pnmscale -xysize 100 80 | ".
								"$CFG->pnmpath/cjpeg -optimize -progressive -quality 85 > ".
								$upload_name_thumb, $result
							);
							$thumbsize = getimagesize ($upload_name_thumb);
						} else {
							$thumbsize[0] = 0;
							$thumbsize[1] = 0;
						}
// if we are updating a product image and the old imagetype
// is empty or different from the new one, then we remove the old image
						if ($type != $oldtype)
							system ("$CFG->rmf $upload_name_old", $result);

						$qid->Query("
							UPDATE products SET
								 imagetype = '$type'
								,imagewidth = '$size[0]'
								,imageheight = '$size[1]'
								,thumbwidth = '$thumbsize[0]'
								,thumbheight = '$thumbsize[1]'
							WHERE id = '$product_id'
						");
					}
				}
			} else $errormsg.= "<li> $file: $no_products_with_such_id";
		}
	}
	closedir($handle); 

	// Let's remove the temporary directory ...
	system ("rm -rf $tmpdir", $result);

	/* Let's close the transaction */
	$qid->commit();
	
	/* The End */
	include ("$CFG->dirroot" . "/form_header.php");
	$t = new Template();
	$t->set_file(array("page" => "templates/data_import_end.ihtml"));
	include ("$CFG->localelangdir" . "/global-common.inc.php");
	include ("$CFG->localelangdir" . "/global-admin.inc.php");
	$t->set_var("ME", $ME);
	$t->set_var("backurl", $SESSION["goback"]["request_uri"]);
	$t->pparse("out", "page");
}

?>

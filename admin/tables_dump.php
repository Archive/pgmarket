<?php
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

if (!isset($_POST["filename"]) || $_POST["filename"] == "") {
	$filename = "mydata.sql";
} else {
	$filename = $_POST["filename"];
}

$user_discounts = (isset($_POST["user_discounts"])) ? $_POST["user_discounts"] : 0;
$zones = (isset($_POST["zones"])) ? $_POST["zones"] : 0;
$zones_i18n = (isset($_POST["zones_i18n"])) ? $_POST["zones_i18n"] : 0;
$delivery_costs = (isset($_POST["delivery_costs"])) ? $_POST["delivery_costs"] : 0;
$categories = (isset($_POST["categories"])) ? $_POST["categories"] : 0;
$categories_i18n = (isset($_POST["categories_i18n"])) ? $_POST["categories_i18n"] : 0;
$brands = (isset($_POST["brands"])) ? $_POST["brands"] : 0;
$iva = (isset($_POST["iva"])) ? $_POST["iva"] : 0;
$colors = (isset($_POST["colors"])) ? $_POST["colors"] : 0;
$colors_i18n = (isset($_POST["colors_i18n"])) ? $_POST["colors_i18n"] : 0;
$products = (isset($_POST["products"])) ? $_POST["products"] : 0;
$products_i18n = (isset($_POST["products_i18n"])) ? $_POST["products_i18n"] : 0;
$products_categories = (isset($_POST["products_categories"])) ? $_POST["products_categories"] : 0;
$products_colors = (isset($_POST["products_colors"])) ? $_POST["products_colors"] : 0;
$orders_states = (isset($_POST["orders_states"])) ? $_POST["orders_states"] : 0;

$users = (isset($_POST["users"])) ? $_POST["users"] : 0;
$ordersca = (isset($_POST["ordersca"])) ? $_POST["ordersca"] : 0;
$orderscc = (isset($_POST["orderscc"])) ? $_POST["orderscc"] : 0;
$order_items = (isset($_POST["order_items"])) ? $_POST["order_items"] : 0;
$languages = (isset($_POST["languages"])) ? $_POST["languages"] : 0;

if (str_replace("Opera", "", $_SERVER["HTTP_USER_AGENT"]) == $_SERVER["HTTP_USER_AGENT"]
	&&
    str_replace("MSIE", "", $_SERVER["HTTP_USER_AGENT"]) == $_SERVER["HTTP_USER_AGENT"]) {

// Correct.
//
// No problems with Netscape 4.5x for Windows
//
// Not correctly working with Opera 5.0 for Linux,
// which shows the output in the browser window,
// but it considers the filename when using "Save as...".
// It works with Opera 5.12 for Windows.
//
// Not correctly working with IE 4.0,
// which shows the output in the browser window
// and ignores the filename when using "Save as...".
//
// Not correctly working with IE 5.5,
// which shows the output in the browser window
// and ignores the filename when using "Save as...".
//
// IE 6.0 has a rather "funny" (stupid) behaviour
// (nothing new and nothing strange, right ?
// a new release --> new interesting bugs^H^H^H^Hfeatures... :)
// It forces you to save the file adding a ".txt" extension
// (ROTFLOL...) to the filename provided by the next header
// even *without* advising/showing you that it is faking
// the filename shown in the dialog box, and it results
// to be impossible to choose to save the file avoiding the use
// of that additional extension
//
header("Content-type: application/octet-stream");

} else {

// Uncorrect: it lacks a dash in "octetstream".
//
// Netscape 4.5x for Windows: "octetstream" is unknown;
// however, you can choose to save on disk and the filename
// is correctly considered
//
// It works with Opera 5.0 for Linux.
// Not correctly working with Opera 5.12 for Windows,
// which shows the output in the browser window,
// but it considers the filename when using "Save as...".
//
// Simply *not working* with IE 4.0
//
// It works with IE 5.5
//
// It works with IE 6.0, that shows in the dialog box
// that it's saving the file as a ".Sql document" (ROTFL...)
//
//header("Content-type: application/octetstream");

// Uncorrect... to my knowledge, the "download" type does not exist.
// It behaves just as "octetstream" on the browsers I have tested with.
// The same should hold true using "unknown" instead of "download"
header("Content-type: application/download");

// No problems in any case with:
// Mozilla 0.9.3 for Linux
// Netscape 4.7x for Linux
// Konqueror 2.2
// IE 5.0

}

header("Content-disposition: attachment; filename=$filename");
header("Pragma: no-cache");
header("Expires: 0");

$qid = new PGM_Sql;
$qid->begin();

//echo "<pre>";
echo "\n\n";

if ($user_discounts) {
	$table = "user_discounts";
	$table_fields = array("id", "discount", "name", "description");
	$where_clause = "WHERE id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($zones) {
	$table = "zones";
	$table_fields = array("id", "code", "name", "description");
	$where_clause = "WHERE id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($zones_i18n) {
	$table = "zones_i18n";
	$table_fields = array("zone_id", "lang", "name");
	$where_clause = "";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($delivery_costs) {
	$table = "delivery_costs";
	$table_fields = array("zone_id", "maxqty", "cost", "type");
	$where_clause = "";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($categories) {
	$table = "categories";
	$table_fields = array("id", "parent_id", "name", "special_level", "description");
	$where_clause = "WHERE id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($categories_i18n) {
	$table = "categories_i18n";
	$table_fields = array("category_id", "lang", "name");
	$where_clause = "WHERE category_id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($brands) {
	$table = "brands";
	$table_fields = array("id", "name", "description");
	$where_clause = "WHERE id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($iva) {
	$table = "iva";
	$table_fields = array("id", "iva", "description");
	$where_clause = "WHERE id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($colors) {
	$table = "colors";
	$table_fields = array("id", "name", "description");
	$where_clause = "WHERE id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($colors_i18n) {
	$table = "colors_i18n";
	$table_fields = array("color_id", "lang", "name");
	$where_clause = "WHERE color_id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($products) {
	$table = "products";
	$table_fields = array("id", "code", "name", "brand_id", "price", "iva_id", "discount", "discqty", "weight", "special_flag", "special_level", "description", "extended_description", "imagetype", "imagewidth", "imageheight", "thumbtype", "thumbwidth", "thumbheight");
	$where_clause = "WHERE id > 0";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($products_i18n) {
	$table = "products_i18n";
	$table_fields = array("product_id", "lang", "name", "description", "extended_description");
	$where_clause = "WHERE product_id > 0";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($products_categories) {
	$table = "products_categories";
	$table_fields = array("product_id", "category_id");
	$where_clause = "";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($products_colors) {
	$table = "products_colors";
	$table_fields = array("product_id", "color_id");
	$where_clause = "";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($orders_states) {
	$table = "orders_states";
	$table_fields = array("id", "name", "description");
	$where_clause = "WHERE id > 1";
	print_sync_queries ($table, $table_fields, $where_clause);
}

//

if ($users) {
	$table = "users";
	$table_fields = array("username", "password", "usertype", "priv", "user_discount_id", "zone_id", "firstname", "lastname", "fiscalcode", "address", "number", "zip_code", "city", "country", "phone", "fax", "mobilephone", "email", "authdata", "acceptadvert", "notes", "ournotes", "unixtime", "year", "month", "day", "weekday", "fromip");
	$where_clause = "WHERE username <> 'root'";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($ordersca) {
	$table = "ordersca";
	$table_fields = array("id", "username", "unixtime", "year", "month", "day", "weekday", "fromip", "custinfo", "notes", "ournotes", "amount", "user_discount_id", "user_discount", "delivery", "state_id");
	$where_clause = "";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($orderscc) {
	$table = "orderscc";
	$table_fields = array("id", "username", "unixtime", "year", "month", "day", "weekday", "fromip", "custinfo", "notes", "ournotes", "amount", "user_discount_id", "user_discount", "delivery", "otpric", "otpris", "codaut", "state_id");
	$where_clause = "";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($order_items) {
	$table = "order_items";
	$table_fields = array("order_id", "product_id", "color_id", "name", "color", "price", "discount", "discqty", "iva", "weight", "qty");
	$where_clause = "";
	print_sync_queries ($table, $table_fields, $where_clause);
}

if ($languages) {
	$table = "languages";
	$table_fields = array("lang");
	$where_clause = "";
	print_sync_queries ($table, $table_fields, $where_clause);
}

//echo "</pre>";

$qid->commit();

/**
* Prints DELETE and INSERT queries that can be used to synchronize the content of involved tables of another PgMarket installation
*
* Printed queries can be used on another PgMarket installation
* even if they are printed on an installation using PostgreSQL (MySQL)
* and subsequently used on an installation using MySQL (PostgreSQL),
* provided that they are used through the suited synchronization script.
*
* @param string $table the table w.r.t. which queries have to be printed
* @param array $table_fields array of strings, listing the fields
*   that are involved in queries to be printed
* @param string $where_clause WHERE clause to be used to prepare DELETE
*   and INSERT queries to be printed (in some cases, we must not "dump"
*   all the table tuples)
* @return void
*/
function print_sync_queries ($table, $table_fields, $where_clause) {
	global $qid;

	echo "DELETE FROM $table $where_clause;\n\n";
//echo "<br><br>";

	$insert = "INSERT INTO " . $table . " (";
	reset($table_fields);
	while (list(, $field) = each($table_fields)) {
		$insert .= $field . ",";
	}
	$insert = substr($insert, 0, strlen($insert) - 1);
	$insert .= ") VALUES (";
	//echo $insert;
	$qid->query("SELECT * FROM $table $where_clause");
	while ($qid->next_record()) {
		$insert_row = $insert;
		reset($table_fields);
		while (list(, $field) = each($table_fields)) {
			$insert_row .= "'" . addslashes($qid->f($field)) . "',";
		}
		$insert_row = substr($insert_row, 0, strlen($insert_row) - 1);
		$insert_row = ereg_replace(chr(13), "", $insert_row);
		$insert_row = ereg_replace(chr(10), "\\n", $insert_row);
		$insert_row .= ");";
		echo $insert_row . "\n";
//echo "<br>";
	}
	echo "\n\n";
//echo "<br><br>";
}

?>

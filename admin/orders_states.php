<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "Orders_States_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "add":
		print_add_order_state_form();
		break;

	case "edit":
		if ($id != 1) {
			print_edit_order_state_form($id);
		}
		break;

	case "del":
		if ($id != 1) {
			delete_order_state($id);
		}
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_order_state_name($_POST["name"], $errors, $errormsg);
		validate_order_state_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			insert_order_state($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_Order_State";
			include ($CFG["dirroot"] . "form_header.php");
			load_order_state_form_template($frm, $errors);
		}
		break;

	case "update":
		if ($id == 1) {
			break;
		}
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_order_state_name($_POST["name"], $errors, $errormsg);
		validate_order_state_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			update_order_state($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_order_state_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_order_states();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_order_state_form_template($frm, $errors) {
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/order_state_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"id"			=> nvl($frm["id"], ""),
		"frm_name"		=> ov($frm["name"]),
		"errors_name"		=> errmsg(nvl($errors["order_state_name"], "")),
		"frm_description"	=> ov($frm["description"]),
		"errors_description"	=> errmsg(nvl($errors["order_state_description"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_add_order_state_form() {
	// set default values for the reset of the fields
	$frm["newmode"] = "insert";
	$frm["name"] = "";
	$frm["description"] = "";
	$frm["submit_caption"] = "Add_Order_State";
	$errors = array();

	load_order_state_form_template($frm, $errors);
}

function print_edit_order_state_form($id) {
	$qid = new PGM_Sql("SELECT name, description FROM orders_states WHERE id = '$id'");
	$qid->next_record();
	$frm["id"] = $id;
	$frm["name"] = $qid->f("name");
	$frm["description"] = $qid->f("description");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_order_state_form_template($frm, $errors);
}

function delete_order_state($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();
	// find out this order state name */
	$qid->query("SELECT name FROM orders_states WHERE id = '$id'");
	$qid->next_record();
	$frm["name"] = $qid->f("name");
	// delete this order state
	$qid->query("DELETE FROM orders_states WHERE id = '$id'");
	// reset to 1 the state_id for all orders in this state
	$qid->query("UPDATE ordersca SET state_id = 1 WHERE state_id = '$id'");
	$qid->query("UPDATE orderscc SET state_id = 1 WHERE state_id = '$id'");
	$qid->commit();

	$t = new Template();
	$t->set_file("page", "templates/order_state_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function insert_order_state($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		INSERT INTO orders_states (name, description)
		VALUES ('" . $frm["name"] . "', '" . $frm["description"] . "')
	");

	$t = new Template();
	$t->set_file("page", "templates/order_state_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_order_state($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		UPDATE orders_states SET
			 name = '" . $frm["name"] . "'
			,description = '" . $frm["description"] . "'
		WHERE id = '" . $frm["id"] . "'
	");

	$t = new Template();
	$t->set_file("page", "templates/order_state_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_order_states() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM orders_states WHERE id > 1");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["orders_states_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT id, name, description FROM orders_states WHERE id > 1 ORDER BY name
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/order_state_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "order_state_row", "order_state_rows");
	$t->set_var("order_state_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"		=> ov($qid->f("id")),
			"name"		=> ov($qid->f("name")),
			"description"	=> ovwbr($qid->f("description"))
		));
		$t->parse("order_state_rows", "order_state_row", true);
	}
	$t->pparse("out", "page");
}

?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "IVA_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "add":
		print_add_iva_form();
		break;

	case "edit":
		if ($id != 1) {
			print_edit_iva_form($id);
		}
		break;

	case "del":
		if ($id != 1) {
			delete_iva($id);
		}
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_iva($_POST["iva"], $errors, $errormsg);
		validate_iva_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			insert_iva($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_IVA";
			include ($CFG["dirroot"] . "form_header.php");
			load_iva_form_template($frm, $errors);
		}
		break;

	case "update":
		if ($id == 1) {
			break;
		}
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_iva($_POST["iva"], $errors, $errormsg);
		validate_iva_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			update_iva($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_iva_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_iva();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_iva_form_template($frm, $errors) {
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/iva_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"id"			=> nvl($frm["id"], ""),
		"frm_iva"		=> $frm["iva"],
		"errors_iva"		=> errmsg(nvl($errors["iva"], "")),
		"frm_description"	=> ov($frm["description"]),
		"errors_description"	=> errmsg(nvl($errors["iva_description"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_add_iva_form() {
	// set default values for the reset of the fields
	$frm["newmode"] = "insert";
	$frm["iva"] = "";
	$frm["description"] = "";
	$frm["submit_caption"] = "Add_IVA";
	$errors = array();

	load_iva_form_template($frm, $errors);
}

function print_edit_iva_form($id) {
	$qid = new PGM_Sql("SELECT iva, description FROM iva WHERE id = '$id'");
	$qid->next_record();
	$frm["id"] = $id;
	$frm["iva"] = $qid->f("iva");
	$frm["description"] = $qid->f("description");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_iva_form_template($frm, $errors);
}

function delete_iva($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();
	// select the iva percentage to be deleted
	$qid->query("SELECT iva FROM iva WHERE id = '$id'");
	$qid->next_record();
	$frm["iva"] = $qid->f("iva");
	// delete this iva percentage
	$qid->query("DELETE FROM iva WHERE id = '$id'");
	// reset to 1 the iva_id for all products with this iva_id
	$qid->query("UPDATE products SET iva_id = 1 WHERE iva_id = '$id'");
	$qid->commit();

	$t = new Template();
	$t->set_file("page", "templates/iva_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_iva"	=> ov($frm["iva"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function insert_iva($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		INSERT INTO iva (iva, description)
		VALUES ('" . $frm["iva"] . "', '" . $frm["description"] . "')
	");

	$t = new Template();
	$t->set_file("page", "templates/iva_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_iva"	=> ov($frm["iva"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_iva($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) {
			$frm[$key] = addslashes($val);
		}
	}
	$qid = new PGM_Sql("
		UPDATE iva SET
			 iva = '" . $frm["iva"] . "'
			,description = '" . $frm["description"] . "'
		WHERE id = '" . $frm["id"] . "'
	");
	$t = new Template();
	$t->set_file("page", "templates/iva_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_iva"	=> ov($frm["iva"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_iva() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM iva WHERE id > 1");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["iva_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT id, iva, description FROM iva WHERE id > 1 ORDER BY iva
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/iva_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "iva_row", "iva_rows");
	$t->set_var("iva_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"		=> $qid->f("id"),
			"iva"		=> $qid->f("iva"),
			"description"	=> ovwbr($qid->f("description"))
		));
		$t->parse("iva_rows", "iva_row", true);
	}
	$t->pparse("out", "page");
}

?>

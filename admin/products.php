<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");
include ($CFG["libdir"] . $CFG["dbms"] . "_for_admin.inc.php");

$DOC_TITLE = "Products_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$product_id = nvl($_GET["product_id"], 1);
	$category_id = nvl($_GET["category_id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$product_id = nvl($_POST["product_id"], 1);
	$category_id = nvl($_POST["category_id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "details":
		print_product_details($product_id);
		break;

	case "add":
		print_add_product_form(nvl($category_id, 1));
		break;

	case "edit":
		print_edit_product_form($product_id);
		break;

	case "del":
		delete_product($product_id);
//		list_products();
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		validate_product_form($_POST, $errors, $errormsg);
		if ($errormsg == "") {
			insert_product($_POST);
//			list_products();
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_Product";
			build_category_tree($category_options, $frm["categories"]);
			if ($CFG["color_used"]) {
				build_color_options($color_options, $frm["colors"]);
			} else {
				$color_options == "";
			}
			include ($CFG["dirroot"] . "form_header.php");
			load_product_form_template($frm, $category_options, $color_options, $errors);
		}
		break;

	case "update":
		$errors = array();
		$errormsg = "";
		validate_product_form($_POST, $errors, $errormsg);
		if ($errormsg == "") {
			update_product($_POST);
//			list_products();
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			build_category_tree($category_options, $frm["categories"]);
			if ($CFG["color_used"]) {
				build_color_options($color_options, $frm["colors"]);
			} else {
				$color_options == "";
			}
			include ($CFG["dirroot"] . "form_header.php");
			load_product_form_template($frm, $category_options, $color_options, $errors);
		}
		break;

	case "impimg":
		print_import_images_form();
		break;

	case "update_impimg":
		if (nvl($_FILES["userfile"]["tmp_name"], "") == "none" || nvl($_FILES["userfile"]["tmp_name"], "") == "") {	// "" --> Opera 5
			print_import_images_form();
		} else {
			import_images();
		}
		break;

	case "imp_uploaded_img":
		import_images(false);
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_products();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

/**
* Validates values submitted to add/update a product
* @param array $frm the array containing submitted values
* @param array $errors array of booleans indicating fields with errors
* @param string $errormsg HTML code containing all error messages
* @return void
*/
function validate_product_form(&$frm, &$errors, &$errormsg) {
	global $CFG;
	include ($CFG["libdir"] . "pgm_validate.inc.php");

	validate_product_code($frm["code"], $errors, $errormsg);
	validate_product_name($frm["name"], $errors, $errormsg);
	validate_price($frm["price"], $errors, $errormsg);
	validate_discount($frm["discount"], $frm["discqty"], $errors, $errormsg);
	if ($CFG["weight_used"]) {
		validate_weight($frm["weight"], $errors, $errormsg);
	}
	validate_special_level($frm["special_level"], $errors, $errormsg);
	validate_new_level($frm["new_level"], $errors, $errormsg);
	validate_product_description($frm["description"], $errors, $errormsg);
	validate_product_extended_description($frm["extended_description"], $errors, $errormsg);
}

/**
* Loads an add/update product form
* @param array $frm the array containing current or default values
* @param string $category_options HTML code for the option box needed
*   to select the categories the product belongs to
* @param string $color_options HTML code for the option box needed
*   to select available colors for the product
* @param array $errors array of booleans indicating fields with errors
* @return void
*/
function load_product_form_template($frm, $category_options, $color_options, $errors) {
global $CFG;
	$qid = new PGM_Sql();

	$t = new Template();
	$t->set_file("page", "templates/product_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"			=> $frm["newmode"],
		"frm_product_id"		=> nvl($frm["product_id"], ""),
		"frm_old_imagetype"		=> nvl($frm["old_imagetype"], ""),
		"frm_old_imagewidth"		=> nvl($frm["old_imagewidth"], ""),
		"frm_old_imageheight"		=> nvl($frm["old_imageheight"], ""),
		"frm_old_thumbtype"		=> nvl($frm["old_thumbtype"], ""),
		"frm_old_thumbwidth"		=> nvl($frm["old_thumbwidth"], ""),
		"frm_old_thumbheight"		=> nvl($frm["old_thumbheight"], ""),
		"max_file_size_images"		=> $CFG["max_file_size_images"],
		"category_options"		=> $category_options,
		"frm_product_code"		=> ov($frm["code"]),
		"errors_product_code"		=> errmsg(nvl($errors["product_code"], "")),
		"frm_product_name"		=> ov($frm["name"]),
		"errors_product_name"		=> errmsg(nvl($errors["product_name"], "")),
		"frm_product_price"		=> $frm["price"],
		"errors_product_price"		=> errmsg(nvl($errors["price"], "")),
		"frm_product_discount"		=> $frm["discount"],
		"errors_product_discount"	=> errmsg(nvl($errors["discount"], "")),
		"frm_product_discqty"		=> $frm["discqty"],
		"errors_product_discqty"	=> errmsg(nvl($errors["discqty"], "")),
		"frm_special_flag"		=> nvl($frm["special_flag"], "") ? "checked" : "",
		"frm_product_special_level"	=> $frm["special_level"],
		"errors_product_special_level"	=> errmsg(nvl($errors["special_level"], "")),
		"frm_new_flag"		=> $frm["new_flag"] ? "checked" : "",
		"frm_product_new_level"		=> $frm["new_level"],
		"errors_product_new_level"	=> errmsg($errors->new_level),
		"frm_nostock_flag"		=> $frm["nostock_flag"] ? "checked" : "",
		"frm_product_description"	=> ov($frm["description"]),
		"errors_product_description"	=> errmsg(nvl($errors["description"], "")),
		"frm_product_extended_description"	=> ov($frm["extended_description"]),
		"errors_product_extended_description"	=> errmsg(nvl($errors["extended_description"], "")),
		"frm_submit_caption"		=> $t->get_var($frm["submit_caption"])
	));
	$t->set_block("page", "color_used", "color_used_blck");
	$t->set_var("color_used_blck", "");
	if($CFG["color_used"]) {
		$t->set_var("color_options", $color_options);
		$t->parse("color_used_blck", "color_used", true);
	}
	$t->set_block("page", "brand_used", "brand_used_blck");
	$t->set_var("brand_used_blck", "");
	if($CFG["brand_used"]) {
		$qid->query("SELECT id, name FROM brands ORDER BY name");
		$t->set_block("brand_used", "brand_used_option", "brand_used_options");
		$t->set_var("brand_used_options", "");
		while ($qid->next_record()) {
			$t->set_var(array(
				"frm_brand_id"		=> $qid->f("id"),
				"frm_brand_selected"	=> ($frm["brand_id"] == $qid->f("id")) ? "selected" : "",
				"frm_brand_name"	=> $qid->f("name")
			));
			$t->parse("brand_used_options", "brand_used_option", true);
		}
		$t->parse("brand_used_blck", "brand_used", true);
	}
	$t->set_block("page", "iva_used", "iva_used_blck");
	$t->set_var("iva_used_blck", "");
	if ($CFG["iva_used"]) {
		$qid->query("SELECT id, iva FROM iva ORDER BY iva");
		$t->set_block("iva_used", "iva_used_option", "iva_used_options");
		$t->set_var("iva_used_options", "");
		while ($qid->next_record()) {
			$t->set_var(array(
				"frm_iva_id"		=> $qid->f("id"),
				"frm_iva_selected"	=> ($frm["iva_id"] == $qid->f("id")) ? "selected" : "",
				"frm_iva_iva"		=> $qid->f("iva")
			));
			$t->parse("iva_used_options", "iva_used_option", true);
		}
		$t->parse("iva_used_blck", "iva_used", true);
	}
	$t->set_block("page", "weight_used", "weight_used_blck");
	$t->set_var("weight_used_blck", "");
	if ($CFG["weight_used"]) {
		$t->set_var(array(
			"frm_product_weight"	=> $frm["weight"],
			"errors_product_weight"	=> errmsg(nvl($errors["weight"], ""))
		));
		$t->parse("weight_used_blck", "weight_used", true);
	}
	$t->set_block("page", "delete_current_image", "delete_current_image_blck");
	$t->set_var("delete_current_image_blck", "");
	if (	$frm["newmode"] == "update"
		&&
		(
			($CFG["images_on_file_system"] && file_exists($CFG["productsdir"] . nvl($frm["product_id"], "") . "." . $frm["old_imagetype"]))
			||
			(!$CFG["images_on_file_system"] && $frm["old_imagetype"] != "")
		)
	) {
		$t->set_var("frm_delete_image_checked", nvl($frm["delete_image"], "") ? "checked" : "");
		$t->parse("delete_current_image_blck", "delete_current_image", true);
	}
	$t->set_block("page", "create_image_thumb", "create_image_thumb_blck");
	$t->set_var("create_image_thumb_blck", "");
	if ($CFG["use_imagemagick"] || $CFG["use_libgd"] || $CFG["use_netpbm"]) {
		$t->set_var("frm_create_thumb_checked", nvl($frm["create_thumb"], "") ? "checked" : "");
		$t->parse("create_image_thumb_blck", "create_image_thumb", true);
	}
	$t->set_block("page", "delete_current_thumb", "delete_current_thumb_blck");
	$t->set_var("delete_current_thumb_blck", "");
	if (	$frm["newmode"] == "update"
		&&
		(
			($CFG["images_on_file_system"] && file_exists($CFG["productsdir"] . nvl($frm["product_id"], "") . ".thumb." . $frm["old_thumbtype"]))
			||
			(!$CFG["images_on_file_system"] && $frm["old_thumbtype"] != "")
		)
	) {
		$t->set_var("frm_delete_thumb_checked", nvl($frm["delete_thumb"], "") ? "checked" : "");
		$t->parse("delete_current_thumb_blck", "delete_current_thumb", true);
	}
	$t->pparse("out", "page");
}

/**
* Prepares a page with details of a given product
* @param integer $product_id the identifier of the product whose details
*   have to be shown
* @return void
*/
function print_product_details($product_id) {
	global $CFG;

	$qid = new PGM_Sql("
		SELECT
			 p.code
			,p.name
			,p.brand_id
			,p.price
			,p.iva_id
			,p.discount
			,p.discqty
			,p.weight
			,p.special_flag
			,p.special_level
			,p.special_flag
			,p.special_level
			,p.nostock_flag
			,p.description
			,p.extended_description
			,p.imagetype
			,p.imagewidth
			,p.imageheight
			,p.thumbtype
			,p.thumbwidth
			,p.thumbheight
			,b.name AS bname
			,i.iva
		FROM products p, brands b, iva i
		WHERE p.brand_id = b.id AND p.iva_id = i.id AND p.id = '$product_id'
	");
	$qid->next_record();
	$frm["id"] = $product_id;
	$frm["code"] = $qid->f("code");
	$frm["name"] = $qid->f("name");
	$frm["brand_id"] = $qid->f("brand_id");
	$frm["price"] = $qid->f("price");
	$frm["iva_id"] = $qid->f("iva_id");
	$frm["discount"] = $qid->f("discount");
	$frm["discqty"] = $qid->f("discqty");
	$frm["weight"] = $qid->f("weight");
	$frm["special_flag"] = $qid->f("special_flag");
	$frm["special_level"] = $qid->f("special_level");
	$frm["new_flag"] = $qid->f("new_flag");
	$frm["new_level"] = $qid->f("new_level");
	$frm["nostock_flag"] = $qid->f("nostock_flag");
	$frm["description"] = $qid->f("description");
	$frm["extended_description"] = $qid->f("extended_description");
	$frm["imagetype"] = $qid->f("imagetype");
	$frm["imagewidth"] = $qid->f("imagewidth");
	$frm["imageheight"] = $qid->f("imageheight");
	$frm["thumbtype"] = $qid->f("thumbtype");
	$frm["thumbwidth"] = $qid->f("thumbwidth");
	$frm["thumbheight"] = $qid->f("thumbheight");
	$frm["bname"] = $qid->f("bname");
	$frm["iva"] = $qid->f("iva");

	$t = new Template();
	$t->set_file("page", "templates/product_details.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name"			=> ov($frm["name"]),
		"product_id"			=> $product_id,
		"frm_product_code"		=> ov($frm["code"]),
		"frm_description"		=> ovwbr($frm["description"]),
		"frm_extended_description"	=> ovwbr($frm["extended_description"]),
		"frm_fprice"			=> formatted_price($frm["price"]),
		"frm_special_level"		=> $frm["special_level"],
		"frm_new_level"			=> $frm["new_level"],
	));
	$t->set_block("page", "brand_used", "brand_used_blck");
	$t->set_var("brand_used_blck", "");
	if ($CFG["brand_used"] && $frm["bname"] != "") {
		$t->set_var("bname", ov($frm["bname"]));
		$t->parse("brand_used_blck", "brand_used", true);
	}
	$t->set_block("page", "iva_used", "iva_used_blck");
	$t->set_var("iva_used_blck", "");
	if ($CFG["iva_used"]) {
		$t->set_var("iva", $frm["iva"]);
		$t->parse("iva_used_blck", "iva_used", true);
	}
	$t->set_block("page", "discount_block", "discount_block_blck");
	$t->set_var("discount_block_blck", "");
		$t->set_block("discount_block", "discqty_block", "discqty_block_blck");
		$t->set_var("discqty_block_blck", "");
	if ($frm["discount"] > 0) {
		$t->set_var("discount", $frm["discount"]);
		if ($frm["discqty"] > 1) {
			$t->set_var("discqty", $frm["discqty"]);
			$t->parse("discqty_block_blck", "discqty_block", true);
		}
		$t->parse("discount_block_blck", "discount_block", true);
	}
	$t->set_block("page", "weight_used", "weight_used_blck");
	$t->set_var("weight_used_blck", "");
	if ($CFG["weight_used"]) {
		$t->set_var("fweight", formatted_weight($frm["weight"]));
		$t->parse("weight_used_blck", "weight_used", true);
	}
	$t->set_block("page", "special_flag", "special_flag_blck");
	$t->set_var("special_flag_blck", "");
	if ($frm["special_flag"]) {
		$t->parse("special_flag_blck", "special_flag", true);
	}
	$t->set_block("page", "new_flag", "new_flag_blck");
	$t->set_var("new_flag_blck", "");
	if ($frm["new_flag"])
		$t->parse("new_flag_blck", "new_flag", true);
	$t->set_block("page", "nostock_flag", "nostock_flag_blck");
	$t->set_var("nostock_flag_blck", "");
	if ($frm["nostock_flag"]) {
		$t->parse("nostock_flag_blck", "nostock_flag", true);
	}
	$t->set_block("page", "color_used", "color_used_blck");
	$t->set_var("color_used_blck", "");
	if ($CFG["color_used"]) {
		$foobar = get_product_color_options($product_id, $color_options, 0);
		if ($foobar > 0) {
			$t->set_var("color_options", $color_options);
			$t->parse("color_used_blck", "color_used", true);
		}
	}
	$t->set_block("page", "adminprodthumb", "adminprodthumb_blck");
	$t->set_var("adminprodthumb_blck", "");
	if ($CFG["images_on_file_system"]) {
		if (file_exists($CFG["productsdir"] . $product_id . ".thumb." . $frm["thumbtype"])) {
			$t->set_var(array(
				"thumbsrc"	=> "getimage.php?image=" . $product_id . ".thumb." . $frm["thumbtype"] . "&amp;type=" . $frm["thumbtype"],
				"thumbwidth"	=> $frm["thumbwidth"],
				"thumbheight"	=> $frm["thumbheight"]
			));
			$t->parse("adminprodthumb_blck", "adminprodthumb", true);
		}
	} else {	// images in the DB
		if ($frm["thumbtype"] != "") {
			$t->set_var(array(
				"thumbsrc"	=> "dbgetthumb.php?id=" . $product_id,
				"thumbwidth"	=> $frm["thumbwidth"],
				"thumbheight"	=> $frm["thumbheight"]
			));
			$t->parse("adminprodthumb_blck", "adminprodthumb", true);
		}
	}
	$t->set_block("page", "adminprodimage", "adminprodimage_blck");
	$t->set_var("adminprodimage_blck", "");
	if ($CFG["images_on_file_system"]) {
		if (file_exists($CFG["productsdir"] . $product_id . "." . $frm["imagetype"])) {
			$t->set_var(array(
				"imagesrc"	=> "getimage.php?image=" . $product_id . "." . $frm["imagetype"] . "&amp;type=" . $frm["imagetype"],
				"imagewidth"	=> $frm["imagewidth"],
				"imageheight"	=> $frm["imageheight"]
			));
			$t->parse("adminprodimage_blck", "adminprodimage", true);
		}
	} else {	// images in the DB
		if ($frm["imagetype"] != "") {
			$t->set_var(array(
				"imagesrc"	=> "dbgetimage.php?id=" . $product_id,
				"imagewidth"	=> $frm["imagewidth"],
				"imageheight"	=> $frm["imageheight"]
			));
			$t->parse("adminprodimage_blck", "adminprodimage", true);
		}
	}
	$t->pparse("out", "page");
}

/**
* Prepares an empty form to add a new product
* @param integer $category_id the category id to set as preselected
* @return void
*/
function print_add_product_form($category_id = 1) {
	global $CFG;

	// set default values for the reset of the fields
	$frm["categories"] = array($category_id);
	$frm["colors"] = array();
	$frm["newmode"] = "insert";
	$frm["code"] = "";
	$frm["name"] = "";
	$frm["brand_id"] = "";
	$frm["price"] = "";
	$frm["iva_id"] = "";
	$frm["discount"] = "";
	$frm["discqty"] = "";
	$frm["weight"] = "";
	$frm["special_flag"] = 0;
	$frm["special_level"] = "";
	$frm["new_flag"] = 0;
	$frm["new_level"] = "";
	$frm["nostock_flag"] = 0;
	$frm["description"] = "";
	$frm["extended_description"] = "";
	$frm["create_thumb"] = 1;
	$frm["submit_caption"] = "Add_Product";

	// build the categories listbox options, preselect the selected item
	build_category_tree($category_options, $frm["categories"]);

	if ($CFG["color_used"]) {
		build_color_options($color_options, $frm["colors"]);
	} else {
		$color_options = "";
	}

	$errors = array();

	load_product_form_template($frm, $category_options, $color_options, $errors);
}

/**
* Prepares a form to allow editing fields of a product
* @param integer $product_id the identifier of the product to be edited
* @return void
*/
function print_edit_product_form($product_id) {
	global $CFG;

	$qid = new PGM_Sql("SELECT category_id FROM products_categories WHERE product_id = '$product_id'");
	$frm["categories"] = array();
	while ($qid->next_record()) {
		$frm["categories"][] = $qid->f("category_id");
	}
	build_category_tree($category_options, $frm["categories"]);

	$qid->query("SELECT color_id FROM products_colors WHERE product_id = '$product_id'");
	if ($CFG["color_used"]) {
		$frm["colors"] = array();
		while ($qid->next_record()) {
			$frm["colors"][] = $qid->f("color_id");
		}
		build_color_options($color_options, $frm["colors"]);
	} else {
		$color_options = "";
	}

	$qid->query("
		SELECT
			 code
			,name
			,brand_id
			,price
			,iva_id
			,discount
			,discqty
			,weight
			,special_flag
			,special_level
			,new_flag
			,new_level
			,nostock_flag
			,description
			,extended_description
			,imagetype
			,imagewidth
			,imageheight
			,thumbtype
			,thumbwidth
			,thumbheight
		FROM products
		WHERE id = '$product_id'
	");
	$qid->next_record();
	$frm["id"] = $product_id;
	$frm["code"] = $qid->f("code");
	$frm["name"] = $qid->f("name");
	$frm["brand_id"] = $qid->f("brand_id");
	// workaround to avoid ".00" with MySQL 3.22.32
	// and "2.5e+07" with MySQL 3.23.36
	// ereg_replace is needed with PHP 4.0.0 and 4.0.1pl2
	if ($CFG["price_must_be_integer"]) {
		$frm["price"] = ereg_replace (",", "", number_format ($qid->f("price"), 0, "", ""));
	} else {
		$frm["price"] = $qid->f("price");
	}
	$frm["iva_id"] = $qid->f("iva_id");
	$frm["discount"] = $qid->f("discount");
	$frm["discqty"] = $qid->f("discqty");
	$frm["weight"] = $qid->f("weight");
	$frm["special_flag"] = $qid->f("special_flag");
	$frm["special_level"] = $qid->f("special_level");
	$frm["new_flag"] = $qid->f("new_flag");
	$frm["new_level"] = $qid->f("new_level");
	$frm["nostock_flag"] = $qid->f("nostock_flag");
	$frm["description"] = $qid->f("description");
	$frm["extended_description"] = $qid->f("extended_description");
	$frm["imagetype"] = $qid->f("imagetype");
	$frm["imagewidth"] = $qid->f("imagewidth");
	$frm["imageheight"] = $qid->f("imageheight");
	$frm["thumbtype"] = $qid->f("thumbtype");
	$frm["thumbwidth"] = $qid->f("thumbwidth");
	$frm["thumbheight"] = $qid->f("thumbheight");
	$frm["old_product_id"] = $product_id;
	$frm["old_imagetype"] = $frm["imagetype"];
	$frm["old_imagewidth"] = $frm["imagewidth"];
	$frm["old_imageheight"] = $frm["imageheight"];
	$frm["old_thumbtype"] = $frm["thumbtype"];
	$frm["old_thumbwidth"] = $frm["thumbwidth"];
	$frm["old_thumbheight"] = $frm["thumbheight"];
	$frm["create_thumb"] = 1;
	$frm["product_id"] = $product_id;

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_product_form_template($frm, $category_options, $color_options, $errors);
}

/**
* Deletes a product.
*
* We have to delete the product and related entries in related tables.
* Also removal of image and thumb has to be handled if images are on the
* File System and not in the DB.
*
* @param integer $product_id the identifier of the product to be deleted
* @return void
*/
function delete_product($product_id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();
	$qid->query("SELECT name, imagetype, thumbtype FROM products WHERE id = '$product_id'");
	$qid->next_record();
	$qid->query("DELETE FROM products WHERE id = '$product_id'");
	$qid->query("DELETE FROM products_categories WHERE product_id = '$product_id'");
	$qid->query("DELETE FROM products_colors WHERE product_id = '$product_id'");
	$qid->query("DELETE FROM products_i18n WHERE product_id = '$product_id'");
	$qid->commit();

	if ($CFG["images_on_file_system"]) {
		// Remove the product image
		if ($qid->f("imagetype") != "") {
			$upload_name = $CFG["productsdir"] . $product_id . "." . $qid->f("imagetype");
/*
			if ($CFG["unix"]) {
*/
				// Note: This function may not work on Windows systems.
				unlink ($upload_name);
/*
			} else {
				// FIXME: in this case we should check safe_mode and open_basedir to check
				// if it is possible to remove the product image and, if it is not possible,
				// issue an error message!!!
				system ($CFG["rmf"] . " " . $upload_name, $result);
			}
*/
		}
		// Remove the product thumb
		if ($qid->f("thumbtype") != "") {
			$upload_thumb_name = $CFG["productsdir"] . $product_id . ".thumb." . $qid->f("thumbtype");
/*
			if ($CFG["unix"]) {
*/
				// Note: This function may not work on Windows systems.
				unlink ($upload_thumb_name);
/*
			} else {
				system ($CFG["rmf"] . " " . $upload_thumb_name, $result);
			}
*/
		}
	}

	$t = new Template();
	$t->set_file("page", "templates/product_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"prod_name"	=> ov($qid->f("name")),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

/**
* Adds a new product
* @param array $frm the array containing values to be inserted in the DB
* @return void
*/
function insert_product($frm) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["globalerror"]);

	if ($frm["extended_description"] == "") {
		$frm["extended_description"] = $frm["description"];
	}
	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	if (!$CFG["brand_used"]) {
		$frm["brand_id"] = 1;
	}
	if (!$CFG["iva_used"]) {
		$frm["iva_id"] = 1;
	}

	$qid = new PGM_Sql();
	$qid->begin();

	$qid->lock("products");
	$qid->query("
		INSERT INTO products (
			 code
			,name
			,brand_id
			,price
			,iva_id
			,discount
			,discqty
			,weight
			,special_flag
			,special_level
			,new_flag
			,new_level
			,nostock_flag
			,description
			,extended_description
		)
		VALUES (
			 '" . nvl($frm["code"], "") . "'
			,'" . nvl($frm["name"], "") . "'
			,'" . nvl($frm["brand_id"], "") . "'
			,'" . nvl($frm["price"], "") . "'
			,'" . nvl($frm["iva_id"], "") . "'
			,'" . nvl($frm["discount"], "") . "'
			,'" . nvl($frm["discqty"], "") . "'
			,'" . nvl($frm["weight"], "") . "'
			,'" . nvl($frm["special_flag"], "") . "'
			,'" . nvl($frm["special_level"], "") . "'
			,'" . nvl($frm["new_flag"], "") . "'
			,'" . nvl($frm["new_level"], "") . "'
			,'" . nvl($frm["nostock_flag"], "") . "'
			,'" . nvl($frm["description"], "") . "'
			,'" . nvl($frm["extended_description"], "") . "'
		)
	");
	$limit_clause = prepare_limit_clause(1,0);
	$qid->query("SELECT id FROM products ORDER BY id DESC $limit_clause");
	$qid->next_record();
	$inserted_product_id = $qid->f("id");
	$qid->unlock();

	if (count($frm["categories"]) == 0) {
		$frm["categories"][0] = 1;
	}
	for ($i=0; $i<count($frm["categories"]); $i++) {
		$qid->query("
			INSERT INTO products_categories (category_id, product_id)
			VALUES ('" . $frm["categories"][$i] . "', '$inserted_product_id')
		");
	}

	if (isset($frm["colors"])) {
		for ($i=0; $i<count($frm["colors"]); $i++) {
			$qid->query("
				INSERT INTO products_colors (color_id, product_id)
				VALUES ('" . $frm["colors"][$i] . "', '$inserted_product_id')
			");
		}
	}

	$qid->commit();

	$frm["product_id"] = $inserted_product_id;
	update_images($frm);

	$t = new Template();
	$t->set_file("page", "templates/product_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"product_id"	=> $inserted_product_id,
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

/**
* Updates a product
* @param array $frm the array containing values to update the product
* @return void
*/
function update_product($frm) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["globalerror"]);

	if ($frm["extended_description"] == "") {
		$frm["extended_description"] = $frm["description"];
	}
	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	if (!$CFG["brand_used"]) {
		$frm["brand_id"] = 1;
	}
	if (!$CFG["iva_used"]) {
		$frm["iva_id"] = 1;
	}

	$qid = new PGM_Sql();
	$qid->begin();

	$qid->query("
		UPDATE products SET
			 code = '"	. nvl($frm["code"], "") . "'
			,name = '"	. nvl($frm["name"], "") . "'
			,brand_id = '"	. nvl($frm["brand_id"], "") . "'
			,price = '"	. nvl($frm["price"], "") . "'
			,iva_id = '"	. nvl($frm["iva_id"], "") . "'
			,discount = '"	. nvl($frm["discount"], "") . "'
			,discqty = '"	. nvl($frm["discqty"], "") . "'
			,weight = '"	. nvl($frm["weight"], "") . "'
			,special_flag = '"	. nvl($frm["special_flag"], "") . "'
			,special_level = '"	. nvl($frm["special_level"], "") . "'
			,new_flag = '"	. nvl($frm["new_flag"], "") . "'
			,new_level = '"	. nvl($frm["new_level"], "") . "'
			,nostock_flag = '"	. nvl($frm["nostock_flag"], "") . "'
			,description = '"	. nvl($frm["description"], "") . "'
			,extended_description = '"	. nvl($frm["extended_description"], "") . "'
		WHERE id = '" . $frm["product_id"] . "'
	");

	// delete all the categories this product was associated with
	$qid->query("
		DELETE FROM products_categories
		WHERE product_id = '" . $frm["product_id"] . "'
	");
	// add associations for all the categories this product belongs to;
 	// if no categories were selected, we will make it belong
	// to the top category
	if (count($frm["categories"]) == 0) {
		$frm["categories"][] = 1;
	}
	for ($i=0; $i<count($frm["categories"]); $i++) {
		$qid->query("
			INSERT INTO products_categories (category_id, product_id)
			VALUES ('" . $frm["categories"][$i] . "', '" . $frm["product_id"] . "')
		");
	}

	// analogously for the colors...
	$qid->query("
		DELETE FROM products_colors
		WHERE product_id = '" . $frm["product_id"] . "'
	");
	if (isset($frm["colors"])) {
		for ($i=0; $i<count($frm["colors"]); $i++) {
			$qid->query("
				INSERT INTO products_colors (color_id, product_id)
				VALUES ('" . $frm["colors"][$i] . "', '" . $frm["product_id"] . "')
			");
		}
	}

	$qid->commit();

	update_images($frm);

	$t = new Template();
	$t->set_file("page", "templates/product_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"frm_product_id"	=> ov($frm["product_id"]),
		"backurl"		=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

/**
* Prepares a page providing a list of products
* @return void
*/
function list_products() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(product_id) AS cnt FROM products_categories");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["products_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT
			 p.id AS product_id
			,p.code
			,p.name
			,p.price
			,p.discount
			,p.discqty
			,p.weight
			,p.thumbtype
			,p.thumbwidth
			,p.thumbheight
			,c.name AS category
			,i.iva
		FROM products p, products_categories pc, categories c, iva i
		WHERE p.id = pc.product_id AND c.id = pc.category_id AND p.iva_id = i.id
		ORDER BY category, name
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/product_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
		$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "weight_used_th", "weight_used_th_blck");
	$t->set_var("weight_used_th_blck", "");
	if ($CFG["weight_used"]) {
		$t->parse("weight_used_th_blck", "weight_used_th", true);
	}
	$t->set_block("page", "product_row", "product_rows");
	$t->set_var("product_rows", "");
		$t->set_block("product_row", "adminprodthumb", "adminprodthumb_blck");
		$t->set_block("product_row", "adminprodnothumb", "adminprodnothumb_blck");
		$t->set_block("product_row", "iva_used", "iva_used_blck");
		$t->set_block("product_row", "weight_used_td", "weight_used_td_blck");
	while ($qid->next_record()) {
		$t->set_var("adminprodthumb_blck", "");
		$t->set_var("adminprodnothumb_blck", "");
		$t->set_var("iva_used_blck", "");
		$t->set_var("weight_used_td_blck", "");
		$t->set_var(array(
			"product_id"		=> ov($qid->f("product_id")),
			"product_code"		=> ov($qid->f("code")),
			"product_name"		=> ov($qid->f("name")),
			"product_category"	=> ($qid->f("category") != "") ? ov($qid->f("category")) : "-",
			"product_fprice"	=> formatted_price($qid->f("price"))
		));
		if ($CFG["images_on_file_system"] && file_exists($CFG["productsdir"] . $qid->f("product_id") . ".thumb." . $qid->f("thumbtype"))) {
			$t->set_var(array(
				"thumbsrc"	=> "getimage.php?image=" . $qid->f("product_id") . ".thumb." . $qid->f("thumbtype") . "&amp;type=" . $qid->f("thumbtype"),
				"thumbwidth"	=> $qid->f("thumbwidth"),
				"thumbheight"	=> $qid->f("thumbheight")
			));
			$t->parse("adminprodthumb_blck", "adminprodthumb", true);
		} else if (!$CFG["images_on_file_system"] && $qid->f("thumbtype") != "") {
			$t->set_var(array(
				"thumbsrc"	=> "dbgetthumb.php?id=" . $qid->f("product_id"),
				"thumbwidth"	=> $qid->f("thumbwidth"),
				"thumbheight"	=> $qid->f("thumbheight")
			));
			$t->parse("adminprodthumb_blck", "adminprodthumb", true);
		} else {
			$t->parse("adminprodnothumb_blck", "adminprodnothumb", true);
		}
		if ($CFG["iva_used"]) {
			$t->set_var("product_iva", $qid->f("iva"));
			$t->parse("iva_used_blck", "iva_used", true);
		}
		if ($CFG["weight_used"]) {
			$t->set_var("product_fweight", formatted_weight($qid->f("weight")));
			$t->parse("weight_used_td_blck", "weight_used_td", true);
		}
		$t->parse("product_rows", "product_row", true);
	}
	$t->pparse("out", "page");
}

/**
* Prepares an import form to allow to add/update images/thumbs using an archive file to be uploaded
* @return void
*/
function print_import_images_form() {
	global $CFG, $ME;

	$frm["submit_caption"] = "Import_Images_Button_Label";
	$frm["newmode"] = "update_impimg";

	$t = new Template();
	$t->set_file("page", "templates/data_import_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("ME", $ME);
	$t->set_var(array(
		"frm_title"		=> nvl($t->get_var($frm["newmode"]), ""),
		"frm_newmode"		=> $frm["newmode"],
		"max_file_size_import"	=> $CFG["max_file_size_import"],
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"]) . " (.tar.gz, .tgz, .zip)"
	));
	$t->pparse("out", "page");
}

/**
* Imports images/thumbs using full size images provided in an uploaded compressed archive
*
* Filenames of full size images must correspond to the respective products codes
*
* @param boolean $submitted it is true if file to be imported has been
*   uploaded via HTTP, it is false if file has been uploaded another way,
*   e.g. via FTP
* @return void
*/
function import_images($uploaded=true) {
	global $_GET;
	global $_FILES;
	global $PGM_SESSION;
	global $CFG, $ME;
	include ($CFG["globalerror"]);

	$qid = new PGM_Sql();
	// Let's open a transaction
	$qid->begin();

	$errormsg = "";

	// Let's unzip the file with images into tempdir ...
	$tmpdir = $CFG["productsdir"] . "tmp/";
	if ($uploaded) {
		system ($CFG["rmrf"] . " " . $tmpdir, $result);
		mkdir ($tmpdir, 0777);
		system ("unzip -qq " . $_FILES["userfile"]["tmp_name"] . " -d " . $tmpdir, $result);
		system ("gzip -dc " . $_FILES["userfile"]["tmp_name"] . " | tar xf - -C " . $tmpdir, $result);
	}
	// Let's manage all files in the tempdir
	$handle = opendir ($tmpdir);
//echo "Directory handle: $handle\n";
	while ($file = readdir ($handle)) {
		$pcode = strtok ($file, '.');
		if (!empty($pcode)) {
			$qid->query("SELECT id, imagetype, thumbtype FROM products WHERE code = '$pcode' ORDER BY id DESC");
			if ($qid->num_rows() > 0) {
				$qid->next_record();
				$product_id = $qid->f("id");
				$old_imagetype = $qid->f("imagetype");
				$old_thumbtype = $qid->f("thumbtype");
				$size = getimagesize ($tmpdir . $file);
				switch ($size[2]) {
					case 1:
						$type = "gif";
						break;
					case 2:
						$type = "jpg";
						break;
					case 3:
						$type = "png";
						break;
					default:
						$type = "";
						$errormsg .= "<li> " . $pcode . ": " . $unsupported_image_type;
				}
				if ($type != "") {
					$upload_name = $CFG["productsdir"] . $product_id . "." . $type;
					$old_image_name = $CFG["productsdir"] . $product_id . "." . $old_imagetype;
					$upload_thumb_name = $CFG["productsdir"] . $product_id . ".thumb.jpg";
					$old_thumb_name = $CFG["productsdir"] . $product_id . ".thumb." . $old_thumbtype;
					if (!copy($tmpdir . $file, $upload_name)) {
						$errormsg .= "<li> 26" . $image_upload_failed;
					} else {
						if ($CFG["unix"]) {
							// Note: This function is not implemented on Windows platforms.
							chmod ($upload_name, 0644);
						}
						if ($old_image_name != $upload_name && file_exists($old_image_name)) {
/*
							if ($CFG["unix"] {
*/
								// Note: This function may not work on Windows systems.
								unlink ($old_image_name);
/*
							} else {
								system ($CFG["rmf"] . " " . $old_image_name, $result);
							}
*/
						}
						$thumbsize = array();
						$thumbsize[0] = 0;
						$thumbsize[1] = 0;
						$thumbtype = "";
						if ($CFG["create_thumbs"]) {
							if (!create_thumb($upload_name, $size, $upload_thumb_name, $thumbsize)) {
								error($could_not_create_thumb);
							}
						} else {
							if ($CFG["unix"]) {
								// Note: This function is not implemented on Windows platforms.
								chmod ($upload_thumb_name, 0644);
							}
							if ($old_thumb_name != $upload_thumb_name && file_exists($old_thumb_name)) {
/*
								if ($CFG["unix"] {
*/
									// Note: This function may not work on Windows systems.
									unlink ($old_thumb_name);
/*
								} else {
									system ($CFG["rmf"] . " " . $old_thumb_name, $result);
								}
*/
							}
						}

						if (!$CFG["images_on_file_system"]) {
							$imagedata = "";
							if (isset($upload_name) && file_exists($upload_name)) {
								$fd = fopen ($upload_name, "rb");
								$contents = fread ($fd, filesize ($upload_name));
								fclose ($fd);
								$imagedata = base64_encode($contents);
								$qid = new PGM_Sql("
									UPDATE products SET imagedata = '$imagedata'
									WHERE id = '$product_id'
								");
/*
								if ($CFG["unix"]) {
*/
									// Note: This function may not work on Windows systems.
									unlink ($upload_name);
/*
								} else {
									system ($CFG["rmf"] . " " . $upload_name, $result);
								}
*/
							}
							$thumbdata = "";
							if (isset($upload_thumb_name) && file_exists($upload_thumb_name)) {
								$fd = fopen ($upload_thumb_name, "rb");
								$contents = fread ($fd, filesize ($upload_thumb_name));
								fclose ($fd);
								$thumbdata = base64_encode($contents);
								$qid = new PGM_Sql("
									UPDATE products SET thumbdata = '$thumbdata'
									WHERE id = '$product_id'
								");
/*
								if ($CFG["unix"]) {
*/
									// Note: This function may not work on Windows systems.
									unlink ($upload_thumb_name);
/*
								} else {
									system ($CFG["rmf"] . " " . $upload_thumb_name, $result);
								}
*/
							}
						}

						$qid->query("
							UPDATE products SET
								 imagetype = '$type'
								,imagewidth = '$size[0]'
								,imageheight = '$size[1]'
								,thumbtype = 'jpg'
								,thumbwidth = '$thumbsize[0]'
								,thumbheight = '$thumbsize[1]'
							WHERE id = '$product_id'
						");
					}
				}
			} else {
				$errormsg .= "<li> " . $pcode . ": " . $no_products_with_such_code;
			}
		}
	}
	closedir($handle); 

	if ($uploaded) {
		// Let's remove the temporary directory ...
		system ($CFG["rmrf"] . " " . $tmpdir, $result);
	}

	// Let's close the transaction
	$qid->commit();
	
	// The End
	include ($CFG["dirroot"] . "form_header.php");
	$t = new Template();
	$t->set_file("page", "templates/data_import_end.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("ME", $ME);
	$t->set_var("backurl", $PGM_SESSION["goback"]["request_uri"]);
	$t->pparse("out", "page");
}

/**
* Generates on the fly a thumb of the full size image
* @param string $image_filename filename (with the path) of the full size image
* @param array $imagesize array related to the full size image and resulting
*   from getimagesize()
* @param string $thumb_filename filename (with the path) of the thumb
*   to be generated
* @param array $thumbsize analogous to $imagesize, it is passed by reference
*   to return values related to the thumb
* @return boolean
*/
function create_thumb($image_filename, $imagesize, $thumb_filename, &$thumbsize) {
	global $CFG;
	include ($CFG["globalerror"]);

	if ($CFG["use_imagemagick"]) {
//echo "IMAGEMAGICK";
		system ("\"" . $CFG["imagemagick_path"] . "convert" . "\" -geometry 100x75 -quality 85 $image_filename $thumb_filename", $result);
		if (file_exists($thumb_filename . ".0")) {
			rename ($thumb_filename . ".0", $thumb_filename);
			system ($CFG["rmf"] . " " . $thumb_filename . ".*");
		}
		$thumbsize = getimagesize ($thumb_filename);
		return true;
	} else if ($CFG["use_libgd"]) {
//echo "LIBGD";
		switch ($imagesize[2]) {
			case 1:
				$thumbsize[0] = 0;
				$thumbsize[1] = 0;
				error($gifs_are_not_supported_by_libgd);
				return false;
				break;
			case 2:
				$src_img = imagecreatefromjpeg($image_filename);
				break;
			case 3:
				$src_img = imagecreatefrompng($image_filename);
				break;
		}
		$imagewidth = $imagesize[0];
		$imageheight = $imagesize[1];
		$thumbwidth = min(100, (int) ($imagewidth*75.0/$imageheight + 0.5));
		$thumbheight = min(75, (int) ($imageheight*100.0/$imagewidth + 0.5));
		$dst_img = imagecreate($thumbwidth, $thumbheight);
		imagecopyresized($dst_img, $src_img, 0, 0, 0, 0, $thumbwidth, $thumbheight, $imagewidth, $imageheight);
		imagejpeg($dst_img, $thumb_filename);
		imagedestroy($dst_img);
		$thumbsize[0] = $thumbwidth;
		$thumbsize[1] = $thumbheight;
		return true;
	} else if ($CFG["use_netpbm"]) {
//echo "NETPBM";
		switch ($imagesize[2]) {
			case 1:
				$convert = $CFG["netpbm_path"] . "giftopnm";
				break;
			case 2:
				$convert = $CFG["libjpeg_path"] . "djpeg";
				break;
			case 3:
				$convert = $CFG["netpbm_path"] . "pngtopnm";
				break;
		}
		system ($convert . " " . $image_filename . " | " .
			$CFG["netpbm_path"] . "pnmscale -xysize 100 80 | " .
			$CFG["libjpeg_path"] . "cjpeg -optimize -progressive -quality 85 > $thumb_filename",
			$result
		);
		$thumbsize = getimagesize ($thumb_filename);
		return true;
	}

	return false;
}

/**
* Updates the image and the thumb for a given product
* @param array $frm the array containing values to be used for updating
* @return void
*/
function update_images($frm) {
	global $_FILES;
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["globalerror"]);

/* does PHP 4.0.4pl1 have a bug causing $HTTP_POST_FILES['image']['type'] == "" ?
echo ";".$HTTP_POST_FILES['image']['name'].";";
echo     $HTTP_POST_FILES['image']['tmp_name'].";";
echo     $HTTP_POST_FILES['image']['size'].";";
echo     $HTTP_POST_FILES['image']['type'].";";
echo "<br><br>";
reset($HTTP_POST_FILES['image']);
while (list($key, $value) = each($HTTP_POST_FILES['image'])) {
	echo "\$HTTP_POST_FILES['image'][$key] => ;$value;";
	echo "<br>";
}
*/

	$imagesize = array();
	$imagesize[0] = nvl($frm["old_imagewidth"], 0);
	$imagesize[1] = nvl($frm["old_imageheight"], 0);
	$imagetype = nvl($frm["old_imagetype"], "");
	$thumbsize = array();
	$thumbsize[0] = nvl($frm["old_thumbwidth"], 0);
	$thumbsize[1] = nvl($frm["old_thumbheight"], 0);
	$thumbtype = nvl($frm["old_thumbtype"], "");

	$thumb_has_to_be_created = nvl($frm["create_thumb"], 0); 
	$thumb_has_been_uploaded = !(nvl($_FILES["thumb"]["tmp_name"], "") == "none") && !(nvl($_FILES["thumb"]["tmp_name"], "") == "");

	if (nvl($frm["delete_image"], 0)) {
		if ($CFG["images_on_file_system"]) {
			$upload_name = $CFG["productsdir"] . $frm["product_id"] . "." . $imagetype;
			if (file_exists($upload_name)) {
/*
				if ($CFG["unix"]) {
*/
					// Note: This function may not work on Windows systems.
					unlink ($upload_name);
/*
				} else {
					system ($CFG["rmf"] . " " . $upload_name, $result);
				}
*/
			}
		} else {
			$qid = new PGM_Sql("
				UPDATE products SET imagedata = ''
				WHERE id = '" . $frm["product_id"] . "'
			");
		}
		$imagesize[0] = 0;
		$imagesize[1] = 0;
		$imagetype = "";
	}
	if (nvl($frm["delete_thumb"], 0)) {
		if ($CFG["images_on_file_system"]) {
			$upload_thumb_name = $CFG["productsdir"] . $frm["product_id"] . ".thumb." . $thumbtype;
			if (file_exists($upload_thumb_name)) {
/*
				if ($CFG["unix"]) {
*/
					// Note: This function may not work on Windows systems.
					unlink ($upload_thumb_name);
/*
				} else {
					system ($CFG["rmf"] . " " . $upload_thumb_name, $result);
				}
*/
			}
		} else {
			$qid = new PGM_Sql("
				UPDATE products SET thumbdata = ''
				WHERE id = '" . $frm["product_id"] . "'
			");
		}
		$thumbsize[0] = 0;
		$thumbsize[1] = 0;
		$thumbtype = "";
	}

	if (nvl($_FILES["image"]["tmp_name"], "") == "none" || nvl($_FILES["image"]["tmp_name"], "") == "") {	// "" --> Opera 5
		message($no_product_image_chosen);
	} else {
		$upload_name = $CFG["productsdir"] . $frm["product_id"];
		if (!move_uploaded_file($_FILES["image"]["tmp_name"], $upload_name)) {
			error("1346".$image_upload_failed." Could not move ".$_FILES["image"]["tmp_name"]." to $upload_name<br>");
		} else {
			$newimagesize = getimagesize ($upload_name);
			switch ($newimagesize[2]) {
				case 1:
					$newimagetype = "gif";
					break;
				case 2:
					$newimagetype = "jpg";
					break;
				case 3:
					$newimagetype = "png";
					break;
				default:
					$newimagetype = "";
					error($unsupported_image_type);
			}
			if ($newimagetype != "") {
				$foobar = $upload_name;
				$upload_name .= "." . $newimagetype;
				rename ($foobar, $upload_name);
				$old_image_name = $CFG["productsdir"] . $frm["product_id"] . "." . $imagetype;
				$imagesize = $newimagesize;
				$imagetype = $newimagetype;
				if ($CFG["unix"]) {
					// Note: This function is not implemented on Windows platforms.
					chmod ($upload_name, 0644);
				}
				if ($old_image_name != $upload_name && file_exists($old_image_name)) {
/*
					if ($CFG["unix"]) {
*/
						// Note: This function may not work on Windows systems.
						unlink ($old_image_name);
/*
					} else {
						system ($CFG["rmf"] . " " . $old_image_name, $result);
					}
*/
				}
				if ($CFG["create_thumbs"] && $thumb_has_to_be_created && !$thumb_has_been_uploaded) {
					$old_thumb_name = $CFG["productsdir"] . $frm["product_id"] . ".thumb." . $thumbtype;
					$upload_thumb_name = $CFG["productsdir"] . $frm["product_id"] . ".thumb.jpg";
					if (!create_thumb($upload_name, $imagesize, $upload_thumb_name, $newthumbsize)) {
						error($could_not_create_thumb);
					} else {
						$thumbsize = $newthumbsize;
						$thumbtype = "jpg";
						if ($CFG["unix"]) {
							// Note: This function is not implemented on Windows platforms.
							chmod ($upload_thumb_name, 0644);
						}
						if ($old_thumb_name != $upload_thumb_name && file_exists($old_thumb_name)) {
/*
							if ($CFG["unix"]) {
*/
								// Note: This function may not work on Windows systems.
								unlink ($old_thumb_name);
/*
							} else {
								system ($CFG["rmf"] . " " . $old_thumb_name, $result);
							}
*/
						}
					}
				}
			} else {
/*
				if ($CFG["unix"]) {
*/
					// Note: This function may not work on Windows systems.
					unlink ($upload_name);
/*
				} else {
					system ($CFG["rmf"] . " " . $upload_name, $result);
				}
*/
			}
		}
	}

	if ($thumb_has_been_uploaded) {
		$upload_thumb_name = $CFG["productsdir"] . $frm["product_id"] . ".thumb";
		if (!move_uploaded_file($_FILES["thumb"]["tmp_name"], $upload_thumb_name)) {
			error("1430".$thumb_upload_failed);
		} else {
			$newthumbsize = getimagesize ($upload_thumb_name);
			switch ($newthumbsize[2]) {
				case 1:
					$newthumbtype = "gif";
					break;
				case 2:
					$newthumbtype = "jpg";
					break;
				case 3:
					$newthumbtype = "png";
					break;
				default:
					$newthumbtype = "";
					error($unsupported_thumb_type);
					break;
			}
			if ($newthumbtype != "") {
				$foobar = $upload_thumb_name;
				$upload_thumb_name .= "." . $newthumbtype;
				rename($foobar, $upload_thumb_name);
				$old_thumb_name = $CFG["productsdir"] . $frm["product_id"] . ".thumb." . $thumbtype;
				$thumbsize = $newthumbsize;
				$thumbtype = $newthumbtype;
				if ($CFG["unix"]) {
					// Note: This function is not implemented on Windows platforms.
					chmod ($upload_thumb_name, 0644);
				}
				if ($old_thumb_name != $upload_thumb_name && file_exists($old_thumb_name)) {
/*
					if ($CFG["unix"]) {
*/
						// Note: This function may not work on Windows systems.
						unlink ($old_thumb_name);
/*
					} else {
						system ($CFG["rmf"] . " " . $old_thumb_name, $result);
					}
*/
				}
			} else {
/*
				if ($CFG["unix"]) {
*/
					// Note: This function may not work on Windows systems.
					unlink ($upload_thumb_name);
/*
				} else {
					system ($CFG["rmf"] . " " . $upload_thumb_name, $result);
				}
*/
			}
		}
	}

	if (!$CFG["images_on_file_system"]) {
		$imagedata = "";
		if (isset($upload_name) && file_exists($upload_name)) {
			$fd = fopen ($upload_name, "rb");
			$contents = fread ($fd, filesize ($upload_name));
			fclose ($fd);
			$imagedata = base64_encode($contents);
			$qid = new PGM_Sql("
				UPDATE products SET imagedata = '$imagedata'
				WHERE id = '" . $frm["product_id"] . "'
			");
/*
			if ($CFG["unix"]) {
*/
				// Note: This function may not work on Windows systems.
				unlink ($upload_name);
/*
			} else {
				system ($CFG["rmf"] . " " . $upload_name, $result);
			}
*/
		}
		$thumbdata = "";
		if (isset($upload_thumb_name) && file_exists($upload_thumb_name)) {
			$fd = fopen ($upload_thumb_name, "rb");
			$contents = fread ($fd, filesize ($upload_thumb_name));
			fclose ($fd);
			$thumbdata = base64_encode($contents);
			$qid = new PGM_Sql("
				UPDATE products SET thumbdata = '$thumbdata'
				WHERE id = '" . $frm["product_id"] . "'
			");
/*
			if ($CFG["unix"]) {
*/
				// Note: This function may not work on Windows systems.
				unlink ($upload_thumb_name);
/*
			} else {
				system ($CFG["rmf"] . " " . $upload_thumb_name, $result);
			}
*/
		}
	}

	$qid = new PGM_Sql("
		UPDATE products SET
			 imagetype = '$imagetype'
			,imagewidth = '$imagesize[0]'
			,imageheight = '$imagesize[1]'
			,thumbtype = '$thumbtype'
			,thumbwidth = '$thumbsize[0]'
			,thumbheight = '$thumbsize[1]'
		WHERE id = '" . $frm["product_id"] . "'
	");
}

?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "Colors_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "add":
		print_add_color_form();
		break;

	case "edit":
		if ($id != 1) {
			print_edit_color_form($id);
		}
		break;

	case "del":
		if ($id != 1) {
			delete_color($id);
		}
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_color_name($_POST["name"], $errors, $errormsg);
		validate_color_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			insert_color($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_Color";
			include ($CFG["dirroot"] . "form_header.php");
			load_color_form_template($frm, $errors);
		}
		break;

	case "update":
		if ($id == 1) {
			break;
		}
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_color_name($_POST["name"], $errors, $errormsg);
		validate_color_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			update_color($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_color_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_colors();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_color_form_template($frm, $errors) {
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/color_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"id"			=> nvl($frm["id"], ""),
		"frm_name"		=> ov($frm["name"]),
		"errors_name"		=> errmsg(nvl($errors["color_name"], "")),
		"frm_description"	=> ov($frm["description"]),
		"errors_description"	=> errmsg(nvl($errors["color_description"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_add_color_form() {
	// set default values for the reset of the fields
	$frm["newmode"] = "insert";
	$frm["name"] = "";
	$frm["description"] = "";
	$frm["submit_caption"] = "Add_Color";
	$errors = array();

	load_color_form_template($frm, $errors);
}

function print_edit_color_form($id) {
	$qid = new PGM_Sql("SELECT name, description FROM colors WHERE id = '$id'");
	$qid->next_record();
	$frm["id"] = $id;
	$frm["name"] = $qid->f("name");
	$frm["description"] = $qid->f("description");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_color_form_template($frm, $errors);
}

function delete_color($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();
	// find out the color name
	$qid->query("SELECT name from colors WHERE id = '$id'");
	$qid->next_record();
	$frm["name"] = $qid->f("name");
	// delete this color
	$qid->query("DELETE FROM colors WHERE id = '$id'");
	// make the color no more available for any product
	$qid->query("DELETE FROM products_colors WHERE color_id = '$id'");
	// delete this color from the colors_i18n table
	$qid->query("DELETE FROM colors_i18n WHERE color_id = '$id'");
	$qid->commit();

	$t = new Template();
	$t->set_file("page", "templates/color_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function insert_color($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql();
	$qid->lock("colors");
	$qid->query("
		INSERT INTO colors (name, description)
		VALUES ('" . $frm["name"] . "', '" . $frm["description"] . "')
	");
	$qid->query("SELECT id FROM colors ORDER BY id DESC");
	$qid->next_record();
	$inserted_color_id = $qid->f("id");
	$qid->unlock();

	$t = new Template();
	$t->set_file("page", "templates/color_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"id"		=> $inserted_color_id,
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_color($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		UPDATE colors SET
			 name = '" . $frm["name"] . "'
			,description = '" . $frm["description"] . "'
		WHERE id = '" . $frm["id"] . "'
	");

	$t = new Template();
	$t->set_file("page", "templates/color_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"id"		=> $frm["id"],
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_colors() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM colors WHERE id > 1");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["colors_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT id, name, description FROM colors WHERE id > 1 ORDER BY name
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/color_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "color_row", "color_rows");
	$t->set_var("color_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"		=> $qid->f("id"),
			"name"		=> ov($qid->f("name")),
			"description"	=> ovwbr($qid->f("description"))
		));
		$t->parse("color_rows", "color_row", true);
	}
	$t->pparse("out", "page");
}

?>

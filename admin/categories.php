<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");
include ($CFG["libdir"] . $CFG["dbms"] . "_for_admin.inc.php");
include ($CFG["libdir"] . $CFG["dbms"] . "_layersmenu_queries.inc.php");

$DOC_TITLE = "Categories_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "add":
		print_add_category_form($id);
		break;

	case "edit":
		print_edit_category_form($id);
		break;

	case "del":
		if ($id != 1) {
			delete_category($id);
			if ($CFG["layersmenu_cached"]) {
				refresh_cache();
			}
			if ($PGM_SESSION["frames_used"]) { ?>
			<script language="JavaScript" type="text/javascript">
			<!--
			parent.categories.location.reload();
			// -->
			</script>
			<?php }
		}
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_category_name($_POST["name"], $errors, $errormsg);
		validate_special_level($_POST["special_level"], $errors, $errormsg);
		validate_category_description($_POST["description"], $errors, $errormsg);
		if ($errormsg == "") {
			insert_category($_POST);
			if ($CFG["layersmenu_cached"]) {
				refresh_cache();
			}
			if ($PGM_SESSION["frames_used"]) { ?>
			<script language="JavaScript" type="text/javascript">
			<!--
			parent.categories.location.reload();
			// -->
			</script>
			<?php }
//			list_categories();
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["parent"] = array($_POST["parent"]);
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_Subcategory";
			// build the categories listbox options, preselect the selected item
			build_category_tree($category_options, $frm["parent"]);
			include ($CFG["dirroot"] . "form_header.php");
			load_category_form_template($frm, $category_options, $errors);
		}
		break;

	case "update":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_category_name($_POST["name"], $errors, $errormsg);
		validate_special_level($_POST["special_level"], $errors, $errormsg);
		validate_category_description($_POST["description"], $errors, $errormsg);
		if ($id == $_POST["parent"]) {
			$errors["category_parent"] = true;
			include ($CFG["globalerror"]);
			$errormsg .= "<li>" . $category_cannot_be_parent_of_itself;
		}
		if ($errormsg == "") {
			update_category($_POST);
			if ($CFG["layersmenu_cached"]) {
				refresh_cache();
			}
			if ($PGM_SESSION["frames_used"]) { ?>
			<script language="JavaScript" type="text/javascript">
			<!--
			parent.categories.location.reload();
			// -->
			</script>
			<?php }
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["parent"] = array($_POST["parent"]);
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			// build the categories listbox options, preselect the selected item
			build_category_tree($category_options, $frm["parent"]);
			include ($CFG["dirroot"] . "form_header.php");
			load_category_form_template($frm, $category_options, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_categories();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_category_form_template($frm, $category_options, $errors) {
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/category_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"id"			=> nvl($frm["id"], ""),
		"category_options"	=> $category_options,
		"errors_category_parent" => errmsg2(nvl($errors["category_parent"], "")),
		"frm_name"		=> ov(nvl($frm["name"], "")),
		"errors_name"		=> errmsg(nvl($errors["category_name"], "")),
		"frm_special_level"	=> nvl($frm["special_level"], ""),
		"errors_special_level"	=> errmsg(nvl($errors["special_level"], "")),
		"frm_description"	=> ov(nvl($frm["description"], "")),
		"errors_description"	=> errmsg(nvl($errors["category_description"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

/**
* Prepares an empty form to add a new category
* @param integer $id the category id to set as preselected parent
*   for the new category
* @return void
*/
function print_add_category_form($id) {
	// set default values for the reset of the fields
	$frm["parent"] = array($id);
	$frm["newmode"] = "insert";
	$frm["name"] = "";
	$frm["special_level"] = "";
	$frm["description"] = "";
	$frm["submit_caption"] = "Add_Subcategory";

	// build the categories listbox options, preselect the selected item
	build_category_tree($category_options, $frm["parent"]);
	$errors = array();

	load_category_form_template($frm, $category_options, $errors);
}

function print_edit_category_form($id) {
	$qid = new PGM_Sql("
		SELECT name, special_level, description, parent_id
		FROM categories
		WHERE id = '$id'
	");
	$qid->next_record();
	$frm["id"] = $id;
	$frm["name"] = $qid->f("name");
	$frm["special_level"] = $qid->f("special_level");
	$frm["description"] = $qid->f("description");
	$frm["parent_id"] = $qid->f("parent_id");

	$frm["parent"] = array($frm["parent_id"]);
	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";

	// build the categories listbox options, preselect the selected item
	build_category_tree($category_options, $frm["parent"]);
	$errors = array();

	load_category_form_template($frm, $category_options, $errors);
}

/**
* Deletes a category
*
* All products and categories under the category to be deleted
* are moved to the immediate parent
*
* @param integer $id the id of the category to be deleted
* @return void
*/
function delete_category($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
	$qid->begin();

	// find the parent of this category
	$qid->query("
		SELECT cat.name, cat.parent_id, parent.name AS parent
		FROM categories cat, categories parent
		WHERE parent.id = cat.parent_id AND cat.id = '$id'
	");
	$qid->next_record();
	$cat = array();
	$cat["name"] = $qid->f("name");
	$cat["parent_id"] = $qid->f("parent_id");
	$cat["parent"] = $qid->f("parent");

	// delete products in this category that also belong
	// to the parent category, to avoid duplicates forbidden
	// by the primary key constraint
/* Nested queries aren't still supported by MySQL :(
 *	$qid->query("
 *		DELETE FROM products_categories WHERE ( product_id,category_id ) IN (
 *			SELECT * FROM products_categories pc WHERE category_id = '$id' AND
 *			EXISTS (
 *				SELECT * FROM products_categories
 *				WHERE product_id = pc.product_id
 *					AND category_id = '$cat->parent_id'
 *			)
 *		)
 *	");
 * a workaround in the following */
	$qid->query("
		SELECT pc1.product_id
		FROM products_categories pc1 , products_categories pc2
		WHERE
			pc1.product_id = pc2.product_id
			AND pc1.category_id = '" . $cat["parent_id"] ."'
			AND pc2.category_id = '$id'
	");
	if (($numr = $qid->num_rows()) > 0) {
		$toberemoved = "";
		for ($i=0; $i<$numr-1; $i++) {
			$qid->next_record();
			$toberemoved .= "'" . $qid->f("product_id") . "', ";
		}
		$qid->next_record();
		$toberemoved .= "'" . $qid->f("product_id") . "'";
		$qid->query("
			DELETE FROM products_categories
			WHERE category_id = '$id' AND product_id IN ($toberemoved)
		");
	}

	// re-assign all the products in this category to the parent category
	$qid->query("
		UPDATE products_categories
		SET category_id = '" . $cat["parent_id"] . "'
		WHERE category_id = '$id'
	");

	// re-assign all sub categories of this category to the parent category
	$qid->query("
		UPDATE categories
		SET parent_id = '" . $cat["parent_id"] . "'
		WHERE parent_id = '$id'
	");

	// delete this category from the categories table
	$qid->query("DELETE FROM categories WHERE id = '$id'");
	// delete this category from the categories_i18n table
	$qid->query("DELETE FROM categories_i18n WHERE category_id = '$id'");

	$qid->commit();

	$t = new Template();
	$t->set_file("page", "templates/category_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"name"		=> ov($cat["name"]),
		"parent"	=> ov($cat["parent"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function insert_category($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql();
	$qid->lock("categories");
	$qid->query("
		INSERT INTO categories (
			 parent_id
			,name
			,special_level
			,description
		)
		VALUES (
			 '" . $frm["parent"] . "'
			,'" . $frm["name"] . "'
			,'" . $frm["special_level"] . "'
			,'" . $frm["description"] . "'
		)
	");
	$qid->query("SELECT id FROM categories ORDER BY id DESC");
	$qid->next_record();
	$inserted_category_id = $qid->f("id");
	$qid->unlock();

	$t = new Template();
	$t->set_file("page", "templates/category_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"id"		=> $inserted_category_id,
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_category($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		UPDATE categories SET
			 parent_id = '" . $frm["parent"] . "'
			,name = '" . $frm["name"] . "'
			,special_level = '" . $frm["special_level"] . "'
			,description = '" . $frm["description"] . "'
		WHERE id = '" . $frm["id"] . "'
	");

	$t = new Template();
	$t->set_file("page", "templates/category_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"id"		=> $frm["id"],
		"frm_name"	=> ov($frm["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_categories() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM categories WHERE id > 1");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["categories_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT cat.id, cat.name, cat.special_level, cat.description, parent.name AS parent
		FROM categories cat, categories parent
		WHERE parent.id = cat.parent_id AND cat.id > 1 ORDER BY cat.name
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/category_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "category_row", "category_rows");
	$t->set_var("category_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"		=> $qid->f("id"),
			"parent"	=> ov($qid->f("parent")),
			"name"		=> ov($qid->f("name")),
			"special_level"	=> $qid->f("special_level"),
			"description"	=> ovwbr($qid->f("description"))
		));
		$t->parse("category_rows", "category_row", true);
	}
	$t->pparse("out", "page");
}

?>

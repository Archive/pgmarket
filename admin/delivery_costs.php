<?php
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$qid = new PGM_Sql();

$DOC_TITLE = "DC_Management";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$maxqty = nvl($_GET["maxqty"], "");
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$maxqty = nvl($_POST["maxqty"], "");
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "add":
		print_add_dc_form($id);
		break;

	case "edit":
		print_edit_dc_form($id, $maxqty);
		break;

	case "del":
		delete_dc($id, $maxqty);
		break;

	case "insert":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_weight($_POST["maxqty"], $errors, $errormsg);
		if (!nvl($errors["weight"], "")) {
			$qid->query("SELECT zone_id FROM delivery_costs WHERE zone_id = " . $_POST["id"] . " AND maxqty = " . $_POST["maxqty"]);
			if ($qid->num_rows() > 0) {
				$errors["weight"] = true;
				include ($CFG["globalerror"]);
				$errormsg .= "<li>" . $maxqty_already_used_for_this_zone;
			}
		}
		validate_cost($_POST["cost"], $errors, $errormsg);
		if ($errormsg == "") {
			insert_dc($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "insert";
			$frm["submit_caption"] = "Add_DC";
			include ($CFG["dirroot"] . "form_header.php");
			load_dc_form_template($frm, $errors);
		}
		break;

	case "update":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_cost($_POST["cost"], $errors, $errormsg);
		if ($errormsg == "") {
			update_dc($_POST);
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_dc_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		if (!isset($id)) {
			$id = 1;
		}
		list_dc($id);
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_dc_form_template($frm, $errors) {
	global $CFG;

	$qid = new PGM_Sql();

	$t = new Template();
	get_zone_name($qid, nvl($frm["id"], ""));
	$qid->next_record();
	$zone_name = ($qid->f("lname") != "") ? ov($qid->f("lname")) : $qid->f("name");
	$t->set_var("zone_name", $zone_name);
	$t->set_file("page", "templates/delivery_costs_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	
	$t->set_block("page", "update_cost", "update_cost_blck");
	$t->set_var("update_cost_blck", "");
	$t->set_block("page", "insert_cost", "insert_cost_blck");
	$t->set_var("insert_cost_blck", "");
	if($frm["newmode"] == "update") {
		$t->parse("update_cost_blck", "update_cost", true);
	} else {
		$t->parse("insert_cost_blck", "insert_cost", true);
	}
	$t->set_var("total_selected", "");
	$t->set_var("unitary_selected", "");
	if ($frm["type"] == "T") {
		$t->set_var("total_selected", "selected");
	} else if ($frm["type"] == "U") {
		$t->set_var("unitary_selected", "selected");
	}
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"id"			=> nvl($frm["id"], ""),
		"frm_maxqty"		=> $frm["maxqty"],
		"frm_fmaxqty"		=> formatted_weight($frm["maxqty"]),
		"frm_cost"		=> $frm["cost"],
		"errors_weight"		=> errmsg(nvl($errors["weight"], "")),
		"errors_cost"		=> errmsg(nvl($errors["cost"], "")),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

/**
* Prepares an empty form to add a new delivery cost
* @param integer $id identifier of the zone to which the delivery cost
*   has to be added
* @return void
*/
function print_add_dc_form($id) {
	// set default values for the reset of the fields
	$frm["newmode"] = "insert";
	$frm["id"] = $id;
	$frm["cost"] = "";
	$frm["maxqty"] = "";
//	$frm["type"] = "U";
	$frm["type"] = "";
	$frm["submit_caption"] = "Add_DC";
	$errors = array();

	load_dc_form_template($frm, $errors);
}

/**
* Prepares a form to allow editing a delivery cost
* @param integer $id identifier of the zone for which the delivery cost
*   has to be edited
* @param float $qty max quantity for which the delivery cost has to be edited
* @return void
*/
function print_edit_dc_form($id, $qty) {
	$qid = new PGM_Sql("
		SELECT zone_id, maxqty, cost, type
		FROM delivery_costs
		WHERE zone_id = '$id' AND maxqty = '$qty'
	");
	$qid->next_record();
	$frm["id"] = $qid->f("zone_id");
	$frm["maxqty"] = $qid->f("maxqty");
	$frm["cost"] = $qid->f("cost");
	$frm["type"] = $qid->f("type");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_dc_form_template($frm, $errors);
}

/**
* Deletes a delivery cost
* @param integer $id identifier of the zone for which the delivery cost
*   has to be deleted
* @param float $qty max quantity for which the delivery_cost has to be deleted
* @return void
*/
function delete_dc($id, $qty) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("DELETE FROM delivery_costs WHERE zone_id = '$id' AND maxqty = '$qty'");

	$t = new Template();
	$t->set_file("page", "templates/delivery_cost_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("backurl", $PGM_SESSION["goback"]["request_uri"]);
	$t->pparse("out", "page");
}

function insert_dc($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		INSERT INTO delivery_costs (
			 zone_id
			,maxqty
			,cost
			,type
		)
		VALUES (
			 '" . nvl($frm["id"], "") . "'
			,'" . nvl($frm["maxqty"], "") . "'
			,'" . nvl($frm["cost"], "") . "'
			,'" . nvl($frm["type"], "") . "'
		)
	");

	$t = new Template();
	$t->set_file("page", "templates/delivery_cost_added.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var("backurl", $PGM_SESSION["goback"]["request_uri"]);
	$t->pparse("out", "page");
}

function update_dc($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		UPDATE delivery_costs SET
			 cost = '" . $frm["cost"] . "'
			,type = '" . $frm["type"] . "'
		WHERE zone_id = '" . $frm["id"] . "' AND maxqty = '" . $frm["maxqty"] . "'
	");

	$t = new Template();
	$t->set_file("page", "templates/delivery_cost_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("backurl", $PGM_SESSION["goback"]["request_uri"]);
	$t->pparse("out", "page");
}

/**
* Prepares a page providing a list of delivery costs for a given zone
* @param integer $id identifier of the zone for which the delivery costs
*   have to be listed
* @return void
*/
function list_dc($id) {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(zone_id) AS cnt FROM delivery_costs WHERE zone_id = '$id'");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["dc_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT maxqty, cost, type FROM delivery_costs WHERE zone_id = '$id' ORDER BY maxqty
		$limit_clause
	");

	$t = new Template();
	$t->set_file("page", "templates/delivery_costs_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("id", $id);
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_block("page", "dc_row", "dc_rows");
	$t->set_var("dc_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"maxqty"	=> $qid->f("maxqty"),
			"fmaxqty"	=> formatted_weight($qid->f("maxqty")),
			"fcost"		=> formatted_price($qid->f("cost")),
			"type"		=> ($qid->f("type") == "T") ? $t->get_var("DC_Fixed") : $t->get_var("DC_Unitary")
		));
		$t->parse("dc_rows", "dc_row", true);
	}
	$qid_zone = new PGM_Sql();
	get_zone_name($qid_zone, $id);
	$qid_zone->next_record();
	$zone_name = ($qid_zone->f("lname") != "") ? ov($qid_zone->f("lname")) : ov($qid_zone->f("name"));
	$t->set_var("zone_name", $zone_name);
	$t->pparse("out", "page");
}

?>

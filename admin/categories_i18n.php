<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");
include ($CFG["libdir"] . $CFG["dbms"] . "_for_admin.inc.php");
include ($CFG["libdir"] . $CFG["dbms"] . "_layersmenu_queries.inc.php");

$DOC_TITLE = "Categories_Management_i18n";
include ("header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}

switch (nvl($mode)) {
	case "edit":
		print_edit_category_i18n_form($id);
		break;

	case "del":
		if ($id != 1) {
			delete_category_i18n($id);
			if ($CFG["layersmenu_cached"]) {
				refresh_cache();
			}
			if ($PGM_SESSION["frames_used"]) { ?>
			<script language="JavaScript" type="text/javascript">
			<!--
			parent.categories.location.reload();
			// -->
			</script>
			<?php }
		}
		break;

	case "update":
		$errors = array();
		$errormsg = "";
		include ($CFG["libdir"] . "pgm_validate.inc.php");
		validate_category_name($_POST["lname"], $errors, $errormsg);
		if ($errormsg == "") {
			update_category_i18n($_POST);
			if ($CFG["layersmenu_cached"]) {
				refresh_cache();
			}
			if ($PGM_SESSION["frames_used"]) { ?>
			<script language="JavaScript" type="text/javascript">
			<!--
			parent.categories.location.reload();
			// -->
			</script>
			<?php }
		} else {
			// set default values for the reset of the fields
			$frm = $_POST;
			$frm["newmode"] = "update";
			$frm["submit_caption"] = "Save_Changes";
			include ($CFG["dirroot"] . "form_header.php");
			load_category_i18n_form_template($frm, $errors);
		}
		break;

	default:
		$PGM_SESSION["goback"]["request_uri"] = stripsid(me() . "?" . $_SERVER["QUERY_STRING"]);
		list_categories_i18n();
		break;
}

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function load_category_i18n_form_template($frm, $errors) {
	global $PGM_SESSION;
	global $CFG;

	$t = new Template();
	$t->set_file("page", "templates/category_i18n_form.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var("star", return_star());
	$t->set_var(array(
		"frm_newmode"		=> $frm["newmode"],
		"id"			=> nvl($frm["id"], ""),
		"frm_name"		=> ov($frm["name"]),
		"i18n"			=> ov($PGM_SESSION["i18n"]),
		"frm_lname"		=> ov($frm["lname"]),
		"errors_name"		=> errmsg(nvl($errors["category_name"], "")),
		"frm_description"	=> ov($frm["description"]),
		"frm_submit_caption"	=> $t->get_var($frm["submit_caption"])
	));
	$t->pparse("out", "page");
}

function print_edit_category_i18n_form($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql();
//	get_category_info($qid, $id);
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("categories c", "categories_i18n l"),
		array(array("c.id", "l.category_id", 1, $PGM_SESSION["i18n"])),
		array(array("l.name AS lname")),
		"c.id, c.name, c.special_level, c.description",
		"",
		"c.id = '$id'",
		"",
		""
	);
	$qid->next_record();
	$frm["id"] = $id;
	$frm["name"] = $qid->f("name");
	$frm["lname"] = $qid->f("lname");
	$frm["description"] = $qid->f("description");

	$frm["newmode"] = "update";
	$frm["submit_caption"] = "Save_Changes";
	$errors = array();

	load_category_i18n_form_template($frm, $errors);
}

function delete_category_i18n($id) {
	global $PGM_SESSION;
	global $CFG;

	$qid = new PGM_Sql("
		DELETE FROM categories_i18n
		WHERE category_id = '$id' AND lang = '" . $PGM_SESSION["i18n"] . "'
	");
	$qid->query("
		SELECT name FROM categories
		WHERE id = '$id'
	");
	$qid->next_record();
	$cat["name"] = $qid->f("name");

	$t = new Template();
	$t->set_file("page", "templates/category_i18n_deleted.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"cat_name"	=> ov($cat["name"]),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function update_category_i18n($frm) {
	global $PGM_SESSION;
	global $CFG;

	while (list($key, $val) = each($frm)) {
		if (is_string($val)) $frm[$key] = addslashes($val);
	}

	$qid = new PGM_Sql("
		DELETE FROM categories_i18n
		WHERE category_id = '" . $frm["id"] . "' AND lang = '" . $PGM_SESSION["i18n"] . "'
	");
	$qid->query("
		INSERT INTO categories_i18n (category_id, lang, name)
		VALUES ('" . $frm["id"] . "', '" . $PGM_SESSION["i18n"] . "', '" . $frm["lname"] . "')
	");
	$qid->query("
		SELECT name FROM categories
		WHERE id = '" . $frm["id"] . "'
	");
	$qid->next_record();

	$t = new Template();
	$t->set_file("page", "templates/category_i18n_updated.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_var(array(
		"frm_name"	=> ov($qid->f("name")),
		"backurl"	=> $PGM_SESSION["goback"]["request_uri"]
	));
	$t->pparse("out", "page");
}

function list_categories_i18n() {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM categories WHERE id > 1");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["categories_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
//	$limit_clause = prepare_limit_clause($limit, $result["first"]);
//	get_allcategories_info($qid, $limit_clause);
	$qid->pgm_lang_join_query(
		$CFG["base_lang"],
		array("categories c", "categories_i18n l"),
		array(array("c.id", "l.category_id", 1, $PGM_SESSION["i18n"])),
		array(array("l.name AS lname")),
		"c.id, c.name, c.special_level, c.description, parent.name AS parent",
		"categories parent",
		"c.parent_id = parent.id AND c.id > 1",
		"name, special_level",
		array($result["first"], $limit)
	);

	$t = new Template();
	$t->set_file("page", "templates/category_i18n_list.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	include ($CFG["localelangdir"] . "global-admin.inc.php");
	$t->set_block("page", "pages_links_top", "pages_links_top_blck");
	$t->set_var("pages_links_top_blck", "");
	$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
	$t->set_var("pages_links_bottom_blck", "");
	if ($result["num_pages"] > 1) {
		$t->set_var("pages_links", $result["html"]);
		$t->parse("pages_links_top_blck", "pages_links_top", true);
		$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
	}
	$t->set_var("i18n", ov($PGM_SESSION["i18n"]));
	$t->set_block("page", "category_row", "category_rows");
	$t->set_var("category_rows", "");
	while ($qid->next_record()) {
		$t->set_var(array(
			"id"		=> $qid->f("id"),
			"name"		=> ov($qid->f("name")),
			"parent"	=> ov($qid->f("parent")),
			"lname"		=> ov($qid->f("lname")),
			"special_level"	=> $qid->f("special_level"),
			"description"	=> ovwbr($qid->f("description"))
		));
		$t->parse("category_rows", "category_row", true);
	}
	$t->pparse("out", "page");
}

?>

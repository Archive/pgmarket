<?php
// (C) 2001-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
require_priv("admin");

$DOC_TITLE = "Data_Import";
include ("header.php");

$t = new Template();
$t->set_file("page", "templates/tables_before_import.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->pparse("out", "page");

include ("footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>

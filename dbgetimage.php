<?php
include ("config.inc.php");
include ($CFG["libdir"] . "phplib/db_" . $CFG["dbms"] . ".inc.php");	// PHPLib...
include ($CFG["libdir"] . "pgm_" . $CFG["dbms"] . ".inc.php");		// PGM extension...
$qid = new PGM_Sql("SELECT imagedata, imagetype FROM products WHERE id = '" . $_GET["id"] . "'");
$qid->next_record();
$type = $qid->f("imagetype");
if ($type == "jpg") $type = "jpeg";
header("Content-Type: image/" . $type);
header("Cache-Control: max-age=7200");
header("Expires: " . gmdate("D, d M Y H:i:s", time()+7200) . " GMT");
echo base64_decode($qid->f("imagedata"));
?>

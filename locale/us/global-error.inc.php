<?php

/* (c) 2000-2002 Marco Pratesi <pratesi@telug.it> */
/* (c) 2000-2001 Fabio Molinari <fabio.m@mclink.it> */

global $IVA;

$Errors = "Errors";
$file_line = "line";

////////////////////////////////////////////////////////////
// Top level scripts
////////////////////////////////////////////////////////////

$login_failed = "Login failed, please try again";

////////////////////////////////////////////////////////////
// user discounts
////////////////////////////////////////////////////////////

$not_valid_user_discount = "Discount not valid";
$user_discount_not_between_0_and_100 = "The Discount is not between 0 and 100";
$empty_user_discount_name = "The Discount Name is empty; please fill it";
$user_discount_name_too_long = "Discount Name too long: " . $SETTING["user_discount_name_length"] . " characters max";
$user_discount_description_too_long = "Discount Description too long: " . $SETTING["user_discount_description_length"] . " characters max";

$Err_ImpUserDiscount = ": the User Discount doesn't exist";

////////////////////////////////////////////////////////////
// add and update zones
////////////////////////////////////////////////////////////

$empty_zone_code = "The delivery Area Code is empty; please fill it";
$zone_code_too_long = "Delivery Area Code too long: " . $SETTING["zone_code_length"] . " characters max";
$empty_zone_name = "The delivery Area Name is empty; please fill it";
$zone_name_too_long = "Delivery Area Name too long: " . $SETTING["zone_name_length"] . " characters max";
$zone_description_too_long = "Delivery Area Description too long: " . $SETTING["zone_description_length"] . " characters max";

$Err_ImpZone = ": There's no Delivery Area with this Code";

////////////////////////////////////////////////////////////
// add and update delivery costs
////////////////////////////////////////////////////////////

$maxqty_already_used_for_this_zone = "Max Quantity already used for this zone";

////////////////////////////////////////////////////////////
// signup, contact us
////////////////////////////////////////////////////////////

$the_username = "The Username";
$already_exists = " is already used";

$empty_username = "Please specify the Username";
$username_too_long = "Username too long: " . $SETTING["username_length"] . " characters max";
$username_too_short = "Username too short: " . $SETTING["username_length_min"] . " characters min";
$not_valid_username = "Username not valid: please use only alphanumeric characters";

$passwords_do_not_match = "Passwords do not match";
$empty_password = "Please specify the Password";
$please_enable_javascript =
"<b>
For security reasons, to purchase products in our virtual store,
the use of JavaScript is required.
<p>
The JavaScript language, apart from providing functionalities that are
very useful for Web software development, also allows the encryption of passwords,
thus it offers much more security to this store's users.
<p>
Hence, to buy products in our virtual store, a browser supporting JavaScript
is required, as Mozilla, Netscape, or Explorer, for example.
If you are using a browser supporting JavaScript but you have disabled
such support, please edit your preferences in order to enable JavaScript.
<p>
Note: JavaScript is enabled by default in Mozilla, Netscape, and Explorer.
</b><br><br>";

$empty_firstname = "Please specify the First Name";
$firstname_too_long = "First Name too long: " . $SETTING["firstname_length"] . " characters max";

$empty_ragionesociale = "Please specify the Company Name";
$ragionesociale_too_long = "Company Name too long: " . $SETTING["firstname_length"] . " characters max";

$empty_lastname = "Please specify your Last Name";
$lastname_too_long = "Last Name too long: " . $SETTING["lastname_length"] . " characters max";

$empty_name = "Please specify the Name";
$name_too_long = "Name too long: " . $SETTING["name_length"] . " characters max";

$empty_fiscalcode = "Please specify the Fiscal Code";
$bad_fiscalcode_length = "Bad Fiscal Code length: the Fiscal Code must consist of " . $SETTING["fiscalcode_length"] . " alphanumeric characters";
$not_valid_fiscalcode = "Fiscal Code is not valid";

$empty_partitaiva = "Please specify the Company identification number";
$bad_partitaiva_length = "Bad Company identification number length: the Company identification number must consist of " . $SETTING["partitaiva_length"] . " digits";
$not_valid_partitaiva = "Company identification number not valid";

$empty_address = "Please specify the address";
$address_too_long = "Address too long: " . $SETTING["address_length"] . " characters max";

$number_too_long = "Number too long: " . $SETTING["number_length"] . " characters max";

$empty_zip_code = "Please specify the Zip Code";
$zip_code_is_zero = "The Zip Code must be greater than zero";
$zip_code_notint = "The Zip Code entry must contain an integer number: only digits, not any other character, spaces are not allowed";
$zip_code_notvalid = "Zip Code not valid";
$zip_code_too_long = "Zip Code too long: " . $SETTING["zip_code_length_max"] . " characters max";
$zip_code_too_short = "Zip Code too short: " . $SETTING["zip_code_length_min"] . " characters min";
$zip_code_length_is_wrong = "The Zip Code must consist of exactly " . $SETTING["zip_code_length"] . " digits";
// $zip_code_length_is_wrong = "The Zip Code must consist of a number of digits equal to ";

$empty_state = "Please select a state";
$empty_city = "Please specify the city";
$city_too_long = "City too long: " . $SETTING["city_length"] . " characters max";

$empty_country = "Please specify the country";
$country_too_long = "Country too long: " . $SETTING["country_length"] . " characters max";

$empty_phone = "Please specify the phone number";
$phone_too_long = "Phone number too long: " . $SETTING["phone_length"] . " characters max";
$phone_too_short = "Phone number too short: " . $SETTING["phone_length_min"] . " characters min";
$not_valid_phone = "Phone number not valid; only cyphers, spaces, +-()/ are allowed characters";

$fax_too_long = "Fax number too long: " . $SETTING["fax_length"] . " characters max";
$fax_too_short = "Fax number too short: " . $SETTING["fax_length_min"] . " characters min";
$not_valid_fax = "Fax number not valid; only cyphers, spaces, +-()/ are allowed characters";

$mobilephone_too_long = "Mobile Phone number too long: " . $SETTING["mobilephone_length"] . " characters max";
$mobilephone_too_short = "Mobile Phone number too short: " . $SETTING["mobilephone_length_min"] . " characters min";
$not_valid_mobilephone = "Mobile Phone number not valid; only cyphers, spaces, +-()/ are allowed characters";

$empty_email = "Please specify the E-mail address";
$email_too_long = "E-mail address too long: " . $SETTING["email_length"] . " characters max";
$not_valid_email = "E-mail address not valid";
//$the_email_address = "The E-mail address";
//$email_not_on_file = "The specified email address is not on file";

$notes_too_long = "Notes too long: " . $SETTING["notes_length"] . " characters max";
$ournotes_too_long = "Notes too long in the field \"" . $Our_Notes . "\": " . $SETTING["ournotes_length"] . " characters max";
$empty_message = "You did not write a message";
$message_too_long = "Message too long: " . $SETTING["message_length"] . " characters max";

$priv_too_long = "Privilege too long: " . $SETTING["priv_length"] . " characters max";
$not_valid_priv = "Privilege is not valid: please use only alphanumeric characters";

////////////////////////////////////////////////////////////
// purchase now
////////////////////////////////////////////////////////////

$empty_customer = "You did not specify the Customer";
$appoggio = $SETTING["firstname_length"] + $SETTING["lastname_length"] + 1;
$customer_too_long = "Customer too long: " . $appoggio . " characters max";
$empty_contact = "You did not specify a Phone to contact you";
$contact_too_long = "Contact Phone number too long: " . $SETTING["phone_length"] . " characters max";
$contact_too_short = "Contact Phone number too short: " . $SETTING["phone_length_min"] . " characters min";
$not_valid_contact = "Contact Phone number not valid; only cyphers, spaces, +-()/ are allowed characters";
$comments_too_long = "Comments too long: " . $SETTING["comments_length"] . " characters max";

////////////////////////////////////////////////////////////
// change password
////////////////////////////////////////////////////////////

$no_old_password = "You did not specify your old password";
$old_password_not_valid = "Your old password is not valid";
$no_new_password = "You did not specify your new password";
$no_new_password2 = "You did not confirm your new password";
$new_passwords_dont_match = "Your new passwords do not match";

////////////////////////////////////////////////////////////
// languages
////////////////////////////////////////////////////////////

$the_lang = "The language ";
$empty_lang = "The language is empty; please fill it in";
$lang_too_long = "The language identificator is too long: " . $SETTING["lang_length"] . " characters max";
$not_valid_lang = "The language identificator contains not valid characters: only alphanumeric characters, cyphers, @._ are allowed characters";

////////////////////////////////////////////////////////////
// add and update category
////////////////////////////////////////////////////////////

$empty_category_name = "The category name is empty; please fill it in";
$category_name_too_long = "Category Name too long: " . $SETTING["category_name_length"] . " characters max";
$category_description_too_long = "Category Description too long: " . $SETTING["category_description_length"] . " characters max";
$category_cannot_be_parent_of_itself = "The Category can not be parent of itself";

$Err_ImpCat = ": the Parent Category doesn't exist";
$Err_ImpCat_i18n = ": the Category doesn't exist";

////////////////////////////////////////////////////////////
// add and update brand
////////////////////////////////////////////////////////////

$empty_brand_name = "The Brand Name is empty; please fill it";
$brand_name_too_long = "Brand Name too long: " . $SETTING["brand_name_length"] . " characters max";
$brand_description_too_long = "Brand Description too long: " . $SETTING["brand_description_length"] . " characters max";

$Err_ImpBrand = ": the Brand doesn't exist";

////////////////////////////////////////////////////////////
// add and update iva
////////////////////////////////////////////////////////////

$iva_description_too_long = $IVA . " Description too long: " . $SETTING["iva_description_length"] . " characters max";

$Err_ImpIVA = ": the " . $IVA . " doesn't exist";

////////////////////////////////////////////////////////////
// add and update color
////////////////////////////////////////////////////////////

$empty_color_name = "The color name is empty; please fill it";
$color_name_too_long = "Color Name too long: " . $SETTING["color_name_length"] . " characters max";
$color_description_too_long = "Color Description too long: " . $SETTING["color_description_length"] . " characters max";

$Err_ImpColor_i18n = ": Color not existing";

////////////////////////////////////////////////////////////
// add and update product
////////////////////////////////////////////////////////////

$product_code_is_empty = "The product Code is empty; please fill it";
$product_code_wspace = "The Code <b>" . ov($HTTP_POST_VARS["product_code"]) . "</b> contains spaces. Please don't use spaces in product Codes";
$product_code_wampersand = "The Code <b>" . ov($HTTP_POST_VARS["product_code"]) . "</b> contains the character \"&amp;\". Please don't use this character in product Codes";
$product_code_wquestionm = "The Code <b>" . ov($HTTP_POST_VARS["product_code"]) . "</b> contains the character \"?\". Please don't use this character in product Codes";
$product_code_too_long = "Product Code too long: " . $SETTING["product_code_length"] . " characters max";
$the_product_code = "The product Code ";

$empty_product_name = "The product Name is empty; please fill it";
$product_name_too_long = "Product Name too long: " . $SETTING["product_name_length"] . " characters max";

$empty_price = "Please specify the Price";
$price_is_zero = "The Price must be greater than zero";
$price_notint = "The Price entry must contain an integer number: only digits, not any other character";
$price_notfloat = "The Price entry must contain a float number: only digits and optionally a dot, not any other character";

$empty_cost = "Please specify the Amount";
$cost_is_lower_than_zero = "The Amount can not be lower than zero";
$cost_notint = "The Amount entry must contain an integer number: only digits, not any other character";
$cost_notfloat = "The Amount entry must contain a float number: only digits and optionally a dot, not any other character";

$not_valid_iva = $IVA . " not valid";
$iva_not_between_0_and_100 = "The " . $IVA . " is not between 0 and 100";

$discount_not_completely_defined = "If you want to discount the product, you have to fill both the fields \"" . $Discount . "\" and \"" . $Starting_from . "\"; if you do not want to discount the product, please leave empty both the fields";
$not_valid_discount = "Discount not valid";
$discount_not_between_0_and_100 = "The Discount is not between 0 and 100";
$discqty_notint = "The \"" . $Starting_from . "\" entry is not an integer number: only digits, not any other character";
$discqty_is_negative = "The \"" . $Starting_from . "\" entry can not be a negative number";
$discqty_is_too_large = "The \"" . $Starting_from . "\" entry is too large: " . $SETTING["discqtymax"] . " max";

$empty_weight = "Please specify the Weight";
$not_valid_weight = "Weight not valid";
$weight_is_zero = "The Weight must be greater than zero";
$weight_over_max = "Weight too large: " . $SESSION["weightmax"] . " " . $weightunity . " max";

$special_level_empty = "The \"" . $Special_Level . "\" entry is empty; please fill it";
$special_level_notint = "The \"" . $Special_Level . "\" entry must contain an integer number: only digits, not any other character";

$product_description_too_long = "Product Description too long: " . $SETTING["product_description_length"] . " characters max";
$product_extended_description_too_long = "Product Extended Description too long: " . $SETTING["product_extended_description_length"] . " characters max";

$appoggio = floor ($SETTING["max_file_size_images"] / 1024);
$no_product_image_chosen = "No new image has been chosen to represent the product, or the submitted image is too large: " . $appoggio . " kbytes max";
$image_upload_failed = "Image upload failed";
$unsupported_image_type = "Unsupported image type";
$no_products_with_such_code = "no products with such code";
$no_products_with_such_id = "no products with such ID";

$Err_ImpProd_i18n = ": the Product doesn't exist";

////////////////////////////////////////////////////////////
// add and update order state
////////////////////////////////////////////////////////////

$empty_order_state_name = "The Order State Name is empty; please fill it";
$order_state_name_too_long = "Order State Name too long: " . $SETTING["order_state_name_length"] . " characters max";
$order_state_description_too_long = "Order State Description too long: " . $SETTING["order_state_description_length"] . " characters max";

////////////////////////////////////////////////////////////
// sync data warning...
////////////////////////////////////////////////////////////

$current_products_data_will_be_lost = "<b><u>Warning:</u></b> <b>ALL</b> current products data will be lost; if you want to save them, use the Data Export feature";

////////////////////////////////////////////////////////////
// miscellaneous...
////////////////////////////////////////////////////////////

$user_not_found = "user not found";

?>

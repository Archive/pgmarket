<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

$Errors = "Errori";
$file_line = "riga";

////////////////////////////////////////////////////////////
// Top level scripts
////////////////////////////////////////////////////////////

$login_failed = "Login fallito, riprova";

////////////////////////////////////////////////////////////
// user discounts
////////////////////////////////////////////////////////////

$not_valid_user_discount = "Valore dello Sconto non valido";
$user_discount_not_between_0_and_100 = "Lo Sconto non &egrave; compreso tra 0 e 100";
$empty_user_discount_name = "Il campo del nome &egrave; stato lasciato vuoto, si prega di riempirlo";
$user_discount_name_too_long = "Il nome dello sconto &egrave; troppo lungo: " . $CFG["user_discount_name_length"] . " caratteri max";
$user_discount_description_too_long = "La descrizione dello sconto &egrave; troppo lunga: " . $CFG["user_discount_description_length"] . " caratteri max";

$Err_ImpUserDiscount = ": lo Sconto utente non esiste";

////////////////////////////////////////////////////////////
// add and update zones
////////////////////////////////////////////////////////////

$empty_zone_code = "Il campo del codice &egrave; stato lasciato vuoto, si prega di riempirlo";
$zone_code_too_long = "Il codice della zona di consegna &egrave; troppo lungo: " . $CFG["zone_code_length"] . " caratteri max";
$empty_zone_name = "Il campo del nome &egrave; stato lasciato vuoto, si prega di riempirlo";
$zone_name_too_long = "Il nome della zona di consegna &egrave; troppo lungo: " . $CFG["zone_name_length"] . " caratteri max";
$zone_description_too_long = "La descrizione della zona di consegna &egrave; troppo lunga: " . $CFG["zone_description_length"] . " caratteri max";

$Err_ImpZone = ": Non ci sono Aree di Consegna con questo Codice";

////////////////////////////////////////////////////////////
// add and update delivery costs
////////////////////////////////////////////////////////////

$maxqty_already_used_for_this_zone = "Quantit&agrave; massima gi&agrave; in uso per questa zona";

////////////////////////////////////////////////////////////
// signup, contact us
////////////////////////////////////////////////////////////

$the_username = "La Username";
$already_exists = " &egrave; gi&agrave; in uso";

$empty_username = "Non &egrave; stata specificata la Username";
$username_too_long = "Username troppo lunga: " . $CFG["username_length"] . " caratteri max";
$username_too_short = "Username troppo corta: " . $CFG["username_length_min"] . " caratteri min";
$not_valid_username = "Username non valida: usare solo caratteri alfanumerici";

$passwords_do_not_match = "Le password fornite non coincidono";
$empty_password = "Non &egrave; stata specificata la Password";

$empty_firstname = "Non &egrave; stato specificato il Nome";
$firstname_too_long = "Nome troppo lungo: " . $CFG["firstname_length"] . " caratteri max";

$empty_ragionesociale = "Non &egrave; stata specificata la Ragione Sociale";
$ragionesociale_too_long = "Ragione Sociale troppo lunga: " . $CFG["firstname_length"] . " caratteri max";

$empty_lastname = "Non &egrave; stato specificato il Cognome";
$lastname_too_long = "Cognome troppo lungo: " . $CFG["lastname_length"] . " caratteri max";

$empty_name = "Non &egrave; stato specificato il Nome";
$name_too_long = "Nome troppo lungo: " . $CFG["name_length"] . " caratteri max";

$empty_fiscalcode = "Non &egrave; stato specificato il Codice Fiscale";
$bad_fiscalcode_length = "Il Codice Fiscale deve contenere " . $CFG["fiscalcode_length"] . " caratteri alfanumerici";
$not_valid_fiscalcode = "Codice Fiscale non valido";

$empty_partitaiva = "Non &egrave; stata specificata la Partita IVA";
$bad_partitaiva_length = "La Partita IVA deve contenere " . $CFG["partitaiva_length"] . " cifre";
$not_valid_partitaiva = "Partita IVA non valida";

$empty_address = "Non &egrave; stato specificato l'indirizzo";
$address_too_long = "Indirizzo troppo lungo: " . $CFG["address_length"] . " caratteri max";

$number_too_long = "Numero troppo lungo: " . $CFG["number_length"] . " caratteri max";

$empty_zip_code = "Riempire il campo del CAP";
$zip_code_is_zero = "Il CAP deve essere maggiore di zero";
$zip_code_notint = "Il campo del CAP deve contenere un numero intero, quindi solo cifre, nessun altro carattere; neanche gli spazi sono ammessi";
$zip_code_notvalid = "CAP non valido";
$zip_code_too_long = "CAP troppo lungo: " . $CFG["zip_code_length_max"] . " caratteri max";
$zip_code_too_short = "CAP troppo corto: " . $CFG["zip_code_length_min"] . " caratteri min";
$zip_code_length_is_wrong = "Il CAP deve contenere esattamente " . $CFG["zip_code_length"] . " cifre";
// $zip_code_length_is_wrong = "Il CAP deve contenere un numero di cifre pari a ";

$empty_city = "Non &egrave; stato riempito il campo Localit&agrave;/Comune";
$city_too_long = "Campo Localit&agrave;/Comune troppo lungo: " . $CFG["city_length"] . " caratteri max";

$empty_country = "Non &egrave; stata specificata la Provincia";
$country_too_long = "Provincia troppo lunga: " . $CFG["country_length"] . " caratteri max";

$empty_phone = "Non &egrave; stato specificato il numero di telefono";
$phone_too_long = "Numero di telefono troppo lungo: " . $CFG["phone_length"] . " caratteri max";
$phone_too_short = "Numero di telefono troppo corto: " . $CFG["phone_length_min"] . " caratteri min";
$not_valid_phone = "Numero di telefono non valido; sono ammessi solo spazi, cifre e i seguenti caratteri: +-()/";

$fax_too_long = "Numero di fax troppo lungo: " . $CFG["fax_length"] . " caratteri max";
$fax_too_short = "Numero di fax troppo corto: " . $CFG["fax_length_min"] . " caratteri min";
$not_valid_fax = "Numero di fax non valido; sono ammessi solo spazi, cifre e i seguenti caratteri: +-()/";

$mobilephone_too_long = "Numero di cellulare troppo lungo: " . $CFG["mobilephone_length"] . " caratteri max";
$mobilephone_too_short = "Numero di cellulare troppo corto: " . $CFG["mobilephone_length_min"] . " caratteri min";
$not_valid_mobilephone = "Numero di cellulare non valido; sono ammessi solo spazi, cifre e i seguenti caratteri: +-()/";

$empty_email = "Non &egrave; stato specificato l'indirizzo e-mail";
$email_too_long = "Indirizzo e-mail troppo lungo: " . $CFG["email_length"] . " caratteri max";
$not_valid_email = "Indirizzo E-mail non valido";
//$the_email_address = "L'indirizzo E-mail";
//$email_not_on_file = "L'indirizzo E-mail specificato non ci risulta noto";

$Our_Notes = "Nostre Annotazioni";
$notes_too_long = "Annotazioni troppo lunghe: " . $CFG["notes_length"] . " caratteri max";
$ournotes_too_long = "Annotazioni troppo lunghe nel campo \"" . $Our_Notes . "\": " . $CFG["ournotes_length"] . " caratteri max";
$empty_message = "Non &egrave; stato scritto nessun Messaggio";
$message_too_long = "Messaggio troppo lungo: " . $CFG["message_length"] . " caratteri max";

$priv_too_long = "Privilegio troppo lungo: " . $CFG["priv_length"] . " caratteri max";
$not_valid_priv = "Privilegio non valido: usare solo caratteri alfanumerici";

////////////////////////////////////////////////////////////
// purchase now
////////////////////////////////////////////////////////////

$empty_customer = "Non &egrave; stato specificato il Cliente";
$foobar = $CFG["firstname_length"] + $CFG["lastname_length"] + 1;
$customer_too_long = "Campo Cliente troppo lungo: " . $foobar . " caratteri max";
$empty_contact = "Non &egrave; stato specificato un recapito telefonico";
$contact_too_long = "Recapito telefonico troppo lungo: " . $CFG["phone_length"] . " caratteri max";
$contact_too_short = "Recapito telefonico troppo corto: " . $CFG["phone_length_min"] . " caratteri min";
$not_valid_contact = "Recapito telefonico non valido; sono ammessi solo spazi, cifre e i seguenti caratteri: +-()/";
$comments_too_long = "Commenti troppo lunghi: " . $CFG["comments_length"] . " caratteri max";

////////////////////////////////////////////////////////////
// change password
////////////////////////////////////////////////////////////

$no_old_password = "Non &egrave; stata specificata la vecchia password";
$old_password_not_valid = "La vecchia password &egrave; sbagliata";
$no_new_password = "Non &egrave; stata specificata la nuova password";
$no_new_password2 = "Non &egrave; stata confermata la nuova password";
$new_passwords_dont_match = "Le nuove password non coincidono";

////////////////////////////////////////////////////////////
// languages
////////////////////////////////////////////////////////////

$the_lang = "La lingua ";
$empty_lang = "Non &egrave; stata scelta una lingua; sceglierne una";
$lang_too_long = "L'identificativo scelto per la lingua &egrave; troppo lungo: " . $CFG["lang_length"] . " caratteri max";
$not_valid_lang = "L'identificativo scelto per la lingua contiene caratteri non validi; sono ammessi solo spazi, cifre e i seguenti caratteri: @._";

////////////////////////////////////////////////////////////
// add and update category
////////////////////////////////////////////////////////////

$empty_category_name = "Il campo del nome &egrave; stato lasciato vuoto, si prega di riempirlo";
$category_name_too_long = "Il nome della categoria &egrave; troppo lungo: " . $CFG["category_name_length"] . " caratteri max";
$category_description_too_long = "La descrizione della categoria &egrave; troppo lunga: " . $CFG["category_description_length"] . " caratteri max";
$category_cannot_be_parent_of_itself = "La categoria non pu&ograve; essere categoria padre di se stessa";

$Err_ImpCat = ": la Categoria Padre non esiste";
$Err_ImpCat_i18n = ": la Categoria non esiste";

////////////////////////////////////////////////////////////
// add and update brand
////////////////////////////////////////////////////////////

$empty_brand_name = "Il campo del nome &egrave; stato lasciato vuoto, si prega di riempirlo";
$brand_name_too_long = "Il nome della marca &egrave; troppo lungo: " . $CFG["brand_name_length"] . " caratteri max";
$brand_description_too_long = "La descrizione della marca &egrave; troppo lunga: " . $CFG["brand_description_length"] . " caratteri max";

$Err_ImpBrand = ": la Marca non esiste";

////////////////////////////////////////////////////////////
// add and update iva
////////////////////////////////////////////////////////////

$iva_description_too_long = "La descrizione della " . $CFG["IVA"] . " &egrave; troppo lunga: " . $CFG["iva_description_length"] . " caratteri max";

$Err_ImpIVA = ": la " . $CFG["IVA"] . " non esiste";

////////////////////////////////////////////////////////////
// add and update color
////////////////////////////////////////////////////////////

$empty_color_name = "Il campo del colore &egrave; stato lasciato vuoto, si prega di riempirlo";
$color_name_too_long = "Il nome del colore &egrave; troppo lungo: " . $CFG["color_name_length"] . " caratteri max";
$color_description_too_long = "La descrizione del colore &egrave; troppo lunga: " . $CFG["color_description_length"] . " caratteri max";

$Err_ImpColor_i18n = ": Colore non esistente";

////////////////////////////////////////////////////////////
// add and update product
////////////////////////////////////////////////////////////

$product_code_is_empty = "Riempire il campo del Codice del prodotto";
$product_code_wspace = "Il Codice <b>" . ov(nvl($_POST["product_code"], "")) . "</b> contiene almeno uno spazio. Si prega di non usare spazi nei Codici dei prodotti.";
$product_code_wampersand = "Il Codice <b>" . ov(nvl($_POST["product_code"], "")) . "</b> contiene il carattere \"&amp;\". Si prega di non usarlo nei Codici dei prodotti.";
$product_code_wquestionm = "Il Codice <b>" . ov(nvl($_POST["product_code"], "")) . "</b> contiene il carattere \"?\". Si prega di non usarlo nei Codici dei prodotti.";
$product_code_too_long = "Il Codice del prodotto &egrave; troppo lungo: " . $CFG["product_code_length"] . " caratteri max";
$the_product_code = "Il Codice ";

$empty_product_name = "Riempire il campo del Nome del prodotto";
$product_name_too_long = "Il nome del prodotto &egrave; troppo lungo: " . $CFG["product_name_length"] . " caratteri max";

$empty_price = "Riempire il campo del prezzo";
$price_is_zero = "Il Prezzo deve essere maggiore di zero";
$price_notint = "Il campo del Prezzo deve contenere un numero intero, quindi solo cifre, nessun altro carattere";
$price_notfloat = "Il campo del Prezzo deve contenere un numero decimale, quindi solo cifre e al pi&ugrave; un punto decimale, nessun altro carattere";

$empty_cost = "Riempire il campo dell'importo";
$cost_is_lower_than_zero = "L'Importo non pu&ograve; essere minore di zero";
$cost_notint = "Il campo dell'Importo deve contenere un numero intero, quindi solo cifre, nessun altro carattere";
$cost_notfloat = "Il campo dell'Importo deve contenere un numero decimale, quindi solo cifre e al pi&ugrave; un punto decimale, nessun altro carattere";

$not_valid_iva = "Valore della " . $CFG["IVA"] . " non valido";
$iva_not_between_0_and_100 = "La " . $CFG["IVA"] . " non &egrave; compresa tra 0 e 100";

$Discount = "Sconto";
$Starting_from = "a partire da";
$Special_Level = "Livello";
$discount_not_completely_defined = "Se si vuole fare sconti sul prodotto, bisogna riempire sia il campo \"" . $Discount . "\" che il campo \"" . $Starting_from . "\"; se non si vuole fare sconti sul prodotto, lasciare vuoti entrambi i campi";
$not_valid_discount = "Sconto non valido";
$discount_not_between_0_and_100 = "Lo Sconto non &egrave; compreso tra 0 e 100";
$discqty_notint = "Il campo \"" . $Starting_from . "\" deve contenere un numero intero, quindi solo cifre, nessun altro carattere";
$discqty_is_negative = "Il campo \"" . $Starting_from . "\" non pu&ograve; essere un numero negativo";
$discqty_is_too_large = "Il campo \"" . $Starting_from . "\" &egrave; troppo grande: " . $CFG["discqtymax"] . " max";

$empty_weight = "Riempire il campo del Peso";
$not_valid_weight = "Peso non valido";
$weight_is_zero = "Il Peso deve essere maggiore di zero";
$weight_over_max = "Peso eccessivo: " . $CFG["max_weight"] . " " . $CFG["weight_unit"] . " max";

$special_level_empty = "Riempire il campo \"" . $Special_Level . "\"";
$special_level_notint = "Il campo \"" . $Special_Level . "\" deve contenere un numero intero, quindi solo cifre, nessun altro carattere";

$product_description_too_long = "La descrizione del prodotto &egrave; troppo lunga: " . $CFG["product_description_length"] . " caratteri max";
$product_extended_description_too_long = "La descrizione estesa del prodotto &egrave; troppo lunga: " . $CFG["product_extended_description_length"] . " caratteri max";

$foobar = floor ($CFG["max_file_size_images"] / 1024);
$no_product_image_chosen = "Non &egrave; stata scelta una nuova immagine per rappresentare il prodotto, oppure l'immagine scelta &egrave; troppo grande: " . $foobar . " kbytes max";
$unsupported_image_type = "Immagine: tipo di immagine non supportato";
$image_upload_failed = "Upload dell'immagine fallito";
$import_file_upload_failed = "&Egrave; fallito l'upload del file da importare";
$unsupported_thumb_type = "Miniatura: tipo di immagine non supportato";
$thumb_upload_failed = "Upload della miniatura fallito";
$gifs_are_not_supported_by_libgd = "Le GIF non sono supportate da Libgd, quindi non &egrave; possibile creare la miniatura dell'immagine fornita";
$could_not_create_thumb = "Non &egrave; stato possibile creare la miniatura dell'immagine";
$no_products_with_such_code = "nessun prodotto con tale codice";
$no_products_with_such_id = "nessun prodotto con tale ID";

$Err_ImpProd_i18n = ": il Prodotto non esiste";

////////////////////////////////////////////////////////////
// add and update order state
////////////////////////////////////////////////////////////

$empty_order_state_name = "Il campo del nome &egrave; stato lasciato vuoto, si prega di riempirlo";
$order_state_name_too_long = "Il nome dello stato dell'ordine &egrave; troppo lungo: " . $CFG["order_state_name_length"] . " caratteri max";
$order_state_description_too_long = "La descrizione dello stato dell'ordine &egrave; troppo lunga: " . $CFG["order_state_description_length"] . " caratteri max";

////////////////////////////////////////////////////////////
// sync data warning...
////////////////////////////////////////////////////////////

$current_products_data_will_be_lost = "<b><u>Avviso:</u></b> <b>TUTTI</b> gli attuali dati relativi ai prodotti verranno persi; se si vuole salvarli, usare l'Esportazione Dati";

////////////////////////////////////////////////////////////
// miscellaneous...
////////////////////////////////////////////////////////////

$user_not_found = "utente non trovato";

?>

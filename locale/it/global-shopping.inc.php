<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Fabio Molinari <fabio.m@mclink.it>

$t->set_var(array(

"Product_not_found" => "Prodotto non trovato",
"Sorry_product_not_found" => "Spiacente, il prodotto cercato non &egrave; stato trovato.  Sei pregato di cercare <a href=\"" . $CFG["wwwroot"] . "shopping/\">nel nostro catalogo</a> per trovare quello che cerchi.",
"Add_to_cart" => "Aggiungi al carrello",
"Add_to_Shopping_Cart" => "+ Aggiungi al carrello della spesa",
"In_the_cart" => "Nel carrello",
"Items_in_the_cart" => "Pezzi nel carrello",
"Categories_Browsing" => "Navigazione delle Categorie",
"Your_shopping_cart_is_empty" => "Il tuo carrello &egrave; vuoto, <a href=\"" . $CFG["wwwroot"] . "shopping/\">consulta il catalogo dei prodotti</a>.",
"Step_1_Enter_Billing_Information" => "Passo 1. Inserisci le informazioni per l'acquisto",
"Step_2_Comments_and_Special_Instructions" => "Passo 2. Commenti / Istruzioni speciali",
"Next_Step" => "Passo Successivo",
"Billing_Details" => "Dettagli dell'acquisto",
"Continue_Shopping" => "Altri acquisti",
"Recalculate_Cart" => "Ricalcola Carrello",
"Empty_the_Cart" => "Svuota il carrello",
"Your_Shopping_Cart_Contains" => "Il tuo carrello contiene",
"Purchase_Now" => "Acquista ora",
"min" => "min &nbsp;",
"max" => "&nbsp; max",
"Subset_or_Equal" => "",

));

?>

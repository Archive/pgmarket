<?php

$ordersdb["Customer"]	= "Customer      :";
$ordersdb["Phone"]	= "Phone Contact :";
$ordersdb["Address"]	= "Address       :";
$ordersdb["Cash_On_Delivery"] = "Cash on Delivery";
$ordersdb["Order"] = "Order";
$ordersdb["Username"] = "Username";
$ordersdb["Date"] = "Date";
$ordersdb["Comments"] = "Comments";
$ordersdb["TOTAL"] = "TOTAL";
$ordersdb["Product"] = "Product";
$ordersdb["Unit_Price"] = "Unit price";
$ordersdb["Number_of_items"] = "Number of items";
$ordersdb["Delivery"] = "Delivery";

$contact["NameOrCompanyName"]	= "Name (Company Name): ";
$contact["Address"] = "Address:";
$contact["Phone"]		= "Phone:        ";
$contact["Fax"]			= "Fax:          ";
$contact["MobilePhone"]		= "Mobile Phone: ";
$contact["EMail"]		= "E-Mail:       ";
$contact["AuthData"]		= "Authorization for data handling: ";
$contact["AuthAdvert"]		= "Acceptance of advertising material: ";
$contact["Message"]		= "Message:";

$adminday[1] = "Monday";
$adminday[2] = "Tuesday";
$adminday[3] = "Wednesday";
$adminday[4] = "Thursday";
$adminday[5] = "Friday";
$adminday[6] = "Saturday";
$adminday[0] = "Sunday";

$adminmonth[0] = "0";
$adminmonth[1] = "January";
$adminmonth[2] = "February";
$adminmonth[3] = "March";
$adminmonth[4] = "April";
$adminmonth[5] = "May";
$adminmonth[6] = "June";
$adminmonth[7] = "July";
$adminmonth[8] = "August";
$adminmonth[9] = "September";
$adminmonth[10] = "October";
$adminmonth[11] = "November";
$adminmonth[12] = "December";

?>

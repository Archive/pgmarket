<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000-2001 Fabio Molinari <fabio.m@mclink.it>

$Errors = "Errors";
$file_line = "line";

////////////////////////////////////////////////////////////
// Top level scripts
////////////////////////////////////////////////////////////

$login_failed = "Login failed, please try again";

////////////////////////////////////////////////////////////
// user discounts
////////////////////////////////////////////////////////////

$not_valid_user_discount = "Discount not valid";
$user_discount_not_between_0_and_100 = "The Discount is not between 0 and 100";
$empty_user_discount_name = "The Discount Name is empty; please fill it";
$user_discount_name_too_long = "Discount Name too long: " . $CFG["user_discount_name_length"] . " characters max";
$user_discount_description_too_long = "Discount Description too long: " . $CFG["user_discount_description_length"] . " characters max";

$Err_ImpUserDiscount = ": the User Discount doesn't exist";

////////////////////////////////////////////////////////////
// add and update zones
////////////////////////////////////////////////////////////

$empty_zone_code = "The delivery Zone Code is empty; please fill it";
$zone_code_too_long = "Delivery Zone Code too long: " . $CFG["zone_code_length"] . " characters max";
$empty_zone_name = "The delivery Zone Name is empty; please fill it";
$zone_name_too_long = "Delivery Zone Name too long: " . $CFG["zone_name_length"] . " characters max";
$zone_description_too_long = "Delivery Zone Description too long: " . $CFG["zone_description_length"] . " characters max";

$Err_ImpZone = ": There's no Delivery Zone with this Code";

////////////////////////////////////////////////////////////
// add and update delivery costs
////////////////////////////////////////////////////////////

$maxqty_already_used_for_this_zone = "Max Quantity already used for this zone";

////////////////////////////////////////////////////////////
// signup, contact us
////////////////////////////////////////////////////////////

$the_username = "The Username";
$already_exists = " is already used";

$empty_username = "Please specify the Username";
$username_too_long = "Username too long: " . $CFG["username_length"] . " characters max";
$username_too_short = "Username too short: " . $CFG["username_length_min"] . " characters min";
$not_valid_username = "Username not valid: please use only alphanumeric characters";

$passwords_do_not_match = "Passwords do not match";
$empty_password = "Please specify the Password";

$empty_firstname = "Please specify the First Name";
$firstname_too_long = "First Name too long: " . $CFG["firstname_length"] . " characters max";

$empty_ragionesociale = "Please specify the Company Name";
$ragionesociale_too_long = "Company Name too long: " . $CFG["firstname_length"] . " characters max";

$empty_lastname = "Please specify your Last Name";
$lastname_too_long = "Last Name too long: " . $CFG["lastname_length"] . " characters max";

$empty_name = "Please specify the Name";
$name_too_long = "Name too long: " . $CFG["name_length"] . " characters max";

$empty_fiscalcode = "Please specify the Fiscal Code";
$bad_fiscalcode_length = "Bad Fiscal Code length: the Fiscal Code must consist of " . $CFG["fiscalcode_length"] . " alphanumeric characters";
$not_valid_fiscalcode = "Fiscal Code is not valid";

$empty_partitaiva = "Please specify the Company identification number";
$bad_partitaiva_length = "Bad Company identification number length: the Company identification number must consist of " . $CFG["partitaiva_length"] . " digits";
$not_valid_partitaiva = "Company identification number not valid";

$empty_address = "Please specify the address";
$address_too_long = "Address too long: " . $CFG["address_length"] . " characters max";

$number_too_long = "Number too long: " . $CFG["number_length"] . " characters max";

$empty_zip_code = "Please specify the Zip Code";
$zip_code_is_zero = "The Zip Code must be greater than zero";
$zip_code_notint = "The Zip Code entry must contain an integer number: only digits, not any other character, spaces are not allowed";
$zip_code_notvalid = "Zip Code not valid";
$zip_code_too_long = "Zip Code too long: " . $CFG["zip_code_length_max"] . " characters max";
$zip_code_too_short = "Zip Code too short: " . $CFG["zip_code_length_min"] . " characters min";
$zip_code_length_is_wrong = "The Zip Code must consist of exactly " . $CFG["zip_code_length"] . " digits";
// $zip_code_length_is_wrong = "The Zip Code must consist of a number of digits equal to ";

$empty_city = "Please specify the city";
$city_too_long = "City too long: " . $CFG["city_length"] . " characters max";

$empty_country = "Please specify the country";
$country_too_long = "Country too long: " . $CFG["country_length"] . " characters max";

$empty_phone = "Please specify the phone number";
$phone_too_long = "Phone number too long: " . $CFG["phone_length"] . " characters max";
$phone_too_short = "Phone number too short: " . $CFG["phone_length_min"] . " characters min";
$not_valid_phone = "Phone number not valid; only cyphers, spaces, +-()/ are allowed characters";

$fax_too_long = "Fax number too long: " . $CFG["fax_length"] . " characters max";
$fax_too_short = "Fax number too short: " . $CFG["fax_length_min"] . " characters min";
$not_valid_fax = "Fax number not valid; only cyphers, spaces, +-()/ are allowed characters";

$mobilephone_too_long = "Mobile Phone number too long: " . $CFG["mobilephone_length"] . " characters max";
$mobilephone_too_short = "Mobile Phone number too short: " . $CFG["mobilephone_length_min"] . " characters min";
$not_valid_mobilephone = "Mobile Phone number not valid; only cyphers, spaces, +-()/ are allowed characters";

$empty_email = "Please specify the E-mail address";
$email_too_long = "E-mail address too long: " . $CFG["email_length"] . " characters max";
$not_valid_email = "E-mail address not valid";
//$the_email_address = "The E-mail address";
//$email_not_on_file = "The specified email address is not on file";

$Our_Notes = "Our Notes";
$notes_too_long = "Notes too long: " . $CFG["notes_length"] . " characters max";
$ournotes_too_long = "Notes too long in the field \"" . $Our_Notes . "\": " . $CFG["ournotes_length"] . " characters max";
$empty_message = "You did not write a message";
$message_too_long = "Message too long: " . $CFG["message_length"] . " characters max";

$priv_too_long = "Privilege too long: " . $CFG["priv_length"] . " characters max";
$not_valid_priv = "Privilege is not valid: please use only alphanumeric characters";

////////////////////////////////////////////////////////////
// purchase now
////////////////////////////////////////////////////////////

$empty_customer = "You did not specify the Customer";
$foobar = $CFG["firstname_length"] + $CFG["lastname_length"] + 1;
$customer_too_long = "Customer too long: " . $foobar . " characters max";
$empty_contact = "You did not specify a Phone to contact you";
$contact_too_long = "Contact Phone number too long: " . $CFG["phone_length"] . " characters max";
$contact_too_short = "Contact Phone number too short: " . $CFG["phone_length_min"] . " characters min";
$not_valid_contact = "Contact Phone number not valid; only cyphers, spaces, +-()/ are allowed characters";
$comments_too_long = "Comments too long: " . $CFG["comments_length"] . " characters max";

////////////////////////////////////////////////////////////
// change password
////////////////////////////////////////////////////////////

$no_old_password = "You did not specify your old password";
$old_password_not_valid = "Your old password is not valid";
$no_new_password = "You did not specify your new password";
$no_new_password2 = "You did not confirm your new password";
$new_passwords_dont_match = "Your new passwords do not match";

////////////////////////////////////////////////////////////
// languages
////////////////////////////////////////////////////////////

$the_lang = "The language ";
$empty_lang = "The language is empty; please fill it in";
$lang_too_long = "The language identificator is too long: " . $CFG["lang_length"] . " characters max";
$not_valid_lang = "The language identificator contains not valid characters: only alphanumeric characters, cyphers, @._ are allowed characters";

////////////////////////////////////////////////////////////
// add and update category
////////////////////////////////////////////////////////////

$empty_category_name = "The category name is empty; please fill it in";
$category_name_too_long = "Category Name too long: " . $CFG["category_name_length"] . " characters max";
$category_description_too_long = "Category Description too long: " . $CFG["category_description_length"] . " characters max";
$category_cannot_be_parent_of_itself = "The Category can not be parent of itself";

$Err_ImpCat = ": the Parent Category doesn't exist";
$Err_ImpCat_i18n = ": the Category doesn't exist";

////////////////////////////////////////////////////////////
// add and update brand
////////////////////////////////////////////////////////////

$empty_brand_name = "The Brand Name is empty; please fill it";
$brand_name_too_long = "Brand Name too long: " . $CFG["brand_name_length"] . " characters max";
$brand_description_too_long = "Brand Description too long: " . $CFG["brand_description_length"] . " characters max";

$Err_ImpBrand = ": the Brand doesn't exist";

////////////////////////////////////////////////////////////
// add and update iva
////////////////////////////////////////////////////////////

$iva_description_too_long = $CFG["IVA"] . " Description too long: " . $CFG["iva_description_length"] . " characters max";

$Err_ImpIVA = ": the " . $CFG["IVA"] . " doesn't exist";

////////////////////////////////////////////////////////////
// add and update color
////////////////////////////////////////////////////////////

$empty_color_name = "The color name is empty; please fill it";
$color_name_too_long = "Color Name too long: " . $CFG["color_name_length"] . " characters max";
$color_description_too_long = "Color Description too long: " . $CFG["color_description_length"] . " characters max";

$Err_ImpColor_i18n = ": Color not existing";

////////////////////////////////////////////////////////////
// add and update product
////////////////////////////////////////////////////////////

$product_code_is_empty = "The product Code is empty; please fill it";
$product_code_wspace = "The Code <b>" . ov(nvl($_POST["product_code"]), "") . "</b> contains spaces. Please don't use spaces in product Codes";
$product_code_wampersand = "The Code <b>" . ov(nvl($_POST["product_code"]), "") . "</b> contains the character \"&amp;\". Please don't use this character in product Codes";
$product_code_wquestionm = "The Code <b>" . ov(nvl($_POST["product_code"]), "") . "</b> contains the character \"?\". Please don't use this character in product Codes";
$product_code_too_long = "Product Code too long: " . $CFG["product_code_length"] . " characters max";
$the_product_code = "The product Code ";

$empty_product_name = "The product Name is empty; please fill it";
$product_name_too_long = "Product Name too long: " . $CFG["product_name_length"] . " characters max";

$empty_price = "Please specify the Price";
$price_is_zero = "The Price must be greater than zero";
$price_notint = "The Price entry must contain an integer number: only digits, not any other character";
$price_notfloat = "The Price entry must contain a float number: only digits and optionally a dot, not any other character";

$empty_cost = "Please specify the Amount";
$cost_is_lower_than_zero = "The Amount can not be lower than zero";
$cost_notint = "The Amount entry must contain an integer number: only digits, not any other character";
$cost_notfloat = "The Amount entry must contain a float number: only digits and optionally a dot, not any other character";

$not_valid_iva = $CFG["IVA"] . " not valid";
$iva_not_between_0_and_100 = "The " . $CFG["IVA"] . " is not between 0 and 100";

$Discount = "Discount";
$Starting_from = "starting from";
$Special_Level = "Special Level";
$discount_not_completely_defined = "If you want to discount the product, you have to fill both the fields \"" . $Discount . "\" and \"" . $Starting_from . "\"; if you do not want to discount the product, please leave empty both the fields";
$not_valid_discount = "Discount not valid";
$discount_not_between_0_and_100 = "The Discount is not between 0 and 100";
$discqty_notint = "The \"" . $Starting_from . "\" entry is not an integer number: only digits, not any other character";
$discqty_is_negative = "The \"" . $Starting_from . "\" entry can not be a negative number";
$discqty_is_too_large = "The \"" . $Starting_from . "\" entry is too large: " . $CFG["discqtymax"] . " max";

$empty_weight = "Please specify the Weight";
$not_valid_weight = "Weight not valid";
$weight_is_zero = "The Weight must be greater than zero";
$weight_over_max = "Weight too large: " . $CFG["max_weight"] . " " . $CFG["weight_unit"] . " max";

$special_level_empty = "The \"" . $Special_Level . "\" entry is empty; please fill it";
$special_level_notint = "The \"" . $Special_Level . "\" entry must contain an integer number: only digits, not any other character";

$product_description_too_long = "Product Description too long: " . $CFG["product_description_length"] . " characters max";
$product_extended_description_too_long = "Product Extended Description too long: " . $CFG["product_extended_description_length"] . " characters max";

$foobar = floor ($CFG["max_file_size_images"] / 1024);
$no_product_image_chosen = "No new image has been chosen to represent the product, or the submitted image is too large: " . $foobar . " kbytes max";
$unsupported_image_type = "Image: unsupported image type";
$image_upload_failed = "Image upload failed";
$import_file_upload_failed = "Failed upload of file to be imported";
$unsupported_thumb_type = "Thumb: unsupported image type";
$thumb_upload_failed = "Thumb upload failed";
$gifs_are_not_supported_by_libgd = "GIFs are not supported by Libgd, hence the thumb can not be created";
$could_not_create_thumb = "It has not been possible to create the thumb";
$no_products_with_such_code = "no products with such code";
$no_products_with_such_id = "no products with such ID";

$Err_ImpProd_i18n = ": the Product doesn't exist";

////////////////////////////////////////////////////////////
// add and update order state
////////////////////////////////////////////////////////////

$empty_order_state_name = "The Order State Name is empty; please fill it";
$order_state_name_too_long = "Order State Name too long: " . $CFG["order_state_name_length"] . " characters max";
$order_state_description_too_long = "Order State Description too long: " . $CFG["order_state_description_length"] . " characters max";

////////////////////////////////////////////////////////////
// sync data warning...
////////////////////////////////////////////////////////////

$current_products_data_will_be_lost = "<b><u>Warning:</u></b> <b>ALL</b> current products data will be lost; if you want to save them, use the Data Export feature";

////////////////////////////////////////////////////////////
// miscellaneous...
////////////////////////////////////////////////////////////

$user_not_found = "user not found";

$empty_state = "You need to select a state.";

?>

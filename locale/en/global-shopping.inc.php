<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Fabio Molinari <fabio.m@mclink.it>

$t->set_var(array(

"Product_not_found" => "Product not found",
"Sorry_product_not_found" => "Sorry, the product you are looking for was not found on file.  Please try our <a href=\"" . $CFG["wwwroot"] . "shopping/\">shopping catalog</a> to find what you are looking for.",
"Add_to_cart" => "Add to basket",
"Add_to_Shopping_Cart" => "+ Add to Shopping Basket",
"In_the_cart" => "In the basket",
"Items_in_the_cart" => "Items in the basket",
"Categories_Browsing" => "Categories Browsing",
"Your_shopping_cart_is_empty" => "Your shopping basket is empty, <a href=\"" . $CFG["wwwroot"] . "shopping/\">see the products catalog</a>.",
"Step_1_Enter_Billing_Information" => "Step 1. Enter Billing Information",
"Step_2_Comments_and_Special_Instructions" => "Step 2. Comments / Special Instructions",
"Next_Step" => "Next Step",
"Billing_Details" => "Billing Details",
"Continue_Shopping" => "Continue Shopping",
"Recalculate_Cart" => "Recalculate Basket",
"Empty_the_Cart" => "Empty the Basket",
"Empty_the_Wishlist" => "Empty the Wishlist",
"Your_Shopping_Cart_Contains" => "Your Shopping Basket Contains",
"The_Wishlist_Contains" => "The Wishlist Contains",
"Purchase_Now" => "Purchase Now",
"min" => "min &nbsp;",
"max" => "&nbsp; max",
"Subset_or_Equal" => "",

));

?>

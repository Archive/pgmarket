<?
include ("config.inc.php");
include ("common.inc.php");

$doc_titles = array (
	'competitions' => 'Competitions',
	'howtoorder' => 'How_To_Order',
	'howtoshop' => 'How_To_Shop',
	'service' => 'Service_Center',
	'scrap-club' => 'Scrap_Club',
	'layout-gallery' => 'Layout_Gallery',
	'workshop' => 'Workshops',
	'legal' => 'Legal',
    'postage' => 'Shipping_and_Postage',
	'security' => 'Security',

	);
$infopage = nvl($_GET['infopage'], '');
if (!isset($doc_titles[$infopage])) {
   redirect($CFG['firstpage']);
}
$DOC_TITLE = $doc_titles["$infopage"];
include ($CFG["dirroot"] . "header.php");

$t = new Template;
$t->set_file('page',  "info/templates/$infopage.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->set_var(array(
		"DOC_TITLE"			=> ov($DOC_TITLE),
		"wwwroot"			=> $CFG['wwwroot'],
		"firstpage"			=> $CFG['firstpage']
		));

$t->pparse("out", "page");
include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);
?>

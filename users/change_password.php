<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$frm = $_POST;
	$errormsg = validate_change_password_form($frm, $errors);
	if ($errormsg == "") {
		update_user_password($frm["newresponse"]);
		$noticemsg = "Password_Change_Successful";
	}
}

$DOC_TITLE = "Change_Password";

include ($CFG["dirroot"] . "header.php");
include ($CFG["dirroot"] . "form_header.php");

$t = new Template();
$t->set_file("page", "templates/change_password_form.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
$t->set_var("ME", $ME);
$t->set_var(array(
	"libwww"		=> $CFG["libwww"],
	"errors_oldpassword"	=> errmsg(nvl($errors["oldpassword"], 0)),
	"errors_newpassword"	=> errmsg(nvl($errors["newpassword"], 0)),
	"errors_newpassword2"	=> errmsg(nvl($errors["newpassword2"], 0))
));
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function validate_change_password_form(&$frm, &$errors) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["globalerror"]);

	$errors = array();
	$msg = "";

	if ($frm["oldresponse"] == "" || $frm["newresponse"] == "" || $frm["newresponse2"] == "") {	// the browser is not using JavaScript
		$frm["oldresponse"] = md5($frm["oldpassword"]);
		$frm["newresponse"] = md5($frm["newpassword"]);
		$frm["newresponse2"] = md5($frm["newpassword2"]);
	}
	// md5("") = d41d8cd98f00b204e9800998ecf8427e
	if ($frm["oldresponse"] == "d41d8cd98f00b204e9800998ecf8427e") {
		$errors["oldpassword"] = true;
		$msg .= $no_old_password;
	} else if (!password_valid($frm["oldresponse"])) {
		$errors["oldpassword"] = true;
		$msg .= $old_password_not_valid;
	} else if ($frm["newresponse"] == "d41d8cd98f00b204e9800998ecf8427e") {
		$errors["newpassword"] = true;
		$msg .= $no_new_password;
	} else if ($frm["newresponse2"] == "d41d8cd98f00b204e9800998ecf8427e") {
		$errors["newpassword2"] = true;
		$msg .= $no_new_password2;
	} else if ($frm["newresponse"] != $frm["newresponse2"]) {
		$errors["newpassword"] = true;
		$errors["newpassword2"] = true;
		$msg .= $new_passwords_dont_match;
	}

	return $msg;
}

/**
* It checks if the user has submitted the right value of the old password
* @param string $password
* @return boolean
*/
function password_valid($password) {
	global $PGM_SESSION;

	$username = $PGM_SESSION["user"]["username"];
	$qid = new PGM_Sql("
		SELECT 1
		FROM users
		WHERE username = '$username' AND password = '$password'
	");
	return ($qid->num_rows() >= 1);
}

/**
* Changes the user's password
* @param string $newpassword the new password
* @return void
*/
function update_user_password($newpassword) {
	global $PGM_SESSION;
	global $CFG;

	$username = $PGM_SESSION["user"]["username"];
	$qid = new PGM_Sql("
		UPDATE users
		SET password = '$newpassword'
		WHERE username = '$username'
	");

	if (has_priv("admin")) {
		mail(
			$CFG["seller_fullname"] . " <" . $CFG["seller_email"] . ">",
			"PgMarket root password changed",
			"PgMarket root password changed",
			"From: " . $CFG["support"]
		);
	}
}

?>

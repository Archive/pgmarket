<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");

$usertype = nvl($_GET["usertype"], "pf");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
/* form has been submitted, try to create the new user account */
	$frm = $_POST;
	$frm["usertype"] = $usertype;
	$errors = array();
	$errormsg = "";
	validate_signup_form($frm, $errors, $errormsg);
	if ($errormsg == "") {
		user_insert_user($frm);
		$DOC_TITLE = "Signup_Successful";
		include ($CFG["dirroot"] . "header.php");
		$t = new Template();
		$t->set_file("page", "templates/" . $PGM_SESSION["lang"] . "/signup_success.ihtml");
		include ($CFG["localelangdir"] . "global-common.inc.php");
		$t->set_var(array(
			"frm_firstname"	=> ov($frm["firstname"]),
			"frm_username"	=> ov($frm["username"]),
//			"frm_password"	=> ov($frm["password"]),
			"wwwroot"	=> $CFG["wwwroot"]
		));
		$t->pparse("out", "page");
		include ($CFG["dirroot"] . "footer.php");
		pgm_session_close($PGM_SESSION, $session_name);
		die;
	}
}

$DOC_TITLE = "Signup";
include ($CFG["dirroot"] . "header.php");
include ($CFG["dirroot"] . "form_header.php");

if (empty($errormsg)) {	// we are loading the form for the first time
	$frm["acceptadvert"] = 1;
}

$t = new Template();
$t->set_file("page", "templates/signup_form-" . $usertype . ".ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
$foobar = $ME . "?usertype=" . $usertype;
if ($usertype == "pf" && (nvl($_GET["country"], "") == "it" || nvl($_POST["country"], "") == "it")) {
	$foobar .= "&amp;country=it";
}
$t->set_var("POSTURL", $foobar);
$t->set_var("star", return_star());
$t->set_var(array(
	"libwww"		=> $CFG["libwww"],
	"frm_username"		=> ov(nvl($frm["username"], "")),
	"errors_username"	=> errmsg(nvl($errors["username"], "")),
	"errors_password"	=> errmsg(nvl($errors["password"], "")),
	"frm_firstname"		=> ov(nvl($frm["firstname"], "")),
	"errors_firstname"	=> errmsg(nvl($errors["firstname"], "")),
	"frm_lastname"		=> ov(nvl($frm["lastname"], "")),
	"errors_lastname"	=> errmsg(nvl($errors["lastname"], "")),
	"frm_address"		=> ov(nvl($frm["address"], "")),
	"errors_address"	=> errmsg(nvl($errors["address"], "")),
	"errors_state"		=> errmsg(nvl($errors["state"], "")),
	"frm_zip_code"		=> ov(nvl($frm["zip_code"], "")),
	"errors_zip_code"	=> errmsg(nvl($errors["zip_code"], "")),
	"frm_city"		=> ov(nvl($frm["city"], "")),
	"errors_city"		=> errmsg(nvl($errors["city"], "")),
	"errors_country"	=> errmsg(nvl($errors["country"], "")),
	"frm_phone"		=> ov(nvl($frm["phone"], "")),
	"errors_phone"		=> errmsg(nvl($errors["phone"], "")),
	"frm_fax"		=> ov(nvl($frm["fax"], "")),
	"errors_fax"		=> errmsg(nvl($errors["fax"], "")),
	"frm_mobilephone"	=> ov(nvl($frm["mobilephone"], "")),
	"errors_mobilephone"	=> errmsg(nvl($errors["mobilephone"], "")),
	"frm_email"		=> ov(nvl($frm["email"], "")),
	"errors_email"		=> errmsg(nvl($errors["email"], "")),
	"frm_authdata"		=> nvl($frm["authdata"], 0) ? "checked" : "",
	"frm_acceptadvert"	=> nvl($frm["acceptadvert"], 0) ? "checked" : "",
	"frm_notes"		=> ov(nvl($frm["notes"], "")),
	"errors_notes"		=> errmsg(nvl($errors["notes"], "")),
));
$t->set_var(nvl($frm["state"],'')."_selected", "selected");
$t->set_var(nvl($frm["country"],'au')."_selected", "selected");
$t->set_block("page", "delivery_used", "delivery_used_blck");
$t->set_var("delivery_used_blck", "");
if ($CFG["delivery_used"] && !($CFG["only_one_delivery_zone"])) {
	$t->set_block("delivery_used", "delivery_zone_option", "delivery_zone_options");
	$t->set_var("delivery_zone_options", "");
	$qid = new PGM_Sql();
	get_zones_names($qid);
	while ($qid->next_record()) {
		$t->set_var(array(
			"frm_user_zone_id"		=> $qid->f("zone_id"),
			"frm_user_zone_selected"	=> ($qid->f("zone_id") == nvl($frm["zone_id"], 1)) ? "selected" : "",
			"frm_user_zone_name"		=> ($qid->f("lname") != "") ? ov($qid->f("lname")) : ov($qid->f("name"))
		));
		$t->parse("delivery_zone_options", "delivery_zone_option", true);
	}
	$t->parse("delivery_used_blck", "delivery_used", true);
}
$t->set_block("page", "fiscalcode", "fiscalcode_blck");
$t->set_var("fiscalcode_blck", "");
if (nvl($_GET["country"], "") == "it" || nvl($_POST["hiddencountry"], "") == "it" || nvl($_GET["usertype"], "") == "az") {
	$t->set_var(array(
		"frm_fiscalcode"	=> ov(nvl($frm["fiscalcode"], "")),
		"errors_fiscalcode"	=> errmsg(nvl($errors["fiscalcode"], ""))
	));
	$t->parse("fiscalcode_blck", "fiscalcode", true);
}
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function validate_signup_form(&$frm, &$errors, &$errormsg) {
	global $CFG;
	include ($CFG["libdir"] . "pgm_validate.inc.php");
	include ($CFG["globalerror"]);

	validate_username($frm["username"], $errors, $errormsg);
	if ($frm["response"] == "" || $frm["response2"] == "") {	// the browser is not using JavaScript
		$frm["response"] = md5($frm["password"]);
		$frm["response2"] = md5($frm["password2"]);
	}
	if ($frm["response"] == $frm["response2"]) {
		validate_password($frm["response"], $errors, $errormsg);
	} else {
		$errors["password"] = true;
		$errormsg .= "<li>" . $passwords_do_not_match;
	}
	if ($frm["usertype"] == "pf") {
		validate_firstname($frm["firstname"], $errors, $errormsg);
		validate_lastname($frm["lastname"], $errors, $errormsg);
		if (nvl($frm["hiddencountry"], "") == "it") {
			validate_fiscalcode($frm["fiscalcode"], $errors, $errormsg);
		}
	} else if ($frm["usertype"] == "az") {
		validate_ragionesociale($frm["firstname"], $errors, $errormsg);
		validate_partitaiva($frm["fiscalcode"], $errors, $errormsg);
	}
	validate_address($frm["address"], $errors, $errormsg);
	validate_state($frm["state"], $errors, $errormsg);
	if (nvl($frm["hiddencountry"], "") == "it") {
		validate_cap($frm["zip_code"], $errors, $errormsg);
	} else {
		validate_zip_code($frm["zip_code"], $errors, $errormsg);
	}
	validate_city($frm["city"], $errors, $errormsg);
	validate_country($frm["country"], $errors, $errormsg);
	validate_phone($frm["phone"], $errors, $errormsg);
	validate_fax($frm["fax"], $errors, $errormsg);
	validate_mobilephone($frm["mobilephone"], $errors, $errormsg);
	validate_email($frm["email"], $errors, $errormsg);
	validate_notes($frm["notes"], $errors, $errormsg);

	if (username_exists($frm["username"])) {
		$errors["username"] = true;
		$errormsg = "<li>" . $the_username . " <b>" . ov($frm["username"]) ."</b> " . $already_exists . $errormsg;

	} /* else if (email_exists($frm["email"])) {
		$errors["email"] = true;
		$errormsg .= "<li>" . $the_email_address . " <b>" . ov($frm["email"]) ."</b> " . $already_exists;
	} */
}

function user_insert_user(&$frm) {
	global $_SERVER;

//	$date = $myday[date("w")] . date(" j") . "-" . date("n") . "-" . date("Y H:i:s");
	$Dateusec = gettimeofday(); $unixtime = $Dateusec["sec"];
	$year = date("Y"); $month = date("n"); $day = date("j"); $weekday = date("w");
	$fromip = $_SERVER["REMOTE_ADDR"];

	$qid = new PGM_Sql("
		INSERT INTO users (
			 username
			,password
			,usertype
			,zone_id
			,firstname
			,lastname
			,fiscalcode
			,address
			,state
			,zip_code
			,city
			,country
			,phone
			,fax
			,mobilephone
			,email
			,authdata
			,acceptadvert
			,notes
			,unixtime
			,year
			,month
			,day
			,weekday
			,fromip
		) VALUES (
			 '" . nvl($frm["username"], "") . "'
			,'" . nvl($frm["response"], "") . "'
			,'" . nvl($frm["usertype"], "") . "'
			,'" . nvl($frm["zone_id"], "") . "'
			,'" . nvl($frm["firstname"], "") . "'
			,'" . nvl($frm["lastname"], "") . "'
			,'" . nvl($frm["fiscalcode"], "") . "'
			,'" . nvl($frm["address"], "") . "'
			,'" . nvl($frm["state"], "") . "'
			,'" . nvl($frm["zip_code"], "") . "'
			,'" . nvl($frm["city"], "") . "'
			,'" . nvl($frm["country"], "au") . "'
			,'" . nvl($frm["phone"], "") . "'
			,'" . nvl($frm["fax"], "") . "'
			,'" . nvl($frm["mobilephone"], "") . "'
			,'" . nvl($frm["email"], "") . "'
			,'" . nvl($frm["authdata"], "") . "'
			,'" . nvl($frm["acceptadvert"], "") . "'
			,'" . nvl($frm["notes"], "") . "'
			,'$unixtime'
			,'$year'
			,'$month'
			,'$day'
			,'$weekday'
			,'$fromip'
		)
	");
}

?>

<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)

include ("../config.inc.php");
include ("../common.inc.php");
require_login();
include ($CFG["libdir"] . "pgm_orders.inc.php");

$DOC_TITLE = "Orders_Log_cc";
include ($CFG["dirroot"] . "header.php");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
	$id = nvl($_GET["id"], 1);
	$mode = nvl($_GET["mode"], "");
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$id = nvl($_POST["id"], 1);
	$mode = nvl($_POST["mode"], "");
}


switch (nvl($mode)) {
	case "details" :
		generate_invoice('C', $id, $PGM_SESSION["user"]["username"]);
		break;

	default :
	 	//$dbchanged = TRUE;
		user_list_orders($PGM_SESSION["user"]["username"]);
		break;
}

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function user_print_order_details($id, $username) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["localedir"] . $PGM_SESSION["lang"] . "/global-names.inc.php");

	$qid_order = new PGM_Sql("
		SELECT o.*, os.name AS state_name
		FROM orderscc o, orders_states os
		WHERE o.state_id = os.id AND o.id = '$id' AND o.username = '$username'
	");
	if ($qid_order->num_rows() < 1) {
		$t = new Template();
		$t->set_file("page", "templates/no_such_order.ihtml");
		include ($CFG["localelangdir"] . "global-common.inc.php");
		$t->pparse("out", "page");
		return;
	} else {
		$qid_order->next_record();
	}

	$qid_items = new PGM_Sql();
	get_order_items($qid_items, $id);

	$qid_user = new PGM_Sql("SELECT usertype, fiscalcode, email FROM users WHERE username = '$username'");
	$qid_user->next_record();

	$t = new Template();
	$t->set_file("page", "templates/ordercc_details.ihtml");
	include ($CFG["localelangdir"] . "global-common.inc.php");
	$t->set_var(array(
		"order_id"		=> ov($id),
		"order_state_name"	=> $qid_order->f("state_name"),
		"order_date"		=> $userday[$qid_order->f("weekday")] . " " . $qid_order->f("day") . " " . $usermonth[$qid_order->f("month")] . " " . $qid_order->f("year"),
		"order_famount"		=> formatted_price($qid_order->f("amount")),
		"order_custinfo"	=> nl2br(ov($qid_order->f("custinfo"))),
		"user_fiscalcode"	=> $qid_user->f("fiscalcode"),
		"user_email"		=> $qid_user->f("email"),
		"order_notes"		=> nl2br(ov($qid_order->f("notes")))
	));
	$t->set_block("page", "usertype_pf", "usertype_pf_blck");
	$t->set_var("usertype_pf_blck", "");
	$t->set_block("page", "usertype_az", "usertype_az_blck");
	$t->set_var("usertype_az_blck", "");
	if ($qid_user->f("usertype") == "pf") {
		$t->parse("usertype_pf_blck", "usertype_pf", true);
	} else {
		$t->parse("usertype_az_blck", "usertype_az", true);
	}
	$t->set_block("page", "weight_used", "weight_used_blck");
	$t->set_var(array("weight_used_blck" => ""));
	if ($CFG["weight_used"]) {
		$t->parse("weight_used_blck", "weight_used", true);
	}
	$t->set_block("page", "item_row", "item_rows");
	$t->set_var("item_rows", "");
		$t->set_block("item_row", "discount_used_row", "discount_used_row_blck");
		$t->set_block("item_row", "iva_used_row", "iva_used_row_blck");
		$t->set_block("item_row", "weight_used_row", "weight_used_row_blck");
		$t->set_block("item_row", "color_used", "color_used_blck");
	while ($qid_items->next_record()) {
		$t->set_var("discount_used_row_blck", "");
		$t->set_var("iva_used_row_blck", "");
		$t->set_var("weight_used_row_blck", "");
		$t->set_var("color_used_blck", "");
		$discount = ($qid_items->f("qty") >= $qid_items->f("purchase_discqty")) ? $qid_items->f("purchase_discount") : 0;
		$total = $qid_items->f("purchase_price")*$qid_items->f("qty")*(1.0+$qid_items->f("purchase_iva")/100.0)*(1.0-$discount/100.0);
		$t->set_var(array(
			"prod_qty"		=> $qid_items->f("qty"),
			"prod_fcurr_price"	=> formatted_price($qid_items->f("curr_price")),
			"prod_fpurchase_price"	=> formatted_price($qid_items->f("purchase_price")),
			"prod_ftotal"		=> formatted_price($total)
		));
		$foobar = ($qid_items->f("lname") != "" && $qid_items->f("name") == $qid_items->f("oiname")) ? ov($qid_items->f("lname")) : ov($qid_items->f("oiname"));
		if ($qid_items->f("name") != $qid_items->f("oiname")) {
			$foobar = "<div class=\"warning\">" . $foobar . "</div>";
		}
		if ($qid_items->f("name") != "") {
			$foobar = "<a href=\"" . $CFG["wwwroot"] . "shopping/product_details.php?product_id=" . $qid_items->f("product_id") . "\">" . $foobar . "</a>";
		}
		$t->set_var("prod_name", $foobar);
		if ($discount > 0) {
			$t->set_var("prod_purchase_discount", $discount);
			$t->parse("discount_used_row_blck", "discount_used_row", true);
		}
		if ($CFG["iva_used"]) {
			$t->set_var("prod_purchase_iva", $qid_items->f("purchase_iva"));
			$t->parse("iva_used_row_blck", "iva_used_row", true);
		}
		if ($CFG["weight_used"]) {
			$t->set_var("prod_fpurchase_weight", formatted_weight ($qid_items->f("purchase_weight")));
			$t->parse("weight_used_row_blck", "weight_used_row", true);
		}
		if ($qid_items->f("color") != "") {
			$t->set_var("prod_color", $qid_items->f("color"));
			$t->parse("color_used_blck", "color_used", true);
		}
		$t->parse("item_rows", "item_row", true);
	}
	$t->set_block("page", "user_discount_used", "user_discount_used_blck");
	$t->set_var(array("user_discount_used_blck" => ""));
	if ($qid_order->f("user_discount") > 0) {
		$t->set_var("order_user_discount", $qid_order->f("user_discount"));
		$t->parse("user_discount_used_blck", "user_discount_used", true);
	}
	$t->set_block("page", "delivery_used", "delivery_used_blck");
	$t->set_var(array("delivery_used_blck" => ""));
	if ($CFG["delivery_used"]) {
		$t->set_var("order_fdelivery", formatted_price($qid_order->f("delivery")));
		$t->parse("delivery_used_blck", "delivery_used", true);
	}
	$t->pparse("out", "page");
}
function user_list_orders($username) {
	global $_GET;
	global $PGM_SESSION;
	global $CFG, $ME;
	include ($CFG["localedir"] . $PGM_SESSION["lang"] . "/global-names.inc.php");

	if (!isset($_GET["page_number"])) {
		$page_number = 1;
	} else {
		$page_number = max(1, abs(intval($_GET["page_number"])));
	}

	$qid = new PGM_Sql("SELECT COUNT(id) AS cnt FROM orderscc WHERE username = '$username'");
	$qid->next_record();
	include ($CFG["libdir"] . "pgm_pager.inc.php");
	$result = pgm_pager($ME, $qid->f("cnt"), $PGM_SESSION["orders_per_page"], $page_number);
	$limit = $result["last"] - $result["first"] + 1;
	$limit_clause = prepare_limit_clause($limit, $result["first"]);
	$qid->query("
		SELECT o.*, os.name AS state_name
		FROM orderscc o, orders_states os
		WHERE o.state_id = os.id AND username = '$username'
		ORDER BY os.name, o.unixtime ASC
		$limit_clause
	");

	$t = new Template();
	include ($CFG["localelangdir"] . "global-common.inc.php");
	if ($qid->num_rows() == 0) {
		$t->set_file("page", "templates/no_orders.ihtml");
		$t->pparse("out", "page");
	} else {
		$t->set_file("page", "templates/ordercc_list.ihtml");
		$t->set_block("page", "pages_links_top", "pages_links_top_blck");
		$t->set_var("pages_links_top_blck", "");
		$t->set_block("page", "pages_links_bottom", "pages_links_bottom_blck");
		$t->set_var("pages_links_bottom_blck", "");
		if ($result["num_pages"] > 1) {
			$t->set_var("pages_links", $result["html"]);
			$t->parse("pages_links_top_blck", "pages_links_top", true);
			$t->parse("pages_links_bottom_blck", "pages_links_bottom", true);
		}
		$t->set_block("page", "item_row", "item_rows");
		$t->set_var("item_rows", "");
		while ($qid->next_record()) {
			$t->set_var(array(
				"order_id"		=> ov($qid->f("id")),
				"order_state_name"	=> ov($qid->f("state_name")),
				"order_date"		=> $userday[$qid->f("weekday")] . " " . $qid->f("day") . " " . $usermonth[$qid->f("month")] . " " . $qid->f("year"),
				"order_famount"		=> formatted_price($qid->f("amount"))
			));
			$t->parse("item_rows", "item_row", true);
		}
		$t->pparse("out", "page");
	}
}

?>

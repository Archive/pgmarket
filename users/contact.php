<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>

include ("../config.inc.php");
include ("../common.inc.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
/* form has been submitted, try to send the message */
	$frm = $_POST;
	$errors = array();
	$errormsg = "";
	validate_contact_form($frm, $errors, $errormsg);
	if ($errormsg == "") {
		send_form($frm);
		$DOC_TITLE = "Contact_Successful";
		include ($CFG["dirroot"] . "header.php");
		$t = new Template();
		$t->set_file("page", "templates/" . $PGM_SESSION["lang"] . "/contact_success.ihtml");
		include ($CFG["localelangdir"] . "global-common.inc.php");
		$t->set_var("firstpage", $CFG["firstpage"]);
		$t->pparse("out", "page");
		include ($CFG["dirroot"] . "footer.php");
		pgm_session_close($PGM_SESSION, $session_name);
		die;
	}
} else if (is_logged_in()) {
	$frm = load_user_profile();
	$frm["name"] = $frm["firstname"] . " " . $frm["lastname"];
}

$DOC_TITLE = "Contact_Us";
include ($CFG["dirroot"] . "header.php");
include ($CFG["dirroot"] . "form_header.php");
if (empty($errormsg)) {	// we are loading the form for the first time
	$frm["acceptadvert"] = 1;
}

$t = new Template();
$t->set_file("page", "templates/contact_form.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
$t->set_var("ME", $ME);
$t->set_var("star", return_star());
$t->set_var(array(
	"frm_name"		=> ov(nvl($frm["name"], "")),
	"errors_name"		=> errmsg(nvl($errors["name"], "")),
	"frm_address"		=> ov(nvl($frm["address"], "")),
	"errors_address"	=> errmsg(nvl($errors["address"], "")),
	"errors_number"		=> errmsg(nvl($errors["number"], "")),
	"frm_zip_code"		=> ov(nvl($frm["zip_code"], "")),
	"errors_zip_code"	=> errmsg(nvl($errors["zip_code"], "")),
	"frm_city"		=> ov(nvl($frm["city"], "")),
	"errors_city"		=> errmsg(nvl($errors["city"], "")),
	"errors_country"	=> errmsg(nvl($errors["country"], "")),
	"frm_phone"		=> ov(nvl($frm["phone"], "")),
	"errors_phone"		=> errmsg(nvl($errors["phone"], "")),
	"frm_fax"		=> ov(nvl($frm["fax"], "")),
	"errors_fax"		=> errmsg(nvl($errors["fax"], "")),
	"frm_mobilephone"	=> ov(nvl($frm["mobilephone"], "")),
	"errors_mobilephone"	=> errmsg(nvl($errors["mobilephone"], "")),
	"frm_email"		=> ov(nvl($frm["email"], "")),
	"errors_email"		=> errmsg(nvl($errors["email"], "")),
	"frm_authdata"		=> nvl($frm["authdata"], 0) ? "checked" : "",
	"frm_acceptadvert"	=> nvl($frm["acceptadvert"], 0) ? "checked" : "",
	"frm_message"		=> ov(nvl($frm["message"], "")),
	"errors_message"	=> errmsg(nvl($errors["message"], "")),
));
$t->set_var(nvl($frm["state"],'')."_selected", "selected");
$t->set_var(nvl($frm["country"],'au')."_selected", "selected");
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function validate_contact_form(&$frm, &$errors, &$errormsg) {
	global $CFG;
	include ($CFG["libdir"] . "pgm_validate.inc.php");

	validate_name($frm["name"], $errors, $errormsg);
	validate_address($frm["address"], $errors, $errormsg);
	validate_state($frm["state"], $errors, $errormsg);
	validate_zip_code($frm["zip_code"], $errors, $errormsg);
	validate_city($frm["city"], $errors, $errormsg);
	validate_country($frm["country"], $errors, $errormsg);
	validate_phone($frm["phone"], $errors, $errormsg);
	validate_fax($frm["fax"], $errors, $errormsg);
	validate_mobilephone($frm["mobilephone"], $errors, $errormsg);
	validate_email($frm["email"], $errors, $errormsg);
	validate_message($frm["message"], $errors, $errormsg);
}

function send_form(&$frm) {
	global $CFG;
	global $contact;
	include ($CFG["globalerror"]);

	$emailbody = "";
	$emailbody .= $contact["NameOrCompanyName"] . stripslashes($frm["name"]);
	$emailbody .= "\n\n" . $contact["Address"] . "\n" . stripslashes($frm["address"]);
	$emailbody .= "\n" .   $frm["zip_code_lang"] . ": " . stripslashes($frm["zip_code"]);
	$emailbody .= "\n" . stripslashes($frm["city"]);
	$emailbody .= " " . stripslashes($frm["state"]);
	$emailbody .= "\n" . print_country(stripslashes($frm["country"]));
	$emailbody .= "\n\n" . $contact["Phone"] . $frm["phone"];
	$emailbody .= "\n\n" . $contact["Fax"] . $frm["fax"];
	$emailbody .= "\n\n" . $contact["MobilePhone"] . $frm["mobilephone"];
	$emailbody .= "\n\n" . $contact["EMail"] . $frm["email"];
	$emailbody .= "\n\n" . $contact["AuthData"];
	if (nvl($frm["authdata"], 0)) {
		$emailbody .= strtoupper($CFG["yes"]);
	}
	$emailbody .= "\n\n" . $contact["AuthAdvert"];
	if (nvl($frm["acceptadvert"], 0)) {
		$emailbody .= strtoupper($CFG["yes"]);
	}
	$emailbody .= "\n\n" . $contact["Message"] . "\n\n" . stripslashes($frm["message"]) . "\n";

	mail(
		$CFG["seller_fullname"] . " <" . $CFG["seller_email"] . ">",
		$CFG["subject_contact"],
		$emailbody,
		"From: " . $frm["email"]
	);
}

?>

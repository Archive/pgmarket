<?php
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

include ("../config.inc.php");
include ("../common.inc.php");
require_login();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$frm = $_POST;
	$errors = array();
	$errormsg = "";
	validate_change_settings_form($frm, $errors, $errormsg);
	if ($errormsg == "") {
		update_settings($frm);
		$PGM_SESSION["zone_id"] = $frm["zone_id"];
		$PGM_SESSION["CART"]->calc_grandtotal();
		$noticemsg = "Settings_Change_Successful";
	}
} else {
	$frm = load_user_profile();
}

$DOC_TITLE = "Change_Settings";

include ($CFG["dirroot"] . "header.php");
include ($CFG["dirroot"] . "form_header.php");

$t = new Template();
$t->set_file("page", "templates/change_settings_form-" . $PGM_SESSION["user"]["usertype"] . ".ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
$t->set_var("ME", $ME);
$t->set_var("star", return_star());
$t->set_var(array(
	"session_user_username"		=> ov($PGM_SESSION["user"]["username"]),
	"session_user_firstname"	=> ov($PGM_SESSION["user"]["firstname"]),
	"frm_fiscalcode"		=> ov($frm["fiscalcode"]),
	"errors_fiscalcode"		=> errmsg(nvl($errors["fiscalcode"], 0)),
	"frm_address"			=> ov($frm["address"]),
	"errors_address"		=> errmsg(nvl($errors["address"], 0)),
	"errors_state"			=> errmsg(nvl($errors["state"], 0)),
	"frm_zip_code"			=> ov($frm["zip_code"]),
	"errors_zip_code"		=> errmsg(nvl($errors["zip_code"], 0)),
	"frm_city"			=> ov($frm["city"]),
	"errors_city"			=> errmsg(nvl($errors["city"], 0)),
	"errors_country"		=> errmsg(nvl($errors["country"], 0)),
	"frm_phone"			=> ov($frm["phone"]),
	"errors_phone"			=> errmsg(nvl($errors["phone"], 0)),
	"frm_fax"			=> ov($frm["fax"]),
	"errors_fax"			=> errmsg(nvl($errors["fax"], 0)),
	"frm_mobilephone"		=> ov($frm["mobilephone"]),
	"errors_mobilephone"		=> errmsg(nvl($errors["mobilephone"], 0)),
	"frm_email"			=> ov($frm["email"]),
	"errors_email"			=> errmsg(nvl($errors["email"], 0)),
	"frm_authdata"			=> nvl($frm["authdata"], 0) ? "checked" : "",
	"frm_acceptadvert"		=> nvl($frm["acceptadvert"], 0) ? "checked" : "",
	"frm_notes"			=> ov($frm["notes"]),
	"errors_notes"			=> errmsg(nvl($errors["notes"], 0))
));
$t->set_var(nvl($frm["state"],'')."_selected", "selected");
$t->set_var(nvl($frm["country"],'au')."_selected", "selected");
if ($PGM_SESSION["user"]["usertype"] == "pf") {
	$t->set_var("session_user_lastname", ov($PGM_SESSION["user"]["lastname"]));
	if ($PGM_SESSION["country"] == "it") {
		$t->set_block("page", "italy", "italy_blck");
		$t->set_var("italy_blck", "");
		$t->parse("italy_blck", "italy", true);
	}
}
$t->set_block("page", "delivery_used", "delivery_used_blck");
$t->set_var("delivery_used_blck", "");
if ($CFG["delivery_used"] && !($CFG["only_one_delivery_zone"])) {
	$t->set_block("delivery_used", "delivery_zone_option", "delivery_zone_options");
	$t->set_var("delivery_zone_options", "");
	$qid = new PGM_Sql();
	get_zones_names($qid);
	while ($qid->next_record()) {
		$t->set_var(array(
			"frm_user_zone_id"		=> $qid->f("zone_id"),
			"frm_user_zone_selected"	=> ($qid->f("zone_id") == $frm["zone_id"]) ? "selected" : "",
			"frm_user_zone_name"		=> ($qid->f("lname") != "") ? ov($qid->f("lname")) : ov($qid->f("name"))
		));
		$t->parse("delivery_zone_options", "delivery_zone_option", true);
	}
	$t->parse("delivery_used_blck", "delivery_used", true);
}
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

/* *******************************************************************
 * FUNCTIONS
 ****************************************************************** */

function validate_change_settings_form(&$frm, &$errors, &$errormsg) {
	global $PGM_SESSION;
	global $CFG;
	include ($CFG["libdir"] . "pgm_validate.inc.php");
	include ($CFG["globalerror"]);

	if ($PGM_SESSION["user"]["usertype"] == "pf" && $PGM_SESSION["country"] == "it") {
		validate_fiscalcode($frm["fiscalcode"], $errors, $errormsg);
	} else if ($PGM_SESSION["user"]["usertype"] == "az") {
		validate_partitaiva($frm["fiscalcode"], $errors, $errormsg);
	}
	validate_address($frm["address"], $errors, $errormsg);
	validate_state($frm["state"], $errors, $errormsg);
	if ($PGM_SESSION["country"] == "it") {
		validate_cap($frm["zip_code"], $errors, $errormsg);
	} else {
		validate_zip_code($frm["zip_code"], $errors, $errormsg);
	}
	validate_city($frm["city"], $errors, $errormsg);
	validate_country($frm["country"], $errors, $errormsg);
	validate_phone($frm["phone"], $errors, $errormsg);
	validate_fax($frm["fax"], $errors, $errormsg);
	validate_mobilephone($frm["mobilephone"], $errors, $errormsg);
	validate_email($frm["email"], $errors, $errormsg);
	validate_notes($frm["notes"], $errors, $errormsg);
/*
	if (email_exists($frm["email"]) && $frm["email"] != $PGM_SESSION["user"]["email"]) {
		$errors["email"] = true;
		$errormsg .= "<li>" . $the_email_address . " <b>" . ov($frm["email"]) . "</b> " . $already_exists;
	}
*/
}

function update_settings(&$frm) {
	global $PGM_SESSION;
	
	$qid = new PGM_Sql("
		UPDATE users SET
			 zone_id = '" .		nvl($frm["zone_id"], "") . "'
			,fiscalcode = '" .	nvl($frm["fiscalcode"], "") . "'
			,address = '" .		nvl($frm["address"], "") . "'
			,state = '" .		nvl($frm["state"], "") . "'
			,zip_code = '" .	nvl($frm["zip_code"], "") . "'
			,city = '" .		nvl($frm["city"], "") . "'
			,country = '" .		nvl($frm["country"], "au") . "'
			,phone = '" .		nvl($frm["phone"], "") . "'
			,fax = '" .		nvl($frm["fax"], "") . "'
			,mobilephone = '" .	nvl($frm["mobilephone"], "") . "'
			,email = '" .		nvl($frm["email"], "") . "'
			,authdata = '" .	nvl($frm["authdata"], 0) . "'
			,acceptadvert = '" .	nvl($frm["acceptadvert"], 0) . "'
			,notes = '" .		nvl($frm["notes"], "") . "'
		WHERE username = '" .		$PGM_SESSION["user"]["username"] . "'
	");
}

?>

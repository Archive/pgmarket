<?
if (file_exists('config.inc.php')) {
	include ("config.inc.php");
	include ("common.inc.php");
} else {
	include ("../config.inc.php");
	include ("../common.inc.php");
}

$DOC_TITLE = "PgMarket_Home_Page";

include ($CFG["dirroot"] . "header.php");

//trigger_error("log(x) for x <= 0 is undefined, you used: scale = $scale", FATAL);
$qid = new PGM_Sql;
//get_new_products(&$qid);
$qid->pgm_lang_join_query(
	$CFG["base_lang"],
	array("products p", "products_i18n l"),
	array(array("p.id", "l.product_id", 1, $PGM_SESSION["lang"])),
	array(array("l.name AS lname",  "l.description AS ldescription")),
	"p.id, p.name, b.name AS bname, p.price, i.iva, p.discount, p.discqty, p.weight, p.special_level, p.description, p.thumbtype, p.thumbwidth, p.thumbheight",
	"brands b, iva i",
	"p.brand_id = b.id AND p.iva_id = i.id AND new_flag = 1",
	"new_level ASC",
	array()
);
$t = new Template;
$t->set_file(array(
		"page" => $CFG['dirroot']."info/templates/index.ihtml",
		"new_products"	=> "$CFG[dirroot]/info/templates/new_products.ihtml"
	));
include($CFG['localelangdir']  . "global-common.inc.php");
include($CFG['localelangdir'] .  "global-admin.inc.php");
$t->set_var(array(
		"DOC_TITLE"			=> ov($t->get_var($DOC_TITLE)),
		"wwwroot"			=> $CFG['wwwroot'],
		"images"			=> $CFG['imageswww'],
		"firstpage"			=> $CFG['firstpage'],
		));

$t->set_block("page", "there_are_new_products", "there_are_new_products_blck");
if (!($qid->num_rows() > 0))
	$t->set_var(array("there_are_new_products_blck" => ""));
else {
	$t->set_block("new_products", "new_products_product", "new_products_products");
	$t->set_block("new_products_product", "new_product_image", "new_product_image_blck");
	$t->set_var(array("new_products_products" => ""));
        $t->set_var("productswww", $CFG['productswww']);
	while($qid->next_record()) {
		$t->set_var("new_product_image_blck", "");
		$t->set_var(array(
				"r_id"		=> $qid->f('id'),
				"r_name"	=> ov($qid->f('name')),
				"r_description"	=> ovwbr($qid->f('description')),
				"r_price"	=> $qid->f('price'),
				"r_fprice"	=> formatted_price($qid->f('price'))
		));
		if (file_exists($CFG['productsdir'] . $qid->f("id") . ".thumb.jpg")) {
			$t->set_var(array(
				"imagefilename"	=> $qid->f("id"),
				"thumbwidth"	=> $qid->f("thumbwidth"),
				"thumbheight"	=> $qid->f("thumbheight")
			));
			$t->parse("new_product_image_blck", "new_product_image", true);
			}
		$t->parse("new_products_products", "new_products_product", true);
	}
	$t->parse("there_are_new_products_blck", "there_are_new_products", true);
}

$t->pparse("out", array("new_products", "page"));
include ($CFG["dirroot"] . "footer.php");

/******************************************************************************
 * FUNCTIONS
 *****************************************************************************/


?>

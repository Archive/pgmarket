<?
$doc = getenv('REQUEST_URI');
if (preg_match('/\.(gif|jpeg|png|jpg)$/', $doc)) {
	header("Content-type: image/png");
	$im = imagecreatefrompng('images/noimage.png');
	imagepng($im);
} else {
include ("config.inc.php");
include ("common.inc.php");


$DOC_TITLE = "Site_Error";
include ($CFG["dirroot"] . "header.php");

$t = new Template;
$t->set_file('page',  "templates/error.ihtml");
$t->set_block('page', '404', '404_blck');
$t->set_var('404_blck', '');
include ($CFG["localelangdir"] . "global-common.inc.php");
include ($CFG["localelangdir"] . "global-admin.inc.php");
$t->set_var(array(
		"DOC_TITLE"			=> ov($DOC_TITLE),
		"wwwroot"			=> $CFG['wwwroot'],
		"firstpage"			=> $CFG['firstpage'],
		'redirect_url'      => $doc,
		'redirect_status'   => $REDIRECT_STATUS
		));

if ($REDIRECT_STATUS == 404) {
  $t->parse('404_blck', '404');
}
$t->pparse("out", "page");
include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

}
    $to = "csmall@enc.com.au";
	$subject = "Problem with website: $doc";
	$body = "There has been a problem with the website.  The error is ". $REDIRECT_STATUS
	. ". The document which caused the error was $doc and it was called from the"
	. "referrer ".$_SERVER['HTTP_REFERER'].".";
	mail ($to, $subject, $body);
    
?>

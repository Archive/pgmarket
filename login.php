<?php
// (C) 2000-2002 Marco Pratesi <marco@pgmarket.net>
// (C) 2000 Ying Zhang (ying@zippydesign.com)
// (C) 2001 Fabio Molinari <fabio.m@mclink.it>

include ("config.inc.php");
include ("common.inc.php");
include ($CFG["globalerror"]);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
//echo "user $_POST[username] password/response $_POST[response] <br><br>";
/* form has been submitted, check if the user login information is correct */
	$qid = new PGM_Sql("
		SELECT
			 username
			,password
			,usertype
			,priv
			,user_discount_id
			,zone_id
			,firstname
			,lastname
			,fiscalcode
			,email
		FROM users
		WHERE username = '" . $_POST["username"] . "'
	");

	if ($qid->num_rows() > 0) {
		$qid->next_record();
		$foobar = md5($_POST["username"] . ":" . $qid->f("password") . ":" . $PGM_SESSION["challenge"]);
		//echo "chaleenge ".$PGM_SESSION['challenge']." <br>";
		//echo "Comparing passwords posted ".md5($_POST['password'])." to stored ".$qid->f("password")."<br> ";
		//echo "Comparing responses posted ".($_POST['response'])." to stored $foobar <br>";
		if (
		    $_POST["response"] == $foobar	// the browser is using JavaScript
		     ||
		    ($_POST["password"] != "" && md5($_POST["password"]) == $qid->f("password"))	// the browser is not using JavaScript
		   ) {
			$PGM_SESSION["user"] = array();
			$PGM_SESSION["user"]["username"] = $qid->f("username");
			$PGM_SESSION["user"]["password"] = $qid->f("password");
			$PGM_SESSION["user"]["usertype"] = $qid->f("usertype");
			$PGM_SESSION["user"]["priv"] = $qid->f("priv");
			$PGM_SESSION["user"]["zone_id"] = $qid->f("zone_id");
			$PGM_SESSION["user"]["user_discount_id"] = $qid->f("user_discount_id");
			$PGM_SESSION["user"]["firstname"] = $qid->f("firstname");
			$PGM_SESSION["user"]["lastname"] = $qid->f("lastname");
			$PGM_SESSION["user"]["fiscalcode"] = $qid->f("fiscalcode");
			$PGM_SESSION["user"]["email"] = $qid->f("email");
			$foobar = $PGM_SESSION["user"]["user_discount_id"];
			$qid->query("SELECT discount FROM user_discounts WHERE id = '$foobar'");
			$qid->next_record();
			$PGM_SESSION["user"]["user_discount"] = $qid->f("discount");
			$PGM_SESSION["zone_id"] = $PGM_SESSION["user"]["zone_id"];
			$PGM_SESSION["ip"] = $_SERVER["REMOTE_ADDR"];
			$PGM_SESSION["CART"]->calc_grandtotal();

			if ($PGM_SESSION["user"]["fiscalcode"] != "") {
				$PGM_SESSION["country"] = "it";
			} else {
				$PGM_SESSION["country"] = "";
			}
			/* if wantsurl is set, that means we came from a page that required
			 * log in, so let's go back there.  otherwise go back to the main page */
			$goto = (nvl($PGM_SESSION["wantsurl"], "") == "") ? $CFG["firstpage"] : $PGM_SESSION["wantsurl"];
			pgm_session_close($PGM_SESSION, $session_name);
			redirect($goto);
			die;
		}
	}

	$errormsg = $login_failed;
	$frm["username"] = $_POST["username"];
}

$DOC_TITLE = "PgMarket_Login";
include ($CFG["dirroot"] . "header.php");

srand((double) microtime() * 1000000);
//$PGM_SESSION["challenge"] = md5(uniqid(rand()));
$t = new Template();
$t->set_file("page", "templates/login_form.ihtml");
include ($CFG["localelangdir"] . "global-common.inc.php");
$t->set_var(array(
	"wwwroot"	=> $CFG["wwwroot"],
	"libwww"	=> $CFG["libwww"],
	"errormsg"	=> nvl($errormsg, ""),
	"frm_username"	=> ov(nvl($frm["username"], "")),
	"frm_challenge"	=> $PGM_SESSION["challenge"]
));
$t->pparse("out", "page");

include ($CFG["dirroot"] . "footer.php");

pgm_session_close($PGM_SESSION, $session_name);

?>
